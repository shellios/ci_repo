<?php	 	
class Commonfunction {

	function generate_random_data($length = 8) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++):
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		endfor;
		return $randomString;
	}

	public function current_time()
	{
		$created = date('Y-m-d h:i:s');
		return $created;
	}
	
	public function current_date()
	{
		$created = date('Y-m-d');
		return $created;
	}
	
	public function current_date_time()
	{
		$created = date('m/d/Y h:i A');
		return $created;
	}
	
	public function get_current_date()
	{
		$created = date('mdY');
		return $created;
	}
	
	public function get_current_time()
	{
		$created = time();
		return $created;
	}
	
	public function convert_to_username($original) {
		$reparray	=	array(" ","`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")","_","-","+","=","{","}","[","]","\"","'","<",">","?","/","\\",",".".",'"');
		$uname 		= 	str_replace($reparray, '', $this->strip_html_tags(trim($original)));	
		$uname 		= 	strtolower($uname);
		return $uname;
	}
	
	public function save_image_to_folder($photo_name='',$photo_path='',$photodata='') {
		if($photo_name && $photo_path && $photodata && $photodata !='photodata'):
			/* Decoding image */
			$binary = base64_decode($photodata);
			/* Opening image */
			//header('Content-Type: bitmap; charset=utf-8');
			$file = fopen ($photo_name,'wb');
			/* Writing to server */
			fwrite($file,$binary, strlen($binary));
			/* Closing image file */
			fclose($file);
			if(file_exists($photo_name)):
				return $photo_path;
			else:
				return false;
			endif;
		endif;
	}
	
	function get_distance_between_points($latitude1, $longitude1, $latitude2, $longitude2) {
		$theta 		= 	$longitude1 - $longitude2;
		$miles 		= 	(sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
		$miles 		= 	acos($miles);
		$miles 		= 	rad2deg($miles);
		$miles 		= 	$miles * 60 * 1.1515;
		$feet 		= 	$miles * 5280;
		$yards 		= 	$feet / 3;
		$kilometers = 	$miles * 1.609344;
		$meters 	= 	$kilometers * 1000;
		return $meters; 
	}
	
	public function strip_html_decoded_tags($text)
	{
		$repprearray	=	array('&lt;','&gt;');
		$repposarray	=	array('<','>');
		return str_replace($repprearray,$repposarray,$text);
	}
	
	public function strip_html_tags($text)
	{
		$text = preg_replace(
			array(
			  // Remove invisible content
				'@<head[^>]*?>.*?</head>@siu',
				'@<style[^>]*?>.*?</style>@siu',
				'@<script[^>]*?.*?</script>@siu',
				'@<object[^>]*?.*?</object>@siu',
				'@<embed[^>]*?.*?</embed>@siu',
				'@<applet[^>]*?.*?</applet>@siu',
				'@<noframes[^>]*?.*?</noframes>@siu',
				'@<noscript[^>]*?.*?</noscript>@siu',
				'@<noembed[^>]*?.*?</noembed>@siu',
			  // Add line breaks before and after blocks
				'@</?((address)|(blockquote)|(center)|(del))@iu',
				'@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
				'@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
				'@</?((table)|(th)|(td)|(caption))@iu',
				'@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
				'@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
				'@</?((frameset)|(frame)|(iframe))@iu',
			),
			array(
				' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
				"\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
				"\n\$0", "\n\$0",
			),
			$text );
		return strip_tags( $text );
	}


	function addTimeInDate($date) {
		return $date.' 00:00:00';
	}

	function removeTimeFromDate($date) {
		$datedata				=	explode(' ',$date);
		return $datedata[0];
	}

	function hourDiffrenceBetweenTwoTime($firstTime='',$secondTime='') {
		if($firstTime && $secondTime): 
			$difference 	= 	abs($firstTime - $secondTime);//round(abs($firstTime - $secondTime) / 60,2);
			//print_r($difference); die;
			return $difference;
		else:
			return false;
		endif;
	}
}