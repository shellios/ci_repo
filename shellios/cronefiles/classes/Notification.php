<?php	 	
class Notification {

	#########################################################################################################################
	################################################		USER SECTION 		#############################################
	#########################################################################################################################

	/* * *********************************************************************
	 * * Function name : sendNotificationToUserFunction 
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function use for send Notification To User Function
	 * * Date : 27 SEPTEMBER 2018
	 * * **********************************************************************/
	public function sendNotificationToUserFunction($registrationIds='',$message='',$data=array(),$deviceType='') {
		if(!empty($registrationIds) && !empty($message) && !empty($data) && !empty($deviceType)):
			
			$fields 		= 	array('to'=>$registrationIds,'notification'=>$message,'data'=>$data);
			if($deviceType == 'Andriod'):
				$headers 	= 	array('Authorization: key='.CABBEE_API_ACCESS_KEY,'Content-Type:application/json');
			elseif($deviceType == 'IOS'):
				$headers 	= 	array('Authorization: key='.CABBEE_IOS_API_ACCESS_KEY,'Content-Type:application/json');
			endif;
			#Send Reponse To FireBase Server	
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL,'https://fcm.googleapis.com/fcm/send');
			curl_setopt( $ch,CURLOPT_POST,true);
			curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);
			#Echo Result Of FireBase Server
			return $result;
		endif;
	}	// END OF FUNCTION	

	/***********************************************************************
	** Function name : sendRideAlertNotificationToUser
	** Developed By : Manoj Kumar
	** Purpose  : This is use for send Ride Alert Notification ToUser
	** Date : 27 SEPTEMBER 2018
	************************************************************************/
	function sendRideAlertNotificationToUser($registrationIds='',$deviceType='',$rmessage='',$rideId='') {  
		if($registrationIds && $deviceType && $rmessage && $rideId):
			$message 		= 	array('body'=>$rmessage,//'Your today ride on 27 sep 19 6:pm from noida.',
						 	 		  'title'=>'Today ride',
             	         	 		  'icon'=>'myicon',/*Default Icon*/
              	         	 		  'sound'=>'mySound'/*Default sound*/
          				 	 		  );
			$data			=	array('notCatId'=>'1','rideId'=>$rideId);
			$returnMessage	=	$this->sendNotificationToUserFunction($registrationIds,$message,$data,$deviceType);
			return $returnMessage;
		endif;
	}






	#########################################################################################################################
	################################################		DRIVER SECTION 		#############################################
	#########################################################################################################################
	/* * *********************************************************************
	 * * Function name : sendNotificationToDriverFunction 
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function use for send Notification To Driver Function
	 * * Date : 27 SEPTEMBER 2018
	 * * **********************************************************************/
	public function sendNotificationToDriverFunction($registrationIds='',$message=array(),$data=array(),$deviceType='') {
		if(!empty($registrationIds) && !empty($message) && !empty($data) && !empty($deviceType)):
			
			$fields 		= 	array('to'=>$registrationIds,'notification'=>$message,'data'=>$data);
			if($deviceType == 'Andriod'):
				$headers 	= 	array('Authorization: key='.CABBEE_DRIVER_API_ACCESS_KEY,'Content-Type:application/json');
			elseif($deviceType == 'IOS'):
				$headers 	= 	array('Authorization: key='.CABBEE_IOS_DRIVER_API_ACCESS_KEY,'Content-Type:application/json');
			endif;
			#Send Reponse To FireBase Server	
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL,'https://fcm.googleapis.com/fcm/send');
			curl_setopt( $ch,CURLOPT_POST,true);
			curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);
			#Echo Result Of FireBase Server
			return $result;
		endif;
	}	// END OF FUNCTION	

	/***********************************************************************
	** Function name : sendBookingNotificationToDriver
	** Developed By : Manoj Kumar
	** Purpose  : This is use for send Booking Notification To Driver
	** Date : 27 SEPTEMBER 2018
	************************************************************************/
	function sendRideAlertNotificationToDriver($registrationIds='',$deviceType='',$rmessage='',$rideId='') {  
		if($registrationIds && $deviceType && $rmessage && $rideId):
			$message 		= 	array('body'=>$rmessage,//'Today ride on 27 sep 19 6:pm from noida.',
						 	 		  'title'=>'Today ride',
             	         	 		  'icon'=>'myicon',/*Default Icon*/
              	         	 		  'sound'=>'mySound'/*Default sound*/
          				 	 		  );
			$data			=	array('notCatId'=>'1','rideId'=>$rideId);
			$returnMessage	=	$this->sendNotificationToDriverFunction($registrationIds,$message,$data,$deviceType);
			return $returnMessage;
		endif;
	}
}