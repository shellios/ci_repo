<?php	
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK
require_once '../twilio-php-master/Twilio/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client; 
	
class Sms {

	/* * *********************************************************************
	 * * Function name : sendMessageFunction 
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function use for send Message Function
	 * * Date : 27 SEPTEMBER 2018
	 * * **********************************************************************/
	public function sendMessageFunction($mobileNumber='',$message='') {
		if(!empty($mobileNumber) && !empty($message)):
			$indianNumber		=	array('9555465323','9716260606','9971325321','9711882641','9560223629','9718740037','9315172821','9910826821','9576496687');
			if(in_array($mobileNumber,$indianNumber)):
				$smsToNumber	=	'+91'.$mobileNumber;
			else:
				$smsToNumber	=	SMS_COUNTRY_CODE.$mobileNumber;
			endif;			
			// Your Account SID and Auth Token from twilio.com/console
			$sid 		= 	SMS_SID;//'AC72c8e68e6bd4c58a6a86aa07dc142d0a';
			$token 		= 	SMS_TOKEN;//'5e459dc0a38c43b6590900fec24927f0';
			$client 	= 	new Client($sid,$token);
			
			// Use the client to do fun stuff like send text messages!
			/*$messages = $client->messages->create(
				// the number you'd like to send the message to
				'+91 97118 82641',//'+19098159799',
				array(
					// A Twilio phone number you purchased at twilio.com/console
					'from' => '+19093216959',
					// the body of the text message you'd like to send
					'body' => "Hey Jenny! Good luck on the bar exam!"
				)
			);*/
			
			$messages 	= 	$client->messages->create($smsToNumber,array('From'=>SMS_FROM,'Body'=>$message));
			return true;
		endif;
	}	// END OF FUNCTION	


	#########################################################################################################################
	################################################		USER SECTION 		#############################################
	#########################################################################################################################
	
	/***********************************************************************
	** Function name : sendRideAlertSmsToUser
	** Developed By : Manoj Kumar
	** Purpose  : This is use for send Ride Alert Sms To User
	** Date : 27 SEPTEMBER 2018
	************************************************************************/
	function sendRideAlertSmsToUser($mobileNumber='',$rmessage='') {  
		if($mobileNumber && $rmessage):
			$message		=	$rmessage;
			$returnMessage	=	$this->sendMessageFunction($mobileNumber,$message);
			return $returnMessage;
		endif;
	}





	#########################################################################################################################
	################################################		DRIVER SECTION 		#############################################
	#########################################################################################################################
	
	/***********************************************************************
	** Function name : sendRideAlertSmsToDriver
	** Developed By : Manoj Kumar
	** Purpose  : This is use for send Ride Alert Sms To Driver
	** Date : 27 SEPTEMBER 2018
	************************************************************************/
	function sendRideAlertSmsToDriver($mobileNumber='',$rmessage='') {  
		if($mobileNumber && $rmessage):
			$message		=	$rmessage;
			$returnMessage	=	$this->sendMessageFunction($mobileNumber,$message);
			return $returnMessage;
		endif;
	}
}