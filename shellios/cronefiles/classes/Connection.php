<?php
class Connection {

	protected $mysqli;
	protected $stmt;
	
	//Connection to database
	public function __construct($DB_HOST='',$DB_USER='',$DB_PASSWORD='',$DB_NAME='')
	{  
		//open mysql connection
		$this->mysqli = new mysqli($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_NAME);
		//output error and exit if there's an error
		if ($this->mysqli->connect_error):
			die('Error : ('. $this->mysqli->connect_errno .') '. $this->mysqli->connect_error);
		endif;
	}
	
	//Get total record count
	public function get_number_of_record($Sfeilds='',$table='',$where=array())
	{	
		if($table && $Sfeilds):
			$feilds				=	array();	
			$param				=	array();
			if($where):
				$this->set_fielda_and_param($where,$feilds,$param);
			endif;	
			$feilds				=	implode(' ',$feilds);
			$query 				= 	"SELECT count(".$Sfeilds.") as total FROM ".$table." WHERE 1=1 ".$feilds;
			$this->stmt 		=	$this->mysqli->prepare($query);
			if($param):
				@array_unshift($param,(Connection::paramtypez($param)));
				@call_user_func_array(array($this->stmt, 'bind_param'), Connection::makeValuesReferenced($param));
			endif;
			if($this->stmt->execute()):
				$this->stmt->bind_result($total);
				while($this->stmt->fetch()):
					if($total):
						$this->stmt->close();
						return $total;
					else:
						$this->stmt->close();
						return false;
					endif;
				endwhile;
			else:
				return	false;
			endif;	
		else:
			return	false;
		endif;
	}
	
	//get swingle row data in array
	public function get_single_record($Sfeilds='',$table='',$where=array(),$groupby='',$orderby='')
	{
		if($Sfeilds && $table):
			$resultarray		=	array();
			$feilds				=	array();	
			$param				=	array();
			if($where):
				$this->set_fielda_and_param($where,$feilds,$param);
			endif;
			$feilds				=	implode(' ',$feilds);
			
			if($groupby):	$groupby	=	'GROUP BY '.$groupby;		endif;
			if($orderby):	$orderby	=	'ORDER BY '.$orderby;		endif;
			
			$query 				= 	"SELECT ".$Sfeilds." FROM ".$table." WHERE 1=1 ".$feilds." ".$groupby." ".$orderby." LIMIT 0,1";
			$this->stmt 		=	$this->mysqli->prepare($query);
			if($param): 
				@array_unshift($param,(Connection::paramtypez($param)));
				@call_user_func_array(array($this->stmt, 'bind_param'), Connection::makeValuesReferenced($param));
			endif;
			if($this->stmt->execute()):
				if($Sfeilds):	
					$Sfeildsarray		=	explode(',',$Sfeilds);
					@call_user_func_array(array($this->stmt, 'bind_result'), Connection::makeValuesReferenced($Sfeildsarray));
				endif;
				while($this->stmt->fetch()):
					$Sfeildsdata		=	explode(',',$Sfeilds); 
					$i=0;
					foreach($Sfeildsarray as $Sfeildsvalue):
						//$resultarray[$Sfeildsdata[$i]]	=	$Sfeildsvalue;
						if(strpos($Sfeildsdata[$i],' as ')):
							$sfieldsdata	=	explode(' as ',$Sfeildsdata[$i]);
							$resultarray[trim($sfieldsdata[1])]	=	$this->replace_unexpected_string($Sfeildsvalue);
						elseif(strpos($Sfeildsdata[$i],'.')):
							$sfieldsdata	=	explode('.',$Sfeildsdata[$i]);
							$resultarray[trim($sfieldsdata[1])]	=	$this->replace_unexpected_string($Sfeildsvalue);
						else:
							$sfieldsdata	=	trim(preg_replace('/\s+/', '',$Sfeildsdata[$i]));
							$resultarray[trim($sfieldsdata)]	=	$this->replace_unexpected_string($Sfeildsvalue);
						endif;
						$i++;
					endforeach;	
				endwhile;	
			endif;
			if(count($resultarray) > 0):
				$this->stmt->close();
				return $resultarray;
			else:
				$this->stmt->close();
				return false;
			endif;
		else:
			return	false;
		endif;
	}	
	
	//Get multiple row record data in array
	public function get_multiple_record($Sfeilds='',$table='',$where=array(),$groupby='',$orderby='',$limit='')
	{
		if($Sfeilds && $table):
			$resultarray		=	array();
			$feilds				=	array();	
			$param				=	array();
			if($where):
				$this->set_fielda_and_param($where,$feilds,$param);
			endif;
			$feilds						=	implode(' ',$feilds);
			if($groupby):	$groupby	=	'GROUP BY '.$groupby;		endif;
			if($orderby):	$orderby	=	'ORDER BY '.$orderby;		endif;
			if($limit):		$limit		=	'LIMIT '.$limit;			endif;	
			if(isset($where['name']) && strpos($where['name'],'LIKEOR')):
				$feilds					=	substr($feilds,3);
				$query 					= 	"SELECT ".$Sfeilds." FROM ".$table." WHERE ".$feilds." ".$groupby." ".$orderby." ".$limit;
			elseif(isset($where['f_name___CONCAT___l_name']) && strpos($where['f_name___CONCAT___l_name'],'LIKEOR')):
				$feilds					=	substr($feilds,3);
				$query 					= 	"SELECT ".$Sfeilds." FROM ".$table." WHERE ".$feilds." ".$groupby." ".$orderby." ".$limit;
			else:
				$query 					= 	"SELECT ".$Sfeilds." FROM ".$table." WHERE 1=1 ".$feilds." ".$groupby." ".$orderby." ".$limit;
			endif;
			
			$this->stmt 		=	$this->mysqli->prepare($query);
			if($param):
				@array_unshift($param,(Connection::paramtypez($param))); 
				@call_user_func_array(array($this->stmt, 'bind_param'), Connection::makeValuesReferenced($param));
			endif;
			if($this->stmt->execute()):
				if($Sfeilds):	
					$Sfeildsarray		=	explode(',',$Sfeilds);
					@call_user_func_array(array($this->stmt, 'bind_result'), Connection::makeValuesReferenced($Sfeildsarray));
				endif;
				$i=0;
				while($this->stmt->fetch()):
					$Sfeildsdata		=	explode(',',$Sfeilds); 
					$j=0;
					foreach($Sfeildsarray as $Sfeildsvalue):
						//$resultarray[$i][$Sfeildsdata[$j]]	=	$Sfeildsvalue;
						if(strpos($Sfeildsdata[$j],' as ')):
							$sfieldsdata	=	explode(' as ',$Sfeildsdata[$j]);
							$resultarray[$i][trim($sfieldsdata[1])]	=	$this->replace_unexpected_string($Sfeildsvalue);
						elseif(strpos($Sfeildsdata[$j],'.')):
							$sfieldsdata	=	explode('.',$Sfeildsdata[$j]);
							$resultarray[$i][trim($sfieldsdata[1])]	=	$this->replace_unexpected_string($Sfeildsvalue);
						else:
							$sfieldsdata	=	trim(preg_replace('/\s+/', '',$Sfeildsdata[$j]));
							$resultarray[$i][trim($sfieldsdata)]	=	$this->replace_unexpected_string($Sfeildsvalue);
						endif;
						$j++;
					endforeach;	
					$i++;
				endwhile;	
			endif;
			if(count($resultarray) > 0):
				$this->stmt->close();
				return $resultarray;
			else:
				$this->stmt->close();
				return false;
			endif;
		else:
			return	false;
		endif;
	}	
	
	//Insert data to table
	public function insert_record($table='',$data=array())
	{
		if($table):
			$feilds				=	array();
			$feilddata			=	array();	
			$param				=	array();
			if($data):
				foreach($data as $feild=>$value):
					array_push($feilds,$feild);
					array_push($feilddata,'?');
					array_push($param,htmlspecialchars(htmlentities($value),ENT_QUOTES));
				endforeach;
			endif;
			$feilds				=	implode(',',$feilds);
			$feilddata			=	implode(',',$feilddata);
			$query				=	"INSERT INTO ".$table." (".$feilds.") VALUES (".$feilddata.")";
			$this->stmt 		=	$this->mysqli->prepare($query);
			if($param):  
				@array_unshift($param,(Connection::paramtypez($param)));
				@call_user_func_array(array($this->stmt, 'bind_param'), Connection::makeValuesReferenced($param)); 
			endif;
			if($this->stmt->execute()):
				$lastinsertid	=	$this->stmt->insert_id;
				$this->stmt->close();
				return	$lastinsertid;
			else:
				$this->stmt->close();
				return	false;
			endif;
		else:
			return	false;
		endif;
	}	
	
	//Update data to table by condition
        public function update_record($table='',$data=array(),$where=array())
	{
		if($table):
			$feilds				=	array();
			$param				=	array();
			if($data):
				foreach($data as $feild=>$value):
					array_push($feilds,$feild.'=?');
					array_push($param,htmlspecialchars(htmlentities($value),ENT_QUOTES));
				endforeach;
			endif;
			$feilds				=	implode(', ',$feilds);
			$wherecon			=	array();
			if($where):
				foreach($where as $wfeild=>$wvalue):
					array_push($wherecon,'AND '.$wfeild.'=?');
					array_push($param,$wvalue);
				endforeach;
			endif;
			$wherecon			=	implode(' ',$wherecon);
			$query				=	"UPDATE ".$table." SET ".$feilds." WHERE 1=1 ".$wherecon;
		    //print_r($query); die;
			$this->stmt 		=	$this->mysqli->prepare($query);
			if($param):
				@array_unshift($param,(Connection::paramtypez($param))); 
				@call_user_func_array(array($this->stmt, 'bind_param'), Connection::makeValuesReferenced($param));
			endif;
			if($this->stmt->execute()):
				$this->stmt->close();
				return	true;
			else:
				$this->stmt->close();
				return	false;
			endif;
		else:
			return	false;
		endif;
	}
	
	//Delete data to table by condition
	public function delete_record($table='',$where=array())
	{
		if($table):
			$wherecon			=	array();
			$param				=	array();
			if($where):
				foreach($where as $wfeild=>$wvalue):
					array_push($wherecon,'AND '.$wfeild.'=?');
					array_push($param,$wvalue);
				endforeach;
			endif;
			$wherecon			=	implode(' ',$wherecon);
			$query				=	"DELETE FROM ".$table." WHERE 1=1 ".$wherecon;
			$this->stmt 		=	$this->mysqli->prepare($query);
			if($param):
				@array_unshift($param,(Connection::paramtypez($param))); 
				@call_user_func_array(array($this->stmt, 'bind_param'), Connection::makeValuesReferenced($param));
			endif;
			if($this->stmt->execute()):
				$this->stmt->close();
				return	true;
			else:
				$this->stmt->close();
				return	false;
			endif;
		else:
			return	false;
		endif;
	}	
	
	//Closed connection
	public function closed()
	{
		$this->mysqli->close();
	}
	
	//Initial sting with types
	public function paramtypez($val=array())
    {
		$types = '';                        //initial sting with types
		foreach($val as $para): 
			if(is_int($para)):
				$types .= 'i';              //integer
			elseif(is_float($para)):
				$types .= 'd';              //double
			elseif(is_string($para)):
				$types .= 's';              //string
			else:
				$types .= 'b';              //blob and unknown
			endif;
		endforeach;
		return $types;
    }
	
	//Make array value reference 
	protected function makeValuesReferenced(&$arr=array())
    {
        $refs = array();
        foreach($arr as $key => $value):
           	 $refs[$key] = &$arr[$key];
		endforeach;
        return $refs;
    }
	
	//Set condition for fields and param
	public function set_fielda_and_param($where=array(),&$feilds=array(),&$param=array())
	{
		foreach($where as $feild=>$value):
			if(strpos($value,'LIKEOR')):
				if(strpos($feild,'CONCAT')):
					$feilddata	=	explode('___CONCAT___',$feild);
					$valuedata	=	explode('_____',$value);
					$valuedata[0]	=	str_replace('&','&amp;amp;amp;',$valuedata[0]);
					array_push($feilds,'OR CONCAT('.$feilddata[0].', " ",'.$feilddata[1].') LIKE ?');
					array_push($param,"%{$valuedata[0]}%");
				else:
					$valuedata	=	explode('_____',$value);
					$valuedata[0]	=	str_replace('&','&amp;amp;amp;',$valuedata[0]);
					array_push($feilds,'OR '.$feild.' LIKE ?');
					array_push($param,"%{$valuedata[0]}%");
				endif;	
			elseif(strpos($value,'LIKE')):
				if(strpos($feild,'CONCAT')):
					$feilddata	=	explode('___CONCAT___',$feild);
					$valuedata	=	explode('_____',$value);
					$valuedata[0]	=	str_replace('&','&amp;amp;amp;',$valuedata[0]);
					array_push($feilds,'AND CONCAT('.$feilddata[0].', " ",'.$feilddata[1].') LIKE ?');
					array_push($param,"%{$valuedata[0]}%");
				else:
					$valuedata	=	explode('_____',$value);
					$valuedata[0]	=	str_replace('&','&amp;amp;amp;',$valuedata[0]);
					array_push($feilds,'AND '.$feild.' LIKE ?');
					array_push($param,"%{$valuedata[0]}%");
				endif;	
			elseif(strpos($value,'NOTEQUAL')):
				$valuedata	=	explode('_____',$value);
				$valuedata[0]	=	str_replace('&','&amp;amp;amp;',$valuedata[0]);
				array_push($feilds,'AND '.$feild.' !=?');
				array_push($param,$valuedata[0]);
			elseif(strpos($value,'WHEREOR')):
				$valuedata	=	explode('_____',$value);
				$valuedata[0]	=	str_replace('&','&amp;amp;amp;',$valuedata[0]);
				array_push($feilds,'OR '.$feild.' =?');
				array_push($param,$valuedata[0]);
			elseif(strpos($value,'INCLAUSE')):
				$valuedata	=	explode('_____',$value);
				$indataarray= 	explode(',',$valuedata[0]);
				$indata 	= 	str_repeat("?,", count($indataarray)-1) . "?";
				array_push($feilds,'AND '.$feild.' IN('.$indata.')');
				foreach($indataarray as $invalue):
					array_push($param,$invalue);
				endforeach;
			elseif(strpos($value,'GREATEQUAL')):
				$valuedata	=	explode('_____',$value);
				$valuedata[0]	=	str_replace('&','&amp;amp;amp;',$valuedata[0]);
				array_push($feilds,'AND '.$feild.' >=?');
				array_push($param,$valuedata[0]);
			elseif(strpos($value,'LESSEQUAL')):
				$valuedata	=	explode('_____',$value);
				$valuedata[0]	=	str_replace('&','&amp;amp;amp;',$valuedata[0]);
				array_push($feilds,'AND '.$feild.' <=?');
				array_push($param,$valuedata[0]);
			else:	
				$value	=	str_replace('&','&amp;amp;amp;',$value);
				array_push($feilds,'AND '.$feild.'=?');
				array_push($param,$value);
			endif;
		endforeach;
	}
	
	//devode html entity
	public function replace_unexpected_string($string='')
	{
		$prereparray	=	array('amp;');
		$posreparray	=	array('');
		return html_entity_decode(str_replace($prereparray,$posreparray,trim($string)));
	}
}