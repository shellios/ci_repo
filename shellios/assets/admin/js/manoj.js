$(document).ready(function(){
	// Form Validation
    $("#currentPageForm").validate({
		rules:{
			new_password: { minlength: 6, maxlength: 25 },
			conf_password: { minlength: 6, equalTo: "#new_password" },
	        admin_mobile_number:{ minlength:10, maxlength:15, numberandsign:true },
			vendor_mobile_number:{ minlength:10, maxlength:15, numberandsign:true }
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('success');
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	jQuery.validator.addMethod("numberonly", function(value, element) 
	{
		return this.optional(element) || /^[0-9]+$/i.test(value);
	}, "Number only please");
});

 // Cookies
function createCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	}
	else var expires = "";               
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length).replace(/%2F/gi,'/').replace(/\+/gi,' ').replace(/\%26%23xa3%3B/gi,'&#xa3;');
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}

$(document).on('change','#Data_Form #showLength',function(){ 
	$('#Data_Form').submit();														 
});

$(document).on('keypress','#Data_Form #searchValue',function(e){ 
	if (e.keyCode == '13') {
        $('#Data_Form').submit();
    }																  
});

//////////////////////////////////   Image Upload Through Ajax
function UploadImage(count)
{	
	var imgcount = count;
	if (document.getElementById('uploadIds'+imgcount)) 
	{
		var btnUpload=$('#uploadIds'+imgcount);
		var status=$('#uploadstatus'+imgcount);
		status.text('');
	
		new AjaxUpload(btnUpload, {
			action: FULLSITEURL+CURRENTCLASS+'/UplodeImage',
			name: 'uploadfile',
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
					status.text('Only JPG, PNG, GIF files are allowed');
					return false;
				}			
				status.text('Uploding...');
			},
			onComplete: function(file, response){ 
				//alert(response);
				responsedata	=	response.split('_____');
				if(responsedata[0] == "ERROR"){ 
					status.text(responsedata[1]);
					return false;
				}
				status.text('');
				if(responsedata[0] != "UPLODEERROR"){ 
					document.getElementById('uploadimage'+imgcount).value = responsedata[0];
					document.getElementById('uploadphoto'+imgcount).innerHTML ='<img src="'+responsedata[0]+'" border="0" width="100" /><a href="javascript:void(0);" onClick="DeleteImage(\''+responsedata[0]+'\',\''+imgcount+'\');"><img src="'+ASSETADMINURL+'images/cross.png" border="0" alt="" /></a>';
				}
			}
		});
	}
}

//////////////////////////////////   Image delete Through Ajax
function DeleteImage(imagename,imgcount)
{
    if(confirm("Sure to delete?"))
    {
        $.ajax({
		     type: 'post',
              url: FULLSITEURL+CURRENTCLASS+'/DeleteImage',
			 data: {imagename:imagename},
              success: function(response) { 
			  	document.getElementById('uploadimage'+imgcount).value = '';
				document.getElementById('uploadphoto'+imgcount).innerHTML ='';
              }
            });
    }
}