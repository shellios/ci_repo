$(document).ready(function(){
	var login = $('#loginform');
	var recover = $('#recoverform');
	var speed = 400;
	$('#to-recover').click(function(){
		$("#loginform").slideUp();
		$("#recoverform").slideDown();
	});
	
	$('#to-login').click(function(){
		$("#recoverform").slideUp();
		$("#loginform").slideDown();
	});

	if(forgoterror == 'YES'){
		$("#loginform").slideUp();
		$("#recoverform").slideDown();
	}
	
	$("#loginform").validate({
		rules:{
			userPassword:{
				required: true,
				minlength:6,
				maxlength:25
			}
		},
		errorClass: "error",
		errorElement: "label",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('error');
			$(element).parents('.form-group').addClass('success');
		}
	});
	
	$("#recoverform").validate({
		errorClass: "error",
		errorElement: "label",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('error');
			$(element).parents('.form-group').addClass('success');
		}
	});
	
	$("#passwordRecoverForm").validate({
		rules:{
			userOtp:{
				required: true,
				numberonly: true,
				minlength:6,
				maxlength:6
			},
			userPassword:{
				required: true,
				minlength:6,
				maxlength:25
			},
			userConfPassword:{
				required: true,
				minlength:6,
				maxlength:25,
				equalTo: "#userPassword"
			}
		},
		errorClass: "error",
		errorElement: "label",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('error');
			$(element).parents('.form-group').addClass('success');
		}
	});
	
	jQuery.validator.addMethod("numberonly", function(value, element) 
	{
		return this.optional(element) || /^[0-9]+$/i.test(value);
	}, "Number only please");
});