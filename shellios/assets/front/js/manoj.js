$(function(){
	// Form Validation
    $("#currentPageForm").validate({
		rules:{
			password: { minlength: 6, maxlength: 25 },
			conf_password: { minlength: 6, equalTo: "#password" },
	        user_phone:{ minlength:10, maxlength:15, numberandsign:true }
		},
		errorClass: "help-inline",
		errorElement: "p",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('success');
			$(element).parents('.form-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('error');
			$(element).parents('.form-group').addClass('success');
		}
	});

	$("#newsletterForm").validate({
		errorClass: "help-inline",
		errorElement: "p",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.news_letter').removeClass('success');
			$(element).parents('.news_letter').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.news_letter').removeClass('error');
			$(element).parents('.news_letter').addClass('success');
		},
		submitHandler: function(form) {
			var userEmail  = 	$('#newsletterForm #newsletter_email').val();
			if(userEmail){
		        $.ajax({
	                type: 'post',
	                 url: FRONTSITEURL+'welcome/addtonewsletter',
	                data: {newsletter_email:userEmail},
	             success: function(response){ 
	             		$('#newsletterForm #newsletter_email').val('');
	                    if(response.success == 0) {
	                        alertMessageModelPopup(response.message,'Warning');
	                    } else if(response.success == 1) {
	                        alertMessageModelPopup(response.message,'Success');
	                    } 
	                }
	            });
		    }
	        return false;
	    }
	});

	$("#searchForm").validate({
		errorClass: "help-inline",
		errorElement: "p",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.input-group').removeClass('success');
			$(element).parents('.input-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.input-group').removeClass('error');
			$(element).parents('.input-group').addClass('success');
		},
		submitHandler: function(form) {
			var searchText  = 	$('#searchForm #searchText').val(); 
			if(searchText){
		        form.submit();
		    } 
	        return false;
	    }
	});

	jQuery.validator.addMethod("numberonly", function(value, element) 
	{
		return this.optional(element) || /^[0-9]+$/i.test(value);
	}, "Number only please");
});

 // Cookies
function createCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	}
	else var expires = "";               
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length).replace(/%2F/gi,'/').replace(/\+/gi,' ').replace(/\%26%23xa3%3B/gi,'&#xa3;');
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}

$(function(){
	// check and create cart cookie
	if(!readCookie('currentCartCookie')){
		var randomNumber = getRandomInt(1000000000000000,9999999999999999); 
		createCookie('currentCartCookie', randomNumber, 365);
		window.location.reload();
	} 
	// check and create recent view cookie
	if(!readCookie('recentViewCookie')){
		var randomNumber = getRandomInt(1000000000000000,9999999999999999); 
		createCookie('recentViewCookie', randomNumber, 365);
		window.location.reload();
	} 
});

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
} //The maximum is exclusive and the minimum is inclusive