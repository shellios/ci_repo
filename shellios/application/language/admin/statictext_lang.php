<?php	
$lang['INVALID_ACCESS']				    			= 	"Invalid Access";
$lang['ATMID_NOTFOUND']				    			= 	"ATM ID Not Found";
$lang['USER_NOTFOUND']				    			= 	"User Not Found";
$lang['USER_INACTIVATE']			    			= 	"User Not Activated";
$lang['ACCOUNT_BLOCKED']			    			= 	"Your Accound is Suspended, Please Contact Administrator";
$lang['WRONG_PASSWORD']				    			= 	"ATM ID OR Password is In-correct";
$lang['SUCCESSFULL_LOGIN']				    		= 	"Login Successfuly, Redirecting Please Wait...";




$lang['invalidpassword']	            			= 	'Log Failed. Password Do Not Match';
$lang['invalidlogindetails']            			= 	"You Have Entered An Invalid Email Or Password";
$lang['emailnotavailable']            				= 	"This Email Id Is Not Available.";
$lang['accountblock']            					= 	"You Account Is Blocked. Please Contact To Administration.";
$lang['invalidemail']            					= 	"You Have Entered Aan Invalid Email";
$lang['sendforgotpassmail']            				= 	"Password Recovery Link Send To Your e-mail.";
$lang['invalidotp']            						= 	"You Have Entered An Invalid OTP";
$lang['passrecoversuccess']            				= 	"You Have Recover Password Successfully.";

////////////////////////    alert mesaage  //////////
$lang['addsuccess']									=	'Data Added Successfully.';
$lang['updatesuccess']								=	'Data Updated Successfully.';
$lang['uploadimgsuccess']							=	'Upload Image Successfully.';
$lang['deletesuccess']								=	'Data Deleted Successfully.';
$lang['statussuccess']								=	'Status Changed Successfully.';
$lang['multipledeletesuccess']						=	'Miltiple Data Deleted Successfully.';

$lang['addwarning']									=	'Data Not Add Successfully.';
$lang['updatewarning']								=	'Data Not Update Successfully.';
$lang['deletewarning']								=	'Data Not Delete Successfully.';

$lang['adderror']									=	'Data Not Add Successfully.';
$lang['updateerror']								=	'Data Not Update Successfully.';
$lang['deleteerror']								=	'Data Not Delete Successfully.';
$lang['blockerror']									=	'Data Not Delete Successfully.';

$lang['accessdenied']								=	'You Are Not Able To Access This Page.';
$lang['accessstatusdenied']							=	'You Are Not Able To Change Status.';
$lang['accessdeletedenied']							=	'You Are Not Able To Selete Data.';

$lang['PERERROR']									=	'Please Give Atlist One Module Permission.';

$lang['PHONEERROR']									=	'Please Enter Correct Number.';

$lang['verifyseller']								=	'Verify seller Successfully.';
$lang['SR_NO']                                      =     'Always Enterd Unique Data';
