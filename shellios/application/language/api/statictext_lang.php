<?php	
$lang['SUCESS_STATUS']	    						= 	1;
$lang['SUCESS_MESSAGE']	    						= 	"Data fetched successfully";
$lang['ERROR_STATUS']	    						= 	0;
$lang['ERROR_MESSAGE']	    						= 	"Data not found";

$lang['INVALID_APIKEY']	    						= 	"Invalid API key";
$lang['OTP_INCORRECT']	    				= 	"Incorrect OTP";
$lang['USERID_INCORRECT']	    					= 	"Incorrect User ID";
$lang['USER_NOT_REGISTER']	    			= 	"Account does not exist. Please register";
$lang['USER_CREDENTIAL_INCORRECT']	    			= 	"Invalid login details.";
$lang['ACCOUNT_BLOCK']	    				= 	"Your account has been blocked. Please contact administrator.";
$lang['ACCOUNT_DELETE']	    				= 	"Your account has been deactivated. Please contact administrator.";
$lang['ACCOUNT_INACTIVE']	    			= 	"Your account is inactive. Please activate your account.";
$lang['ACCOUNT_NOTACTIVE']    						= 	"Your account is not verified yet. OTP has been sent to your mobile number.";
$lang['ALREADY_REGISTER']    				= 	"Account already exist. Please login or try with another number.";
$lang['mobilenotverified']            				= 	"You Mobile Number is Not Verified.";
$lang['mobilenotavailable']            				= 	"Invalid Mobile Number";
$lang['EMAIlALREADY_REGISTER']    					= 	"Account already exist. Please login or try with another email id.";
$lang['INVALID_MOBILE']	    				= 	"Mobile number or token not valid";
$lang['INVALID_EMAIL']	    						= 	"Email ID not valid";
$lang['TYPE_EMPTY']	    							= 	"Address Type is empty"; 
$lang['TYPE_INCORRECT']	    						= 	"Address Type is incorrect"; 
$lang['CARBRANDID_EMPTY']	    					= 	"Car Brand ID is empty";
$lang['CARMODELID_EMPTY']	    					= 	"Car Model ID is empty";
$lang['CATEGORY_INCORRECT']	    					= 	"Incorrect Category ID";
$lang['CATEGORY_EMPTY']	    						= 	"Category ID is empty";
$lang['SUBCATEGORY_INCORRECT']	    				= 	"Incorrect Sub Category ID";
$lang['SUBCATEGORY_EMPTY']	    					= 	"Empty Sub Category ID";
$lang['PRODUCT_INCORRECT']	    					= 	"Incorrect Product ID";
$lang['PRODUCT_EMPTY']	    				= 	"Product ID is empty";
$lang['QUANTITY_EMPTY']	    						= 	"Quantity is empty";
$lang['INSTALLATIONCHARGE_EMPTY']	    			= 	"Installation charge is empty";
$lang['NETWORKID_EMPTY']	    					= 	"Network id is empty";
$lang['LOGINTYPE_EMPTY']	    			= 	"Login type is empty";
$lang['SEARCHTEXT_EMPTY']	    					= 	"Search Text is empty";
$lang['MOBILE_EMPTY']	    				= 	"Mobile number is empty";
$lang['OTP_EMPTY']	    							= 	"OTP is empty";
$lang['DEVICEID_EMPTY']	    						= 	"Device ID is empty";
$lang['DEVICETYPE_EMPTY']	    					= 	"Device Type is empty";
$lang['USERID_EMPTY']	    						= 	"User ID is empty";
$lang['NAME_EMPTY']	    							= 	"Name is empty";
$lang['ADDRESS_EMPTY']	    						= 	"Address is empty";
$lang['ADDRESSID_EMPTY']	    					= 	"Address ID is empty";
$lang['EMAIL_EMPTY']	    						= 	"Email Address is empty";
$lang['USERIMG_EMPTY']	    						= 	"User Image is empty";
$lang['AVAILABLETYPE_EMPTY']	    				= 	"Available Type is empty";
$lang['PASSWORD_EMPTY']	    						= 	"Password is empty";
$lang['NEW_PASSWORD_EMPTY']	    					= 	"New Password is empty";
$lang['REASON_EMPTY']	    						= 	"Reason is empty";
$lang['MESSAGE_EMPTY']	    						= 	"Message is empty";
$lang['PRODUCT_ID']                         =   "Sorry!!! Product not linked yet.";

$lang['GET_PHONE_NO_EMPTY']	   				= 	"Get phone no is empty";
$lang['LOGIN_TYPE_INCORRECT']	    		= 	"Login type is incorrect";
$lang['GET_PHONE_TYPE_INCORRECT']	   		= 	"Get phone no type incorrect";

$lang['OLD_PASSWORD_NOT_MATCH']	    				= 	"Incorrect Old Password";
$lang['PASSWORD_CHANGED_SUCCESSFULLY']	    		= 	"Password changed successfully.";
$lang['SEND_OTP_TO_RMN']	    			= 	"OTP has been sent to your mobile number.";
$lang['NUMBER_INPUT']	    				= 	"Please input your mobile number.";
$lang['VERIFY_OTP']	    					= 	"OTP verified successfully. You are logged in successfully.";
$lang['RESET_SUCCESSFULLY']	    					= 	"Your password have been reset successfully.";
$lang['LOGIN_SUCCESSFULLY']	    			= 	"You have been logged in successfully.";
$lang['UPDATE_SUCCESSFULLY']	    				= 	"Your record have been updated successfully.";
$lang['SUBMIT_SUCCESSFULLY']	    				= 	"You record have been submitted successfully.";
$lang['PROFILE_UPDATE_SUCCESS']	    		= 	"Profile updated successfully.";
$lang['USER_PROFILE_UPDATE_SUCCESS']	    = 	"User Profile updated successfully, you are logged in now.";
$lang['CREDENTIAL_ERROR']	    			= 	"E-Mail or token is incorrect.";
$lang['FEEDBACK_SUCCESS']	    			= 	"Your feedback submitted successfully.";

$lang['SELLERREQUEST_SUCCESS']	    		= 	"Thanks for your interest. Your Service request accepted. Shellios team will contact you shortly.";
$lang['GIVEREVIEW_SUCCESS']	    					= 	"Thanks for your review!";
$lang['UPDATEDEVICEID_SUCCESS']	    				= 	"Device ID updated successfully!";

$lang['INVALID_PSERIAL_ID']	  				= 	"Invalid product serial number and product id";
$lang['PRODUCT_DIFFRENT']	  				= 	"The product linked by diffrent user";

            /////////////////////////// New Message //////////////////////////

$lang['FAQ_LIST']	    					        = 	"Get Faq List successfully.";
$lang['PRODUCT_LIST']	    				= 	"Get Product List successfully";
$lang['PRODUCT_ALREADY_LINKED']	   	        = 	"Product already linked.";
$lang['LOGIN_SUCCESS']                              =   "You Can Login";
$lang['LOGIN_STATUS']                               =   "Y";
$lang['TOKEN_STATUS']                               =   "Token No. Allready Exist";
$lang['TOKEN_NO']                                   =   "Token No. is Empty";
$lang['USER_LOGOUT']                                =   "User Successfully Logout";
$lang['USER_STATUS']                                =   "User Allready Login";
$lang['PRODUCT_LINKED']	    		        = 	"Product linked successfully.";
$lang['PRODUCT_UNLINKED']	    		            = 	"Product Unlinked successfully.";
$lang['ADD_PRODUCT_LIST']	    			= 	"Get Product List successfully. by user";
$lang['PRODUCT_SR_EMPTY']	    					= 	"Product Serial number is empty";
$lang['ALREADY_SERVICE_REQUEST']    		= 	"Service Request allready Processing.";
$lang['GET_SERVICE_REQUEST']    			= 	"Get service Request list successfully.";
$lang['SERVICE_REQUEST_ERROR']    			= 	"Service Request not found.";
