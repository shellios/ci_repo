<?php	
$lang['INVALID_ACCESS']				    			= 	"Invalid Access";
$lang['ATMID_NOTFOUND']				    			= 	"ATM ID Not Found";
$lang['USER_NOTFOUND']				    			= 	"User Not Found";
$lang['USER_INACTIVATE']			    			= 	"User Not Activated";
$lang['ACCOUNT_BLOCKED']			    			= 	"Your Accound is Suspended, Please Contact Administrator";
$lang['WRONG_PASSWORD']				    			= 	"ATM ID OR Password is In-correct";
$lang['SUCCESSFULL_LOGIN']				    		= 	"Login Successfuly, Redirecting Please Wait...";


$lang['invalidpassword']	            			= 	'Log Failed. Password Do Not Match.';
$lang['invalidlogindetails']            			= 	"You Have Entered An Invalid Login Details.";
$lang['invalidmobile']            					= 	"You Have Entered An Invalid Mobile Number.";
$lang['mobilealreadyused']            			    = 	"This Mobile Number is already used.";
$lang['mobilenotverified']            				= 	"You Mobile Number is Not Verified.";
$lang['emailnotavailable']            				= 	"This Email Id Is Not Available.";
$lang['accountblock']            					= 	"You Account Is Blocked. Please Contact To Administration.";
$lang['invalidemail']            					= 	"You Have Entered Aan Invalid Email";
$lang['sendforgotpassmail']            				= 	"Password Recovery Link Send To Your e-mail.";
$lang['invalidotp']            						= 	"You Have Entered An Invalid OTP";
$lang['passrecoversuccess']            				= 	"You Have Recover Password Successfully.";
$lang['loginfailed']	            				= 	'Log Failed. Please try again.';

////////////////////////    alert mesaage  //////////
$lang['addsuccess']									=	'Data Added Successfully.';
$lang['updatesuccess']								=	'Data Updated Successfully.';
$lang['uploadimgsuccess']							=	'Upload Image Successfully.';
$lang['deletesuccess']								=	'Data Deleted Successfully.';
$lang['statussuccess']								=	'Status Changed Successfully.';
$lang['multipledeletesuccess']						=	'Miltiple Data Deleted Successfully.';

$lang['addwarning']									=	'Data Not Add Successfully.';
$lang['updatewarning']								=	'Data Not Update Successfully.';
$lang['deletewarning']								=	'Data Not Delete Successfully.';

$lang['adderror']									=	'Data Not Add Successfully.';
$lang['updateerror']								=	'Data Not Update Successfully.';
$lang['deleteerror']								=	'Data Not Delete Successfully.';
$lang['blockerror']									=	'Data Not Delete Successfully.';

$lang['accessdenied']								=	'You Are Not Able To Access This Page.';
$lang['accessstatusdenied']							=	'You Are Not Able To Change Status.';
$lang['accessdeletedenied']							=	'You Are Not Able To Selete Data.';

$lang['PERERROR']									=	'Please Give Atlist One Module Permission.';

$lang['PHONEERROR']									=	'Please Enter Correct Number.';

$lang['OLD_PASSWORD_NOT_MATCH']	    				= 	"Old Password is incorrect";
$lang['PASSWORD_CHANGED_SUCCESSFULLY']	    		= 	"Password changed successfully.";
$lang['SEND_OTP_TO_RMN']	    					= 	"OTP has been sent to your mobile number.";
$lang['OTP_INCORRECT']	    						= 	"OTP is incorrect";
$lang['VERIFY_OTP']	    							= 	"OTP verified successfully.";
$lang['VERIFY_MOBILE']	    						= 	"Mobile verified successfully.";
$lang['RESET_SUCCESSFULLY']	    					= 	"You have successfully reset your password.";
$lang['LOGIN_SUCCESSFULLY']	    					= 	"You have successfully logged in.";
$lang['UPDATE_SUCCESSFULLY']	    				= 	"You have successfully update your record.";
$lang['SUBMIT_SUCCESSFULLY']	    				= 	"You have successfully submit your record.";
$lang['PROFILE_UPDATE_SUCCESS']	    				= 	"Profile updated successfully.";

$lang['ALREADY_IN_WISHLIST']	    				= 	"Already in your wishlist.";
$lang['NOT_AVAIL_WISHLIST']	    					= 	"Product is not available in your wishlist.";
$lang['ADDTOWISHLIST_SUCCESS']	    				= 	"Added to widhlist.";
$lang['REMOVEFROMWISHLIST_SUCCESS']	    			= 	"Removed from widhlist.";

$lang['ADDTOCART_SUCCESS']	    					= 	"Added to cart.";
$lang['UPDATETOCART_SUCCESS']	    				= 	"Update to cart.";
$lang['REMOVEFROMCART_SUCCESS']	    				= 	"Removed from cart.";
$lang['ALREADY_IN_CART']	    					= 	"Already in your cart.";

$lang['ADDADDRESS_SUCCESS']	    					= 	"Add address successfully.";
$lang['UPDATEADDRESS_SUCCESS']	    				= 	"Update address successfully..";
$lang['REMOVEADDRESS_SUCCESS']	    				= 	"Removed address successfully.";

$lang['PAYMENTSUCESS_MESSAGE']	    				= 	"You have payment successfully.";
$lang['PAYMENTCANCEL_MESSAGE']	    				= 	"Your order is cancel. Please try again.";

$lang['CONTACTUS_SUCCESS']	    					= 	"We have received your request successfully. Somebody will contact you shortly.";
$lang['GIVEREVIEW_SUCCESS']	    					= 	"Thanks for your review.";

$lang['ADDTONEWSLETTER_SUCCESS']	    			= 	"Added to newsletter.";
$lang['ALREADY_IN_NEWSLETTER']	    				= 	"Already in newsletter.";

$lang['ORDER_SUCCESS']	    						= 	"Your order has placed successfully.";
$lang['ORDER_UPDATE_SUCCESS']	    				= 	"Order status updated successfully.";
$lang['ORDER_LIST_SUCCESS']	    					= 	"Order list returned successfully.";
$lang['CANCELORDER_SUCCESS']	    				= 	"Your order has canceled successfully.";
$lang['RETURNREQUEST_SUCCESS']	    				= 	"Your request has received. Somebody will contact you shortly.";
$lang['CHECKSUM_SUCCESS']	    					= 	"Hash key returned successfully.";
$lang['FEEDBACK_SUCCESS']	    					= 	"You send feedback successfully.";
$lang['SUBSCRIBE_SUCCESS']	    					= 	"You have subscribe successfully.";

$lang['SELLERREQUEST_SUCCESS']	    				= 	"Thanks for your interest. Somebody will contact you shortly.";
$lang['GIVEREVIEW_SUCCESS']	    					= 	"Thanks for your review!";