<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard"><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="lnr lnr-user"></i>Profile Details</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline">
        <div class="panel-heading row">
          <h3 class="tab panel-title">Profile Details</h3>
        </div>
        <hr class="differ">
        <div class="dash">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th width="10%">Sr.No.</th>
                <th width="20%">Name</th>
                <th width="20%">Display Name</th>
                <th width="20%">Email Id</th>
                <th width="10%">Mobile No</th>
                <th width="10%">Image</th>
                <th width="10%" class="center">--</th>
              </tr>
            </thead>
            <tbody>
              <?php if($ADMINDATA <> ""): $i=1; foreach($ADMINDATA as $ADMININFO): ?>
                <tr class="<?php if($i%2 == 0): echo 'odd'; else: echo 'even'; endif; ?> gradeX">
                  <td><?=$i++?></td>
                  <td><?=stripslashes($ADMININFO['admin_name'])?></td>
                  <td><?=stripslashes($ADMININFO['admin_display_name'])?></td>
                  <td><?=stripslashes($ADMININFO['admin_email_id'])?></td>
                  <td><?=stripslashes($ADMININFO['admin_mobile_number'])?></td>
                  <td><?php echo $ADMININFO['admin_image']?'<img src="'.stripslashes($ADMININFO['admin_image']).'" alt="Profile image"':''; ?></td>
                  <td class="center">
                    <div class="btn-group">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="{FULL_SITE_URL}editprofile/<?=$ADMININFO['encrypt_id']?>"><i class="fa fa-edit"></i> Edit Details</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php $i++; endforeach; endif; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>