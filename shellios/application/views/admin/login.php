<div class="content">
  <form name="loginform" id="loginform" class="form-auth-small" action="" method="post">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
    <div class="header">
      <div class="logo text-center"><img src="{ASSET_ADMIN_URL}images/logo-2.png" alt="Klorofil Logo"></div>
      <p class="lead">Admin Login</p>
      <?php if($error): ?><p class="error"><?=$error?></p><?php endif; ?>
    </div>
    <div class="form-group">
      <label for="signin-email" class="control-label sr-only">Email</label>
      <input type="text" name="userEmail" id="userEmail" class="form-control required email" value="<?php if(set_value('userEmail')): echo set_value('userEmail'); endif; ?>" placeholder="Email" autocomplete="off"/>
        <?php if(form_error('userEmail')): ?>
        <label for="userEmail" generated="true" class="error"><?php echo form_error('userEmail'); ?></label>
        <?php endif; ?>
    </div>
    <div class="form-group">
      <label for="signin-password" class="control-label sr-only">Password</label>
      <input type="password" name="userPassword" id="userPassword" class="form-control required" value="<?php if(set_value('userPassword')): echo set_value('userPassword'); endif; ?>" placeholder="Password" autocomplete="off"/>
        <?php if(form_error('userPassword')): ?>
        <label for="userPassword" generated="true" class="error"><?php echo form_error('userPassword'); ?></label>
        <?php endif; ?>
    </div>
    <input type="hidden" name="loginFormSubmit" id="loginFormSubmit" value="Login">
    <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
    <?php /*?><div class="bottom">
      <span class="helper-text"><i class="fa fa-lock"></i> <a href="javascript:void(0);" id="to-recover">Forgot Password?</a></span>
    </div><?php */?>
  </form>
  <?php /*?><form name="recoverform" id="recoverform" action="" method="post" class="form-auth-small">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
    <div class="header">
      <div class="logo text-center"><img src="{ASSET_ADMIN_URL}images/logo.png" alt="Klorofil Logo"></div>
      <p class="lead">Enter Your e-mail Address Below And We Will Send You Link To Recover a Password.</p>
      <?php if($forgoterror): ?><p class="error"><?=$forgoterror?></p><?php endif; ?>
      <?php if(!$forgotsuccess): ?><p class="success"><?=$forgotsuccess?></p><?php endif; ?>
    </div>
    <div class="form-group">
      <input type="text" name="forgotEmail" id="forgotEmail" class="form-control required email" value="<?php if(set_value('forgotEmail') && $forgotsuccess ==''): echo set_value('forgotEmail'); endif; ?>" placeholder="Email" autocomplete="off"/>
        <?php if(form_error('forgotEmail')): ?>
        <label for="forgotEmail" generated="true" class="error"><?php echo form_error('forgotEmail'); ?></label>
        <?php endif; ?>
    </div>
    <input type="hidden" name="recoverformSubmit" id="recoverformSubmit" value="Login">
    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
    <div class="bottom">
      <span class="helper-text"> <a href="javascript:void(0);" id="to-login">&laquo; Back To Login</a></span>
    </div>
  </form><?php */?>
</div>
<script>
var forgoterror = 'NO'
<?php if($forgoterror): ?>
    forgoterror = 'YES';
<?php elseif($forgotsuccess): ?>
  forgoterror = 'YES';
<?php endif; ?>
</script>