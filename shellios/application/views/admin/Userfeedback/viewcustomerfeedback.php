<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard" class=""><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="<?php echo correctLink('userfeedbackAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class=""><i class="lnr lnr-user"></i>Feedback</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="fa fa-edit"></i>View Users Feedback Details</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline inr-form">
        <div class="panel-heading row">
          <h3 class="panel-title tab">View Feedback Details</h3>
          <a href="<?php echo correctLink('userfeedbackAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-default add_btn">Back</a>
        </div>
        <hr class="differ">
        <div class="panel">
          <div class="panel-body row">
            <fieldset>
              <legend>Personal details</legend>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group">
                    <label class="fancy-checkbox form-headings">Name</label>
                    <?php echo stripslashes($VIEWDATA['user_name']); ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group">
                    <label class="fancy-checkbox form-headings">Phone</label>
                    <?php echo stripslashes($VIEWDATA['userPhone']); ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group">
                    <label class="fancy-checkbox form-headings">Email</label>
                    <?php echo stripslashes($VIEWDATA['user_email']); ?>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-9 col-sm-9 col-xs-9">
                  <div class="form-group">
                    <label class="fancy-checkbox form-headings">Feedback Content</label>
                    <td><?php echo stripslashes($VIEWDATA['message']) ?></td>
                    
                  </div>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>