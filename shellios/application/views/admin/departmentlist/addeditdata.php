<?php //echo "<pre>"; print_r($this->session->userdata()); die; ?>
<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard" class=""><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="<?php echo correctLink('departmentlistAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class=""><i class="lnr lnr-user"></i>Department</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="fa fa-edit"></i><?=$EDITDATA?'Edit':'Add'?> Department</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline inr-form">
        <div class="panel-heading row">
          <h3 class="panel-title tab"><?=$EDITDATA?'Edit':'Add'?> Department</h3>
          <a href="<?php echo correctLink('departmentlistAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-default add_btn">Back</a>
        </div>
        <hr class="differ">
        <div class="panel">
          <div class="panel-body row">
            <form id="currentPageForm" name="currentPageForm" class="form-auth-small" method="post" action="">
              <input type="hidden" name="CurrentDataID" id="CurrentDataID" value="<?=$EDITDATA['encrypt_id']?>"/>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('department_name')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Name<span class="required">*</span></label>
                    <input type="text" name="department_name" id="department_name"value="<?php if(set_value('department_name')): echo set_value('department_name'); else: echo stripslashes($EDITDATA['department_name']);endif; ?>" class="form-control required" placeholder="Name">
                    <?php if(form_error('department_name')): ?>
                      <p for="department_name" generated="true" class="error"><?php echo form_error('department_name'); ?></p>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <input type="hidden" name="SaveChanges" id="SaveChanges" value="Submit">
              <button type="submit" class="btn btn-primary btn-lg form-btn">Submit</button>
              <a href="<?php echo correctLink('departmentlistAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-primary btn-lg form-btn">Cancel</a> 
              <span class="tools pull-right"> <span class="btn btn-primary btn-lg btn-block">Note
                  :- <strong><span style="color:#FF0000;">*</span> Indicates
                  Required Fields</strong> </span> 
              </span>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>