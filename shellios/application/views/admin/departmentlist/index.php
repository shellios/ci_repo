<?php //echo "<pre>";  print_r($this->session->userdata()); die; ?>
<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard"><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="lnr lnr-user"></i>Department</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline">
        <div class="panel-heading row">
          <h3 class="tab panel-title">Department Details</h3>
          <a href="{FULL_SITE_URL}{CURRENT_CLASS}/addeditdata" class="btn btn-default add_btn">Add Department</a>
        </div>
        <hr class="differ">
        <form id="Data_Form" name="Data_Form" method="get" action="<?php echo $forAction; ?>">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 search">
              <label>Show
                <select name="showLength" id="showLength" class="form-control input-sm">
                  <option value="2" <?php if($perpage == '2')echo 'selected="selected"'; ?>>2</option>
                  <option value="10" <?php if($perpage == '10')echo 'selected="selected"'; ?>>10</option>
                  <option value="25" <?php if($perpage == '25')echo 'selected="selected"'; ?>>25</option>
                  <option value="50" <?php if($perpage == '50')echo 'selected="selected"'; ?>>50</option>
                  <option value="100" <?php if($perpage == '100')echo 'selected="selected"'; ?>>100</option>
                  <option value="All" <?php if($perpage == 'All')echo 'selected="selected"'; ?>>All</option>
                </select>
                entries
              </label>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 search-ryt">
              <input type="text" name="searchValue" id="searchValue" value="<?php echo $searchValue; ?>" class="form-control input-smt" placeholder="Enter Search Text">
            </div>
          </div>
          <div class="dash">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="10%">Sr.No.</th>
                  <th>Department Name</th>
                  <th>Status</th>
                  <?php if($edit_data == 'Y'): ?>
                  <th width="10%" class="center">--</th>
                  <?php endif; ?>
                </tr>
              </thead>
              <tbody>
                <?php if($ALLDATA <> ""): $i=1; foreach($ALLDATA as $ALLDATAINFO): ?>
                  <tr class="<?php if($i%2 == 0): echo 'odd'; else: echo 'even'; endif; ?> gradeX">
                    <td><?=$i++?></td>
                    <td><?=stripslashes($ALLDATAINFO['department_name'])?></td>
                    <td><?=showStatus($ALLDATAINFO['status'])?></td>
                    <?php if($edit_data == 'Y'): ?>
                      <td class="center">
                        <div class="btn-group">
                          <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                          <ul class="dropdown-menu">
                            <?php if($edit_data == 'Y'): ?>
                              <li><a href="{FULL_SITE_URL}{CURRENT_CLASS}/addeditdata/<?=$ALLDATAINFO['encrypt_id']?>"><i class="fa fa-edit"></i> Edit details</a></li>
                              <li class="divider"></li>
                              <?php if($ALLDATAINFO['status'] == 'Y'): ?>
                              <li><a href="{FULL_SITE_URL}{CURRENT_CLASS}/changestatus/<?=$ALLDATAINFO['encrypt_id']?>/N"><i class="fa fa-hand-o-up"></i> Inactive</a></li>
                              <?php elseif($ALLDATAINFO['status'] == 'N'): ?>
                              <li><a href="{FULL_SITE_URL}{CURRENT_CLASS}/changestatus/<?=$ALLDATAINFO['encrypt_id']?>/Y"><i class="fa fa-hand-o-up"></i> Active</a></li>
                            <?php endif; endif; ?>
                          </ul>
                        </div>
                      </td>
                    <?php endif; ?>
                  </tr>
                <?php endforeach; else: ?>
                  <tr>
                    <td colspan="4" style="text-align:center;">No Data Available In Table</td>
                  </tr>
                <?php endif; ?>
              </tbody>
            </table>
            <div class="pagi row">
              <div class="col-md-6 col-sm-6 col-xs-6 pagi-txt"><?php echo $noOfContent; ?></div>
              <div class="col-md-6 col-sm-6 col-xs-6 pagi-bar">
                <?=$PAGINATION?>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
var prevSerchValue  = '<?php echo $searchValue; ?>';
</script>