<div class="content">
  <form name="passwordRecoverForm" id="passwordRecoverForm" class="form-auth-small" action="" method="post">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
    <div class="header">
      <div class="logo text-center"><img src="{ASSET_ADMIN_URL}images/logo.png" alt="Klorofil Logo"></div>
      <p class="lead">Password Recover</p>
      <?php if($recovererror): ?><p class="error"><?=$recovererror?></p><?php endif; ?>
      <?php if($recoversuccess): ?><p class="success"><?=$recoversuccess?></p><?php endif; ?>
    </div>
    <div class="form-group">
      <label for="signin-email" class="control-label sr-only">OTP</label>
      <input type="text" name="userOtp" id="userOtp" class="form-control required" value="<?php if(set_value('userOtp')): echo set_value('userOtp'); endif; ?>" placeholder="OTP" autocomplete="off"/>
      <?php if(form_error('userOtp')): ?>
        <label for="userOtp" generated="true" class="error"><?php echo form_error('userOtp'); ?></label>
      <?php endif; ?>
    </div>
    <div class="form-group">
      <label for="signin-password" class="control-label sr-only">Password</label>
      <input type="password" name="userPassword" id="userPassword" class="form-control required" value="<?php if(set_value('userPassword')): echo set_value('userPassword'); endif; ?>" placeholder="Password" autocomplete="off"/>
        <?php if(form_error('userPassword')): ?>
        <label for="userPassword" generated="true" class="error"><?php echo form_error('userPassword'); ?></label>
        <?php endif; ?>
    </div>
    <div class="form-group">
      <label for="signin-password" class="control-label sr-only">Confirm Password</label>
      <input type="password" name="userConfPassword" id="userConfPassword" class="form-control required" value="<?php if(set_value('userConfPassword')): echo set_value('userConfPassword'); endif; ?>" placeholder="Confirm Password" autocomplete="off"/>
        <?php if(form_error('userConfPassword')): ?>
        <label for="userConfPassword" generated="true" class="error"><?php echo form_error('userConfPassword'); ?></label>
        <?php endif; ?>
    </div>
    <input type="hidden" name="passwordRecoverFormSubmit" id="passwordRecoverFormSubmit" value="Recover">
    <button type="submit" class="btn btn-primary btn-lg btn-block">Recover</button>
    <?php if($recoversuccess): ?>
    <div class="bottom">
      <span class="helper-text"><a href="<?php echo base_url('admin'); ?>">Go To Login</a></span>
    </div>
    <?php endif; ?>
  </form>
</div>
<script>
var forgoterror = 'NO'
<?php if($forgoterror): ?>
    forgoterror = 'YES';
<?php elseif($forgotsuccess): ?>
  forgoterror = 'YES';
<?php endif; ?>
</script>