
<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard" class=""><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="<?php echo correctLink('servicerequestAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class=""><i class="lnr lnr-user"></i>User Service Request</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="fa fa-edit"></i><?=$EDITDATA?'Edit':'Add'?> User Service Request</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline inr-form">
        <div class="panel-heading row">
          <h3 class="panel-title tab"><?=$EDITDATA?'Edit':'Add'?> User Service Request</h3>
          <a href="<?php echo correctLink('servicerequestAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-default add_btn">Back</a>
        </div>
        <hr class="differ">
        <div class="panel">
          <div class="panel-body row">
            <form id="currentPageForm" name="currentPageForm" class="form-auth-small" method="post" action="">
              <input type="hidden" name="CurrentIdForUnique" id="CurrentIdForUnique" value="<?=$EDITDATA['srv_req_id']?>"/>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <fieldset>
                <legend>User Details</legend>
                <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
              <?php /*?><?php $allrating = array(1=>'Created',2=>'Pending',3=>'Closed'); ?><?php */?>
              <?php $allrating = array(2=>'Pending',3=>'Closed'); ?>
            <div class="form-group">
              <label class="col-lg-3 control-label"> User Status.</label>
              <select name="userstatus" id="userstatus" class="form-control required">
                <option value="">Select Status</option>
              <?php foreach($allrating as $rate){ //print_r($allrating); die;?>
                <option value="<?php echo $rate; ?>" <?php if($rate == $EDITDATA['service_request_status']): echo "selected"; endif;?>><?php echo $rate; ?></option>
              <?php } ?>
              </select>
                     <?php if(form_error('userstatus')): ?>
                      <label for="userstatus" generated="true" class="error"><?php echo form_error('userstatus'); ?></label>
                      <?php endif; ?>
                  </div>
                  </div>
              </div>
             
              </fieldset>
              <input type="hidden" name="SaveChanges" id="SaveChanges" value="Submit">
              <button type="submit" class="btn btn-primary btn-lg form-btn">Submit</button>
              <a href="<?php echo correctLink('servicerequestAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-primary btn-lg form-btn">Cancel</a> 
              <span class="tools pull-right"> <span class="btn btn-primary btn-lg btn-block">Note
                  :- <strong><span style="color:#FF0000;">*</span> Indicates
                  Required Fields</strong> </span> 
              </span>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
  UploadImage('0');
});
</script>
<script>
$(function(){
	$("#start_date").datepicker({dateFormat:'dd-mm-yy',changeMonth: true,changeYear: true,yearRange: "1960:<?php echo date('Y'); ?>"});
	$("#end_date").datepicker({dateFormat:'dd-mm-yy',changeMonth: true,changeYear: true,yearRange: "1960:<?php echo date('Y'); ?>"});
});
</script>
<div id="prodAttImageUploadModel" class="modal" role="dialog">
  <div class="modal-dialog modal-sm"> 
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4>
              <center>Position And Size Your Photo</center>
            </h4>
       </div>
      <div class="modal-body">
        <div class="profileimg-plug col-md-12 col-sm-12 col-xs-12">
                <div class="image-editor">
                  <div class="file-input-wrapper">
                    <label for="prodAtt-upload-profile-file" class="file-input-button">Choose Image</label>
                    <input type="file" name="image" id="prodAtt-upload-profile-file" class="cropit-image-input"><br />
                    <span style="color:#FF0000; font-size:12px;" id="prodAttBlinker">Image Must Be Min Width: 400px
                          And Min Height: 400px.</span>
                  </div>
                  <div class="cropit-preview"></div>
                  <div class="image-size-label"> Resize Image </div>
                  <div class="rotat-btn">
                    <input type="range" class="cropit-image-zoom-input"><br />
                    <button class="rotate-ccw"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                    <button class="rotate-cw"><i class="fa fa-repeat"></i></button>
                  </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default prodAttImageClosedButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-default prodAttImageSaveButton">Save</button>
            <input type="hidden" name="prodAttImageLoading" id="prodAttImageLoading" value="NO" />
        </div>
    </div>
  </div>
</div>
<script>
  $(function(){
    $('#prodAttImageUploadModel .image-editor').cropit();
    $('#prodAttImageUploadModel .rotate-cw').click(function() {
      $('#prodAttImageUploadModel .image-editor').cropit('rotateCW');
    });
    $('#prodAttImageUploadModel .rotate-ccw').click(function() {
      $('#prodAttImageUploadModel .image-editor').cropit('rotateCCW');
    });
  });
  $(document).on('click','#prodAttImageUpload',function(){
    $('#prodAttImageUploadModel').modal({backdrop:'static', show: true, keyword: false});
    $('#prodAttImageUploadModel .cropit-preview img').attr('src','');
    $('#prodAttImageUploadModel .modal-footer.plucgin-clodebtn .prodAttImageClosedButton').html('Close');
    $('#prodAttImageUploadModel .modal-footer.plucgin-clodebtn .prodAttImageSaveButton').html('Save');
    $('#prodAttImageUploadModel #prodAttImageLoading').val('NO');
    $(".modal-footer .prodAttImageClosedButton").removeAttr('disabled');
    $(".modal-footer .prodAttImageSaveButton").removeAttr('disabled');
  });
  $(document).on('click','.prodAttImageSaveButton',function() {  
    var imageData = $('#prodAttImageUploadModel .image-editor').cropit('export'); 
    if($('#prodAttImageUploadModel #prodAttImageLoading').val() == 'NO')
    {
      $('#prodAttImageUploadModel .prodAttImageSaveButton').html('Saving..');
      $('#prodAttImageUploadModel #prodAttImageLoading').val('YES');
      $(".modal-footer .prodAttImageClosedButton").attr("disabled", "disabled");
      $(".modal-footer .prodAttImageSaveButton").attr("disabled", "disabled");
      $.ajax({
          type: 'post',
           url: FULLSITEURL+CURRENTCLASS+'/prodAttImageUpload',
          data: {imageData:imageData},
        success: function(rdata){ 
          if(rdata != 'UPLODEERROR') {
            $('#prodAttImageInput').val(rdata);
            $('#prodAttImageDiv').html('<img src="'+rdata+'" border="0" alt="" width="100" /> <a href="javascript:void(0);" onClick="prodAttImageDelete(\''+rdata+'\');"> <img src="{ASSET_ADMIN_URL}images/cross.png" border="0" alt="" /> </a>');
            $('#prodAttImageUploadModel').modal('hide');
            $('#prodAttImageUploadModel #prodAttImageLoading').val('NO');
            return false;
          }
        }
      });
    }
  });

  //////////////////////////////////   Image delete Through Ajax
  function prodAttImageDelete(imageName)
  {
      if(confirm("Sure to delete?"))
      {
          $.ajax({
           type: 'post',
                url: FULLSITEURL+CURRENTCLASS+'/prodAttImageDelete',
         data: {csrf_api_key:csrf_api_value,imageName:imageName},
                success: function(response) { 
            document.getElementById('prodAttImageInput').value = '';
          document.getElementById('prodAttImageDiv').innerHTML ='';
                }
              });
      }
  }
</script>
<script>
  var prodAtt_blink_speed = 500; var prodAtt = setInterval(function () { var prodAttsele = document.getElementById('prodAttBlinker'); prodAttsele.style.visibility = (prodAttsele.style.visibility == 'hidden' ? '' : 'hidden'); }, prodAtt_blink_speed);
</script>