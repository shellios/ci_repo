<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard" class=""><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="{FULL_SITE_URL}profile" class=""><i class="lnr lnr-user"></i>Profile Details</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="fa fa-edit"></i>Edit Profile Details</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline inr-form">
        <div class="panel-heading row">
          <h3 class="panel-title tab">Edit Profile Details</h3>
          <a href="{FULL_SITE_URL}profile" class="btn btn-default add_btn">Back</a>
        </div>
        <hr class="differ">
        <div class="panel">
          <div class="panel-body row">
            <form id="currentPageForm" name="currentPageForm" class="form-auth-small" method="post" action="">
              <input type="hidden" name="CurrentDataID" id="CurrentDataID" value="<?=$profileuserdata['encrypt_id']?>"/>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_name')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Name<span class="required">*</span></label>
                    <input type="text" name="admin_name" id="admin_name" value="<?php if(set_value('admin_name')): echo set_value('admin_name'); else: echo stripslashes($profileuserdata['admin_name']);endif; ?>" class="form-control required" placeholder="Name">
                    <?php if(form_error('admin_name')): ?>
                      <span for="admin_name" generated="true" class="help-inline"><?php echo form_error('admin_name'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_display_name')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Display Name<span class="required">*</span></label>
                    <input type="text" name="admin_display_name" id="admin_display_name" value="<?php if(set_value('admin_display_name')): echo set_value('admin_display_name'); else: echo stripslashes($profileuserdata['admin_display_name']);endif; ?>" class="form-control required" placeholder="Display Name">
                    <?php if(form_error('admin_display_name')): ?>
                      <span for="admin_display_name" generated="true" class="help-inline"><?php echo form_error('admin_display_name'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <?php if(sessionData('SHELLIOS_ADMIN_TYPE') == 'Super admin'): ?>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group <?php if(form_error('admin_slug')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">Slug URL<span class="required">*</span></label>
                      <input type="text" name="admin_slug" id="admin_slug" value="<?php if(set_value('admin_slug')): echo set_value('admin_slug'); else: echo stripslashes($profileuserdata['admin_slug']);endif; ?>" class="form-control required" placeholder="Slug URL">
                      <?php if(form_error('admin_slug')): ?>
                        <span for="admin_slug" generated="true" class="help-inline"><?php echo form_error('admin_slug'); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_email_id')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Email Id<span class="required">*</span></label>
                    <input type="text" name="admin_email_id" id="admin_email_id" value="<?php if(set_value('admin_email_id')): echo set_value('admin_email_id'); else: echo stripslashes($profileuserdata['admin_email_id']);endif; ?>" class="form-control required email" placeholder="Email Id">
                    <?php if(form_error('admin_email_id')): ?>
                      <span for="admin_email_id" generated="true" class="help-inline"><?php echo form_error('admin_email_id'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('new_password')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">New Password</label>
                    <input type="password" name="new_password" id="new_password" value="<?php if(set_value('new_password')): echo set_value('new_password'); endif; ?>" class="form-control" placeholder="New Password">
                    <?php if(form_error('new_password')): ?>
                      <span for="new_password" generated="true" class="help-inline"><?php echo form_error('new_password'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('conf_password')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Confirm Password</label>
                    <input type="password" name="conf_password" id="conf_password" alue="<?php if(set_value('conf_password')): echo set_value('conf_password'); endif; ?>" class="form-control" placeholder="Confirm Password">
                    <?php if(form_error('conf_password')): ?>
                      <span for="conf_password" generated="true" class="help-inline"><?php echo form_error('conf_password'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_mobile_number')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Mobile Number<span class="required">*</span></label>
                    <input type="text" name="admin_mobile_number" id="admin_mobile_number" value="<?php if(set_value('admin_mobile_number')): echo set_value('admin_mobile_number'); else: echo stripslashes($profileuserdata['admin_mobile_number']);endif; ?>" class="form-control required" placeholder="Mobile Number">
                    <?php if(form_error('admin_mobile_number')): ?>
                      <span for="admin_mobile_number" generated="true" class="help-inline"><?php echo form_error('admin_mobile_number'); ?></span>
                    <?php endif; if($mobileerror):  ?>
                      <span for="admin_mobile_number" generated="true" class="help-inline"><?php echo $mobileerror; ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_address')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Address<span class="required">*</span></label>
                    <input type="text" name="admin_address" id="admin_address" value="<?php if(set_value('admin_address')): echo set_value('admin_address'); else: echo stripslashes($profileuserdata['admin_address']);endif; ?>" class="form-control required" placeholder="Address">
                    <?php if(form_error('admin_address')): ?>
                      <span for="admin_address" generated="true" class="help-inline"><?php echo form_error('admin_address'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_locality')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Locality</label>
                    <input type="text" name="admin_locality" id="admin_locality" value="<?php if(set_value('admin_locality')): echo set_value('admin_locality'); else: echo stripslashes($profileuserdata['admin_locality']);endif; ?>" class="form-control" placeholder="Locality">
                    <?php if(form_error('admin_locality')): ?>
                      <span for="admin_locality" generated="true" class="help-inline"><?php echo form_error('admin_locality'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_city')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">City</label>
                    <input type="text" name="admin_city" id="admin_city" value="<?php if(set_value('admin_city')): echo set_value('admin_city'); else: echo stripslashes($profileuserdata['admin_city']);endif; ?>" class="form-control" placeholder="City">
                    <?php if(form_error('admin_city')): ?>
                      <span for="admin_city" generated="true" class="help-inline"><?php echo form_error('admin_city'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_state')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">State</label>
                    <input type="text" name="admin_state" id="admin_state" value="<?php if(set_value('admin_state')): echo set_value('admin_state'); else: echo stripslashes($profileuserdata['admin_state']);endif; ?>" class="form-control" placeholder="State">
                    <?php if(form_error('admin_state')): ?>
                      <span for="admin_state" generated="true" class="help-inline"><?php echo form_error('admin_state'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_zipcode')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Zipcode</label>
                    <input type="text" name="admin_zipcode" id="admin_zipcode" value="<?php if(set_value('admin_zipcode')): echo set_value('admin_zipcode'); else: echo stripslashes($profileuserdata['admin_zipcode']);endif; ?>" class="form-control" placeholder="Zipcode">
                    <?php if(form_error('admin_zipcode')): ?>
                      <span for="admin_zipcode" generated="true" class="help-inline"><?php echo form_error('admin_zipcode'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_image')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Image</label>                   
                    <?php if(set_value('admin_image')): $adminimage = set_value('admin_image'); elseif($profileuserdata['admin_image']): $adminimage = stripslashes($profileuserdata['admin_image']); else: $adminimage = ''; endif; ?>
                    <img border="0" alt="" src="{ASSET_ADMIN_URL}images/browse-white.png" id="firstImageUpload" class="img-responsive" style="cursor:pointer;">
                    <input type="etxt" name="admin_image" id="firstAvtar" value="<?php echo $adminimage; ?>" class="form-control" style="border:none;width:0px;height:0px;margin-top: -14px;" />
                    <?php if(form_error('admin_image')): ?>
                      <span for="admin_image" generated="true" class="help-inline"><?php echo form_error('admin_image'); ?></span>
                    <?php endif; ?>
                    <span id="firstAvtarImageDiv" style="margin-top:5px;">
                      <?php if($adminimage): ?>
                       <img border="0" alt="" src="<?php echo $adminimage; ?>" class="img-responsive">&nbsp;
                       <a class="spancross" onclick="firstImageDelete('<?php echo $adminimage; ?>');" href="javascript:void(0);"> <img border="0" alt="" src="{ASSET_ADMIN_URL}images/cross.png"></a>
                      <?php endif; ?>
                    </span>
                  </div>
                </div>
              </div>
              <input type="hidden" name="SaveChanges" id="SaveChanges" value="Submit">
              <button type="submit" class="btn btn-primary btn-lg form-btn">Submit</button>
              <a href="{FULL_SITE_URL}profile" class="btn btn-primary btn-lg form-btn">Cancel</a> 
              <span class="tools pull-right"> <span class="btn btn-primary btn-lg btn-block">Note
                  :- <strong><span style="color:#FF0000;">*</span> Indicates
                  Required Fields</strong> </span> 
              </span>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>