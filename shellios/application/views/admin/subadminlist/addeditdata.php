<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard" class=""><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="<?php echo correctLink('subadminlistAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class=""><i class="lnr lnr-user"></i>Subadmin</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="fa fa-edit"></i><?=$EDITDATA?'Edit':'Add'?> Subadmin</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline inr-form">
        <div class="panel-heading row">
          <h3 class="panel-title tab"><?=$EDITDATA?'Edit':'Add'?> Subadmin</h3>
          <a href="<?php echo correctLink('subadminlistAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-default add_btn">Back</a>
        </div>
        <hr class="differ">
        <div class="panel">
          <div class="panel-body row">
            <form id="currentPageForm" name="currentPageForm" class="form-auth-small" method="post" action="">
              <input type="hidden" name="CurrentDataID" id="CurrentDataID" value="<?=$EDITDATA['encrypt_id']?>"/>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_department_id')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Department<span class="required">*</span></label>
                    <?php if(set_value('admin_department_id')): $admindepartmentid = set_value('admin_department_id'); elseif($DEPARTMENTDATA): $admindepartmentid = stripslashes($DEPARTMENTDATA['admin_metadata_value']); else: $admindepartmentid = ''; endif; ?>
                    <select name="admin_department_id" id="admin_department_id" class="form-control required">
                      <?php echo $this->admin_model->getAdminDepartment($admindepartmentid); ?>
                    </select>
                    <?php if(form_error('admin_department_id')): ?>
                      <span for="admin_department_id" generated="true" class="help-inline"><?php echo form_error('admin_department_id'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>


                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_name')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Name<span class="required">*</span></label>
                    <input type="text" name="admin_name" id="admin_name" value="<?php if(set_value('admin_name')): echo set_value('admin_name'); else: echo stripslashes($EDITDATA['admin_name']);endif; ?>" class="form-control required" placeholder="Name">
                    <?php if(form_error('admin_name')): ?>
                      <span for="admin_name" generated="true" class="help-inline"><?php echo form_error('admin_name'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_display_name')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Display Name<span class="required">*</span></label>
                    <input type="text" name="admin_display_name" id="admin_display_name" value="<?php if(set_value('admin_display_name')): echo set_value('admin_display_name'); else: echo stripslashes($EDITDATA['admin_display_name']);endif; ?>" class="form-control required" placeholder="Display Name">
                    <?php if(form_error('admin_display_name')): ?>
                      <span for="admin_display_name" generated="true" class="help-inline"><?php echo form_error('admin_display_name'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_email_id')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Email Id<span class="required">*</span></label>
                    <input type="text" name="admin_email_id" id="admin_email_id" value="<?php if(set_value('admin_email_id')): echo set_value('admin_email_id'); else: echo stripslashes($EDITDATA['admin_email_id']);endif; ?>" class="form-control required email" placeholder="Email Id">
                    <?php if(form_error('admin_email_id')): ?>
                      <span for="admin_email_id" generated="true" class="help-inline"><?php echo form_error('admin_email_id'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <?php if($EDITDATA <> ""): ?>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group <?php if(form_error('new_password')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">New Password</label>
                      <input type="password" name="new_password" id="new_password" value="<?php if(set_value('new_password')): echo set_value('new_password'); endif; ?>" class="form-control" placeholder="New Password">
                      <?php if(form_error('new_password')): ?>
                        <span for="new_password" generated="true" class="help-inline"><?php echo form_error('new_password'); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group <?php if(form_error('conf_password')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">Confirm Password</label>
                       <input type="password" name="conf_password" id="conf_password" value="<?php if(set_value('conf_password')): echo set_value('conf_password'); endif; ?>" class="form-control" placeholder="Confirm Password">
                      <?php if(form_error('conf_password')): ?>
                        <span for="conf_password" generated="true" class="help-inline"><?php echo form_error('conf_password'); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
                <?php else: ?>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('new_password')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Password<span class="required">*</span></label>
                    <input type="password" name="new_password" id="new_password" value="<?php if(set_value('new_password')): echo set_value('new_password'); endif; ?>" class="form-control required" placeholder="New password">
                    <?php if(form_error('new_password')): ?>
                      <span for="new_password" generated="true" class="help-inline"><?php echo form_error('new_password'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('conf_password')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Confirm Password<span class="required">*</span></label>
                    <input type="password" name="conf_password" id="conf_password" value="<?php if(set_value('conf_password')): echo set_value('conf_password'); endif; ?>" class="form-control required" placeholder="Confirm Password">
                    <?php if(form_error('conf_password')): ?>
                      <span for="conf_password" generated="true" class="help-inline"><?php echo form_error('conf_password'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <?php endif; ?>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_mobile_number')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Mobile Number<span class="required">*</span></label>
                    <input type="text" name="admin_mobile_number" id="admin_mobile_number" value="<?php if(set_value('admin_mobile_number')): echo set_value('admin_mobile_number'); else: echo stripslashes($EDITDATA['admin_mobile_number']);endif; ?>" class="form-control required" placeholder="Mobile Number">
                    <?php if(form_error('admin_mobile_number')): ?>
                      <span for="admin_mobile_number" generated="true" class="help-inline"><?php echo form_error('admin_mobile_number'); ?></span>
                    <?php endif; if($mobileerror):  ?>
                      <span for="admin_mobile_number" generated="true" class="help-inline"><?php echo $mobileerror; ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_address')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Address<span class="required">*</span></label>
                    <input type="text" name="admin_address" id="admin_address" value="<?php if(set_value('admin_address')): echo set_value('admin_address'); else: echo stripslashes($EDITDATA['admin_address']);endif; ?>" class="form-control required" placeholder="Address">
                    <?php if(form_error('admin_address')): ?>
                      <span for="admin_address" generated="true" class="help-inline"><?php echo form_error('admin_address'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_locality')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Locality</label>
                    <input type="text" name="admin_locality" id="admin_locality" value="<?php if(set_value('admin_locality')): echo set_value('admin_locality'); else: echo stripslashes($EDITDATA['admin_locality']);endif; ?>" class="form-control" placeholder="Locality">
                    <?php if(form_error('admin_locality')): ?>
                      <span for="admin_locality" generated="true" class="help-inline"><?php echo form_error('admin_locality'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_city')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">City</label>
                    <input type="text" name="admin_city" id="admin_city" value="<?php if(set_value('admin_city')): echo set_value('admin_city'); else: echo stripslashes($EDITDATA['admin_city']);endif; ?>" class="form-control" placeholder="City">
                    <?php if(form_error('admin_city')): ?>
                      <span for="admin_city" generated="true" class="help-inline"><?php echo form_error('admin_city'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_state')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">State</label>
                    <input type="text" name="admin_state" id="admin_state" value="<?php if(set_value('admin_state')): echo set_value('admin_state'); else: echo stripslashes($EDITDATA['admin_state']);endif; ?>" class="form-control" placeholder="State">
                    <?php if(form_error('admin_state')): ?>
                      <span for="admin_state" generated="true" class="help-inline"><?php echo form_error('admin_state'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('admin_zipcode')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Zipcode</label>
                    <input type="text" name="admin_zipcode" id="admin_zipcode" value="<?php if(set_value('admin_zipcode')): echo set_value('admin_zipcode'); else: echo stripslashes($EDITDATA['admin_zipcode']);endif; ?>" class="form-control" placeholder="Zipcode">
                    <?php if(form_error('admin_zipcode')): ?>
                      <span for="admin_zipcode" generated="true" class="help-inline"><?php echo form_error('admin_zipcode'); ?></span>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <fieldset>
                  <legend>Permission Section</legend>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row bg_dine">
                      <div class="col-md-4">
                        <?php if($PERERROR): echo '<span generated="true" class="error help-inline">'.$PERERROR.'</span>'; endif; ?>
                      </div>
                      <div class="col-md-2"><h3>View Data</h3></div>
                      <div class="col-md-2"><h3>Add Data</h3></div>
                      <div class="col-md-2"><h3>Edit Data</h3></div>
                      <div class="col-md-2"><h3>Delete Data</h3></div>
                    </div>
                  <?php if($Modirectory <> ""): $i=1; foreach($Modirectory as $MODinfo): $mmc = $MODinfo['encrypt_id'];  
                      if($_POST['mainmodule'.$mmc]):
                      $mainmodactive          = 'Y';
                      if($_POST['mainmodule_view_data'.$mmc]):
                        $mainmod_view_active    = 'Y';
                      else:
                        $mainmod_view_active    = 'N';
                      endif;
                      if($_POST['mainmodule_add_data'.$mmc]):
                        $mainmod_add_active     = 'Y';
                      else:
                        $mainmod_add_active     = 'N';
                      endif;
                      if($_POST['mainmodule_edit_data'.$mmc]):
                        $mainmod_edit_active    = 'Y';
                      else:
                        $mainmod_edit_active    = 'N';
                      endif;
                      if($_POST['mainmodule_delete_data'.$mmc]):
                        $mainmod_delete_active    = 'Y';
                      else:
                        $mainmod_delete_active    = 'N';
                      endif;
                    elseif($MODULEDATA['mainmodule'.$mmc]):
                      $mainmodactive          = 'Y';
                      if($MODULEDATA['mainmodule_view_data'.$mmc]):
                        $mainmod_view_active    = 'Y';
                      else:
                        $mainmod_view_active    = 'N';
                      endif;
                      if($MODULEDATA['mainmodule_add_data'.$mmc]):
                        $mainmod_add_active     = 'Y';
                      else:
                        $mainmod_add_active     = 'N';
                      endif;
                      if($MODULEDATA['mainmodule_edit_data'.$mmc]):
                        $mainmod_edit_active    = 'Y';
                      else:
                        $mainmod_edit_active    = 'N';
                      endif;
                      if($MODULEDATA['mainmodule_delete_data'.$mmc]):
                        $mainmod_delete_active    = 'Y';
                      else:
                        $mainmod_delete_active    = 'N';
                      endif;
                    else:
                      $mainmodactive          = 'N';
                      $mainmod_view_active      = 'N';
                      $mainmod_add_active       = 'N';
                      $mainmod_edit_active      = 'N';
                      $mainmod_delete_active      = 'N';
                    endif;
                  ?> 
                  <?php if($MODinfo['child_data'] == 'N'): ?>
                    <div class="row bg_module">
                      <div class="col-md-2 dice_h">
                        <h4>Module <?php echo $i; ?></h4>
                      </div>
                      <div class="col-md-10">
                        <div class="col-md-4 dice_single">
                          <div class="check_lable">
                            <input type="checkbox" name="mainmodule<?php echo $mmc; ?>" id="mainmodule<?php echo $mmc; ?>" value="Y" class="parentmodule" <?php if($mainmodactive == 'Y'): echo 'checked="checked"'; endif; ?>/>
                      &nbsp;<span for="mainmodule<?php echo $mmc; ?>"><?php echo ucfirst($MODinfo['module_display_name']); ?></span>
                          </div>
                        </div>
                        <div class="col-md-2 dice">
                          <input type="checkbox" name="mainmodule_view_data<?php echo $mmc; ?>" id="mainmodule_view_data<?php echo $mmc; ?>" value="Y" class="parentpermission" <?php if($mainmod_view_active == 'Y'): echo 'checked="checked"'; endif; ?>/>
                        </div>
                        <div class="col-md-2 dice">
                          <input type="checkbox" name="mainmodule_add_data<?php echo $mmc; ?>" id="mainmodule_add_data<?php echo $mmc; ?>" value="Y" class="parentpermission" <?php if($mainmod_add_active == 'Y'): echo 'checked="checked"'; endif; ?>/>
                        </div>
                        <div class="col-md-2 dice">
                          <input type="checkbox" name="mainmodule_edit_data<?php echo $mmc; ?>" id="mainmodule_edit_data<?php echo $mmc; ?>" value="Y" class="parentpermission" <?php if($mainmod_edit_active == 'Y'): echo 'checked="checked"'; endif; ?>/>
                        </div>
                        <div class="col-md-2 dice">
                          <input type="checkbox" name="mainmodule_delete_data<?php echo $mmc; ?>" id="mainmodule_delete_data<?php echo $mmc; ?>" value="Y" class="parentpermission" <?php if($mainmod_delete_active == 'Y'): echo 'checked="checked"'; endif; ?>/>
                        </div>
                      </div>
                    </div>
                  <?php else: ?>
                    <div class="row bg_module">
                      <div class="col-md-2 dice_h">
                        <h4>Module <?php echo $i; ?></h4>
                      </div>
                      <div class="col-md-10">
                        <div class="col-md-4 dice_single">
                          <div class="check_lable">
                            <input type="checkbox" name="mainmodule<?php echo $mmc; ?>" id="mainmodule<?php echo $mmc; ?>" value="Y" class="parentmodule" <?php if($mainmodactive == 'Y'): echo 'checked="checked"'; endif; ?>/>
                      &nbsp;<span for="mainmodule<?php echo $mmc; ?>"><?php echo ucfirst($MODinfo['module_display_name']); ?></span>
                          </div>
                        </div>
                        <div class="col-md-2 dice">&nbsp;</div>
                        <div class="col-md-2 dice">&nbsp;</div>
                        <div class="col-md-2 dice">&nbsp;</div>
                        <div class="col-md-2 dice">&nbsp;</div>
                    <?php $childdata    =   $this->adminauth_model->getModuleChild($MODinfo['encrypt_id']);
                      if($childdata <> ""):
                       foreach($childdata as $CDinfo):   $cmc = $CDinfo['encrypt_id'];  
                        if($_POST['childmodule'.$mmc.'_'.$cmc]):
                        $childmodactive         = 'Y';
                        if($_POST['childmodule_view_data'.$mmc.'_'.$cmc]):
                          $childmod_view_active   = 'Y';
                        else:
                          $childmod_view_active   = 'N';
                        endif;
                        if($_POST['childmodule_add_data'.$mmc.'_'.$cmc]):
                          $childmod_add_active    = 'Y';
                        else:
                          $childmod_add_active    = 'N';
                        endif;
                        if($_POST['childmodule_edit_data'.$mmc.'_'.$cmc]):
                          $childmod_edit_active   = 'Y';
                        else:
                          $childmod_edit_active   = 'N';
                        endif;
                        if($_POST['childmodule_delete_data'.$mmc.'_'.$cmc]):
                          $childmod_delete_active   = 'Y';
                        else:
                          $childmod_delete_active   = 'N';
                        endif;
                      elseif($MODULEDATA['childmodule'.$mmc.'_'.$cmc]):
                        $childmodactive         = 'Y';
                        if($MODULEDATA['childmodule_view_data'.$mmc.'_'.$cmc]):
                          $childmod_view_active   = 'Y';
                        else:
                          $childmod_view_active   = 'N';
                        endif;
                        if($MODULEDATA['childmodule_add_data'.$mmc.'_'.$cmc]):
                          $childmod_add_active    = 'Y';
                        else:
                          $childmod_add_active    = 'N';
                        endif;
                        if($MODULEDATA['childmodule_edit_data'.$mmc.'_'.$cmc]):
                          $childmod_edit_active   = 'Y';
                        else:
                          $childmod_edit_active   = 'N';
                        endif;
                        if($MODULEDATA['childmodule_delete_data'.$mmc.'_'.$cmc]):
                          $childmod_delete_active   = 'Y';
                        else:
                          $childmod_delete_active   = 'N';
                        endif;
                      else:
                        $childmodactive         = 'N';
                        $childmod_view_active     = 'N';
                        $childmod_add_active      = 'N';
                        $childmod_edit_active     = 'N';
                        $childmod_delete_active     = 'N';
                      endif;
                      ?>
                      <div class="col-md-12 bg_child_module">
                          <div class="col-md-4 dice_single">
                                <div class="check_lable">
                                  <input type="checkbox" name="childmodule<?php echo $mmc;?>_<?php echo $cmc;?>" id="childmodule<?php echo $mmc;?>_<?php echo $cmc;?>" value="Y" class="childmodule"  <?php if($childmodactive == 'Y'): echo 'checked="checked"'; endif; ?>/>
                                &nbsp;<span for="childmodule<?php echo $mmc;?>_<?php echo $cmc;?>"><?php echo ucfirst($CDinfo['module_display_name']); ?></span>
                                </div>
                          </div>
                          <div class="col-md-2 dice_single">
                            <div class="check_lable">
                              <input type="checkbox" name="childmodule_view_data<?php echo $mmc; ?>_<?php echo $cmc;?>" id="childmodule_view_data<?php echo $mmc; ?>_<?php echo $cmc;?>" value="Y" class="childpermission" <?php if($childmod_view_active == 'Y'): echo 'checked="checked"'; endif; ?>>
                            </div>
                          </div>
                          <div class="col-md-2 dice_single">
                            <div class="check_lable">
                             <input type="checkbox" name="childmodule_add_data<?php echo $mmc; ?>_<?php echo $cmc;?>" id="childmodule_add_data<?php echo $mmc; ?>_<?php echo $cmc;?>" value="Y" class="childpermission" <?php if($childmod_add_active == 'Y'): echo 'checked="checked"'; endif; ?>>
                            </div>
                          </div>
                          <div class="col-md-2 dice_single">
                            <div class="check_lable">
                              <input type="checkbox" name="childmodule_edit_data<?php echo $mmc; ?>_<?php echo $cmc;?>" id="childmodule_edit_data<?php echo $mmc; ?>_<?php echo $cmc;?>" value="Y" class="childpermission" <?php if($childmod_edit_active == 'Y'): echo 'checked="checked"'; endif; ?>>
                            </div>
                          </div>
                         <div class="col-md-2 dice_single">
                            <div class="check_lable">
                              <input type="checkbox" name="childmodule_delete_data<?php echo $mmc; ?>_<?php echo $cmc;?>" id="childmodule_delete_data<?php echo $mmc; ?>_<?php echo $cmc;?>" value="Y" class="childpermission" <?php if($childmod_delete_active == 'Y'): echo 'checked="checked"'; endif; ?>>
                            </div>
                          </div>
                        </div>
                      <?php $j++; endforeach; endif; ?>
                      </div>
                    </div>
                  <?php endif; ?>
                  <?php $i++; endforeach; endif; ?>  
                  </div>
                </fieldset>
              </div>
              <input type="hidden" name="SaveChanges" id="SaveChanges" value="Submit">
              <button type="submit" class="btn btn-primary btn-lg form-btn">Submit</button>
              <a href="<?php echo correctLink('subadminlistAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-primary btn-lg form-btn">Cancel</a> 
              <span class="tools pull-right"> <span class="btn btn-primary btn-lg btn-block">Note
                  :- <strong><span style="color:#FF0000;">*</span> Indicates
                  Required Fields</strong> </span> 
              </span>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).on('click','#currentPageForm .parentmodule',function(){ 
  var curobj  = $(this); 
  if(curobj.prop("checked") == true){ 
    curobj.closest('.bg_module').find('.parentpermission').prop('checked', true);
    curobj.closest('.bg_module').find('.bg_child_module').find('.childmodule').prop('checked', true);
    curobj.closest('.bg_module').find('.bg_child_module').find('.childpermission').prop('checked', true);
  }
  else if(curobj.prop("checked") == false){  
    curobj.closest('.bg_module').find('.parentpermission').prop('checked', false);
    curobj.closest('.bg_module').find('.bg_child_module').find('.childmodule').prop('checked', false);
    curobj.closest('.bg_module').find('.bg_child_module').find('.childpermission').prop('checked', false);
  }
});

$(document).on('click','#currentPageForm .parentpermission',function(){
  var curobj  = $(this); 
  var count = 0;
  curobj.closest('.bg_module').find('.parentpermission').each(function(){
    if($(this).prop("checked") == true)
      count = 1;
  });
  if(count == 1)
    curobj.closest('.bg_module').find('.parentmodule').prop('checked', true);
  else
    curobj.closest('.bg_module').find('.parentmodule').prop('checked', false);
});

$(document).on('click','#currentPageForm .childmodule',function(){ 
  var curobj  = $(this);  
  if(curobj.prop("checked") == true)
    curobj.closest('.bg_child_module').find('.childpermission').prop('checked', true);
  else if(curobj.prop("checked") == false)
    curobj.closest('.bg_child_module').find('.childpermission').prop('checked', false);
  var count = 0;
  curobj.closest('.bg_module').find('.childmodule').each(function(){
    if($(this).prop("checked") == true)
      count = 1;
  });  
  if(count == 1)
    curobj.closest('.bg_module').find('.parentmodule').prop('checked', true);
  else
    curobj.closest('.bg_module').find('.parentmodule').prop('checked', false);
});

$(document).on('click','#currentPageForm .childpermission',function(){
  var curobj  = $(this); 
  var count = 0;
  curobj.closest('.bg_child_module').find('.childpermission').each(function(){
    if($(this).prop("checked") == true)
      count = 1;
  });
  if(count == 1)
    curobj.closest('.bg_child_module').find('.childmodule').prop('checked', true);
  else
    curobj.closest('.bg_child_module').find('.childmodule').prop('checked', false);
  var counts  = 0;
  curobj.closest('.bg_module').find('.childpermission').each(function(){
    if($(this).prop("checked") == true)
      counts = 1;
  });  
  if(counts == 1)
    curobj.closest('.bg_module').find('.parentmodule').prop('checked', true);
  else
    curobj.closest('.bg_module').find('.parentmodule').prop('checked', false);
});
</script>