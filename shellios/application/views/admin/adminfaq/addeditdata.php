<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard" class=""><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="<?php echo correctLink('FaqAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class=""><i class="fa fa-file-text-o"></i>FAQ</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="fa fa-edit"></i><?=$EDITDATA?'Edit':'Add'?> FAQ</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline inr-form">
        <div class="panel-heading row">
          <h3 class="panel-title tab"><?=$EDITDATA?'Edit':'Add'?> FAQ</h3>
          <a href="<?php echo correctLink('FaqAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-default add_btn">Back</a>
        </div>
        <hr class="differ">
        <div class="panel">
          <div class="panel-body row">
            <form id="currentPageForm" name="currentPageForm" class="form-auth-small" method="post" action="">
              <input type="hidden" name="CurrentDataID" id="CurrentDataID" value="<?=$EDITDATA['user_id']?>"/>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <fieldset>
                <legend>&nbsp;</legend>
                <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group <?php if(form_error('title')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">Title<span class="required">*</span></label>
                      <input type="text" name="title" id="title" value="<?php if(set_value('title')): echo set_value('title'); else: echo stripslashes($EDITDATA['questions']);endif; ?>" class="form-control required" placeholder="Title">
                      <?php if(form_error('title')): ?>
                        <span for="title" generated="true" class="help-inline"><?php echo form_error('title'); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group <?php if(form_error('content')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">Description<span class="required">*</span></label>
                      <textarea name="content" id="content" class="form-control required" placeholder="Description"><?php if(set_value('content')): echo set_value('content'); else: echo stripslashes($EDITDATA['answer']);endif; ?></textarea>
                      <?php if(form_error('content')): ?>
                        <span for="content" generated="true" class="help-inline"><?php echo form_error('content'); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              <input type="hidden" name="SaveChanges" id="SaveChanges" value="Submit">
              <button type="submit" class="btn btn-primary btn-lg form-btn">Submit</button>
              <a href="<?php echo correctLink('smallcmsAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-primary btn-lg form-btn">Cancel</a> 
              <span class="tools pull-right"> <span class="btn btn-primary btn-lg btn-block">Note
                  :- <strong><span style="color:#FF0000;">*</span> Indicates
                  Required Fields</strong> </span> 
              </span>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(function(){
    create_editor_for_textarea('content')
  });
</script>