  <div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard"><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="fa fa-file-text-o"></i>FAQ</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline">
        <div class="panel-heading row">
          <h3 class="tab panel-title">FAQ Details</h3>
          <a href="{FULL_SITE_URL}{CURRENT_CLASS}/addeditdata" class="btn btn-default add_btn">Add FAQ</a>
        </div>
        <hr class="differ">
        <form id="Data_Form" name="Data_Form" method="get" action="<?php echo $forAction; ?>">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 search">
              <label>Show
                <select name="showLength" id="showLength" class="form-control input-sm">
                  <option value="2" <?php if($perpage == '2')echo 'selected="selected"'; ?>>2</option>
                  <option value="10" <?php if($perpage == '10')echo 'selected="selected"'; ?>>10</option>
                  <option value="25" <?php if($perpage == '25')echo 'selected="selected"'; ?>>25</option>
                  <option value="50" <?php if($perpage == '50')echo 'selected="selected"'; ?>>50</option>
                  <option value="100" <?php if($perpage == '100')echo 'selected="selected"'; ?>>100</option>
                  <option value="All" <?php if($perpage == 'All')echo 'selected="selected"'; ?>>All</option>
                </select>
                entries
              </label>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 search-ryt">
              <input type="text" name="searchValue" id="searchValue" value="<?php echo $searchValue; ?>" class="form-control input-smt" placeholder="Enter Search Text">
            </div>
          </div>
          <div class="dash">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="6%">Sr.No.</th>
                  <th width="20%">Question</th>
                  <th width="35%">Answer</th>
                  <th width="8%">Status</th>
                  <?php if($edit_data == 'Y'): ?>
                  <th width="10%">--</th>
                  <?php endif; ?>
                </tr>
              </thead>
              <tbody>
                <?php if($ALLDATA <> ""): $i=1; foreach($ALLDATA as $ALLDATAINFO): ?>
                  <tr class="<?php if($i%2 == 0): echo 'odd'; else: echo 'even'; endif; ?> gradeX">
                    <td><?=$i++?></td>
                    <td><?=stripslashes($ALLDATAINFO['questions'])?></td>
                    <td><?=stripslashes($ALLDATAINFO['answer'])?></td>
                    <td><?=showStatus($ALLDATAINFO['status'])?></td>
                    <?php if($edit_data == 'Y'): ?>
                      <td class="center">
                        <div class="btn-group">
                          <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                          <ul class="dropdown-menu">
                            <?php if($edit_data == 'Y'): ?>
                              <li><a href="{FULL_SITE_URL}{CURRENT_CLASS}/addeditdata/<?=$ALLDATAINFO['user_id']?>"><i class="fa fa-edit"></i> Edit Details</a></li>
                              <li class="divider"></li>
                              <?php if($ALLDATAINFO['status'] == 'A'): ?>
                                <li><a href="{FULL_SITE_URL}{CURRENT_CLASS}/changestatus/<?=$ALLDATAINFO['user_id']?>/I"><i class="fa fa-hand-o-down"></i> Inactive</a></li>
                                <li><a href="{FULL_SITE_URL}{CURRENT_CLASS}/deleteData/<?=$ALLDATAINFO['user_id']?>/D"><i class="fa fa-remove"></i> Delete</a></li>
                              <?php elseif($ALLDATAINFO['status'] == 'I'): ?>
                                <li><a href="{FULL_SITE_URL}{CURRENT_CLASS}/changestatus/<?=$ALLDATAINFO['user_id']?>/A"><i class="fa fa-hand-o-up"></i> Active</a></li>
                              <?php elseif($ALLDATAINFO['status'] == 'B'): ?>
                                <li><a href="{FULL_SITE_URL}{CURRENT_CLASS}/changestatus/<?=$ALLDATAINFO['user_id']?>/A"><i class="fa fa-hand-o-up"></i> Active</a></li>
                              <?php elseif($ALLDATAINFO['status'] == 'D'): ?>
                              <li><a href="{FULL_SITE_URL}{CURRENT_CLASS}/changestatus/<?=$ALLDATAINFO['user_id']?>/A"><i class="fa fa-hand-o-up"></i> Active</a></li>
                            <?php endif; endif; ?>
                          </ul>
                        </div>
                      </td>
                    <?php endif; ?>
                  </tr>
                <?php endforeach; else: ?>
                  <tr>
                    <td colspan="5" style="text-align:center;">No Data Available In Table</td>
                  </tr>
                <?php endif; ?>
              </tbody>
            </table>
            <div class="pagi row">
              <div class="col-md-6 col-sm-6 col-xs-6 pagi-txt"><?php echo $noOfContent; ?></div>
              <div class="col-md-6 col-sm-6 col-xs-6 pagi-bar">
                <?=$PAGINATION?>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
var prevSerchValue  = '<?php echo $searchValue; ?>';
</script>