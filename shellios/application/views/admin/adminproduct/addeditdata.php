<script>
$(function() {
  $("#datepicker")
    .datepicker({
      autoclose: true,
      todayHighlight: true
    })
    .datepicker("update", new Date());
});
</script>
<style>
#datepicker {
  width: 180px;
  margin: 0 20px 20px 20px;
}
#datepicker > span:hover {
  cursor: pointer;
}
</style>
<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard" class=""><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="<?php echo correctLink('productAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class=""><i class="lnr lnr-user"></i>Product</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="fa fa-edit"></i><?=$EDITDATA?'Edit':'Add'?> Product</a></li>
    </ol>
  </div>
  
<?php /*?><div id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd">
    <input class="form-control" type="text" readonly />
    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
</div><?php */?>

  
  
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline inr-form">
        <div class="panel-heading row">
          <h3 class="panel-title tab"><?=$EDITDATA?'Edit':'Add'?> Product</h3>
          <a href="<?php echo correctLink('productAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-default add_btn">Back</a>
        </div>
        <hr class="differ">
        <div class="panel">
          <div class="panel-body row">
            <form id="currentPageForm" name="currentPageForm" class="form-auth-small" method="post" action="">
              <input type="hidden" name="CurrentIdForUnique" id="CurrentIdForUnique" value="<?=$EDITDATA['prod_id']?>"/>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <fieldset>
                <legend>Product details</legend>
                <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group <?php if(form_error('prod_brand_name')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">Product Name<span class="required">*</span></label>
                      <input type="text" name="prod_brand_name" id="seller_business_name" value="<?php if(set_value('prod_brand_name')): echo set_value('prod_brand_name'); else: echo stripslashes($EDITDATA['prod_brand_name']);endif; ?>" class="form-control required" placeholder="Model Name">
                      <?php if(form_error('prod_brand_name')): ?>
                        <span for="prod_brand_name" generated="true" class="help-inline"><?php echo form_error('prod_brand_name'); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group <?php if(form_error('prod_sr_no')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">Prod. Sr No<span class="required">*</span></label>
                      <input type="text" name="prod_sr_no" id="prod_sr_no" value="<?php if(set_value('prod_sr_no')): echo set_value('prod_sr_no'); else: echo stripslashes($EDITDATA['prod_sr_no']);endif; ?>" class="form-control required" placeholder="Prod. Sr No">
                      <?php if(form_error('prod_sr_no')): ?>
                        <span for="prod_sr_no" generated="true" class="help-inline"><?php echo form_error('prod_sr_no'); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group <?php if(form_error('prod_color')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">Prod. Color<span class="required">*</span></label>
                      <input type="text" name="prod_color" id="prod_color" value="<?php if(set_value('prod_color')): echo set_value('prod_color'); else: echo stripslashes($EDITDATA['prod_color']);endif; ?>" class="form-control required" placeholder="Prod. Color">
                      <?php if(form_error('prod_color')): ?>
                        <span for="prod_color" generated="true" class="help-inline"><?php echo form_error('prod_color'); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group <?php if(form_error('prod_material')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">Prod. Material<span class="required">*</span></label>
                      <input type="text" name="prod_material" id="prod_material" value="<?php if(set_value('prod_material')): echo set_value('prod_material'); else: echo stripslashes($EDITDATA['prod_material']);endif; ?>" class="form-control required" placeholder="Prod. Material">
                      <?php if(form_error('prod_material')): ?>
                        <span for="prod_material" generated="true" class="help-inline"><?php echo form_error('prod_material'); ?></span>
                      <?php endif; ?>
                    </div>
                  </div>
                  
				  <?php /*?><div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group <?php if(form_error('prod_warranty')): ?>error<?php endif; ?>">
                      <label class="fancy-checkbox form-headings">Prod. Warranty<span class="required">*</span></label>
                      <input type="text" name="prod_warranty" id="prod_warranty" value="<?php if(set_value('prod_warranty')): echo set_value('prod_warranty'); else: echo stripslashes($EDITDATA['prod_warranty']);endif; ?>" class="form-control required" placeholder="Prod.warranty" autocomplete="off">
                      <?php if(form_error('')): ?>
                        <span for="prod_warranty" generated="true" class="help-inline"><?php echo form_error('prod_warranty'); ?></span>
                      <?php endif; if($prod_warranty):  ?>
                        <span for="prod_warranty" generated="true" class="help-inline"><?php echo $prod_warranty; ?></span>
                      <?php endif; ?>
                    </div>
                  </div><?php */?>
                  <div class="col-md-4 col-sm-4 col-xs-4">
              <?php if(set_value('prod_warranty')): $prod_warranty	= set_value('prod_warranty'); elseif($EDITDATA['prod_warranty']): $prod_warranty	= $EDITDATA['prod_warranty']; else: $prod_warranty	= ''; endif; ?>
            <div class="form-group">
              <label class="col-lg-3 control-label">Prod.Warranty</label>
              <select name="prod_warranty" id="prod_warranty" class="form-control required">
                <option value="">Select Warranty</option>
              <?php if($WARRANTY <> ""): foreach($WARRANTY as $WARRANTYINFO): ?>
                <option value="<?php echo $WARRANTYINFO; ?>" <?php if($prod_warranty == $WARRANTYINFO): echo 'selected="selected"'; endif;?>><?php echo $WARRANTYINFO; ?></option>
              <?php endforeach; endif; ?>
              </select>
                     <?php if(form_error('prod_warranty')): ?>
                      <label for="prod_warranty" generated="true" class="error"><?php echo form_error('prod_warranty'); ?></label>
                      <?php endif; ?>
                  </div>
                  </div>
                  <?php /*?><?php if (strpos($EDITDATA['prod_image'],'puros-white.png')) {
    echo 'Car exists.';
} else {
    echo 'No cars.';
}
				  print_r($EDITDATA['prod_image']); die;?><?php */?>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group <?php if(form_error('prod_image')): ?>error<?php endif; ?>">
                    <label class="fancy-checkbox form-headings">Image</label><?php //print_r($carbrandimage); die; ?>
                    <?php if(set_value('prod_image')): $carbrandimage = set_value('prod_image'); elseif($EDITDATA['prod_image']): $carbrandimage = stripslashes($EDITDATA['prod_image']); else: $carbrandimage = ''; endif; ?>
                    <img border="0" alt="" src="{ASSET_ADMIN_URL}images/browse-white.png" id="prodAttImageUpload" class="img-responsive" style="cursor:pointer;">
                    <input type="etxt" name="prod_image" id="prodAttImageInput" value="<?php echo $carbrandimage; ?>" class="form-control " style="border:none;width:0px;height:0px;margin-top: -14px;" />
                    <?php if(form_error('prod_image')): ?>
                      <span for="prod_image" generated="true" class="help-inline"><?php echo form_error('prod_image'); ?></span>
                    <?php endif; ?>
                    <span id="prodAttImageDiv" style="margin-top:5px;">
                      <?php if($carbrandimage): ?>
                       <img border="0" alt="" src="<?php echo $carbrandimage; ?>" class="img-responsive" width="100">&nbsp;
                       <?php if(strpos($carbrandimage,'puros-white.png') == false): ?>
                       <a class="spancross" onclick="prodAttImageDelete('<?php echo $carbrandimage; ?>');" href="javascript:void(0);"> <img border="0" alt="" src="{ASSET_ADMIN_URL}images/cross.png"></a>
                       <?php endif; ?>
                      <?php endif; ?>
                    </span>
                  </div>
                </div>
                </div>
				<?php if($EDITDATA <> ""):?>
                <div class="col-md-12 col-sm-12 col-xs-12 form-space">
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <label class="fancy-checkbox form-headings">Purchase Date: </label>
                    <div id="datepicker" class="form-group input-group date" data-date-format="yyyy-mm-dd">
                        <input type="text" name="purchasedate" id="purchasedate" class="form-control" type="text" value="<?php if(set_value('purchasedate')): echo set_value('purchasedate'); else: echo stripslashes($EDITDATA['purchagedate']);endif; ?>" placeholder="Purchase Date" readonly />
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <?php if(form_error('purchasedate')): ?>
                        <span for="purchagedate" generated="true" class="help-inline"><?php echo form_error('purchasedate'); ?></span>
                        <?php endif; ?>
                    </div>
                  </div>
              </div>
              <?php endif;?>
              
				<?php /*?><div class="col-md-12 col-sm-12 col-xs-12 form-space">
                <div class="col-md-4 col-sm-4 col-xs-4">
              <?php $allrating = array(1=>'sold',2=>'available'); ?>
            <div class="form-group">
              <label class="col-lg-3 control-label"> Prod.Status.</label>
              <select name="productstatus" id="productstatus" class="form-control required">
                <option value="">Select Status</option>
              <?php foreach($allrating as $rate){ print_r($allrating); ie;?>
                <option value="<?php echo $rate; ?>" <?php if($rate == $EDITDATA['productstatus']): echo "selected"; endif;?>><?php echo $rate; ?></option>
              <?php } ?>
              </select>
                     <?php if(form_error('productstatus')): ?>
                      <label for="productstatus" generated="true" class="error"><?php echo form_error('productstatus'); ?></label>
                      <?php endif; ?>
                  </div>
                  </div>
              </div><?php */?>
             
              </fieldset>
              <input type="hidden" name="SaveChanges" id="SaveChanges" value="Submit">
              <button type="submit" class="btn btn-primary btn-lg form-btn">Submit</button>
              <a href="<?php echo correctLink('productAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-primary btn-lg form-btn">Cancel</a> 
              <span class="tools pull-right"> <span class="btn btn-primary btn-lg btn-block">Note
                  :- <strong><span style="color:#FF0000;">*</span> Indicates
                  Required Fields</strong> </span> 
              </span>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
  UploadImage('0');
});
</script>


<div id="prodAttImageUploadModel" class="modal" role="dialog">
  <div class="modal-dialog modal-sm"> 
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4>
              <center>Position And Size Your Photo</center>
            </h4>
       </div>
      <div class="modal-body">
        <div class="profileimg-plug col-md-12 col-sm-12 col-xs-12">
                <div class="image-editor">
                  <div class="file-input-wrapper">
                    <label for="prodAtt-upload-profile-file" class="file-input-button">Choose Image</label>
                    <input type="file" name="image" id="prodAtt-upload-profile-file" class="cropit-image-input"><br />
                    <span style="color:#FF0000; font-size:12px;" id="prodAttBlinker">Image Must Be Min Width: 400px
                          And Min Height: 400px.</span>
                  </div>
                  <div class="cropit-preview"></div>
                  <div class="image-size-label"> Resize Image </div>
                  <div class="rotat-btn">
                    <input type="range" class="cropit-image-zoom-input"><br />
                    <button class="rotate-ccw"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                    <button class="rotate-cw"><i class="fa fa-repeat"></i></button>
                  </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default prodAttImageClosedButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-default prodAttImageSaveButton">Save</button>
            <input type="hidden" name="prodAttImageLoading" id="prodAttImageLoading" value="NO" />
        </div>
    </div>
  </div>
</div>
<script>
  $(function(){
    $('#prodAttImageUploadModel .image-editor').cropit();
    $('#prodAttImageUploadModel .rotate-cw').click(function() {
      $('#prodAttImageUploadModel .image-editor').cropit('rotateCW');
    });
    $('#prodAttImageUploadModel .rotate-ccw').click(function() {
      $('#prodAttImageUploadModel .image-editor').cropit('rotateCCW');
    });
  });
  $(document).on('click','#prodAttImageUpload',function(){
    $('#prodAttImageUploadModel').modal({backdrop:'static', show: true, keyword: false});
    $('#prodAttImageUploadModel .cropit-preview img').attr('src','');
    $('#prodAttImageUploadModel .modal-footer.plucgin-clodebtn .prodAttImageClosedButton').html('Close');
    $('#prodAttImageUploadModel .modal-footer.plucgin-clodebtn .prodAttImageSaveButton').html('Save');
    $('#prodAttImageUploadModel #prodAttImageLoading').val('NO');
    $(".modal-footer .prodAttImageClosedButton").removeAttr('disabled');
    $(".modal-footer .prodAttImageSaveButton").removeAttr('disabled');
  });
  $(document).on('click','.prodAttImageSaveButton',function() {  
    var imageData = $('#prodAttImageUploadModel .image-editor').cropit('export'); 
    if($('#prodAttImageUploadModel #prodAttImageLoading').val() == 'NO')
    {
      $('#prodAttImageUploadModel .prodAttImageSaveButton').html('Saving..');
      $('#prodAttImageUploadModel #prodAttImageLoading').val('YES');
      $(".modal-footer .prodAttImageClosedButton").attr("disabled", "disabled");
      $(".modal-footer .prodAttImageSaveButton").attr("disabled", "disabled");
      $.ajax({
          type: 'post',
           url: FULLSITEURL+CURRENTCLASS+'/prodAttImageUpload',
          data: {imageData:imageData},
        success: function(rdata){ 
          if(rdata != 'UPLODEERROR') {
            $('#prodAttImageInput').val(rdata);
            $('#prodAttImageDiv').html('<img src="'+rdata+'" border="0" alt="" width="100" /> <a href="javascript:void(0);" onClick="prodAttImageDelete(\''+rdata+'\');"> <img src="{ASSET_ADMIN_URL}images/cross.png" border="0" alt="" /> </a>');
            $('#prodAttImageUploadModel').modal('hide');
            $('#prodAttImageUploadModel #prodAttImageLoading').val('NO');
            return false;
          }
        }
      });
    }
  });

  //////////////////////////////////   Image delete Through Ajax
  function prodAttImageDelete(imageName)
  {
      if(confirm("Sure to delete?"))
      {
          $.ajax({
           type: 'post',
                url: FULLSITEURL+CURRENTCLASS+'/prodAttImageDelete',
         data: {csrf_api_key:csrf_api_value,imageName:imageName},
                success: function(response) { 
            document.getElementById('prodAttImageInput').value = '';
          document.getElementById('prodAttImageDiv').innerHTML ='';
                }
              });
      }
  }
</script>
<script>
  var prodAtt_blink_speed = 500; var prodAtt = setInterval(function () { var prodAttsele = document.getElementById('prodAttBlinker'); prodAttsele.style.visibility = (prodAttsele.style.visibility == 'hidden' ? '' : 'hidden'); }, prodAtt_blink_speed);
</script>

   