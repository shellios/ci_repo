<script src="{ASSET_ADMIN_URL}js/dragndrop/jquery.min.js"></script> 
<script src="{ASSET_ADMIN_URL}js/dragndrop/jquery.ui.custom.js"></script>
<div class="main">
  <div class="bready">
    <ol class="breadcrumb">
      <li><a href="{FULL_SITE_URL}dashboard"><i class="lnr lnr-home"></i>Dashboard</a></li>
      <li><a href="javascript:void(0);" class="active"><i class="fa fa-car"></i>Car Brand</a></li>
    </ol>
  </div>
  <div class="main-content">
    <div class="container-fluid"> 
      <div class="panel panel-headline">
        <div class="panel-heading row">
          <h3 class="tab panel-title">Manage Car Brand Order</h3>
          <a href="<?php echo correctLink('carbrandAdminData','{FULL_SITE_URL}{CURRENT_CLASS}/index'); ?>" class="btn btn-default add_btn">back</a>
        </div>
        <hr class="differ">
        <form id="Data_Form" name="Data_Form" method="get" action="<?php echo $forAction; ?>">
          <div class="dash">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="10%">Sr.No.</th>
                  <th>Car Brand Name</th>
                  <th>Image</th>
                </tr>
              </thead>
              <tbody class="sortablerecord">
                <?php if($ALLDATA <> ""): $i=1; foreach($ALLDATA as $ALLDATAINFO): ?>
                  <tr class="<?php if($i%2 == 0): echo 'odd'; else: echo 'even'; endif; ?> gradeX" id="item-<?php echo $ALLDATAINFO['car_brand_id'];?>">
                    <td><?=$i++?></td>
                    <td><?=stripslashes($ALLDATAINFO['car_brand_name'])?></td>
                    <td><img src="<?=stripslashes($ALLDATAINFO['car_brand_image'])?>" alt="Car brand image" width="60"></td>
                  </tr>
                <?php endforeach; else: ?>
                  <tr>
                    <td colspan="5" style="text-align:center;">No Data Available In Table</td>
                  </tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){  
   $('.sortablerecord').sortable({
      axis: 'y',
      update: function (event, ui) {
          var data = $(this).sortable('serialize')         
          $.ajax({
            type: 'post',
             url: FULLSITEURL+CURRENTCLASS+'/changeImageOrder',
             data: data,
             success: function(responce){ 
                console.log(responce);
               if(responce.trim()=='success')
                {  
                  var i =1;   
                  $('.sortablerecord tr').each(function(){
                    $(this).children('td').first().text(i);
                    i++;
                  });               
                  alertMessageModelPopup('Images Order Has Been Updated Successfully.','Success');       
                }
            }
          }); 
       }
    });
  });
</script>