<!DOCTYPE html>
<html lang="eng">
	<head>
		<title>{title}</title>
		<meta http-equiv="Content-Type" content="width=device-width, initial-scale=1">
		<meta name="description" content="{description}">
		<meta name="keywords" content="{keyword}">
		<meta name="viewport" content="width=device-width">
		<meta name="theme-color" content="#000" />
		<meta name="apple-mobile-web-app-status-bar-style" content="#000">
		{head}
	</head>
	<body data-spy="scroll" data-target=".navbar" data-offset="50">
  		{content}
		{footer_js}
	</body>
</html>