<script src="{ASSET_ADMIN_URL}js/klorofil-common.js"></script> 

<script src="{ASSET_ADMIN_URL}js/jquery.validate.js"></script>
<script src="{ASSET_ADMIN_URL}js/manoj.js"></script>

<!--<script src="{ASSET_ADMIN_URL}js/bootstrap-datepicker.js"></script> -->
<script src="{ASSET_ADMIN_URL}js/jquery.cropit.js"></script>
<script src="{ASSET_ADMIN_URL}js/ajaxupload.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js" type="text/javascript"></script>

<script src="{ASSET_ADMIN_URL}js/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript">
function create_editor_for_textarea(textareaid)
{	
	if (document.getElementById(textareaid)) {
		// Replace the <textarea id="Description"> with a CKEditor
		// instance, using default configuration.
		CKEDITOR.replace(textareaid, {filebrowserBrowseUrl :'{ASSET_ADMIN_URL}js/ckeditor/filemanager/browser/default/browser.html?Connector={ASSET_ADMIN_URL}js/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserImageBrowseUrl : '{ASSET_ADMIN_URL}js/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector={ASSET_ADMIN_URL}js/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserFlashBrowseUrl :'{ASSET_ADMIN_URL}js/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector={ASSET_ADMIN_URL}js/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserUploadUrl :'{ASSET_ADMIN_URL}js/ckeditor/filemanager/connectors/php/upload.php?Type=File',
		filebrowserImageUploadUrl : '{ASSET_ADMIN_URL}js/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
		filebrowserFlashUploadUrl : '{ASSET_ADMIN_URL}js/ckeditor/filemanager/connectors/php/upload.php?Type=Flash',
		allowedContent:true,
		height: '300px',
		toolbar: [
				{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
				[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
				{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
			]
		});	
	}
};
</script>