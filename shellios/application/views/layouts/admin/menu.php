<div id="sidebar-nav" class="sidebar">
  <div class="sidebar-scroll">
    <nav>
      <ul class="nav">
        <li><a href="{FULL_SITE_URL}dashboard" <?php if(strtolower($activeMenu) == 'dashboard'):?> class="active"<?php endif; ?>><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
        <?php if(sessionData('SHELLIOS_ADMIN_TYPE') == 'Super admin'): ?>         
          <li> <a href="#subPages" data-toggle="collapse" class="<?php if($activeMenu == 'subadminlist'):?>active<?php else: ?>collapsed<?php endif; ?>"><i class="lnr lnr-user"></i> <span>Sub-admin</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
            <div id="subPages" class="collapse <?php if($activeMenu == 'subadminlist'):?>in<?php endif; ?>">
              <ul class="nav">
                <li><a href="{FULL_SITE_URL}departmentlist/index" <?php if(strtolower($activeSubMenu) == 'departmentlist'):?>class="active"<?php endif; ?>>Department list</a></li>
                <li><a href="{FULL_SITE_URL}subadminlist/index" <?php if(strtolower($activeSubMenu) == 'subadminlist'):?>class="active"<?php endif; ?>>Sub-admin list</a></li>
              </ul>
            </div>
          </li>
        <?php endif; ?>
        <?php 
          $APURL  = $this->adminauth_model->getMenuModule();
          if($APURL <> ""): foreach($APURL as $APURLinfo):
          $mchilddata = $this->adminauth_model->getMenuChildModule($APURLinfo['encrypt_id']);
          //echo "<pre>"; print_r($mchilddata); die;
         if($mchilddata): ?>
            <li> <a href="#<?php echo stripslashes($APURLinfo['module_name']); ?>" data-toggle="collapse" class="<?php if($activeMenu == stripslashes($APURLinfo['module_name'])):?>active<?php else: ?>collapsed<?php endif; ?>"><?php echo stripslashes($APURLinfo['module_icone']); ?> <span><?php echo stripslashes($APURLinfo['module_display_name']); ?></span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
              <div id="<?php echo stripslashes($APURLinfo['module_name']); ?>" class="collapse <?php if($activeMenu == stripslashes($APURLinfo['module_name'])):?>in<?php endif; ?>">
                <ul class="nav">
                  <?php foreach($mchilddata as $MCDinfo):  ?>
                    <li><a href="{FULL_SITE_URL}<?php echo stripslashes($MCDinfo['module_name']); ?>/index" <?php if(strtolower($activeSubMenu) == strtolower(stripslashes($MCDinfo['module_name']))):?>class="active"<?php endif; ?>><?php echo stripslashes($MCDinfo['module_display_name']); ?></a></li>
                  <?php endforeach; ?>
                </ul>
              </div>
            </li>
          <?php else: ?>
            <li><a href="{FULL_SITE_URL}<?php echo stripslashes($APURLinfo['module_name']); ?>/index" <?php if($activeMenu == stripslashes($APURLinfo['module_name'])):?>class="active"<?php endif; ?>><?php echo stripslashes($APURLinfo['module_icone']); ?> <span><?php echo stripslashes($APURLinfo['module_display_name']); ?></span></a></li>
          <?php endif; ?>
        <?php endforeach; endif; ?>
      </ul>
    </nav>
  </div>
</div>