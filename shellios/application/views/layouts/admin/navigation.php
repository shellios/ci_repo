<nav class="navbar navbar-default navbar-fixed-top">
  <div class="brand"> <a href="{FULL_SITE_URL}dashboard"><img src="{ASSET_ADMIN_URL}images/logo.png" alt="Klorofil Logo" class="img-responsive logo"></a> </div>
  <div class="container-fluid">
    <div class="navbar-btn">
      <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
    </div>
    <div id="navbar-menu">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo sessionData('SHELLIOS_ADMIN_LOGO'); ?>" class="img-circle" alt="Avatar"> <span><?php echo sessionData('SHELLIOS_ADMIN_CURRENT_DISPLAY_NAME'); ?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
          <ul class="dropdown-menu">
            <li><a href="{FULL_SITE_URL}profile"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
            <li><a href="{BASE_URL}admin/logout"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>