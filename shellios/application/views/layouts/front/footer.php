<footer>
	<div class="foot_top" id="foot_top">
    	<div class="container">
        	<div class="col-md-12 col-sm-12 col-xs-12">
            	<div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="f_tleft f_th">
                    	<h3>Newsletter</h3>
                        <form name="newsletterForm" id="newsletterForm" method="post" action="">
                            <div class="news_letter">
                                <button type="submit" class="arrow_btn"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                            	<input type="text" name="newsletter_email" id="newsletter_email" class="required" placeholder="Email Address" />
                            </div>
                        </form>
                        <?php $FsmallCMS  = $this->front_model->getSmallCMS('1000000001'); ?>
                        <h3><?php echo stripslashes($FsmallCMS['title']); ?></h3>
                        <?php echo stripslashes($FsmallCMS['content']); ?>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                	<div class="f_th">
                    	<h3 class="ft_tog">Categories</h3>
                        <ul class="ft-open">
                            <?php $FcateMenu  = $this->front_model->getProductCategory(); 
                            if($FcateMenu <> ""): foreach($FcateMenu as $FcateMenuI): ?>
                            	<li><a href="{FRONT_SITE_URL}product-listing/<?php echo $FcateMenuI['prod_cate_slug']; ?>"><?php echo stripslashes($FcateMenuI['prod_cate_name']); ?></a></li>
                            <?php endforeach; endif; ?>
                            <li><a href="javascript:void(0);">Blog</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                	<div class="f_th">
                    	<h3 class="ft_tog">Help</h3>
                        <ul class="ft-open">
                        	<li><a href="{FRONT_SITE_URL}cancellations-and-refunds">Cancellations and Refunds</a></li>
                            <li><a href="{FRONT_SITE_URL}disclaimer">Disclaimer</a></li>
                            <li><a href="{FRONT_SITE_URL}terms-of-use">Terms of Use</a></li>
                            <li><a href="{FRONT_SITE_URL}about-us">About Us</a></li>
                            <li><a href="{FRONT_SITE_URL}contact-us">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                	<div class="f_th">
                    	<h3 class="ft_tog">My Account</h3>
                        <ul class="ft-open">
                            <?php if(sessionData('SHELLIOS_USER_ID')): ?>
                            	<li><a href="{FRONT_SITE_URL}user/my-account">My Profile</a></li>
                                <li><a href="{FRONT_SITE_URL}user/my-order">Order History</a></li>
                                <li><a href="{FRONT_SITE_URL}user/my-wishlist">Wishlist</a></li>
                            <?php else: ?>
                                <li><a href="javascript:void(0);">My Profile</a></li>
                                <li><a href="javascript:void(0);">Order History</a></li>
                                <li><a href="javascript:void(0);">Wishlist</a></li> 
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="f_last f_th">
                    	<h3><a href="{FRONT_SITE_URL}become-a-seller"><img src="{ASSET_FRONT_URL}image/foot_img.png" class="img-responsive" /></a></h3>
                        <nav class="social">
                        	<small>Get Social</small>
                            <a href="https://www.instagram.com/car_tamaam/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/cartamaam/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/CarTamaam/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="javascript:void(0);" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="foot_bot">
    	<p>Copyright &copy; 2018. All Rights Reserved By CarTamaam</p>
    </div>
</footer>
<div id="alertMessageModal" class="modal" role="dialog" style="display: none; margin-top: 174.333px;">
    <div class="modal-dialog modal-md"> 
        <div class="modal-content">
            <div class="modal-body">
                <div class=" msg_pop alert alert-dismissible pop-up alert-success" role="alert">Loading...</div>
            </div>
        </div>
    </div>
</div>  
<script>
    <?php if($this->session->flashdata('alert_warning')): ?>
        alertMessageModelPopup('<?php echo $this->session->flashdata('alert_warning'); ?>','Warning');
    <?php elseif($this->session->flashdata('alert_error')): ?>
        alertMessageModelPopup('<?php echo $this->session->flashdata('alert_error'); ?>','Error');
    <?php elseif($this->session->flashdata('alert_success')): ?>
        alertMessageModelPopup('<?php echo $this->session->flashdata('alert_success'); ?>','Success');
    <?php endif; ?>
    ///////////         ALERT MESSAGE MODEL         ///////////////////
    function alertMessageModelPopup(message,type,timeout){  
        var wheight     =   (($( window ).height()/3)-50);
        $("#alertMessageModal").modal('show');
        $("#alertMessageModal").css('margin-top',wheight);
        $("#alertMessageModal .modal-body div").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-danger');
        if(type == 'Success'){
            $("#alertMessageModal .modal-body div").addClass('alert-success').html('<i class="fa fa-check-circle"></i> '+message);
        } else if(type == 'Warning') {
            $("#alertMessageModal .modal-body div").addClass('alert-warning').html('<i class="fa fa-info-circle"></i> '+message);
        } else if(type == 'Error') {
            $("#alertMessageModal .modal-body div").addClass('alert-danger').html('<i class="fa fa-times-circle"></i> '+message);
        }
        if(timeout !='NO'){
            setTimeout(AlertMessageModelPopupTimedOut, 5000);
        }
    }
    function AlertMessageModelPopupTimedOut() { 
        $("#alertMessageModal").modal('hide');
    }
</script>
<script>
    $(document).on('click','.add-to-wishlist',function(){  
        var curObj          =   $(this); 
        if(USERID) {
            var productId   =   curObj.attr('data'); 
            var userId      =   window.btoa(USERID);
            $.ajax({
                type: 'post',
                 url: FRONTSITEURL+'user/addtowishlist',
                data: {userId:userId,productId:productId},
             success: function(response){ 
                    if(response.success == 0) {
                        alertMessageModelPopup(response.message,'Error');
                    } else if(response.success == 1) {
                        alertMessageModelPopup(response.message,'Success');
                        curObj.addClass('active');
                    } else if(response.success == 2) {
                        alertMessageModelPopup(response.message,'Success');
                        curObj.removeClass('active');
                    }
                }
            });
        } else { 
            var productId   =   'ADDTOWISHLIST'+curObj.attr('data'); 
            var message     =   '<span>Please Login to save this product as your wishlist.&nbsp;&nbsp;<a href="<?php echo $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'); ?>user/login/<?php echo base64_encode(uri_string()?uri_string():'home'); ?>PRODUCTID'+productId+'" class="btn btn-default">Login</a></span>';
            alertMessageModelPopup(message,'Warning');
        }
    });
</script>
<script>
    $(document).on('click','.add-to-mycart',function(){
        var curObj          =   $(this); 
        var productId       =   curObj.attr('data'); 
        var userId          =   window.btoa(USERID);
        var cookieId        =   window.btoa(readCookie('currentCartCookie'));
        $.ajax({
            type: 'post',
             url: FRONTSITEURL+'user/addtomycart',
            data: {cookieId:cookieId,userId:userId,productId:productId},
         success: function(response){ 
                if(response.success == 0) {
                    alertMessageModelPopup(response.message,'Error');
                } else if(response.success == 1) {
                    alertMessageModelPopup(response.message,'Success');
                    $('#header-cart-count').find('span').remove();
                    $('#header-cart-count').append(response.result);
                } else if(response.success == 2) {
                    alertMessageModelPopup(response.message,'Warning');
                    $('#header-cart-count').find('span').remove();
                    $('#header-cart-count').append(response.result);
                }
            }
        });
    });

    $(function(){
        getHeaderCartCount();
    });

    function getHeaderCartCount(){
        var userId          =   window.btoa(USERID);
        var cookieId        =   window.btoa(readCookie('currentCartCookie'));
        $.ajax({
            type: 'post',
             url: FRONTSITEURL+'user/getfrommycart',
            data: {cookieId:cookieId,userId:userId},
         success: function(response){  
                if(response.success == 0) {
                    $('#header-cart-count').find('span').remove();
                } else if(response.success == 1) { 
                    $('#header-cart-count').find('span').remove();
                    $('#header-cart-count').append(response.result);
                }
            }
        });
    }
</script>
