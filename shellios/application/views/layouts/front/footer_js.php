<script src="{ASSET_FRONT_URL}js/jquery.validate.js"></script>
<script src="{ASSET_FRONT_URL}js/manoj.js"></script>

<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js" type="text/javascript"></script> 
<script type="text/javascript"> 
	$(".search_div").click(function(event){
	  	event.stopPropagation();
	});   
</script> 
<script>
	$(document).ready(function() {
	  	$(".ft_tog").click(function() {
	     	$(this).parent().children(".ft-open").slideToggle("active");
		});
	});
</script>
<script>
	$(function(){
	  	new WOW().init();
	});
</script> 
<script>
	$(document).ready(function() {
		$(".menu-toggle").click(function() {
			$(".menu-toggle").toggleClass("active");
			$(".modal").toggleClass("modal-active");
			$("nav").toggleClass("nav-active");
		});
	});
</script> 
<script>
	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();
	    if (scroll >= 10) {
	        $("header").addClass("fixed");
			$(".top_header .collapse ul li").removeClass("open") ,100
	    } else {
			$("header").removeClass("fixed");
	    }
	});
</script>
<script>
	$(function(){
		$('.carousel-list').flickity({
		  	cellAlign: 'left',
		  	contain: true,
		  	groupCells: true,
		  	pageDots: false
		});
	});
</script>