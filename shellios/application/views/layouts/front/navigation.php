<header class="top_header">
  <nav class="navbar navbar-inverse" id="#nav">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="{FRONT_SITE_URL}"><img src="{ASSET_FRONT_URL}image/logo.png" class="img-responsive" /></a> 
      </div>
      <div>
        <div class="collapse navbar-collapse navbar-right" id="myNavbar">
          <ul class="nav navbar-nav">
            <?php $HcateMenu  = $this->front_model->getProductCategory(); 
            if($HcateMenu <> ""): foreach($HcateMenu as $HcateMenuI): ?>
              <li><a href="{FRONT_SITE_URL}product-listing/<?php echo $HcateMenuI['prod_cate_slug']; ?>" title="<?php echo stripslashes($HcateMenuI['prod_cate_short_name']); ?>"><?php echo stripslashes($HcateMenuI['prod_cate_short_name']); ?></a></li>
            <?php endforeach; endif; ?>
            <li class="line_link bor_l">
              <?php /*?><a class="" href="javascript:void(0);" title="Blog" >Blog</a><?php */?>
              <a class="" href="{FRONT_SITE_URL}blog" title="Blog" target="_blank">Blog</a>
            </li>
            <li class="line_link search_box">
              <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" title="Search" id="togglesearch">
                <i class="fa fa-search" aria-hidden="true"></i>
              </a>
              <ul class="dropdown-menu link_drop search_div">
                <li>
                  <form name="searchForm" id="searchForm" method="get" action="{FRONT_SITE_URL}product/search">
                    <div class="input-group">
                      <input type="text" name="searchText" id="searchText" value="<?php echo $searchText; ?>" class="serch_text" placeholder="Search here"/>
                      <button class="input-group-addon"><i class="fa fa-search"></i></button>
                    </div>
                  </form>
                </li>
              </ul>
            </li>
            <?php if(sessionData('SHELLIOS_USER_ID')): ?>
              <li class="line_link dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" title="<?php echo sessionData('SHELLIOS_USER_NAME'); ?>">
                  <i class="fa fa-user" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu link_drop">
                  <li><a href="{FRONT_SITE_URL}user/my-account"><i class="fa fa-user"></i> Profile</a></li>
                  <li><a href="{FRONT_SITE_URL}user/my-order"><i class="fa fa-shopping-cart"></i> Orders</a></li>
                  <li><a href="{FRONT_SITE_URL}user/logout"><i class="fa fa-lock"></i> Logout</a></li>
                </ul>
              </li>
            <?php else: ?>
              <li class="line_link">
                <a href="{FRONT_SITE_URL}user/login" title="Login"><i class="fa fa-user" aria-hidden="true"></i></a>
              </li>  
            <?php endif; ?>
            <li class="line_link">
              <a class="" href="{FRONT_SITE_URL}user/cart" title="Cart" id="header-cart-count">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                <?php /* ?><span class="cart_item">3</span><?php */ ?>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
</header>