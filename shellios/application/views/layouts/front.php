<!DOCTYPE html>
<html lang="eng">
	<head>
		<title>{title}</title>
		<meta http-equiv="Content-Type" content="width=device-width, initial-scale=1">
		<meta name="description" content="{description}">
		<meta name="keywords" content="{keyword}">
		<meta name="viewport" content="width=device-width">
		<meta name="theme-color" content="#313b63" />
		<meta name="apple-mobile-web-app-status-bar-style" content="#313b63">
		{head}
	</head>
	<body data-spy="scroll" data-target=".navbar" data-offset="50">
		{navigation}
		<div class="inner_wraper"> 
  			{content}
		</div>
		{footer}
		{footer_js}
	</body>
</html>