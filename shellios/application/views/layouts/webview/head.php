<link rel="stylesheet" href="{ASSET_FRONT_URL}css/bootstrap.min.css">
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<link rel="stylesheet" href="{ASSET_FRONT_URL}css/font-awesome.css">
<link rel="stylesheet" href="{ASSET_FRONT_URL}css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
<link rel="stylesheet" href="{ASSET_FRONT_URL}css/main.css">
<link rel="shortcut icon" href="{ASSET_FRONT_URL}image/fav.png">
<script type="text/ecmascript">
var BASEURL 			=	'{BASE_URL}';
var FRONTURL 			=	'{FRONT_URL}';
var ADMINURL 			=	'{ADMIN_URL}';
var SELLERURL 			=	'{SELLER_URL}';
var FRONTSITEURL		=	'{FRONT_SITE_URL}';
var FULLSITEURL 		=	'{FULL_SITE_URL}';
var SELLERSITEURL 		=	'{SELLER_SITE_URL}';
var ASSETURL 			=	'{ASSET_URL}';
var ASSETFRONTURL		=	'{ASSET_FRONT_URL}';	
var ASSETADMINURL 		=	'{ASSET_ADMIN_URL}';
var ASSETSELLERURL 		=	'{ASSET_SELLER_URL}';
var CURRENTCLASS 		=	'{CURRENT_CLASS}';
var CURRENTMETHOD 		=	'{CURRENT_METHOD}';
var csrf_api_key		=	'<?php echo $this->security->get_csrf_token_name(); ?>';
var csrf_api_value 		=	'<?php echo $this->security->get_csrf_hash(); ?>'; 
</script>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script> 
<script src="{ASSET_FRONT_URL}js/bootstrap.min.js"></script> 