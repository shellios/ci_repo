<!doctype html>
<html lang="en" class="fullscreen-bg">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<title>{title}</title>
		<meta name="description" content="{description}">
		<meta name="keywords" content="{keyword}">
		{head}
	</head>
	<body>
		<div id="wrapper">
			{navigation}
			{menu}
			{content}	
			{footer}
		</div>
		{footer_js}
	</body>
</html>