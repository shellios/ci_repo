<div id="sidebar-nav" class="sidebar">
  <div class="sidebar-scroll">
    <nav>
      <ul class="nav">
        <li><a href="{SELLER_SITE_URL}dashboard" <?php if(strtolower($activeMenu) == 'dashboard'):?> class="active"<?php endif; ?>><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
        <li> <a href="#productMenu" data-toggle="collapse" class="<?php if($activeMenu == 'product'):?>active<?php else: ?>collapsed<?php endif; ?>"><i class="fa fa-product-hunt" aria-hidden="true"></i> <span>Products</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
          	<div id="productMenu" class="collapse <?php if($activeMenu == 'product'):?>in<?php endif; ?>">
            	<ul class="nav">
                  	<li><a href="{SELLER_SITE_URL}product/index" <?php if(strtolower($activeSubMenu) == 'product'):?>class="active"<?php endif; ?>>Products</a></li>
              	</ul>
          	</div>
        </li>
        <li> <a href="#ordersMenu" data-toggle="collapse" class="<?php if($activeMenu == 'order'):?>active<?php else: ?>collapsed<?php endif; ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Orders</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
            <div id="ordersMenu" class="collapse <?php if($activeMenu == 'order'):?>in<?php endif; ?>">
              <ul class="nav">
                    <li><a href="{SELLER_SITE_URL}orders/index" <?php if(strtolower($activeSubMenu) == 'order'):?>class="active"<?php endif; ?>>Orders</a></li>
                </ul>
            </div>
        </li>
      </ul>
    </nav>
  </div>
</div>