<div class="clearfix"></div>
<footer>
	<div class="container-fluid">
	  	<p class="copyright">&copy; 2018 | All Rights Reserved.</p>
	</div>
</footer>
<div id="alertMessageModal" class="modal" role="dialog">
	<div class="modal-dialog modal-md"> 
		<div class="modal-content">
			<div class="modal-body">
				<div class="alert alert-dismissible pop-up" role="alert">Loading...</div>
			</div>
		</div>
	</div>
</div>
<script>
<?php if($this->session->flashdata('alert_warning')): ?>
	alertMessageModelPopup('<?php echo $this->session->flashdata('alert_warning'); ?>','Warning');
<?php elseif($this->session->flashdata('alert_error')): ?>
	alertMessageModelPopup('<?php echo $this->session->flashdata('alert_error'); ?>','Error');
<?php elseif($this->session->flashdata('alert_success')): ?>
	alertMessageModelPopup('<?php echo $this->session->flashdata('alert_success'); ?>','Success');
<?php endif; ?>
///////////			ALERT MESSAGE MODEL 		///////////////////
function alertMessageModelPopup(message,type){	
	var wheight		=	(($( window ).height()/3)-50);
	$("#alertMessageModal").modal('show');
	$("#alertMessageModal").css('margin-top',wheight);
	$("#alertMessageModal .modal-body div").removeClass('alert-success').removeClass('alert-warning').removeClass('alert-danger');
	if(type == 'Success'){
		$("#alertMessageModal .modal-body div").addClass('alert-success').html('<i class="fa fa-check-circle"></i> '+message);
	} else if(type == 'Warning') {
		$("#alertMessageModal .modal-body div").addClass('alert-warning').html('<i class="fa fa-info-circle"></i> '+message);
	} else if(type == 'Error') {
		$("#alertMessageModal .modal-body div").addClass('alert-danger').html('<i class="fa fa-times-circle"></i> '+message);
	}
	setTimeout(AlertMessageModelPopupTimedOut, 5000);
}
function AlertMessageModelPopupTimedOut() { 
	$("#alertMessageModal").modal('hide');
}
</script>

<!-- Message Modal -->
<div id="myViewDetailsModal" class="modal" role="dialog">
	<div class="modal-dialog modal-md"> 
		<div class="modal-content">
			<div class="modal-header">
			    <button data-dismiss="modal" class="close" type="button">x</button>
			    <h3>Pop up Header</h3>
			 </div>
			<div class="modal-body">
				<p>Loading...</p>
			</div>
		</div>
	</div>
</div>
<!-- Message Modal -->
<div id="myViewSubDetailsModal" class="modal" role="dialog">
	<div class="modal-dialog modal-md"> 
		<div class="modal-content">
			<div class="modal-header">
			    <button data-dismiss="modal" class="close" type="button">x</button>
			    <h3>Pop up Header</h3>
			 </div>
			<div class="modal-body">
				<p>Loading...</p>
			</div>
		</div>
	</div>
</div>
<script>
///////////			VIEW DETAILS MODEL 		///////////////////
$(document).on('click','.table .view-details-data',function(){ 
	var title	=	$(this).attr('title');
	$("#myViewDetailsModal").modal();
	$("#myViewDetailsModal .modal-header h3").html(title);
	var viewid	=	$(this).attr('data-id');
	var modelwidth	=	$(this).attr('data-width');
	if(modelwidth){
		$("#myViewDetailsModal .modal-dialog").css('width',modelwidth);
	}
		
	$.ajax({
		type: 'post',
		 url: SELLERSITEURL+CURRENTCLASS+'/get_view_data',
		data: {csrf_api_key:csrf_api_value,viewid:viewid},
	 success: function(response){
	 		$("#myViewDetailsModal .modal-body").html(response);
	 	}	
	});
});

///////////			VIEW SUB DETAILS MODEL 		///////////////////
$(document).on('click','a.view-sub-details-data',function(){ 
	var title	=	$(this).attr('title');
	$("#myViewSubDetailsModal").modal();
	$("#myViewSubDetailsModal .modal-header h3").html(title);
	var viewid	=	$(this).attr('data-id');
	var modelwidth	=	$(this).attr('data-width');
	if(modelwidth){
		$("#myViewSubDetailsModal .modal-dialog").css('width',modelwidth);
	}
	
	$.ajax({
		type: 'post',
		 url: SELLERSITEURL+CURRENTCLASS+'/get_view_sub_data',
		data: {csrf_api_key:csrf_api_value,viewid:viewid},
	 success: function(response){
	 		$("#myViewSubDetailsModal .modal-body").html(response);
	 	}	
	});
});
</script>
<div id="firstImageUploadModel" class="modal" role="dialog">
	<div class="modal-dialog modal-md"> 
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4>
		          <center>Position And Size Your Photo</center>
		        </h4>
			 </div>
			<div class="modal-body">
				<div class="profileimg-plug col-md-12 col-sm-12 col-xs-12">
		          	<div class="image-editor">
			            <div class="file-input-wrapper">
			              <label for="first-upload-profile-file" class="file-input-button">Choose Image</label>
			              <input type="file" name="image" id="first-upload-profile-file" class="cropit-image-input"><br />
			              <span style="color:#FF0000; font-size:12px;" id="firstBlinker">Image Must Be Min Width: 400px
			                    And Min Height: 400px.</span>
			            </div>
			            <div class="cropit-preview"></div>
			            <div class="image-size-label"> Resize Image </div>
			            <div class="rotat-btn">
			              <input type="range" class="cropit-image-zoom-input"><br />
			              <button class="rotate-ccw"><i class="fa fa-refresh" aria-hidden="true"></i></button>
			              <button class="rotate-cw"><i class="fa fa-repeat"></i></button>
			            </div>
		          	</div>
		        </div>
			</div>
			<div class="modal-footer">
		        <button type="button" class="btn btn-default firstImagClosedButton" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-default firstImageSaveButton">Save</button>
		        <input type="hidden" name="firstImageLoading" id="firstImageLoading" value="NO" />
		    </div>
		</div>
	</div>
</div>
<script>
$(function(){
	$('#firstImageUploadModel .image-editor').cropit();
	
	$('#firstImageUploadModel .rotate-cw').click(function() {
	  $('#firstImageUploadModel .image-editor').cropit('rotateCW');
	});
	
	$('#firstImageUploadModel .rotate-ccw').click(function() {
	  $('#firstImageUploadModel .image-editor').cropit('rotateCCW');
	});
});
$(document).on('click','#firstImageUpload',function(){
	$('#firstImageUploadModel').modal({backdrop:'static', show: true, keyword: false});
	$('#firstImageUploadModel .cropit-preview img').attr('src','');
	$('#firstImageUploadModel .modal-footer.plucgin-clodebtn .firstImagClosedButton').html('Close');
	$('#firstImageUploadModel .modal-footer.plucgin-clodebtn .firstImageSaveButton').html('Save');
	$('#firstImageUploadModel #firstImageLoading').val('NO');
	
	$(".modal-footer .firstImagClosedButton").removeAttr('disabled');
	$(".modal-footer .firstImageSaveButton").removeAttr('disabled');
});
$(document).on('click','.firstImageSaveButton',function() {  
	var imageData = $('#firstImageUploadModel .image-editor').cropit('export'); 
	if($('#firstImageUploadModel #firstImageLoading').val() == 'NO')
	{
		$('#firstImageUploadModel .firstImageSaveButton').html('Saving..');
		$('#firstImageUploadModel #firstImageLoading').val('YES');
		
		$(".modal-footer .firstImagClosedButton").attr("disabled", "disabled");
		$(".modal-footer .firstImageSaveButton").attr("disabled", "disabled");
		
		$.ajax({
			  type: 'post',
			   url: SELLERSITEURL+CURRENTCLASS+'/firstImageUpload',
			  data: {imageData:imageData},
			success: function(rdata){	
				if(rdata != 'UPLODEERROR') {
					$('#firstAvtar').val(rdata);
					$('#firstAvtarImageDiv').html('<img src="'+rdata+'" width="100" border="0" alt="" /> <a href="javascript:void(0);" onClick="firstImageDelete(\''+rdata+'\');"> <img src="{ASSET_SELLER_URL}images/cross.png" border="0" alt="" /> </a>');
					$('#firstImageUploadModel').modal('hide');
					$('#firstImageUploadModel #firstImageLoading').val('NO');
					return false;
				}
			}
		});
	}
});

//////////////////////////////////   Image delete Through Ajax
function firstImageDelete(imageName)
{
    if(confirm("Sure to delete?"))
    {
        $.ajax({
		     type: 'post',
              url: SELLERSITEURL+CURRENTCLASS+'/firstImageDelete',
			 data: {csrf_api_key:csrf_api_value,imageName:imageName},
              success: function(response) { 
			  	document.getElementById('firstAvtar').value = '';
				document.getElementById('firstAvtarImageDiv').innerHTML ='';
              }
            });
    }
}
</script>
<script>
var first_blink_speed = 500; var first = setInterval(function () { var firsele = document.getElementById('firstBlinker'); firsele.style.visibility = (firsele.style.visibility == 'hidden' ? '' : 'hidden'); }, first_blink_speed);
</script>
<div id="secondImageUploadModel" class="modal" role="dialog">
	<div class="modal-dialog modal-md"> 
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4>
		          <center>Position And Size Your Photo</center>
		        </h4>
			 </div>
			<div class="modal-body">
				<div class="profileimg-plug col-md-12 col-sm-12 col-xs-12">
		          	<div class="image-editor">
			            <div class="file-input-wrapper">
			              <label for="second-upload-profile-file" class="file-input-button">Choose Image</label>
			              <input type="file" name="image" id="second-upload-profile-file" class="cropit-image-input"><br />
			              <span style="color:#FF0000; font-size:12px;" id="secondBlinker">Image Must Be Min Width: 400px
			                    And Min Height: 400px.</span>
			            </div>
			            <div class="cropit-preview"></div>
			            <div class="image-size-label"> Resize Image </div>
			            <div class="rotat-btn">
			              <input type="range" class="cropit-image-zoom-input"><br />
			              <button class="rotate-ccw"><i class="fa fa-refresh" aria-hidden="true"></i></button>
			              <button class="rotate-cw"><i class="fa fa-repeat"></i></button>
			            </div>
		          	</div>
		        </div>
			</div>
			<div class="modal-footer plucgin-clodebtn ">
		        <button type="button" class="btn btn-default secondImagClosedButton" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-default secondImageSaveButton">Save</button>
		        <input type="hidden" name="secondImageLoading" id="secondImageLoading" value="NO" />
		    </div>
		</div>
	</div>
</div>
<script>
$(function(){
	$('#secondImageUploadModel .image-editor').cropit();
	
	$('#secondImageUploadModel .rotate-cw').click(function() {
	  $('#secondImageUploadModel .image-editor').cropit('rotateCW');
	});
	
	$('#secondImageUploadModel .rotate-ccw').click(function() {
	  $('#secondImageUploadModel .image-editor').cropit('rotateCCW');
	});
});
$(document).on('click','#secondImageUpload',function(){
	$('#secondImageUploadModel').modal({backdrop:'static', show: true, keyword: false});
	$('#secondImageUploadModel .cropit-preview img').attr('src','');
	$('#secondImageUploadModel .modal-footer.plucgin-clodebtn .secondImagClosedButton').html('Close');
	$('#secondImageUploadModel .modal-footer.plucgin-clodebtn .secondImageSaveButton').html('Save');
	$('#secondImageUploadModel #secondImageLoading').val('NO');
	
	$(".modal-footer .secondImagClosedButton").removeAttr('disabled');
	$(".modal-footer .secondImageSaveButton").removeAttr('disabled');
});
$(document).on('click','.secondImageSaveButton',function() {  
	var imageData = $('#secondImageUploadModel .image-editor').cropit('export'); 
	if($('#secondImageUploadModel #secondImageLoading').val() == 'NO')
	{
		$('#secondImageUploadModel .secondImageSaveButton').html('Saving..');
		$('#secondImageUploadModel #secondImageLoading').val('YES');
		
		$(".modal-footer .secondImagClosedButton").attr("disabled", "disabled");
		$(".modal-footer .secondImageSaveButton").attr("disabled", "disabled");
		
		$.ajax({
			  type: 'post',
			   url: SELLERSITEURL+CURRENTCLASS+'/secondImageUpload',
			  data: {imageData:imageData},
			success: function(rdata){	
				if(rdata != 'UPLODEERROR') {
					$('#secondAvtar').val(rdata);
					$('#secondAvtarImageDiv').html('<img src="'+rdata+'" width="100" border="0" alt="" /> <a href="javascript:void(0);" onClick="secondImageDelete(\''+rdata+'\');"> <img src="{ASSET_SELLER_URL}images/cross.png" border="0" alt="" /> </a>');
					$('#secondImageUploadModel').modal('hide');
					$('#secondImageUploadModel #secondImageLoading').val('NO');
					return false;
				}
			}
		});
	}
});

//////////////////////////////////   Image delete Through Ajax
function secondImageDelete(imageName)
{
    if(confirm("Sure to delete?"))
    {
        $.ajax({
		     type: 'post',
              url: SELLERSITEURL+CURRENTCLASS+'/secondImageDelete',
			 data: {csrf_api_key:csrf_api_value,imageName:imageName},
              success: function(response) { 
			  	document.getElementById('secondAvtar').value = '';
				document.getElementById('secondAvtarImageDiv').innerHTML ='';
              }
            });
    }
}
</script>
<script>
var second_blink_speed = 500; var secondt = setInterval(function () { var secondele = document.getElementById('secondBlinker'); secondele.style.visibility = (secondele.style.visibility == 'hidden' ? '' : 'hidden'); }, second_blink_speed);
</script>