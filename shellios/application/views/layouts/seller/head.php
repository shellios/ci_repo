<link rel="stylesheet" href="{ASSET_SELLER_URL}css/bootstrap.min.css">
<link rel="stylesheet" href="{ASSET_SELLER_URL}css/font-awesome.min.css">
<link rel="stylesheet" href="{ASSET_SELLER_URL}css/style.css">
<link rel="stylesheet" href="{ASSET_SELLER_URL}css/chartist-custom.css">
<link rel="stylesheet" href="{ASSET_SELLER_URL}css/main.css">
<link rel="stylesheet" href="{ASSET_SELLER_URL}css/demo.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
<link rel="apple-touch-icon" sizes="76x76" href="{ASSET_SELLER_URL}images/apple-icon.png">
<link rel="icon" type="image/png" sizes="96x96" href="{ASSET_SELLER_URL}images/favicon.png">
<link rel="stylesheet" href="{ASSET_SELLER_URL}css/manoj.css">
<link rel="stylesheet" href="{ASSET_SELLER_URL}css/datepicker.css">
<script type="text/ecmascript">
var BASEURL 			=	'{BASE_URL}';
var FRONTURL 			=	'{FRONT_URL}';
var ADMINURL 			=	'{ADMIN_URL}';
var SELLERURL 			=	'{SELLER_URL}';
var FRONTSITEURL		=	'{FRONT_SITE_URL}';
var FULLSITEURL 		=	'{FULL_SITE_URL}';
var SELLERSITEURL 		=	'{SELLER_SITE_URL}';
var ASSETURL 			=	'{ASSET_URL}';
var ASSETFRONTURL		=	'{ASSET_FRONT_URL}';	
var ASSETADMINURL 		=	'{ASSET_ADMIN_URL}';
var ASSETSELLERURL 		=	'{ASSET_SELLER_URL}';
var CURRENTCLASS 		=	'{CURRENT_CLASS}';
var CURRENTMETHOD 		=	'{CURRENT_METHOD}';
var csrf_api_key		=	'<?php echo $this->security->get_csrf_token_name(); ?>';
var csrf_api_value 		=	'<?php echo $this->security->get_csrf_hash(); ?>'; 
</script>
<script src="{ASSET_SELLER_URL}js/jquery.min.js"></script> 
<script src="{ASSET_SELLER_URL}js/bootstrap.min.js"></script> 
<script src="{ASSET_SELLER_URL}js/jquery.slimscroll.min.js"></script> 