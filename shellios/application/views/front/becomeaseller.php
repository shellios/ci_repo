<section class="bred_top">
    <div class="container">
      	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
	          	<li class="breadcrumb-item active" aria-current="page">Become a Seller</li>
	        </ol>
      	</nav>
    </div>
</section>
<section class="accoutbase">
    <div class="container">
      	<div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
	        <section class="terms-use row">
	        	<div class="col-md-12 col-sm-12 col-xs-12">
		         	<div class="col-md-6 col-sm-6 col-xs-12">
		            	<div class="form_lbar">
                            <div class="back_h"><a href="{FRONT_SITE_URL}" title="home"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back to home screen</a></div>
                            <div class="fl_text">
                                <img src="{ASSET_FRONT_URL}image/logo_big.png" class="img-responsive" />
                                <div class="form_anim">
                                    <img src="{ASSET_FRONT_URL}image/sign1.png" class="img-responsive" />
                                    <img src="{ASSET_FRONT_URL}image/sign2.png" class="img-responsive" />
                                </div>
                            </div>
                        </div>
		            </div>                 
		            <div class="col-md-6 col-sm-6 col-xs-12">
		                <div class="form_tight">
                        	<h3>Become a Seller</h3>
                            <form name="currentPageForm" id="currentPageForm" action="" method="post" autocomplete="off">
                            	<div class="form-group error <?php if(form_error('seller_name')): ?>error<?php endif; ?>">
                                	<span class="input_decor"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                	<input type="text" name="seller_name" id="seller_name" value="<?php if(set_value('seller_name')): echo set_value('seller_name'); endif; ?>" placeholder="Name" class="form-control required" />
	                                <?php if(form_error('seller_name')): ?>
	                                    <p><?php echo form_error('seller_name'); ?></p>
	                                <?php endif; ?>
                                </div>
                                <div class="form-group <?php if(form_error('seller_email')): ?>error<?php endif; ?>">
                                	<span class="input_decor"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                	<input type="text" name="seller_email" id="seller_email" value="<?php if(set_value('seller_email')): echo set_value('seller_email'); endif; ?>" placeholder="Email" class="form-control required email" />
	                                <?php if(form_error('seller_email')): ?>
	                                    <p><?php echo form_error('seller_email'); ?></p>
	                                <?php endif; ?>
                                </div>
                                <div class="form-group <?php if(form_error('seller_phone')): ?>error<?php endif; ?>">
                                	<span class="input_decor"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                	<input type="text" name="seller_phone" id="seller_phone" value="<?php if(set_value('seller_phone')): echo set_value('seller_phone'); endif; ?>" placeholder="Phone" class="form-control required" />
	                                <?php if(form_error('seller_phone')): ?>
	                                    <p><?php echo form_error('seller_phone'); ?></p>
	                                <?php endif; if($mobileerror):  ?>
	                                    <p><?php echo $mobileerror; ?></p>
	                                <?php endif; ?>
                                </div>
                              	<div class="form-group <?php if(form_error('seller_city')): ?>error<?php endif; ?>">
                                    <span class="input_decor"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                    <input type="text" name="seller_city" id="seller_city" value="<?php if(set_value('seller_city')): echo set_value('seller_city'); endif; ?>" placeholder="City" class="form-control required" />
                                    <?php if(form_error('seller_city')): ?>
                                        <p><?php echo form_error('seller_city'); ?></p>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group <?php if(form_error('seller_pincode')): ?>error<?php endif; ?>">
                                    <span class="input_decor"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                    <input type="text" name="seller_pincode" id="seller_pincode" value="<?php if(set_value('seller_pincode')): echo set_value('seller_pincode'); endif; ?>" placeholder="Pincode" class="form-control required" />
                                    <?php if(form_error('seller_pincode')): ?>
                                        <p><?php echo form_error('seller_pincode'); ?></p>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group hover_btn">
                                	<div class="dub_btn" data-text="Register"></div>
                                	<input type="Submit" name="currentPageFormSubmit" id="currentPageFormSubmit" class="form-control" value="Submit">
                                </div>
                            </form>
                        </div>
		            </div>
	         	</div>
	        </section>
     	</div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        <?php if($formError): ?>
            alertMessageModelPopup('<?php echo $formError; ?>','Error');
        <?php endif; ?>
    });
</script>