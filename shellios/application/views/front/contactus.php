<section class="bred_top">
    <div class="container">
      	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
	          	<li class="breadcrumb-item active" aria-current="page">Contact Us</li>
	        </ol>
      	</nav>
    </div>
</section>
<section class="accoutbase">
    <div class="container">
      	<div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
	        <section class="terms-use row">
	        	<div class="col-md-12 col-sm-12 col-xs-12">
		         	<div class="col-md-6 col-sm-6 col-xs-12">
		            	<h2>Get In Touch</h2>
		                <div class="contact_pg">
                            <?php echo stripslashes($CMSDATA['content']); ?>
		                </div>
		            </div>                 
		            <div class="col-md-6 col-sm-6 col-xs-12">
		                <div class="form_tight">
                        	<h3>Connect with Us</h3>
                            <form name="currentPageForm" id="currentPageForm" action="" method="post" autocomplete="off">
                            	<div class="form-group error <?php if(form_error('contact_name')): ?>error<?php endif; ?>">
                                	<span class="input_decor"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                	<input type="text" name="contact_name" id="contact_name" value="<?php if(set_value('contact_name')): echo set_value('contact_name'); endif; ?>" placeholder="Name" class="form-control required" />
	                                <?php if(form_error('contact_name')): ?>
	                                    <p><?php echo form_error('contact_name'); ?></p>
	                                <?php endif; ?>
                                </div>
                                 <div class="form-group <?php if(form_error('contact_email')): ?>error<?php endif; ?>">
                                	<span class="input_decor"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                	<input type="text" name="contact_email" id="contact_email" value="<?php if(set_value('contact_email')): echo set_value('contact_email'); endif; ?>" placeholder="Email" class="form-control required email" />
	                                <?php if(form_error('contact_email')): ?>
	                                    <p><?php echo form_error('contact_email'); ?></p>
	                                <?php endif; ?>
                                </div>
                                <div class="form-group <?php if(form_error('contact_phone')): ?>error<?php endif; ?>">
                                	<span class="input_decor"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                	<input type="text" name="contact_phone" id="contact_phone" value="<?php if(set_value('contact_phone')): echo set_value('contact_phone'); endif; ?>" placeholder="Phone" class="form-control required" />
	                                <?php if(form_error('contact_phone')): ?>
	                                    <p><?php echo form_error('contact_phone'); ?></p>
	                                <?php endif; if($mobileerror):  ?>
	                                    <p><?php echo $mobileerror; ?></p>
	                                <?php endif; ?>
                                </div>
                              	<div class="form-group <?php if(form_error('contact_reason')): ?>error<?php endif; ?>">
                                	<span class="input_decor"><i class="fa fa-book" aria-hidden="true"></i></span>
                                    <textarea name="contact_reason" id="contact_reason"class="form-control required" placeholder="Subject"><?php if(set_value('contact_reason')): echo set_value('contact_reason'); endif; ?></textarea>
                                    <?php if(form_error('contact_reason')): ?>
	                                    <p><?php echo form_error('contact_reason'); ?></p>
	                                <?php endif; ?>
                                </div>
                                <div class="form-group <?php if(form_error('contact_message')): ?>error<?php endif; ?>">
                                	<span class="input_decor"><i class="fa fa-book" aria-hidden="true"></i></span>
                                    <textarea name="contact_message" id="contact_message"class="form-control required" placeholder="Message"><?php if(set_value('contact_message')): echo set_value('contact_message'); endif; ?></textarea>
                                    <?php if(form_error('contact_message')): ?>
	                                    <p><?php echo form_error('contact_message'); ?></p>
	                                <?php endif; ?>
                                </div>
                                <div class="form-group hover_btn">
                                	<div class="dub_btn" data-text="Register"></div>
                                	<input type="Submit" name="currentPageFormSubmit" id="currentPageFormSubmit" class="form-control" value="Submit">
                                </div>
                            </form>
                        </div>
		            </div>
	         	</div>
	        </section>
     	</div>
    </div>
</section>