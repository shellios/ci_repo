<section class="bred_top">
  <div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Address Book</li>
      </ol>
    </nav>
  </div>
</section>
<section class="accoutbase">
  <div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
      <div class="col-md-3 col-sm-4 col-xs-12 ac-l">
        <div class="account_left">
          <?php $this->load->view('front/include/userleftmenu'); ?>
        </div>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 ac-l">
        <div class="account_dit">
          <h4>Address Book<a href="{FRONT_SITE_URL}user/add-edit-address" class="pull-right ac-hlink">Add Address</a></h4>
          <div class="my_account">
            <div class="my_03">
              <h3 class="acc_h"></h3>
              <div class="addres_stic row">
                <?php if($addressData <> ""): foreach($addressData as $addressInfo): ?>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="add_h">
                      <h2><?php echo stripslashes($addressInfo['add_name'].' ('.$addressInfo['add_type'].')'); ?></h2>
                      <p>
                        <?php echo stripslashes($addressInfo['add_address1'].', '); ?>
                        <?php echo $addressInfo['add_address2']?stripslashes($addressInfo['add_address2'].', '):''; ?>
                        <?php echo $addressInfo['add_city']?stripslashes($addressInfo['add_city'].', '):''; ?>
                        <?php echo $addressInfo['add_state']?stripslashes($addressInfo['add_state'].', '):''; ?>
                        <?php echo $addressInfo['add_pincode']?stripslashes($addressInfo['add_pincode']):''; ?>
                      </p>
                      <a href="{FRONT_SITE_URL}user/add-edit-address/<?php echo $addressInfo['address_id']; ?>"><i class="fa fa-pencil"></i> Edit Address</a>
                    </div>
                  </div>
                <?php endforeach; else: ?>
                 No Data Found
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>