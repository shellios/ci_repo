<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    //console.log('statusChangeCallback');
    //console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {    
      // Logged into your app and Facebook.
      testAPI();
    }// else {
      // The person is not logged into your app or we are unable to tell.
      //document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
    //}
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {   
    /*FB.getLoginStatus(function(response) {  
        statusChangeCallback(response);
    });*/
    
    FB.login(function(response) {
      if (response.authResponse) {
         FB.getLoginStatus(function(response) {  
            statusChangeCallback(response);
        });
      }
    }, {scope: 'public_profile,email'});
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '325612838253875',  //bd3606e54b9467374b07e5ec567ad8f6
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    //FB.getLoginStatus(function(response) {
    //  statusChangeCallback(response);
    //}, true);

  };

  // Load the SDK asynchronously
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    //console.log('Welcome!  Fetching your information.... ');
    //FB.api('/me', 'get', { fields: 'id,first_name,last_name,name,email' }, function(response) { 
    FB.api('/me', 'get', { fields: 'id,first_name,last_name,name,email' }, function(response) { 
      //console.log('Successful login for: ' + response.id);
      //console.log('Successful login for: ' + response.first_name);
      //console.log('Successful login for: ' + response.last_name);
      //console.log('Successful login for: ' + response.name);
      //console.log('Successful login for: ' + response.email); 
      //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.id + ' '+ response.first_name + ' '+ response.last_name + ' '+ response.name + ' '+ response.email;
        //var im = document.getElementById("profileImage").setAttribute("src", "http://graph.facebook.com/" + response.id + "/picture?width=180&height=180");
      if(response.id && response.name && response.email){ 
          saveTestDataToDataBase(response.id,response.first_name,response.last_name,response.name,response.email);
      } else {
        alert('You have not login properly. Please try again later.');
      }
    });
  }

  function saveTestDataToDataBase(id,first_name,last_name,name,email){  
      var fbLoginData    =   "id="+id+"&first_name="+first_name+"&last_name="+last_name+"&name="+name+"&email="+email;
      var xmlhttp;
      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
      } else {    // code for older browsers
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              //console.log(this.responseText);
              window.location =   this.responseText;
          }
      };
      xmlhttp.open("POST",FRONTSITEURL+"user/fbresponsedata",true);
      xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      xmlhttp.send(fbLoginData);
  }
</script>
<script src="https://apis.google.com/js/api:client.js"></script>
<script>
  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '328549323713-h699am03ne2qvfk5cu3encgen8g8b2k6.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  };

  function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
          var profile = googleUser.getBasicProfile();
          console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
          console.log('Name: ' + profile.getName());
          console.log('Image URL: ' + profile.getImageUrl());
          console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
          saveGPlusTestDataToDataBase(profile.getId(),profile.getName(),profile.getImageUrl(),profile.getEmail());
        }, function(error) {
          alert(JSON.stringify(error, undefined, 2));
        });
  }

  function saveGPlusTestDataToDataBase(id,name,image,email){ 
      var fbLoginData    =   "id="+id+"&name="+name+"&image="+image+"&email="+email;
      var xmlhttp;
      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
      } else {    // code for older browsers
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              //console.log(this.responseText);
              window.location =   this.responseText;
          }
      };
      xmlhttp.open("POST",FRONTSITEURL+"user/gplusresponsedata",true);
      xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      xmlhttp.send(fbLoginData);
  }
</script>
<section class="log_sign">
	<div class="container">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="col-md-12 col-sm-12 col-xs-12 form_bg">
            	<div class="col-md-7 col-sm-7 col-xs-12">
                	<div class="form_lbar">
                    	<div class="back_h"><a href="{FRONT_SITE_URL}" title="home"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back to home screen</a></div>
                        <div class="fl_text">
                        	<img src="{ASSET_FRONT_URL}image/logo_big.png" class="img-responsive" />
                            <div class="form_anim">
                            	<img src="{ASSET_FRONT_URL}image/sign1.png" class="img-responsive" />
                                <img src="{ASSET_FRONT_URL}image/sign2.png" class="img-responsive" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                	<div class="form_tight">
                    	<h5>Member Login</h5>
                        <form name="currentPageForm" id="currentPageForm" action="" method="post" autocomplete="off">
                            <div class="form-group">
                            	<span class="input_decor"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                <input type="text" name="user_phone" id="user_phone" value="<?php if(set_value('user_phone')): echo set_value('user_phone'); else: echo get_cookie('ct_username'); endif; ?>" placeholder="Phone Number" class="form-control required number" minlength="10" maxlength="10" />
                                <?php if(form_error('user_phone')): ?>
                                    <p><?php echo form_error('user_phone'); ?></p>
                                <?php endif; if($mobileerror):  ?>
                                    <p><?php echo $mobileerror; ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                            	<span class="input_decor"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                                <input type="password" name="password" id="password" value="<?php if(set_value('password')): echo set_value('password'); else: echo get_cookie('ct_password'); endif; ?>" placeholder="Password" class="form-control required" />
                                <?php if(form_error('password')): ?>
                                    <p><?php echo form_error('password'); ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                            	<label for="remember_me" class="inline_check">
                                	<input type="checkbox" name="remember_me" id="remember_me" value="YES" class="check_b" <?php if(get_cookie('ct_remember_me') == 'YES') echo 'checked="checked"';?> />
                                    <span class="checkmark"></span>
                                    Remember me
                                </label>
                                <label for="terms" class="inline_check">
                                    <input type="checkbox" name="terms" id="terms" value="YES" class="check_b" checked="checked" /> <span class="checkmark"></span>
                                    I agree to the terms and conditions
                                </label>
                                <?php if(form_error('terms')): ?>
                                    <p><?php echo form_error('terms'); ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group hover_btn">
                            	<div class="dub_btn" data-text="Register"></div>
                            	<input type="Submit" name="currentPageFormSubmit" id="currentPageFormSubmit" class="form-control" value="Login" />
                            </div>
                        </form>
                        <div class="form_linking">
                        	<h3>OR</h3>
                          <div class="f_social">
                          	<a href="javascript:void(0);" onclick="checkLoginState();"><i class="fa fa-facebook"></i> Facebook</a>
                            <a href="javascript:void(0);" id="customBtn" ><i class="fa fa-google"></i> Google</a>
                          </div>
                          <span class="forgot"><a href="{FRONT_SITE_URL}user/forgot-password" title="Forgot Password?">Forgot Password? </a></span>
                        	<p>Not a member? <a href="{FRONT_SITE_URL}user/signup">Sign Up</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        <?php if($formError): ?>
            alertMessageModelPopup('<?php echo $formError; ?>','Error');
        <?php endif; ?>
    });
</script>
<script>startApp();</script>