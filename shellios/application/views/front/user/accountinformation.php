<section class="bred_top">
  <div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Account Information</li>
      </ol>
    </nav>
  </div>
</section>
<section class="accoutbase">
  <div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
      <div class="col-md-3 col-sm-4 col-xs-12 ac-l">
        <div class="account_left">
          <?php $this->load->view('front/include/userleftmenu'); ?>
        </div>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 ac-l">
        <div class="account_dit">
          <h4>Edit Account Information</h4>
          <div class="my_account">
            <div class="my_02">
              <div class="accshadow_box row">
                <form id="currentPageForm" name="currentPageForm" class="form-auth-small" method="post" action="">
                  <input type="hidden" name="CurrentIdForUnique" id="CurrentIdForUnique" value="<?=$userData['encrypt_id']?>"/>
                  <input type="hidden" name="CurrentDataID" id="CurrentDataID" value="<?=$userData['user_id']?>"/>
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group <?php if(form_error('user_name')): ?>error<?php endif; ?>">
                      <input type="text" name="user_name" id="user_name" value="<?php if(set_value('user_name')): echo set_value('user_name'); else: echo stripslashes($userData['user_name']);endif; ?>" placeholder="Name" class="form-control required"/>
                      <?php if(form_error('user_name')): ?>
                        <p><?php echo form_error('user_name'); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group <?php if(form_error('user_email')): ?>error<?php endif; ?>">
                      <input type="text" name="user_email" id="user_email" value="<?php if(set_value('user_email')): echo set_value('user_email'); else: echo stripslashes($userData['user_email']);endif; ?>" placeholder="Email" class="form-control required email" readonly="readonly"/>
                      <?php if(form_error('user_email')): ?>
                        <p><?php echo form_error('user_email'); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group <?php if(form_error('user_phone')): ?>error<?php endif; ?>">
                      <input type="text" name="user_phone" id="user_phone" value="<?php if(set_value('user_phone')): echo set_value('user_phone'); else: echo stripslashes($userData['user_phone']);endif; ?>" placeholder="Phone" class="form-control required" readonly="readonly"/>
                      <?php if(form_error('user_phone')): ?>
                        <p><?php echo form_error('user_phone'); ?></p>
                      <?php endif; if($mobileerror):  ?>
                        <p><?php echo $mobileerror; ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <div class="cen_btn">
                        <div class="save_b"><input type="submit" name="currentPageFormSubmit" id="currentPageFormSubmit" value="Submit" /></div>
                        <div class="back_b"><a href="{FRONT_SITE_URL}user/my-account" title="Back">Back</a></div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>