<section class="bred_top">
  <div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">My Orders</li>
      </ol>
    </nav>
  </div>
</section>
<section class="accoutbase">
  <div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
      <div class="col-md-3 col-sm-4 col-xs-12 ac-l">
        <div class="account_left">
          <?php $this->load->view('front/include/userleftmenu'); ?>
        </div>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 ac-l">
        <div class="account_dit">
          <h4>My Orders</h4>
          <div class="my_account">
            <div class="account_wishlist ordered">
              <div class="whish_detail">
                <?php if($orderList <> ""): $i=0; $orderarray      = array();
                        foreach($orderList as $orderListData): 
                          foreach($orderListData['sessionData'] as $sessionData):
                            foreach($sessionData['orderData'] as $orderData):
                              if(!in_array($sessionData['orderId'],$orderarray)):
                                array_push($orderarray,$sessionData['orderId']);
                                $j=1;
                                $totalPrice = 0;
                ?><?php if($i>0):?></div><?php endif; ?>
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="oredr_head">
                      <div class="order_id"><p><?php echo $sessionData['orderId']; ?></p></div>
                    </div>
                  </div>
              <?php endif; $totalPrice = ($totalPrice+$orderData['productTotalAmount']); ?>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-2 col-sm-2 col-xs-12 p_wish">
                      <div class="wishp_img">
                        <a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$orderData['productCateSlug'],$orderData['productSubCateSlug'],$orderData['productSlug']); ?>"><img src="<?php echo $orderData['productImage']; ?>" class="img-responsive" /></a>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 p_wish">
                      <div class="wish_pname">
                        <a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$orderData['productCateSlug'],$orderData['productSubCateSlug'],$orderData['productSlug']); ?>"><?php echo $orderData['productName'] ?></a>
                        <p><strong>Seller:</strong> <?php echo $orderData['sellerBusinessName']; ?></p>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2 p_wish">
                      <div class="wish_price">
                        <p><i class="fa fa-rupee"></i> <?php echo displayPrice($orderData['productTotalAmount']); ?></p>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 p_wish">
                      <div class="wish_price delever">
                        <?php /* ?><p>Delivered on Thu, Aug 9th '18</p><?php */ ?>
                        <?php /* ?><span>Return policy ended on Sep 8th '18</span><?php */ ?>
                        <?php if($orderData['productcancelDate'] && $orderData['productStatus']=='Canceled'):?>
                          <p><a href="javascript:void(0);" style="color:red;">Canceled</a></p>
                          <p><?php echo date('D M, d Y',strtotime($orderData['productcancelDate'])); ?></p>
                        <?php elseif(futureDateTime($orderData['productOrderDate'],1)>=strtotime(date('Y-m-d')) && $orderData['productStatus']=='Confirmed'): ?>
                          <p><a href="{FRONT_SITE_URL}user/my-order/cancel-order/<?php echo $sessionData['orderId']; ?>" style="color:red;" onclick="return confirmCancelOrder();">Cancel Order</a></p>
                        <?php endif; ?>
                        <?php if($orderData['productDeliveredDate'] && $orderData['productStatus']=='Delivered' && $orderData['productReturnRequest']=='Y'):?>
                          <p><a href="javascript:void(0);" style="color:red;">Return Request Sent</a></p>
                        <?php elseif(futureDateTime($orderData['productDeliveredDate'],1)>=strtotime(date('Y-m-d')) && $orderData['productStatus']=='Delivered'): ?>
                          <p><a href="javascript:void(0);" style="color:red;" class="returnProduct" data="<?php echo $sessionData['orderId'].'_____'.$orderData['productId']; ?>">Return Product</a></p>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="order_traker">
                      <ul>
                        <li class="green">
                          <div class="order_tool">
                            <?php echo date('D M, d Y',strtotime($orderData['productOrderDate'])); ?>
                          </div>
                          <span class="track_ball"></span>CONFIRMED
                        </li>
                        <?php if($orderData['productPackedDate']):?>
                          <li class="green">
                            <div class="order_tool">
                              <?php echo date('D M, d Y',strtotime($orderData['productPackedDate'])); ?>
                            </div>
                            <span class="track_ball"></span>PACKED
                          </li>
                        <?php else: ?>
                          <li>
                            <span class="track_ball"></span>PACKED
                          </li>
                        <?php endif; ?>
                        <?php if($orderData['productShippedDate']):?>
                          <li class="green">
                            <div class="order_tool">
                              <?php echo date('D M, d Y',strtotime($orderData['productShippedDate'])); ?>
                            </div>
                            <span class="track_ball"></span>SHIPPED
                          </li>
                        <?php else: ?>
                          <li>
                            <span class="track_ball"></span>SHIPPED
                          </li>
                        <?php endif; ?>
                        <?php if($orderData['productDeliveredDate']):?>
                          <li class="green">
                            <div class="order_tool">
                              <?php echo date('D M, d Y',strtotime($orderData['productDeliveredDate'])); ?>
                            </div>
                            <span class="track_ball"></span>DELIVERED
                          </li>
                        <?php else: ?>
                          <li>
                            <span class="track_ball"></span>DELIVERED
                          </li>
                        <?php endif; ?>
                      </ul>
                    </div>
                  </div>
                  <?php if($j==count($sessionData['orderData'])):?>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="tracor_tot">
                        <div class="o_on">
                          <strong>Ordered On</strong> <?php echo date('D M, d Y',strtotime($orderData['productOrderDate'])); ?>
                        </div>
                        <div class="o_to">
                          <strong>Order Total </strong> <i class="fa fa-rupee"></i><?php echo displayPrice($totalPrice); ?>
                        </div>
                      </div>
                    </div>
                  <?php endif; ?>
                <?php $i++; $j++; endforeach; endforeach; endforeach; ?></div><?php else: ?>
                  <div class="row">
                    <div class="col-md-12 col-ms-12 col-xs-12">
                      <div class="wishp_img" style="text-align: center;">
                        No Order Available.
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  function confirmCancelOrder() {
    return confirm("Are you sure you want to cancel this order?");
  }
</script>
<div id="myCarBrandModel" class="modal fade car-choice" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request to Return Product</h4>
      </div>
      <div class="modal-body row">
        <form id="returnRequestForm" name="returnRequestForm" class="form-auth-small" method="post" action="">
          <input type="hidden" name="CurrentDataID" id="CurrentDataID" value=""/>
          <input type="hidden" name="returnRequestId" id="returnRequestId" value=""/>
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group <?php if(form_error('order_return_reason')): ?>error<?php endif; ?>">
              <textarea name="order_return_reason" id="order_return_reason" placeholder="Reason" class="form-control required"></textarea>
              <?php if(form_error('order_return_reason')): ?>
                <p><?php echo form_error('order_return_reason'); ?></p>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
              <div class="cen_btn">
                <div class="save_b"><input type="submit" name="returnRequestFormSubmit" id="returnRequestFormSubmit" value="Send"></div>
                <div class="save_b"><input type="button" name="returnRequestFormHide" id="returnRequestFormHide" value="Cancel"></div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).on('click','.returnProduct',function(){
    var prodId  =   $(this).attr('data');
    $('#myCarBrandModel').modal('show');
    $('#order_return_reason').val('');
    $('#returnRequestId').val(prodId);
  });
  $(document).on('click','#returnRequestFormHide',function(){
    ('#myCarBrandModel').modal('hide');
  });

  $("#returnRequestForm").validate({
    errorClass: "help-inline",
    errorElement: "p",
    highlight:function(element, errorClass, validClass) {
      $(element).parents('.input-group').removeClass('success');
      $(element).parents('.input-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element).parents('.input-group').removeClass('error');
      $(element).parents('.input-group').addClass('success');
    },
    submitHandler: function(form) {
      var reason  =   $('#returnRequestForm #order_return_reason').val(); 
      if(reason){
            form.submit();
       } 
      return false;
    }
  });
</script>