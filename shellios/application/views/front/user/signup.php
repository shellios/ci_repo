<section class="log_sign">
	<div class="container">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="col-md-12 col-sm-12 col-xs-12 form_bg">
            	<div class="col-md-7 col-sm-7 col-xs-12">
                	<div class="form_lbar">
                    	<div class="back_h"><a href="{FRONT_SITE_URL}" title="home"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back to home screen</a></div>
                        <div class="fl_text">
                        	<img src="{ASSET_FRONT_URL}image/logo_big.png" class="img-responsive" />
                            <div class="form_anim">
                            	<img src="{ASSET_FRONT_URL}image/sign1.png" class="img-responsive" />
                                <img src="{ASSET_FRONT_URL}image/sign2.png" class="img-responsive" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                	<div class="form_tight">
                    	<h5>Member Signup</h5>
                        <form name="currentPageForm" id="currentPageForm" action="" method="post" autocomplete="off">
                        	<div class="form-group <?php if(form_error('user_name')): ?>error<?php endif; ?>">
                            	<span class="input_decor"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                            	<input type="text" name="user_name" id="user_name" value="<?php if(set_value('user_name')): echo set_value('user_name'); endif; ?>" placeholder="Full Name" class="form-control required" />
                                <?php if(form_error('user_name')): ?>
                                    <p><?php echo form_error('user_name'); ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group <?php if(form_error('user_phone')): ?>error<?php endif; ?>">
                            	<span class="input_decor"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                            	<input type="text" name="user_phone" id="user_phone" value="<?php if(set_value('user_phone')): echo set_value('user_phone'); endif; ?>" placeholder="Phone Number" class="form-control required number" minlength="10" maxlength="10"/>
                                <?php if(form_error('user_phone')): ?>
                                    <p><?php echo form_error('user_phone'); ?></p>
                                <?php endif; if($mobileerror):  ?>
                                    <p><?php echo $mobileerror; ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group <?php if(form_error('user_email')): ?>error<?php endif; ?>">
                            	<span class="input_decor"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                            	<input type="text" name="user_email" id="user_email" value="<?php if(set_value('user_email')): echo set_value('user_email'); endif; ?>" placeholder="Email" class="form-control required email" />
                                <?php if(form_error('user_email')): ?>
                                    <p><?php echo form_error('user_email'); ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group <?php if(form_error('password')): ?>error<?php endif; ?>">
                            	<span class="input_decor"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                            	<input type="password" name="password" id="password" placeholder="Password" class="form-control required" />
                                <?php if(form_error('password')): ?>
                                    <p><?php echo form_error('password'); ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group <?php if(form_error('conf_password')): ?>error<?php endif; ?>">
                            	<span class="input_decor"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                            	<input type="password" name="conf_password" id="conf_password" placeholder="Confirm Password" class="form-control required" />
                                <?php if(form_error('conf_password')): ?>
                                    <p><?php echo form_error('conf_password'); ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group hover_btn">
                            	<div class="dub_btn" data-text="Register"></div>
                            	<input type="Submit" name="currentPageFormSubmit" id="currentPageFormSubmit" class="form-control" value="Sign up" />
                            </div>
                        </form>
                        <div class="form_linking">
                        	<p>Have an Account? <a href="{FRONT_SITE_URL}user/login">Login</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>