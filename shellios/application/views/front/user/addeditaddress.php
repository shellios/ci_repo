<section class="bred_top">
  <div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add Address</li>
      </ol>
    </nav>
  </div>
</section>
<section class="accoutbase">
  <div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
      <div class="col-md-3 col-sm-4 col-xs-12 ac-l">
        <div class="account_left">
          <?php $this->load->view('front/include/userleftmenu'); ?>
        </div>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 ac-l">
        <div class="account_dit">
          <h4>Add Address</h4>
          <div class="my_account">
            <div class="my_02">
              <div class="accshadow_box row">
                <form id="currentPageForm" name="currentPageForm" class="form-auth-small" method="post" action="">
                  <input type="hidden" name="CurrentDataID" id="CurrentDataID" value="<?=$EDITDATA['address_id']?>"/>
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group <?php if(form_error('add_name')): ?>error<?php endif; ?>">
                      <input type="text" name="add_name" id="add_name" value="<?php if(set_value('add_name')): echo set_value('add_name'); else: echo stripslashes($EDITDATA['add_name']);endif; ?>" placeholder="Name" class="form-control required"/>
                      <?php if(form_error('add_name')): ?>
                        <p><?php echo form_error('add_name'); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group <?php if(form_error('add_phone')): ?>error<?php endif; ?>">
                      <input type="text" name="add_phone" id="add_phone" value="<?php if(set_value('add_phone')): echo set_value('add_phone'); else: echo stripslashes($EDITDATA['add_phone']);endif; ?>" placeholder="Phone" class="form-control required"/>
                      <?php if(form_error('add_phone')): ?>
                        <p><?php echo form_error('add_phone'); ?></p>
                      <?php endif; if($mobileerror):  ?>
                        <p><?php echo $mobileerror; ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group <?php if(form_error('add_address1')): ?>error<?php endif; ?>">
                      <input type="text" name="add_address1" id="add_address1" value="<?php if(set_value('add_address1')): echo set_value('add_address1'); else: echo stripslashes($EDITDATA['add_address1']);endif; ?>" placeholder="Address 1" class="form-control required"/>
                      <?php if(form_error('add_address1')): ?>
                        <p><?php echo form_error('add_address1'); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group <?php if(form_error('add_address2')): ?>error<?php endif; ?>">
                      <input type="text" name="add_address2" id="add_address2" value="<?php if(set_value('add_address2')): echo set_value('add_address2'); else: echo stripslashes($EDITDATA['add_address2']);endif; ?>" placeholder="Address 2" class="form-control"/>
                      <?php if(form_error('add_address2')): ?>
                        <p><?php echo form_error('add_address2'); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group <?php if(form_error('add_city')): ?>error<?php endif; ?>">
                      <input type="text" name="add_city" id="add_city" value="<?php if(set_value('add_city')): echo set_value('add_city'); else: echo stripslashes($EDITDATA['add_city']);endif; ?>" placeholder="City" class="form-control required"/>
                      <?php if(form_error('add_city')): ?>
                        <p><?php echo form_error('add_city'); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group <?php if(form_error('add_state')): ?>error<?php endif; ?>">
                      <input type="text" name="add_state" id="add_state" value="<?php if(set_value('add_state')): echo set_value('add_state'); else: echo stripslashes($EDITDATA['add_state']);endif; ?>" placeholder="State" class="form-control required"/>
                      <?php if(form_error('add_state')): ?>
                        <p><?php echo form_error('add_state'); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group <?php if(form_error('add_pincode')): ?>error<?php endif; ?>">
                      <input type="text" name="add_pincode" id="add_pincode" value="<?php if(set_value('add_pincode')): echo set_value('add_pincode'); else: echo stripslashes($EDITDATA['add_pincode']);endif; ?>" placeholder="Pincode" class="form-control required"/>
                      <?php if(form_error('add_pincode')): ?>
                        <p><?php echo form_error('add_pincode'); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group <?php if(form_error('add_type')): ?>error<?php endif; ?>">
                      <?php if(set_value('add_type')): $addtype = set_value('add_type'); elseif($EDITDATA['add_type']): $addtype = stripslashes($EDITDATA['add_type']); else: $addtype = ''; endif; ?>
                      <select name="add_type" id="add_type" class="form-control required">
                        <option value="">Select Address Type</option>
                        <option value="Home" <?php if($addtype == 'Home'): echo'selected="selected"'; endif; ?>>Home</option>
                        <option value="Official" <?php if($addtype == 'Official'): echo'selected="selected"'; endif; ?>>Official</option>
                      </select>
                      <?php if(form_error('add_type')): ?>
                        <p><?php echo form_error('add_type'); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <div class="cen_btn">
                        <div class="save_b"><input type="submit" name="currentPageFormSubmit" id="currentPageFormSubmit" value="Submit" /></div>
                        <div class="back_b"><a href="{FRONT_SITE_URL}user/address-book" title="Back">Back</a></div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>