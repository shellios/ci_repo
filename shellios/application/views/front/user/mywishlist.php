<section class="bred_top">
  <div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">My Wishlist</li>
      </ol>
    </nav>
  </div>
</section>
<section class="accoutbase">
  <div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
      <div class="col-md-3 col-sm-4 col-xs-12 ac-l">
        <div class="account_left">
          <?php $this->load->view('front/include/userleftmenu'); ?>
        </div>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 ac-l">
        <div class="account_dit">
          <h4>Wishlist</h4>
          <div class="my_account">
            <div class="account_wishlist">
              <?php if($wishlistData <> ""): ?>
                <div class="whish_head row">
                  <div class="col-md-2 col-ms-3 col-xs-12 p_wish"></div>
                  <div class="col-md-3 col-ms-3 col-xs-12 p_wish">
                    <h3>Product Name</h3>
                  </div>
                  <div class="col-md-3 col-ms-3 col-xs-4 p_wish">
                    <h3 class="cen_t">Price</h3>
                  </div>
                  <div class="col-md-4 col-ms-3 col-xs-4 p_wish">
                    <h3 class="cen_t">Action</h3>
                  </div>
                </div>
                <div class="whish_detail row">
                  <?php foreach($wishlistData as $wishlistInfo): 
                          $prodImage    =   $this->front_model->getProductImage($wishlistInfo['prod_id'],'1'); 
                    ?>
                    <div class="row wishlist-main-div">
                      <div class="col-md-2 col-ms-3 col-xs-12 p_wish">
                        <div class="wishp_img">
                          <a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$wishlistInfo['prod_cate_slug'],$wishlistInfo['prod_sub_cate_slug'],$wishlistInfo['prod_slug']); ?>"><img src="<?php echo showProductImage($prodImage[0]['prod_image'],'thumb'); ?>" class="img-responsive" /></a>
                        </div>
                      </div>
                      <div class="col-md-3 col-ms-3 col-xs-12 p_wish">
                        <div class="wish_pname">
                          <a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$wishlistInfo['prod_cate_slug'],$wishlistInfo['prod_sub_cate_slug'],$wishlistInfo['prod_slug']); ?>">
                            <?php echo htmlspecialchars_decode(stripslashes($wishlistInfo['prod_name'])); ?>
                          </a>
                        </div>
                      </div>
                      <div class="col-md-3 col-ms-3 col-xs-4 p_wish">
                        <div class="wish_price cen_t">
                          <p><i class="fa fa-rupee"></i> <?php echo stripslashes($wishlistInfo['prod_rmp']); ?></p>
                        </div>
                      </div>
                      <div class="col-md-4 col-ms-3 col-xs-4 p_wish">
                        <div class="wis_acart">
                          <button class="btn btn-default add-to-mycart-delete-from-wishlist" data="<?php echo base64_encode($wishlistInfo['prod_id']); ?>">add to Cart</button>
                        </div>
                        <div class="del_wish cen_t">
                          <button class="wish_delbtn delete-from-wishlist" data="<?php echo base64_encode($wishlistInfo['prod_id']); ?>"><i class="fa fa-trash-o"></i></button>
                        </div>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
              <?php  else: ?>
                <div class="whish_detail row">
                  <div class="row">
                    <div class="col-md-12 col-ms-12 col-xs-12">
                      <div class="wishp_img" style="text-align: center;">
                        No Product in Your Wishlist
                      </div>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).on('click','.add-to-mycart-delete-from-wishlist',function(){
    var curObj          =   $(this); 
    var productId       =   curObj.attr('data'); 
    var userId          =   window.btoa(USERID);
    var cookieId        =   window.btoa(readCookie('currentCartCookie'));
    $.ajax({
      type: 'post',
       url: FRONTSITEURL+'user/addtomycart',
      data: {cookieId:cookieId,userId:userId,productId:productId},
   success: function(response){ 
        if(response.success == 0) {
          alertMessageModelPopup(response.message,'Error');
        } else if(response.success == 1) {
          alertMessageModelPopup(response.message,'Success');
          $('#header-cart-count').find('span').remove();
          $('#header-cart-count').append(response.result);
          onlydeletefromwishlist(curObj,productId);
        } else if(response.success == 2) {
          alertMessageModelPopup(response.message,'Warning');
          $('#header-cart-count').find('span').remove();
          $('#header-cart-count').append(response.result);
          onlydeletefromwishlist(curObj,productId);
        }
      }
    });
  });

  function onlydeletefromwishlist(curObj,productId){
    var userId      =   window.btoa(USERID);
    $.ajax({
      type: 'post',
       url: FRONTSITEURL+'user/onlydeletefromwishlist',
      data: {userId:userId,productId:productId},
   success: function(response){ 
        if(response.success == 1) {
          curObj.closest('.wishlist-main-div').remove();
        } else if(response.success == 2) {
          window.location.reload();
        }
      }
    });
  }

  $(document).on('click','.delete-from-wishlist',function(){  
    var curObj          =   $(this); 
    var productId   =   curObj.attr('data'); 
    var userId      =   window.btoa(USERID);
    if(confirm('Are You Sure to Delete This?')){
      $.ajax({
        type: 'post',
         url: FRONTSITEURL+'user/deletefromwishlist',
        data: {userId:userId,productId:productId},
     success: function(response){ 
            if(response.success == 0) {
              alertMessageModelPopup(response.message,'Error');
            } else if(response.success == 1) {
              curObj.closest('.wishlist-main-div').remove();
              alertMessageModelPopup(response.message,'Success');
            } else if(response.success == 2) {
              window.location.reload();
            }
        }
      });
    }
  });
</script>