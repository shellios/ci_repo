<section class="log_sign">
	<div class="container">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="col-md-12 col-sm-12 col-xs-12 form_bg">
            	<div class="col-md-7 col-sm-7 col-xs-12">
                	<div class="form_lbar">
                    	<div class="back_h"><a href="{FRONT_SITE_URL}" title="home"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back to home screen</a></div>
                        <div class="fl_text">
                        	<img src="{ASSET_FRONT_URL}image/logo_big.png" class="img-responsive" />
                            <div class="form_anim">
                            	<img src="{ASSET_FRONT_URL}image/sign1.png" class="img-responsive" />
                                <img src="{ASSET_FRONT_URL}image/sign2.png" class="img-responsive" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                	<div class="form_tight">
                    	<h5>OTP Verification</h5>
                        <form name="currentPageForm" id="currentPageForm" action="" method="post" autocomplete="off">
                            <div class="form-group <?php if(form_error('user_phone')): ?>error<?php endif; ?>">
                            	<span class="input_decor"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                            	<input type="text" name="user_phone" id="user_phone" value="<?php if(set_value('user_phone')): echo set_value('user_phone'); else: echo sessionData('SHELLIOS_REGISTER_MOBILE'); endif; ?>"  placeholder="Phone Number" class="form-control required" readonly />
                                <?php if(form_error('user_phone')): ?>
                                    <p><?php echo form_error('user_phone'); ?></p>
                                <?php endif; if($mobileerror):  ?>
                                    <p><?php echo $mobileerror; ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group <?php if(form_error('user_otp')): ?>error<?php endif; ?>">
                            	<span class="input_decor"><i class="fa fa-eye" aria-hidden="true"></i></span>
                            	<input type="text" name="user_otp" id="user_otp" value="<?php if(set_value('user_otp')): echo set_value('user_otp'); endif; ?>"  placeholder="OTP" class="form-control required" maxlength="4" />
                                <?php if(form_error('user_otp')): ?>
                                    <p><?php echo form_error('user_otp'); ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="form-group hover_btn">
                            	<div class="dub_btn" data-text="Register"></div>
                            	<input type="Submit" name="currentPageFormSubmit" id="currentPageFormSubmit" class="form-control" value="OTP Verification" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        <?php if($formError): ?>
            alertMessageModelPopup('<?php echo $formError; ?>','Error');
        <?php endif; ?>
    });
</script>