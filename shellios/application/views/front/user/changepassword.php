<section class="bred_top">
  <div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Change Password</li>
      </ol>
    </nav>
  </div>
</section>
<section class="accoutbase">
  <div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
      <div class="col-md-3 col-sm-4 col-xs-12 ac-l">
        <div class="account_left">
          <?php $this->load->view('front/include/userleftmenu'); ?>
        </div>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 ac-l">
        <div class="account_dit">
          <h4>Edit Account Information</h4>
          <div class="my_account">
            <div class="my_02">
              <h3 class="acc_h">Change Password</h3>
              <div class="accshadow_box row">
                <form id="changePasswordForm" name="changePasswordForm" class="form-auth-small" method="post" action="" autocomplete="off">
                  <input type="hidden" name="CurrentIdForUnique" id="CurrentIdForUnique" value="<?=$userData['encrypt_id']?>"/>
                  <input type="hidden" name="CurrentDataID" id="CurrentDataID" value="<?=$userData['user_id']?>"/>
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                  <input type="hidden" name="old_password" id="old_password" value="<?php echo $OLDPASSWORD; ?>">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <div class="form-group <?php if(form_error('current_password')): ?>error<?php endif; ?>">
                      <input type="password" name="current_password" id="current_password" value="<?php if(set_value('current_password')): echo set_value('current_password'); endif; ?>" class="form-control required" placeholder="Current Password" autocomplete="off">
                      <?php if(form_error('current_password')): ?>
                        <p><?php echo form_error('current_password'); ?></p>
                      <?php endif; ?>
                    </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <div class="form-group <?php if(form_error('new_password')): ?>error<?php endif; ?>">
                      <input type="password" name="new_password" id="new_password" value="<?php if(set_value('new_password')): echo set_value('new_password'); endif; ?>" class="form-control required" placeholder="New Password" autocomplete="off">
                      <?php if(form_error('new_password')): ?>
                        <p><?php echo form_error('new_password'); ?></p>
                      <?php endif; ?>
                    </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <div class="form-group <?php if(form_error('conf_password')): ?>error<?php endif; ?>">
                      <input type="password" name="conf_password" id="conf_password" value="<?php if(set_value('conf_password')): echo set_value('conf_password'); endif; ?>" class="form-control required" placeholder="Confirm Password" autocomplete="off">
                      <?php if(form_error('conf_password')): ?>
                        <p><?php echo form_error('conf_password'); ?></p>
                      <?php endif; ?>
                    </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <div class="cen_btn">
                        <div class="save_b"><input type="submit" name="currentPageFormSubmit" id="currentPageFormSubmit" value="Save"/></div>
                        <div class="back_b"><a href="{FRONT_SITE_URL}user/my-account" title="Back">Back</a></div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    // Form Validation
    $("#changePasswordForm").validate({
      rules:{
        old_password: { minlength: 6, maxlength: 25 },
        current_password: { minlength: 6, equalToOld: "#old_password" },
        new_password: { minlength: 6, maxlength: 25 },
        conf_password: { minlength: 6, equalTo: "#new_password" }
      },
      errorClass: "help-inline",
      errorElement: "p",
      highlight:function(element, errorClass, validClass) {
        $(element).parents('.form-group').removeClass('success');
        $(element).parents('.form-group').addClass('error');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.form-group').removeClass('error');
        $(element).parents('.form-group').addClass('success');
      }
    });
  });
</script>