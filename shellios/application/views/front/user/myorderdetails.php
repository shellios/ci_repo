<section class="bred_top">
  <div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">My Order Details</li>
      </ol>
    </nav>
  </div>
</section>
<section class="accoutbase">
      <div class="container">
          <div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
              <div class="col-md-3 col-sm-4 col-xs-12 ac-l">
                  <div class="account_left">
                      <?php $this->load->view('front/include/userleftmenu'); ?>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12 ac-l">
                  <div class="account_dit">
                      <h4>My Orders</h4>
                        <div class="my_account">
                            <div class="account_wishlist ordered">
                                <div class="whish_detail">
                                  <div class="row">
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="oredr_head">
                                              <div class="order_id"><p>ojD12356789065</p></div>
                                                <div class="order_help">
                                                  <a href=""><i class="fa fa-question-circle" aria-hidden="true"></i> Need Help</a>
                                                    <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i> Track</a>
                                                </div>
                                            </div>
                                        </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-2 col-sm-2 col-xs-12 p_wish">
                                                <div class="wishp_img">
                                                    <a href=""><img src="{ASSET_FRONT_URL}image/wish_product.png" class="img-responsive" /></a>
                                                </div>
                                            </div>
                                             <div class="col-md-4 col-sm-4 col-xs-12 p_wish">
                                                <div class="wish_pname">
                                                    <a href="">JBRIDERZ Fog Lamp, Side Marker LED</a>
                                                    <p><strong>Seller:</strong> LOISCARON</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-2 p_wish">
                                                 <div class="wish_price">
                                                    <p><i class="fa fa-rupee"></i> 199</p>
                                                 </div>
                                            </div>
                                            
                                            <div class="col-md-4 col-sm-4 col-xs-4 p_wish">
                                                <div class="wish_price delever">
                                                    <p>Delivered on Thu, Aug 9th '18</p>
                                                    <span>Return policy ended on Sep 8th '18</span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="order_traker">
                                              <ul>
                                                  <li class="green">
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>CONFIRMED</li>
                                                    <li class="green">
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>PACKED</li>
                                                    <li>
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>SHIPPED</li>
                                                    <li>
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>DELIVERED</li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="tracor_tot">
                                              <div class="o_on">
                                                  <strong>Ordered On</strong> Tue, Aug 7th '18
                                                </div>
                                                <div class="o_to">
                                                  <strong>Order Total </strong> <i class="fa fa-rupee"></i>200
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <!--repeat-->
                                    <div class="row">
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="oredr_head">
                                              <div class="order_id"><p>ojD12356789065</p></div>
                                                <div class="order_help">
                                                  <a href=""><i class="fa fa-question-circle" aria-hidden="true"></i> Need Help</a>
                                                    <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i> Track</a>
                                                </div>
                                            </div>
                                        </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-2 col-sm-2 col-xs-12 p_wish">
                                                <div class="wishp_img">
                                                    <a href=""><img src="{ASSET_FRONT_URL}image/wish_product.png" class="img-responsive" /></a>
                                                </div>
                                            </div>
                                             <div class="col-md-4 col-sm-4 col-xs-12 p_wish">
                                                <div class="wish_pname">
                                                    <a href="">JBRIDERZ Fog Lamp, Side Marker LED</a>
                                                    <p><strong>Seller:</strong> LOISCARON</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-2 p_wish">
                                                 <div class="wish_price">
                                                    <p><i class="fa fa-rupee"></i> 199</p>
                                                 </div>
                                            </div>
                                            
                                            <div class="col-md-4 col-sm-4 col-xs-4 p_wish">
                                                <div class="wish_price delever">
                                                    <p>Delivered on Thu, Aug 9th '18</p>
                                                    <span>Return policy ended on Sep 8th '18</span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="order_traker">
                                              <ul>
                                                  <li class="green">
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>CONFIRMED</li>
                                                    <li class="green">
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>PACKED</li>
                                                    <li>
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>SHIPPED</li>
                                                    <li>
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>DELIVERED</li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="tracor_tot">
                                              <div class="o_on">
                                                  <strong>Ordered On</strong> Tue, Aug 7th '18
                                                </div>
                                                <div class="o_to">
                                                  <strong>Order Total </strong> <i class="fa fa-rupee"></i>200
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <!--repeat-->
                                    
                                    <div class="row">
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="oredr_head">
                                              <div class="order_id"><p>ojD12356789065</p></div>
                                                <div class="order_help">
                                                  <a href=""><i class="fa fa-question-circle" aria-hidden="true"></i> Need Help</a>
                                                    <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i> Track</a>
                                                </div>
                                            </div>
                                        </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-2 col-sm-2 col-xs-12 p_wish">
                                                <div class="wishp_img">
                                                    <a href=""><img src="{ASSET_FRONT_URL}image/wish_product.png" class="img-responsive" /></a>
                                                </div>
                                            </div>
                                             <div class="col-md-4 col-sm-4 col-xs-12 p_wish">
                                                <div class="wish_pname">
                                                    <a href="">JBRIDERZ Fog Lamp, Side Marker LED</a>
                                                    <p><strong>Seller:</strong> LOISCARON</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-2 p_wish">
                                                 <div class="wish_price">
                                                    <p><i class="fa fa-rupee"></i> 199</p>
                                                 </div>
                                            </div>
                                            
                                            <div class="col-md-4 col-sm-4 col-xs-4 p_wish">
                                                <div class="wish_price delever">
                                                    <p>Delivered on Thu, Aug 9th '18</p>
                                                    <span>Return policy ended on Sep 8th '18</span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="order_traker">
                                              <ul>
                                                  <li class="green">
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>CONFIRMED</li>
                                                    <li class="green">
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>PACKED</li>
                                                    <li>
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>SHIPPED</li>
                                                    <li>
                                                      <div class="order_tool">
                                                              Thu Sep 18 2018 2:47 PM
                                                         </div>
                                                    <span class="track_ball"></span>DELIVERED</li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="tracor_tot">
                                              <div class="o_on">
                                                  <strong>Ordered On</strong> Tue, Aug 7th '18
                                                </div>
                                                <div class="o_to">
                                                  <strong>Order Total </strong> <i class="fa fa-rupee"></i>200
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>