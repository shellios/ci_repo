<section class="bred_top">
    <div class="container">
      	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
	          	<li class="breadcrumb-item active" aria-current="page">Terms Of Use</li>
	        </ol>
      	</nav>
    </div>
</section>
<section class="accoutbase">
    <div class="container">
      	<div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
        	<section class="terms-use">
            	<?php echo stripslashes($CMSDATA['content']); ?>
        	</section>
      	</div>
    </div>
</section>