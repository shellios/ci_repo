<section class="bred_top">
    <div class="container">
      	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
	          	<li class="breadcrumb-item active" aria-current="page"><?php echo stripslashes($categoryData['prod_cate_name']); ?></li>
	        </ol>
      	</nav>
    </div>
</section>
<section class="blog_page">
    <div class="container">
      	<div class="col-md-12 col-sm-12 col-xs-12 whitebadse"> 
	        <button type="button" class="mob-list" id="">Filter<span class="caret"></span></button>
	        <form name="filterProductForm" id="filterProductForm" method="post">
	        	<input type="hidden" name="categoryId" id="categoryId" value="<?php echo $categoryData['prod_cate_id']; ?>">
	        	<input type="hidden" name="noOfStart" id="noOfStart" value="0">
	        	<input type="hidden" name="noOfEnd" id="noOfEnd" value="12">
	        	<input type="hidden" name="noOfTotal" id="noOfTotal" value="12">
	        	<input type="hidden" name="moreLoading" id="moreLoading" value="start">
		        <div class="col-md-3 col-sm-4 col-xs-12 blog-l">
			        <button type="button" class="close" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			        <?php if($carBrandData <> ""): ?>
						<div class="list_filter">
							<div class="panel-group" id="accordion1">
							  	<div class="panel panel-default">
								    <div class="panel-heading">
								     	<h4 class="panel-title"> <a href="javascript:void(0);" class="car-brand"> Car Brand <span id="clearCardBrandData" style="float: right;">Clear</span></a></h4>
								    </div>
								    <div id="filter1" class="panel-collapse collapse in">
										<div class="panel-body">
											<ul class="chck-list">
												<?php $cb=1; foreach($carBrandData as $carBrandInfo): ?>
													<li>
														<input type="hidden" name="carBrandModel<?php echo $carBrandInfo['car_brand_id']; ?>" id="carBrandModel<?php echo $carBrandInfo['car_brand_id']; ?>" value="ALL">
														<label for="carBrand<?php echo $cb; ?>" class="inline_check">
															<input type="checkbox" name="carBrand[]" id="carBrand<?php echo $cb; ?>" value="<?php echo $carBrandInfo['car_brand_id']; ?>" class="check_b">
															<span class="checkmark2"></span> 
															<?php echo stripslashes($carBrandInfo['car_brand_name']); ?>
														</label> 
													</li>
												<?php $cb++; endforeach; ?>
											</ul>
										</div>
									</div>
							  	</div>
							</div>
						</div>
					<?php endif; ?>
					<?php if($subCategoryData <> ""): ?>
					<div class="list_filter">
						<div class="panel-group" id="accordion2">
						  	<div class="panel panel-default">
							    <div class="panel-heading">
							      	<h4 class="panel-title"> <a href="javascript:void(0);" class="car-brand">Category<span id="clearSubCategoryData" style="float: right;">Clear</span></a></h4>
							    </div>
							    <div id="filter2" class="panel-collapse collapse in">
							      	<div class="panel-body">
										<ul class="chck-list">
											<?php $sc=1; foreach($subCategoryData as $subCategoryInfo): ?>
												<li>
													<label for="subCategory<?php echo $sc; ?>" class="inline_check">
														<input type="checkbox" name="subCategory[]" id="subCategory<?php echo $sc; ?>" value="<?php echo stripslashes($subCategoryInfo['prod_sub_cate_id']); ?>" class="check_b">
														<span class="checkmark2"></span> 
														<?php echo stripslashes($subCategoryInfo['prod_sub_cate_name']); ?>
													</label> 
												</li>
											<?php $sc++; endforeach; ?>
										</ul>
									</div>
							    </div>
						  	</div>
						</div>
					</div>
					<?php endif; ?>
					<?php if($priceData <> ""): ?>
					<div class="list_filter">
						<div class="panel-group" id="accordion3">
						  	<div class="panel panel-default">
							    <div class="panel-heading">
							      	<h4 class="panel-title"> <a href="javascript:void(0);" class="car-brand"> Price</a></h4>
							    </div>
							    <div id="filter3" class="panel-collapse collapse in">
							      	<div class="panel-body">
										<ul class="chck-list">
											<?php $pp=1; foreach($priceData as $priceKey=>$pricevalue): ?>
												<li>
													<label for="priceData<?php echo $pp; ?>" class="inline_check">
														<input type="checkbox" name="priceData" id="priceData<?php echo $pp; ?>" value="<?php echo $priceKey; ?>" class="check_b">
														<span class="checkmark2"></span> 
														<i class="fa fa-rupee"></i> <?php echo $pricevalue; ?>
													</label> 
												</li>
											<?php $pp++; endforeach; ?>
										</ul>
									</div>
							    </div>
						  	</div>
						</div>
					</div>
					<?php endif; ?>
					<div class="add-act"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/add3.png" class="img-responsive" /></a> </div>
		        </div>
		        <div class="col-md-9 col-sm-8 col-xs-12 ac-l">
		          	<div class="row">
			            <div class="col-md-12 col-sm-12 col-xs-12">
			              	<div class="pro_1">
			                	<h2><?php echo stripslashes($categoryData['prod_cate_name']); ?></h2>
			              	</div>
			            </div>
			            <div class="col-md-12 col-sm-12 col-xs-12">
			              	<div class="pro_2 col-md-12 col-sm-12 col-xs-12">
			                	<?php /* ?><div class="tot_pro"> <span>7 Items </span> </div><?php */ ?>
				                <div class="shot_p col-md-6 col-sm-6 col-xs-12"> 
				                	<span class="select_pb"> Sort By
					                  	<select name="shortBy" id="shortBy" class="shot_box">
						                    <option value="prod.prod_name_____ASC">Name</option>
						                    <option value="prod.prod_rmp_____ASC">Price low to high</option>
						                    <option value="prod.prod_rmp_____DESC">Price high to low</option>
						                    <option value="Popular">Popular</option>
					                  	</select>
				                  	</span> 
				              	</div>
				              	<div class="per_page col-md-6 col-sm-6 col-xs-12"> Show
					                <select name="showData" id="showData" class="pro_fselect">
					                  	<option value="12" selected="selected">12</option>
					                  	<option value="30">30</option>
					                  	<option value="60">60</option>
					                </select>
					                Per Page 
				            	</div>
			              	</div>
			            </div>
			            <div class="col-md-12 col-sm-12 col-xs-12 list_data" id="allProductListDiv">&nbsp;</div>
		          	</div>
		        </div>
		    </form>
      	</div>
    </div>
</section>
<div id="myCarBrandModel" class="modal fade car-choice" role="dialog">
	<div class="modal-dialog"> 
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">&nbsp;</h4>
			</div>
			<div class="modal-body row" id="allCarBrandModelDiv">&nbsp;</div>
		</div>
	</div>
</div>
<div class="car_loader"><img style="-webkit-user-select: none;" src="{ASSET_FRONT_URL}image/loadingimg.gif"></div>
<script>
	$(document).ready(function(){
		$( ".mob-list" ).click(function() {
	 		$(".blog-l").addClass("filter_visible");
		});
		$( ".close" ).click(function() {
	 		$(".blog-l").removeClass("filter_visible");
		});
	});
</script>
<script type="text/javascript">
	var carBrandCurObj;
	var carBrandIdCur;
	var closedModel			=	'YES';
	var carBrandModelArray 	= 	[];
	$(function(){
		getAllProductlist('loadfirst');
		$("#myCarBrandModel").on("hidden.bs.modal", function () {
			if(closedModel == 'YES'){
				carBrandCurObj.prop('checked', false);
			}
		});
	});

	$(document).on('change','#filterProductForm .list_filter #accordion1 .chck-list input[type="checkbox"]',function(){
		var curObj			=	$(this);
		var carBrandId 		=	curObj.val();
		var carBrandName 	=	curObj.parent().text();
		carBrandCurObj		=	curObj;
		carBrandIdCur		=	carBrandId;
		if(carBrandId != ''){
			if(curObj.prop("checked") == true){
				closedModel			=	'YES';
				$('#carBrandModel'+carBrandId).val('ALL');
				$.ajax({
		            type: 'post',
		             url: FRONTSITEURL+'product/getcerbrandmodel',
		            data: {carBrandId:carBrandId},
		         success: function(response){ 
		                if(parseInt(response.success) == 1){ 
		                	$('#myCarBrandModel').modal("show");
		                    $('#myCarBrandModel .modal-title').html(carBrandName+' Models');
		                    $('#myCarBrandModel #allCarBrandModelDiv').html(response.result.carBrandModelData);
		                
		                } else if(parseInt(response.success) == 0){
		                	$('#myCarBrandModel').modal("hide");
		                	$('#myCarBrandModel .modal-title').html('Car Brand Models');
		                	$('#myCarBrandModel #allCarBrandModelDiv').html('');

		                	callFromCarBrand();
		                }
		            }
		        });
			} else {
				$('#carBrandModel'+carBrandId).val('ALL');
				callFromCarBrand();
			}
		}
	});

	$(document).on('click','#myCarBrandModel #selectDone',function(){	
		closedModel			=	'NO';
		var count    		=	0;
		$('#myCarBrandModel').find('.car-brand-model input[type="checkbox"]').each(function(){ 
		    if($(this).prop("checked") == true){
		      	carBrandModelArray.push($(this).val());
		      	count++;
		    }
		 });
		if(count > 0){
			$('#carBrandModel'+carBrandIdCur).val(carBrandModelArray.join('_____'));
		} else {
			$('#carBrandModel'+carBrandIdCur).val('ALL');
		}
		carBrandModelArray 	= 	[];
		$('#myCarBrandModel').modal("hide");

		callFromCarBrand();
	});

	$(document).on('click','#clearCardBrandData',function(){
		$('#filterProductForm .list_filter #accordion1 .chck-list input[type="checkbox"]').prop('checked', false);
		$('#filterProductForm .list_filter #accordion1 .chck-list input[type="hidden"]').val('ALL');

		callFromCarBrand();
	});

	function callFromCarBrand(){
		$("#filterProductForm #shortBy option:first").prop("selected", true);
		$("#filterProductForm #showData option:first").prop("selected", true);
		var showData   = $('#filterProductForm #showData').val();
		$("#filterProductForm #noOfStart").val(0);
		$("#filterProductForm #noOfEnd").val(showData);
		$("#filterProductForm #noOfTotal").val(showData);
		$("#filterProductForm #moreLoading").val('start');
		$('#allProductListDiv').html('');
		$('.car_loader').show();
		getAllProductlist('loadfirst');
	}

	$(document).on('change','#filterProductForm .list_filter #accordion2 .chck-list input[type="checkbox"]',function(){
		$("#filterProductForm #shortBy option:first").prop("selected", true);
		$("#filterProductForm #showData option:first").prop("selected", true);
		var showData   = $('#filterProductForm #showData').val();
		$("#filterProductForm #noOfStart").val(0);
		$("#filterProductForm #noOfEnd").val(showData);
		$("#filterProductForm #noOfTotal").val(showData);
		$("#filterProductForm #moreLoading").val('start');
		$('#allProductListDiv').html('');
		$('.car_loader').show();
		getAllProductlist('loadfirst');
	});

	$(document).on('click','#clearSubCategoryData',function(){
		$('#filterProductForm .list_filter #accordion2 .chck-list input[type="checkbox"]').prop('checked', false);

		$("#filterProductForm #shortBy option:first").prop("selected", true);
		$("#filterProductForm #showData option:first").prop("selected", true);
		var showData   = $('#filterProductForm #showData').val();
		$("#filterProductForm #noOfStart").val(0);
		$("#filterProductForm #noOfEnd").val(showData);
		$("#filterProductForm #noOfTotal").val(showData);
		$("#filterProductForm #moreLoading").val('start');
		$('#allProductListDiv').html('');
		$('.car_loader').show();
		getAllProductlist('loadfirst');
	});

	$(document).on('change','#filterProductForm .list_filter #accordion3 .chck-list input[type="checkbox"]',function(){
		$(this).closest('li').siblings().find('.check_b').prop('checked', false);

		$("#filterProductForm #shortBy option:first").prop("selected", true);
		$("#filterProductForm #showData option:first").prop("selected", true);
		var showData   = $('#filterProductForm #showData').val();
		$("#filterProductForm #noOfStart").val(0);
		$("#filterProductForm #noOfEnd").val(showData);
		$("#filterProductForm #noOfTotal").val(showData);
		$("#filterProductForm #moreLoading").val('start');
		$('#allProductListDiv').html('');
		$('.car_loader').show();
		getAllProductlist('loadfirst');
	});

	$(document).on('change','#filterProductForm #shortBy',function(){
		$("#filterProductForm #showData option:first").prop("selected", true);
		var showData   = $('#filterProductForm #showData').val();
		$("#filterProductForm #noOfStart").val(0);
		$("#filterProductForm #noOfEnd").val(showData);
		$("#filterProductForm #noOfTotal").val(showData);
		$("#filterProductForm #moreLoading").val('start');
		$('#allProductListDiv').html('');
		$('.car_loader').show();
		getAllProductlist('loadfirst');
	});

	$(document).on('change','#filterProductForm #showData',function(){
		var showData   = $('#filterProductForm #showData').val();
		$("#filterProductForm #noOfStart").val(0);
		$("#filterProductForm #noOfEnd").val(showData);
		$("#filterProductForm #noOfTotal").val(showData);
		$("#filterProductForm #moreLoading").val('start');
		$('#allProductListDiv').html('');
		$('.car_loader').show();
		getAllProductlist('loadfirst');
	});

	function getAllProductlist(loadtype){
		var formdata	=	$('#filterProductForm').serialize();
		if($('#filterProductForm #moreLoading').val() == 'start') { 
	        $('#filterProductForm #moreLoading').val('end');
	        $.ajax({
	            type: 'post',
	             url: FRONTSITEURL+'product/getproductlist',
	            data: formdata,
	         success: function(response){ 
	                if(parseInt(response.success) == 1){ 
	                    $('#allProductListDiv').append(response.result.prodData);
	                    $('#filterProductForm #noOfStart').val(response.result.noOfStart);
	                    $('#filterProductForm #noOfEnd').val(response.result.noOfEnd);
	                    $('#filterProductForm #noOfTotal').val(response.result.noOfTotal);
	                    $('#filterProductForm #moreLoading').val('start');
	                    $('.car_loader').hide();
	                } else if(parseInt(response.success) == 0){
	                	$('.car_loader').hide();
	                	if(loadtype == 'loadfirst'){
	                		$('#allProductListDiv').html(response.result.prodData);
	                	}
	                }
	            }
	        });
	    }
	}

	$( window ).scroll(function() {
	    if ($(window).scrollTop() >= ($('#allProductListDiv').outerHeight()-window.innerHeight)) {
	        getAllProductlist('loadmore');
	    }
	});
</script>