<?php if($prodData <> ""): $pro=0;
		foreach($prodData as $prodInfo): 
			$SprodImage  	= 	$this->front_model->getProductImage($prodInfo['prod_id'],'1'); 
			$Swishlist  	= 	$this->front_model->checkInWishlist(sessionData('SHELLIOS_USER_ID'),$prodInfo['prod_id']); 
			$SratingData 	=	$this->front_model->getRatingbyProductId($prodInfo['prod_id']); 
?>
	<?php if($pro==0): ?>
		<div class="row">
	<?php elseif($pro%3==0): ?>
		</div><div class="row">
	<?php endif; ?>
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      	<div class="list_box">
	            <div class="pc_box">
	              	<div class="p_img"> 
	              		<a href="javascript:void(0);">
	              			<img src="<?php echo showProductImage($SprodImage[0]['prod_image'],'medium'); ?>" class="img-responsive"/>
	              		</a> 
	              		<a href="javascript:void(0);" class="atc_btn add-to-mycart" data="<?php echo base64_encode($prodInfo['prod_id']); ?>">Add to Cart</a> 
	              	</div>
	              	<span class="add_wish">
	              		<?php if($Swishlist == 'Y'): ?>
	                		<a href="javascript:void(0);" class="add-to-wishlist active" data="<?php echo base64_encode($prodInfo['prod_id']); ?>"><i class="fa fa-heart"></i></a>
	                	<?php else: ?>
	                		<a href="javascript:void(0);" class="add-to-wishlist" data="<?php echo base64_encode($prodInfo['prod_id']); ?>"><i class="fa fa-heart"></i></a>
	                	<?php endif; ?>
	              	</div>
	            <div class="p_data">
	              	<p>
	              		<a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$prodInfo['prod_cate_slug'],$prodInfo['prod_sub_cate_slug'],$prodInfo['prod_slug']); ?>">
	              			<?php echo characterLimit($prodInfo['prod_name'],25); ?>
	              		</a>
	              	</p>
	              	<p>
	            		<?php echo htmlspecialchars_decode(stripslashes($prodInfo['prod_brand_name'])); ?>	
	            	</p>
	              	<?php /* ?><div class="rating"> 
	              		<?php echo showProductRating($SratingData['rating']); ?>
	              	</div><?php */ ?>
	              	<div class="pro_price"> 
	              		<span class="act_pri">
	              			<i class="fa fa-rupee"></i> <?php echo displayPrice($prodInfo['prod_rmp']); ?>
	              		</span> 
	              	</div>
	            </div>
	      	</div>
	    </div>
    <?php if($pro==count($prodData)): ?></div><?php endif; ?>
<?php $pro++; endforeach; endif; ?>