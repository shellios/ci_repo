<section class="bred_top">
    <div class="container">
      	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
	          	<li class="breadcrumb-item active" aria-current="page"><?php echo $categoryName; ?></li>
	        </ol>
      	</nav>
    </div>
</section>
<section class="blog_page">
    <div class="container">
      	<div class="col-md-12 col-sm-12 col-xs-12 whitebadse"> 
	        <button type="button" class="mob-list" id="">Filter<span class="caret"></span></button>
	        <form name="filterForm" id="filterForm" method="get">
		        <div class="col-md-3 col-sm-4 col-xs-12 blog-l">
			        <button type="button" class="close" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
					<div class="list_filter">
						<div class="panel-group" id="accordion">
						  	<div class="panel panel-default">
							    <div class="panel-heading">
							     	<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#filter1"> Car Brand</a> </h4>
							    </div>
							    <div id="filter1" class="panel-collapse collapse">
							      	<div class="panel-body">
								        <ul class="chck-list">
					                      	<li>
						                        <label for="remember" class="inline_check" data-toggle="modal" data-target="#myModal">
						                          	<input value="" class="check_b" id="remember1" type="checkbox">
						                          	<span class="checkmark2"></span> </label>
						                        <a href="">Acura</a> 
						                    </li>
					                      	<li>
						                        <label for="remember" class="inline_check">
						                          	<input value="" class="check_b" id="remember2" type="checkbox">
						                          	<span class="checkmark2"></span> </label>
						                        <a href="">Alfa Romeo</a> 
						                    </li>
					                      	<li>
						                        <label for="remember" class="inline_check">
						                          	<input value="" class="check_b" id="remember3" type="checkbox">
						                          	<span class="checkmark2"></span> </label>
						                        <a href="">Aston Martin</a> 
						                    </li>
						                     <li>
						                        <label for="remember" class="inline_check">
						                          	<input value="" class="check_b" id="remember4" type="checkbox">
						                          	<span class="checkmark2"></span> </label>
						                        <a href="">Audi</a> 
						                    </li>
					                      	<li>
						                        <label for="remember" class="inline_check">
						                          	<input value="" class="check_b" id="remember5" type="checkbox">
						                          	<span class="checkmark2"></span> </label>
						                        <a href="">Bentley</a> 
					                    	</li>
					                      	<li>
						                        <label for="remember" class="inline_check">
						                          	<input value="" class="check_b" id="remember6" type="checkbox">
						                          	<span class="checkmark2"></span> </label>
						                        <a href="">BMW</a> 
					                    	</li>
						                    <li>
						                        <label for="remember" class="inline_check">
						                          	<input value="" class="check_b" id="remember7" type="checkbox">
						                          	<span class="checkmark2"></span> </label>
						                        <a href="">Bugatti</a> 
						                    </li>
					                    </ul>
							      	</div>
							    </div>
						  	</div>
						  	<div class="panel panel-default">
							    <div class="panel-heading">
							      	<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#filter3"> Model</a> </h4>
							    </div>
							    <div id="filter3" class="panel-collapse collapse">
							      	<div class="panel-body">
								        <ul>
								          	<li><a href="javascript:void(0);">3-Series 2005-2011</a></li>
								          	<li><a href="javascript:void(0);">5-Series</a></li>
								          	<li><a href="javascript:void(0);">5-Series 2003-2007</a></li>
								          	<li><a href="javascript:void(0);">5-Series 2007-2010</a></li>
								          	<li><a href="javascript:void(0);">5-Series 2010-2013</a></li>
								          	<li><a href="javascript:void(0);">5-Series 2013-2017</a></li>
								        </ul>
							      	</div>
							    </div>
						  	</div>
						  	<div class="panel panel-default">
							    <div class="panel-heading">
							      	<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#filter2"> Manufacturer</a> </h4>
							    </div>
							    <div id="filter2" class="panel-collapse collapse">
							      	<div class="panel-body">
								        <ul>
								          	<li><a href="javascript:void(0);">1</a></li>
								          	<li><a href="javascript:void(0);">2</a></li>
								          	<li><a href="javascript:void(0);">3</a></li>
								          	<li><a href="javascript:void(0);">4</a></li>
								          	<li><a href="javascript:void(0);">5</a></li>
								          	<li><a href="javascript:void(0);">6</a></li>
								          	<li><a href="javascript:void(0);">7</a></li>
								        </ul>
							      	</div>
							    </div>
						  	</div>
						</div>
					</div>
					<div class="list_filter">
						<h3>Sub Categories</h3>
						<div class="panel-group" id="accordion2">
						  	<div class="panel panel-default">
							    <div class="panel-heading">
							      	<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion2" href="#sub1"> Seat Covers</a> </h4>
							    </div>
							    <div id="sub1" class="panel-collapse collapse">
							      	<div class="panel-body">
								        <ul>
								          	<li> <a href="javascript:void(0);">Acura Seat Covers</a> </li>
								          	<li> <a href="javascript:void(0);">Alfa Romeo Seat Covers</a> </li>
								          	<li> <a href="javascript:void(0);">Aston Martin Seat Covers</a> </li>
								          	<li> <a href="javascript:void(0);">Audi Seat Covers</a> </li>
								          	<li> <a href="javascript:void(0);">Bentley Seat Covers</a> </li>
								          	<li> <a href="javascript:void(0);">BMW Seat Covers</a> </li>
								          	<li> <a href="javascript:void(0);">Bugatti Seat Covers</a> </li>
								        </ul>
							      	</div>
							    </div>
						  	</div>
						  	<div class="panel panel-default">
							    <div class="panel-heading">
							      	<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion2" href="#sub2"> Dashboard</a> </h4>
							    </div>
							    <div id="sub2" class="panel-collapse collapse">
							      	<div class="panel-body">
								        <ul>
								          	<li><a href="javascript:void(0);">Dashboard1</a></li>
								          	<li><a href="javascript:void(0);">Dashboard2</a></li>
								          	<li><a href="javascript:void(0);">Dashboard3</a></li>
								          	<li><a href="javascript:void(0);">Dashboard4</a></li>
								          	<li><a href="javascript:void(0);">Dashboard5</a></li>
								          	<li><a href="javascript:void(0);">Dashboard6</a></li>
								        </ul>
							      	</div>
							    </div>
						  	</div>
						  	<div class="panel panel-default">
							    <div class="panel-heading">
							      	<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion2" href="#sub3"> Sun Shades</a> </h4>
							    </div>
							    <div id="sub3" class="panel-collapse collapse">
							      	<div class="panel-body">
								        <ul>
								          	<li><a href="javascript:void(0);">1</a></li>
								          	<li><a href="javascript:void(0);">2</a></li>
								          	<li><a href="javascript:void(0);">3</a></li>
								          	<li><a href="javascript:void(0);">4</a></li>
								          	<li><a href="javascript:void(0);">5</a></li>
								          	<li><a href="javascript:void(0);">6</a></li>
								          	<li><a href="javascript:void(0);">7</a></li>
								        </ul>
							      	</div>
							    </div>
						  	</div>
						</div>
					</div>
					<div class="list_filter">
						<h3>Sub Categories</h3>
						<div class="panel-group" id="accordion3">
						  	<div class="panel panel-default">
							    <div class="panel-heading">
							      	<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion3" href="#price1"> Price</a> </h4>
							    </div>
							    <div id="price1" class="panel-collapse collapse">
							      	<div class="panel-body">
								        <ul>
								         	<li> <a href="javascript:void(0);"><i class="fa fa-rupee"></i> 500</a> </li>
								          	<li> <a href="javascript:void(0);"><i class="fa fa-rupee"></i> 1000</a> </li>
								          	<li> <a href="javascript:void(0);"><i class="fa fa-rupee"></i> 1500</a> </li>
								          	<li> <a href="javascript:void(0);"><i class="fa fa-rupee"></i> 2000</a> </li>
								          	<li> <a href="javascript:void(0);"><i class="fa fa-rupee"></i> 5000</a> </li>
								          	<li> <a href="javascript:void(0);"><i class="fa fa-rupee"></i> 8000</a> </li>
								          	<li> <a href="javascript:void(0);"><i class="fa fa-rupee"></i> 20,000+</a> </li>
								        </ul>
							      	</div>
							    </div>
						  	</div>
						</div>
					</div>
					<div class="add-act"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/add3.png" class="img-responsive" /></a> </div>
		        </div>
		        <div class="col-md-9 col-sm-8 col-xs-12 ac-l">
		          	<div class="row">
			            <div class="col-md-12 col-sm-12 col-xs-12">
			              	<div class="pro_1">
			                	<h2>Interior</h2>
			              	</div>
			            </div>
			            <div class="col-md-12 col-sm-12 col-xs-12">
			              	<div class="pro_2 col-md-12 col-sm-12 col-xs-12">
			                	<?php /* ?><div class="tot_pro"> <span>7 Items </span> </div><?php */ ?>
				                <div class="shot_p col-md-6 col-sm-6 col-xs-12"> 
				                	<span class="select_pb"> Sort By
					                  	<select name="shortBy" id="shortBy" class="shot_box">
						                    <option value="">Short By</option>
						                    <option value="Price low to high">Price low to high</option>
						                    <option value="Price high to low">Price high to low</option>
						                    <option value="Popular">Popular</option>
					                  	</select>
				                  	</span> 
				              	</div>
				              	<div class="per_page col-md-6 col-sm-6 col-xs-12"> Show
					                <select name="showData" id="showData" class="pro_fselect">
					                  	<option value="6">6</option>
					                  	<option value="9">9</option>
					                  	<option value="12">12</option>
					                  	<option value="15">15</option>
					                </select>
					                Per Page 
				            	</div>
			              	</div>
			            </div>
			            <div class="col-md-12 col-sm-12 col-xs-12 list_data">
			              	<div class="row">
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
			              	</div>
			              	<div class="row">
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                     	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
			              	</div>
			              	<div class="row">
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
			              	</div>
			              	<div class="row">
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
				                <div class="col-md-4 col-sm-4 col-xs-12">
				                  	<div class="list_box">
					                    <div class="pc_box">
					                      	<div class="p_img"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/pro.png" class="img-responsive"/></a> <a href="javascript:void(0);" class="atc_btn">Add to Cart</a> </div>
					                      	<span class="add_wish"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></span> </div>
					                    <div class="p_data">
					                      	<p><a href="javascript:void(0);">JBRIDERZ Fog Lamp, Side Marker LED</a></p>
					                      	<div class="rating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-half-o" aria-hidden="true"></i> </div>
					                      	<div class="pro_price"> <span class="act_pri"><i class="fa fa-rupee"></i> 200</span> <span class="thro_pri"><i class="fa fa-rupee"></i> 400</span> <span class="off_pri">50% Off</span> </div>
					                    </div>
				                  	</div>
				                </div>
			              	</div>
			            </div>
		          	</div>
		        </div>
		    </form>
      	</div>
    </div>
</section>
<div id="myModal" class="modal fade car-choice" role="dialog">
	<div class="modal-dialog"> 
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Acura Models</h4>
		</div>
		<div class="modal-body row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<ul class="chck-list">
						<li>
							<label for="remember" class="inline_check">
							<input value="" class="check_b" id="a1" type="checkbox">
							<span class="checkmark2"></span>Acura </label>
						</li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<ul class="chck-list">
						<li>
							<label for="remember" class="inline_check">
							<input value="" class="check_b" id="a11" type="checkbox">
							<span class="checkmark2"></span>Acura</label>
						</li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<ul class="chck-list">
						<li>
							<label for="remember" class="inline_check">
							<input value="" class="check_b" id="a11" type="checkbox">
							<span class="checkmark2"></span>     Acura </label>
						</li>
					</ul>
				</div>
			</div>
			<input class="button choice-car" value="Done" type="button">
		</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$( ".mob-list" ).click(function() {
	 		$(".blog-l").addClass("filter_visible");
		});
		$( ".close" ).click(function() {
	 		$(".blog-l").removeClass("filter_visible");
		});
	});
</script>
<script type="text/javascript">
	$(document).on('change','#filterForm #shortBy',function(){
		//$('#filterForm').submit();
	});
	$(document).on('change','#filterForm #showData',function(){
		//$('#filterForm').submit();
	});
</script>