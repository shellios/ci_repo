<?php if($carBrandModelData <> ""): ?>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<ul>
			<li>
				<label for="allModel" class="inline_check">
				<input type="checkbox" name="allModel" id="allModel" value="ALL" class="check_b" checked="checked">
				<span class="checkmark2"></span>All </label>
			</li>
		</ul>
	</div>
	<?php $cbm=0; foreach($carBrandModelData as $carBrandModelInfo): ?>
		<div class="col-md-4 col-sm-4 col-xs-12 car-brand-model">
			<ul>
				<li>
					<label for="carBramdModel<?php echo $cbm; ?>" class="inline_check">
					<input type="checkbox" name="carBramdModel<?php echo $cbm; ?>" id="carBramdModel<?php echo $cbm; ?>" value="<?php echo $carBrandModelInfo['car_brand_model_id']; ?>" class="check_b">
					<span class="checkmark2"></span><?php echo stripslashes($carBrandModelInfo['car_brand_model_name']); ?></label>
				</li>
			</ul>
		</div>
	<?php $cbm++; endforeach; ?>			
</div>
<input type="button" name="selectDone" id="selectDone" class="button choice-car" value="Done">
<?php endif; ?>