<section class="bred_top">
    <div class="container">
      	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
	          	
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}product-listing/<?php echo $productData['prod_cate_slug']; ?>"><?php echo $productData['prod_cate_slug']; ?></a></li>
	          	
	          	<li class="breadcrumb-item"><?php echo $productData['prod_sub_cate_slug']; ?></li>
	          	
	          	<li class="breadcrumb-item active" aria-current="page">Product Detail</li>
	        </ol>
      	</nav>
    </div>
</section>
<section class="blog_page">
    <div class="container">
      	<div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
	        <div class="col-md-9 col-sm-9 col-xs-12">
	          	<div class="row">
		            <div class="col-md-5 col-sm-5 col-xs-12"> 
		              	<div id="fancy">
			                <div class="row">
			                  	<div class="large-5 column">
				                    <div class="xzoom-container">
				                    	<?php if($prodImage <> ""): ?>
					                      	<div class="image_home">
					                      		<img class="xzoom4" id="xzoom-fancy" src="<?php echo showProductImage($prodImage[0]['prod_image'],'medium'); ?>" xoriginal="<?php echo showProductImage($prodImage[0]['prod_image'],'original'); ?>" />
					                      	</div>
					                      	<div class="xzoom-thumbs"> 
					                      		<?php $i=0; foreach($prodImage as $prodImgInfo): ?>
						                      		<a href="<?php echo showProductImage($prodImgInfo['prod_image'],'original'); ?>">
						                      			<img class="xzoom-gallery4" width="80" src="<?php echo showProductImage($prodImgInfo['prod_image'],'medium'); ?>" <?php if($i==0):?>xpreview="<?php echo showProductImage($prodImgInfo['prod_image'],'medium'); ?>"<?php endif; ?> title="Product Image">
						                      		</a> 
						                      	<?php $i++; endforeach; ?>
					                      	</div>
					                    <?php else: ?>
					                    	<div class="image_home">
					                      		<img class="xzoom4" id="xzoom-fancy" src="<?php echo showProductImage('','medium'); ?>" xoriginal="<?php echo showProductImage('','original'); ?>" />
					                      	</div>
					                      	<div class="xzoom-thumbs"> 
					                      		<a href="<?php echo showProductImage('','original'); ?>">
					                      			<img class="xzoom-gallery4" width="80" src="<?php echo showProductImage('','medium'); ?>" xpreview="<?php echo showProductImage('','medium'); ?>" title="Product Image">
					                      		</a> 
					                      	</div>
				                        <?php endif;  ?>
				                    </div>
			                  	</div>
			                </div>
		              	</div>
		            </div>
		            <div class="col-md-7 col-sm-7 col-xs-12">
		              	<div class="p_detail_t">
			                <h3><?php echo htmlspecialchars_decode(stripslashes($productData['prod_name'])); ?></h3>
			                <h5><?php echo htmlspecialchars_decode(stripslashes($productData['prod_brand_name'])); ?></h5>
			                <?php /* ?><div class="rate_review"> 
			                	<span class="rating">
			                		<?php echo showProductRating($ratingData['rating']); ?>
			                	</span>
			                	<span class="reviw_C"><?php echo $ratingData['count']?$ratingData['count'].' Review':''; ?></span> 
			                	<?php if($checkInOrder == 'Y'): ?>
				                	<span class="add_review">
				                		<a href="javascript:void(0);" id="add-your-review">Add Your Review</a>
				                	</span> 
			                	<?php endif; ?>
			                </div><?php */ ?>
			                <div class="detail_price"> 
			                	<span class="tot_p"><i class="fa fa-rupee"></i> <?php echo displayPrice($productData['prod_rmp']); ?></span> 
			                	<?php /* ?><span class="old_p"><i class="fa fa-rupee"></i> 400</span> 
			                	<span class="off_p">50% Off</span><?php */ ?>
			                </div>
			                <div class="buy_addbtn"> 
			                	<a href="javascript:void(0);" class="atc_btn add-to-mycart" data="<?php echo base64_encode($productData['prod_id']); ?>">Add to Cart</a> 
			                	<a href="<?php echo sessionData('SHELLIOS_FRONT_CURRENT_PATH'); ?>user/buy-now-checkout/productId/<?php echo $productData['prod_id']; ?>">Buy Now</a> 
			                </div>
			                <div class="seller">
			                  	<p>Seller <span><?php echo htmlspecialchars_decode(stripslashes($productData['seller_business_name'])); ?></span><?php /* ?><i>4.7</i><?php */ ?></p>
			                </div>
			                <div class="wish_share"> 
			                	<span class="wish_hert">
			                		<?php if($wishlist == 'Y'): ?>
				                		<a href="javascript:void(0);" class="add-to-wishlist active" data="<?php echo base64_encode($productData['prod_id']); ?>">
				                			<i class="fa fa-heart"></i> Add to Wish List
				                		</a>
				                	<?php else: ?>
				                		<a href="javascript:void(0);" class="add-to-wishlist" data="<?php echo base64_encode($productData['prod_id']); ?>">
				                			<i class="fa fa-heart"></i> Add to Wish List
				                		</a>
				                	<?php endif; ?>
			                	</span> 
			                	<span class="detail_share"> 
			                		<a href="javascript:void(0);">
			                			<i class="fa fa-share"></i> Share
			                		</a> 
			                	</span> 
			                </div>
			                <?php /* ?><div class="detail_dicription">
			                  	<p>Trest LED Universal Car Projector LED Fog Light with Angel eyes is designed for use as car fog lamp, off-road vehicle spot light and daytime running light.</p>
			                </div><?php */ ?>
		              	</div>
		            </div>
	          	</div>
	          	<div class="row">
		            <div class="tab_detail">
		              	<ul class="nav nav-tabs">
		                	<li class="active"><a data-toggle="tab" href="#details">Details</a></li>
		                	<?php /* ?><li><a data-toggle="tab" href="#reviews">Reviews <?php echo $ratingData['count']?'('.$ratingData['count'].')':''; ?></a></li><?php */ ?>
		              	</ul>
		              	<div class="tab-content">
			                <div id="details" class="tab-pane fade in active">
			                  	<div class="product_d">
				                    <?php echo htmlspecialchars_decode(stripslashes($productData['prod_desc'])); ?>	
			                  	</div>
			                </div>
			                <?php /* ?><div id="reviews" class="tab-pane fade">
			                  	<div class="tab-content row">
				                    <div class="col-md-12 col-sm-12 col-xs-12 tab-pane active" id="reviewRating">
				                      	<div class="col-md-12 col-sm-12 col-xs-12 padding-none rating-desp">
					                        <?php if($reviewData <> ""): foreach($reviewData as $reviewInfo): ?>
					                        <h4><span class="rating"><?php echo showProductRating($reviewInfo['rating']); ?></span> <?php echo stripslashes($reviewInfo['rating']); ?> (Review)</h4>
					                        <h5><?php echo stripslashes($reviewInfo['heading']); ?></h5>
					                        <p><em><?php echo stripslashes($reviewInfo['content']); ?></em></p>
					                        <p class="blue-font"><em><?php echo stripslashes($reviewInfo['user_name']); ?> </em></p>
					                        <?php endforeach; endif; ?>
				                      	</div>
				                      	<?php if($checkInOrder == 'Y'): ?>
					                      	<div class="col-md-12 col-sm-12 col-xs-12 padding-none rating-form" id="write-a-review-form">
					                          	<h4 class="text-center"><span class="rating-icon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </span> <span class="rating-title">Write a Review</span></h4>
						                        <form name="currentPageForm" id="currentPageForm--" action="" method="post" autocomplete="off">
						                          	<div class="form-group">
						                          		<div id="rateYo" <?php if(set_value('rating')): ?>data-rateyo-rating="<?php echo set_value('rating'); ?>"<?php  endif; ?>></div> 
						                          		<div class="counter"><?php if(set_value('rating')): echo set_value('rating'); endif; ?></div>
						                          		<input type="text" name="rating" id="rating" class="required" value="<?php if(set_value('rating')): echo set_value('rating'); endif; ?>" style="width:0px; height:0px; border:none; margin-top: -10px;">
						                          		<?php if(form_error('rating')): ?>
						                                    <p><?php echo form_error('rating'); ?></p>
						                                <?php endif; ?>
						                          	</div>
						                          	<div class="form-group">
						                            	<input type="text" name="heading" id="heading" value="<?php if(set_value('heading')): echo set_value('heading'); endif; ?>" class="form-control required" placeholder="Subject">
						                            	<?php if(form_error('heading')): ?>
						                                    <p><?php echo form_error('heading'); ?></p>
						                                <?php endif; ?>
						                          	</div>
						                          	<div class="form-group">
						                            	<textarea name="content" id="content" rows="5" class="form-control required" placeholder="Write your Review Here..."><?php if(set_value('content')): echo set_value('content'); endif; ?></textarea>
						                            	<?php if(form_error('content')): ?>
						                                    <p><?php echo form_error('content'); ?></p>
						                                <?php endif; ?>
						                          	</div>
						                          	<div class="form-group"> 
						                          		<input type="Submit" name="currentPageFormSubmit" id="currentPageFormSubmit" class="btn btn-default review-btn" value="Submit" />
						                          	</div>
					                            </form>
					                      	</div>
					                    <?php endif; ?>
				                    </div>
			                  	</div>
			                </div><?php */ ?>
		              	</div>
		            </div>
	          	</div>
	        </div>
	        <div class="col-md-3 col-sm-3 col-xs-12">
	          	<div class="row">
	            	<div class="col-md-12 col-sm-12 col-xs-12">
	              		<div class="add-act"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/add1.png" class="img-responsive"></a> </div>
	              		<div class="add-act"> <a href="javascript:void(0);"><img src="{ASSET_FRONT_URL}image/add2.png" class="img-responsive"></a> </div>
	            	</div>
	          	</div>
	        </div>
      	</div>
    </div>
</section>
<?php if($similarProductData <> ""): ?>
<section class="smilar_p">
    <div class="container">
      	<div class="row">
        	<div class="smilar_head">
          		<h3>Similar Products</h3>
        	</div>
      	</div>
      	<div class="carousel-list">
      		<?php foreach($similarProductData as $similarProductInfo): 
      				$SprodImage  	= 	$this->front_model->getProductImage($similarProductInfo['prod_id'],'1'); 
      				$Swishlist  		= 	$this->front_model->checkInWishlist(sessionData('SHELLIOS_USER_ID'),$similarProductInfo['prod_id']); 
      				$SratingData 	=	$this->front_model->getRatingbyProductId($similarProductInfo['prod_id']); 
      		?>
		        <div class="carousel-cell">
		          	<div class="col-md-12 col-sm-12 col-xs-12">
			            <div class="list_box">
			              	<div class="pc_box">
			                	<div class="p_img"> 
			                		<a href="javascript:void(0);">
			                			<img src="<?php echo showProductImage($SprodImage[0]['prod_image'],'medium'); ?>" class="img-responsive"/>
			                		</a> 
			                		<a href="javascript:void(0);" class="atc_btn add-to-mycart" data="<?php echo base64_encode($similarProductInfo['prod_id']); ?>">Add to Cart</a> 
			                	</div>
			                	<span class="add_wish">
			                		<?php if($Swishlist == 'Y'): ?>
				                		<a href="javascript:void(0);" class="add-to-wishlist active" data="<?php echo base64_encode($similarProductInfo['prod_id']); ?>"><i class="fa fa-heart"></i></a>
				                	<?php else: ?>
				                		<a href="javascript:void(0);" class="add-to-wishlist" data="<?php echo base64_encode($similarProductInfo['prod_id']); ?>"><i class="fa fa-heart"></i></a>
				                	<?php endif; ?>
			                	</span> 
			            	</div>
			              	<div class="p_data">
			                	<p>
				            		<a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$similarProductInfo['prod_cate_slug'],$similarProductInfo['prod_sub_cate_slug'],$similarProductInfo['prod_slug']); ?>">
				            			<?php //echo characterLimit($similarProductInfo['prod_name'],25); ?>
				            			<?php echo htmlspecialchars_decode(stripslashes($similarProductInfo['seller_business_name'])); ?>		
				            		</a>
				            	</p>
			                	<?php /* ?><div class="rating"> 
			                		<?php echo showProductRating($SratingData['rating']); ?>
			                	</div><?php */ ?>
			                	<div class="pro_price"> 
			                		<span class="act_pri">
			                			<i class="fa fa-rupee"></i> <?php echo displayPrice($similarProductInfo['prod_rmp']); ?>
			                		</span> 
		                		</div>
			              	</div>
			            </div>
		          	</div>
		        </div>
	        <?php endforeach; ?>
      	</div>
    </div>
</section>
<?php endif; ?>
<script src="{ASSET_FRONT_URL}js/xzoom.min.js"></script> 
<script src="{ASSET_FRONT_URL}js/jquery.fancybox.js"></script> 
<script src="{ASSET_FRONT_URL}js/setup.js"></script> 
<script src="{ASSET_FRONT_URL}js/jquery.rateyo.min.js"></script>
<script>
	$(function () {
	  	$("#rateYo").rateYo({
	    	normalFill: "#A0A0A0",
	    	starWidth: "20px"
	  	}).on("rateyo.change", function (e, data) {
	         $('#rating').val(data.rating);
	         $(this).next().text(data.rating);
	     });
	});
	// Getter
	var starWidth = $("#rateYo").rateYo("option", "starWidth"); //returns 20px
	// Setter
	$("#rateYo").rateYo("option", "starWidth", "20px"); //returns a jQuery Element
</script>
<script type="text/javascript">
    /*$(document).on('click','#add-your-review',function(){
        $('.tab_detail ul.nav-tabs li').removeClass('active');
        $('.tab_detail ul.nav-tabs li:nth-child(2)').addClass('active');

        $('.tab_detail .tab-content div#details').removeClass('active').removeClass('in');
        $('.tab_detail .tab-content div#reviews').addClass('active').addClass('in');

        $('html, body').animate({ scrollTop: ($("#write-a-review-form").offset().top-70) }, 1000);
    });
    $(function(){
	    <?php if($raviewFormError == 'YES'): ?>  
	    	$('.tab_detail ul.nav-tabs li').removeClass('active');
	        $('.tab_detail ul.nav-tabs li:nth-child(2)').addClass('active');

	        $('.tab_detail .tab-content div#details').removeClass('active').removeClass('in');
	        $('.tab_detail .tab-content div#reviews').addClass('active').addClass('in');

	        $('html, body').animate({ scrollTop: ($("#write-a-review-form").offset().top-70) }, 1000);
	    <?php endif; ?>
    });*/
</script>