<ul>
    <li <?php if($leftMenu == 'my-account'): ?>class="active"<?php endif; ?>><a href="{FRONT_SITE_URL}user/my-account" title="My Account">My Account</a></li>
    <li <?php if($leftMenu == 'account-information'): ?>class="active"<?php endif; ?>><a href="{FRONT_SITE_URL}user/account-information" title="Account Information">Account Information</a></li>
    <li <?php if($leftMenu == 'address-book'): ?>class="active"<?php endif; ?>><a href="{FRONT_SITE_URL}user/address-book" title="Address Book">Address Book</a></li>
    <li <?php if($leftMenu == 'my-wishlist'): ?>class="active"<?php endif; ?>><a href="{FRONT_SITE_URL}user/my-wishlist" title="My Wishlist">My Wishlist</a></li>
    <li <?php if($leftMenu == 'my-order'): ?>class="active"<?php endif; ?>><a href="{FRONT_SITE_URL}user/my-order" title="My Orders">My Orders</a></li>
    <?php /* ?><li <?php if($leftMenu == 'track-your-orders'): ?>class="active"<?php endif; ?>><a href="{FRONT_SITE_URL}user/track-your-orders" title="Track Your Orders">Track Your Orders</a></li><?php */ ?>
</ul>