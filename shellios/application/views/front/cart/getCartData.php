<div class="col-md-9 col-sm-9 col-xs-12">
    <div class="cart_table">
        <table class="table-bordered">
            <thead>
                <tr>
                    <td>Items</td>
                    <td class="cp_name"></td>
                    <td>Price </td>
                    <td class="cart_cen">Qty</td>
                    <td class="cart_cen">Subtotal</td>
                    <td class="c_trash"></td>
                </tr>
            </thead>
            <tbody>
                <?php if($cartData <> ""): 
                        $i=0;
                        $subTotal           =   0.00;
                        $gstTotal           =   0.00;
                        $installTotal       =   0.00;
                        $sellerIds          =   array();
                        foreach($cartData as $cartInfo):
                            $prodImage      =   $this->front_model->getProductImage($cartInfo['prod_id'],'1'); 
                            $prodPrice      =   $cartInfo['prod_rmp'];
                            $prodQty        =   $cartInfo['quantity'];
                            $prodGst        =   $cartInfo['prod_gst'];

                            $productTotal       =   ($prodPrice*$prodQty);
                            $subTotal           =   ($subTotal+$productTotal);
                            $gstTotal           =   ($gstTotal+($prodGst*$prodQty));
                            if(!in_array($cartInfo['seller_id'],$sellerIds)):
                                array_push($sellerIds,$cartInfo['seller_id']);
                                $insCharge      =   $cartInfo['seller_installation_charges']?$cartInfo['seller_installation_charges']:'0.00';
                                $installTotal   =   ($installTotal+$insCharge);
                            endif;
                ?>
                    <tr>
                        <td>
                            <div class="cart_img">
                                <a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$cartInfo['prod_cate_slug'],$cartInfo['prod_sub_cate_slug'],$cartInfo['prod_slug']); ?>" title="product">
                                    <img src="<?php echo showProductImage($prodImage[0]['prod_image'],'thumb'); ?>" class="img-responsive" />
                                </a>
                            </div>
                        </td>
                        <td>
                            <div class="cart_name">
                                <p>
                                    <a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$cartInfo['prod_cate_slug'],$cartInfo['prod_sub_cate_slug'],$cartInfo['prod_slug']); ?>" title="product">
                                        <?php echo htmlspecialchars_decode(stripslashes($cartInfo['prod_name'])); ?> 
                                    </a>
                                </p>
                            </div>
                        </td>
                        <td>
                            <div class="cart_price"><i class="fa fa-rupee"></i> <?php echo displayPrice($prodPrice); ?></div>
                        </td>
                        <td class="cart_cen">
                            <div class="cart_qty">
                                <div class="col col-qty layout-inline" data="<?php echo base64_encode($cartInfo['cart_id']); ?>">
                                  <a href="javascript:void(0);" class="qty qty-minus <?php if($prodQty==1): echo 'not-active'; endif; ?>"><i class="fa fa-minus"></i></a>
                                  <input type="numeric" name="quantity" id="quantity<?php echo $i; ?>" value="<?php echo $prodQty; ?>" />
                                  <a href="javascript:void(0);" class="qty qty-plus <?php if($prodQty==100): echo 'not-active'; endif; ?>"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </td>
                        <td class="cart_cen">
                            <div class="cart_price"><i class="fa fa-rupee"></i> <?php echo displayPrice($productTotal); ?></div>
                        </td>
                        <td class="c_trash">
                            <div class="cart_delet">
                                <a href="javascript:void(0);" title="remove" data="<?php echo base64_encode($cartInfo['cart_id']); ?>">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php $i++; endforeach; else: ?>
                    <tr>
                        <td colspan="6" style="text-align:center;">No Item In Your Cart</td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-3 col-sm-3 col-xs-12">
    <?php if($cartData <> ""): ?>
        <div class="cart_summry">
            <h3>Summary</h3>
            <ul>
                <li>
                    <label for="installation_charges">
                        <input type="checkbox" name="installation_charges" id="installation_charges" value="YES">
                        Installation
                    </label>
                </li>
                <li>
                    Subtotal<span class="sum_r"><i class="fa fa-rupee"></i> <?php echo displayPrice($subTotal); ?></span>
                </li>
                <li>
                    Tax (GST)<span class="sum_r"> <?php echo '18%';//$gstTotal; ?></span>
                </li>
                <li class="c_total">
                    Order Total<span class="sum_r"><i class="fa fa-rupee"></i> <?php echo displayPrice($subTotal+(($subTotal*18)/100));//($subTotal+$gstTotal); ?></span>
                </li>
            </ul>
            <div class="pr_check go-to-checkout">
                <a href="<?php echo sessionData('SHELLIOS_FRONT_CURRENT_PATH'); ?>user/checkout" title="Proceed to Checkout">Proceed to Checkout</a>
            </div>
        </div>
    <?php else: ?>
        <div class="cart_summry">
            <div class="pr_check">
                <a href="<?php echo sessionData('SHELLIOS_FRONT_CURRENT_PATH'); ?>" title="Proceed to Checkout">Continue Shopping</a>
            </div>
        </div>
    <?php endif; ?>
</div>