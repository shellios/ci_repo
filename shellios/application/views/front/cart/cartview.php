<section class="cart_page">
	<div class="container">
    	<div class="col-md-12 col-sm-12 col-xs-12 cartbadse">
        	<div class="cart_head"><h3>My Cart</h3></div>
        	<div class="row" id="allCartDataDiv">&nbsp;</div>
        </div>
    </div>
</section>
<div class="car_loader"><img style="-webkit-user-select: none;" src="{ASSET_FRONT_URL}image/loadingimg.gif"></div>
<script type="text/javascript">
    $(function(){
        getAllcartData();
    });

    $(document).on('click','#allCartDataDiv .cart_qty a.qty-minus',function(e){
        e.preventDefault();
        var curObj          =   $(this);
        var inputObj        =   curObj.closest("div.col-qty").find("input");
        var value           =   parseInt(inputObj.val());
        if (value > 1) value = value - 1; else value = 1;
        inputObj.val(value);
        var cartId          =   curObj.closest("div.col-qty").attr('data');
        plusMinusQuantity(cartId,value);
    });

    $(document).on('click','#allCartDataDiv .cart_qty a.qty-plus',function(e){
        e.preventDefault();
        var curObj          =   $(this);
        var inputObj        =   curObj.closest("div.col-qty").find("input");
        var value           =   parseInt(inputObj.val());
        if (value < 100) value = value + 1; else  value = 100;
        inputObj.val(value);
        var cartId          =   curObj.closest("div.col-qty").attr('data');
        plusMinusQuantity(cartId,value);
    });

    $(document).on('blur','#allCartDataDiv .cart_qty input[type="numeric"]',function(e){
        e.preventDefault();
        var curObj          =   $(this);
        var inputObj        =   curObj.closest("div.col-qty").find("input");
        var value           =   parseInt(inputObj.val());
        if(value < 1 || isNaN(value)) value = 1; else if (value > 100)  value = 100;
        inputObj.val(value);
        var cartId          =   curObj.closest("div.col-qty").attr('data');
        plusMinusQuantity(cartId,value);
    });

    function plusMinusQuantity(cartId,quantity){
        $('.car_loader').show();
        $.ajax({
            type: 'post',
             url: FRONTSITEURL+'user/cart/plusminusquantity',
            data: {cartId:cartId,quantity:quantity},
         success: function(response){ 
                getHeaderCartCount();
                getAllcartData();
            }
        });
    }

    $(document).on('click','#allCartDataDiv .cart_delet a',function(e){
        e.preventDefault();
        var curObj          =   $(this);
        var cartId          =   $(this).attr('data');
        if(confirm("Are you sure to delete this product!")){
            deleteProdFromCart(cartId);
        }
    });

    function deleteProdFromCart(cartId){
        $('.car_loader').show();
        $.ajax({
            type: 'post',
             url: FRONTSITEURL+'user/cart/deleteprodfromcart',
            data: {cartId:cartId},
         success: function(response){ 
                getHeaderCartCount();
                getAllcartData();
            }
        });
    }

    function getAllcartData(){
        $('.car_loader').show();
        var userId      =   '<?php echo $userId; ?>';
        var cookieId    =   '<?php echo $cookieId; ?>';

        $.ajax({
            type: 'post',
             url: FRONTSITEURL+'user/cart/getcartdata',
            data: {userId:userId,cookieId:cookieId},
         success: function(response){ 
                $('#allCartDataDiv').html(response.result.cartData);
                $('.car_loader').hide();
            }
        });
    }

    $(document).on('click','#allCartDataDiv #installation_charges',function(){
        if($(this).prop("checked") == true){
            $(this).closest('.cart_summry').find('.go-to-checkout a').attr('href','<?php echo sessionData('SHELLIOS_FRONT_CURRENT_PATH'); ?>user/checkout/installation/YES');
        } else {
            $(this).closest('.cart_summry').find('.go-to-checkout a').attr('href','<?php echo sessionData('SHELLIOS_FRONT_CURRENT_PATH'); ?>user/checkout');
        }
    });
</script>