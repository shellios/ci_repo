<section class="bred_top">
    <div class="container">
      	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
	          	<li class="breadcrumb-item active" aria-current="page">Payment</li>
	        </ol>
      	</nav>
    </div>
</section>
<section class="accoutbase">
    <div class="container">
      	<div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
	        <section class="terms-use about row">
		        <div class="col-md-12 col-sm-12 col-xs-12">
		            <div class="order_bg payment_sucdesign row">
	                    <div class="order_text"><h2>Payment</h2></div>
	                    	<div class="col-md-12 col-sm-12 col-xs-12 pay_lace">
	                    		<div class="col-md-4 col-sm-4 col-xs-12">
	                    			<div class="order-bott"><p><strong>Shipping Address</strong></p>
	                    		</div>
							</div>

                    		<div class="col-md-8 col-sm-8 col-xs-12 order-l_text">
                    			<?php echo stripslashes($ordAddData['add_name']); ?> 
		                    	(<?php echo stripslashes($ordAddData['add_type']); ?>)<br>
		                    	<?php echo stripslashes($ordAddData['add_phone']); ?> <br>
		                    	<?php echo $ordAddData['add_address1']?stripslashes($ordAddData['add_address1']).', ':''; ?>
	                            <?php echo $ordAddData['add_address2']?stripslashes($ordAddData['add_address2']).', ':''; ?>
	                            <?php echo $ordAddData['add_city']?stripslashes($ordAddData['add_city']).', ':''; ?>
	                            <?php echo $ordAddData['add_state']?stripslashes($ordAddData['add_state']):''; ?> 
	                            <strong><?php echo stripslashes($ordAddData['add_pincode']); ?> </strong>
	                        </div>

	                    	</div>

	                    	<div class="col-md-12 col-sm-12 col-xs-12 pay_lace pay_lace2">
	                    		<div class="col-md-4 col-sm-4 col-xs-12">
	                    			<div class="order-bott"><p><strong>Payable Amount</strong></p></div>
	                    		</div>
	                    		<div class="col-md-8 col-sm-8 col-xs-12 order-l_text">
	                    			<i class="fa fa-rupee"></i> <?php echo displayPrice(sessionData('toalPayAmount')); ?>
	                    		</div>
	                    	</div>  

	                    	<!--button-->
	                    	<div class="col-md-12 col-sm-12 col-xs-12">
	                    		<div class="col-md-4 col-sm-4 col-xs-12">
	                    			&nbsp;
	                    		</div>
	                    		<div class="col-md-8 col-sm-8 col-xs-12 order-l_text">
	                    			 <form name="RazorPay" action="<?php echo $successUrl; ?>" method="POST">
									<!-- Note that the amount is in paise = 50 INR -->
										<script
										    src="<?php echo $rozPayUrl; ?>"
										    data-key="<?php echo $keyId; ?>"
										    data-amount="<?php echo $payAmount; ?>"
										    data-buttontext="<?php echo $buttonText; ?>"
										    data-name="<?php echo $siteName; ?>"
										    data-description="<?php echo $siteDesc; ?>"
										    data-image="<?php echo $siteLogo; ?>"
										    data-prefill.name="<?php echo $userName; ?>"
										    data-prefill.email="<?php echo $userEmail; ?>"
										    data-prefill.phone="<?php echo $userPhone; ?>"
										    data-theme.color="<?php echo $themeColor; ?>"
										></script>
										<input type="hidden" value="Hidden Element" name="hidden">
									</form>
	                    		</div>
	                    	</div>  

	                    </div>

	                    
	                    


	                   
	                </div>
		         </div>
	        </section>
      	</div>
    </div>
</section>