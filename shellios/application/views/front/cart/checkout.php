<section class="bred_top">
    <div class="container">
      	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
	          	<li class="breadcrumb-item active" aria-current="page">CheckOut</li>
	        </ol>
      	</nav>
    </div>
</section>
<section class="checkout_page">
	<div class="container">
    	<div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
            <div class="col-md-12 col-sm-12 col-xs-12 ac-l">
            	<div class="row">
                	<div class="col-md-12 col-sm-12 col-xs-12">
                       	<div class="pro_1"><h2><i class="fa fa-lock"></i> CheckOut</h2></div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 check_data">
                        <div class="wick_c">
                        	<ul>
                            	<li class="">
                                	<div class="wick_head check-list-mob">
                                    	<span><i>1</i>Login</span>
                                    </div>
                                    <?php if($userId): ?>
                                        <div class="wick_body check-list-open">
                                            <div class="check_Outlogin">
                                                <div class="tab-content">
                                                    <div class="tab-pane fade in active">
                                                        <p><?php echo sessionData('SHELLIOS_USER_NAME').'<br>'.sessionData('SHELLIOS_USER_EMAIL').'<br>'.sessionData('SHELLIOS_USER_PHONE'); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="wick_body check-list-open">
                                        	<div class="check_Outlogin">
                                            	<ul class="nav nav-tabs">
                                                  	<li class="active"><a data-toggle="tab" href="#existinguser">Existing User</a></li>
                                                  	<li><a data-toggle="tab" href="#newuser">New User</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="existinguser" class="tab-pane fade in active">
                                                  		<p>Please Login to be Continue</p>
                                                   		<a href="{FRONT_SITE_URL}user/login/<?php echo $referalUrl; ?>" class="check_log">Login</a>
                                                    </div>
                                                    <div id="newuser" class="tab-pane fade">
                                                   	 	<p>Please Signup to be Continue</p>
                                                     	<a href="{FRONT_SITE_URL}user/signup/<?php echo $referalUrl; ?>" class="check_log">Sign Up</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </li>
                                <li class="<?php if(sessionData('checkout_tab') == 'Login'):?>disabled<?php endif; ?>">
                                	<div class="wick_head check-list-mob2">
                                    	<span><i>2</i>Address</span>
                                    </div>
                                    <?php if(sessionData('checkout_tab') == 'Billing' && sessionData('cur_address_id')):?>
                                        <div class="wick_body check-list-open2">
                                            <div class="old_addreslist">
                                                <div class="non_check">
                                                    <div class="chck_add checkmark">
                                                        <h3>
                                                            <?php echo stripslashes($curAddData['add_name']); ?> 
                                                            <i><?php echo stripslashes($curAddData['add_type']); ?> </i> 
                                                            <?php echo stripslashes($curAddData['add_phone']); ?> 
                                                        </h3>
                                                        <p>
                                                            <?php echo $curAddData['add_address1']?stripslashes($curAddData['add_address1']).', ':''; ?>
                                                            <?php echo $curAddData['add_address2']?stripslashes($curAddData['add_address2']).', ':''; ?>
                                                            <?php echo $curAddData['add_city']?stripslashes($curAddData['add_city']).', ':''; ?>
                                                            <?php echo $curAddData['add_state']?stripslashes($curAddData['add_state']):''; ?> 
                                                            <strong><?php echo stripslashes($curAddData['add_pincode']); ?> </strong>
                                                        </p>
                                                    </div>
                                                    <div class="pr_check">
                                                        <a href="javascript:void(0);" id="changeAddressButton" title="Change Address">Change Address</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="wick_body check-list-open2 <?php if(sessionData('checkout_tab') == 'Billing'):?>disabled-address<?php endif; ?>">
                                    	<div class="old_addreslist">
                                            <?php if($addressData <> ""): $ia=0;
                                                    foreach($addressData as $addressInfo): 
                                                        if(sessionData('cur_address_id')):
                                                            if(sessionData('cur_address_id') == $addressInfo['address_id']):
                                                                $checked    =   'Y';
                                                            else:
                                                                $checked    =   'N';
                                                            endif;
                                                        else:
                                                            if($ia == 0):
                                                                $checked    =   'Y';
                                                            else:
                                                                $checked    =   'N';
                                                            endif;
                                                        endif;
                                            ?>
                                            <div class="lab_box">
                                                <label class="">
                                                  	<input type="radio" name="shippingAddress" id="shippingAddress<?php echo $ia; ?>" <?php if($checked=='Y'): ?>checked="checked"<?php endif; ?>>
                                                  	<div class="chck_add checkmark">
                                                  		<h3>
                                                            <?php echo stripslashes($addressInfo['add_name']); ?> 
                                                            <i><?php echo stripslashes($addressInfo['add_type']); ?> </i> 
                                                            <?php echo stripslashes($addressInfo['add_phone']); ?> 
                                                        </h3>
                                                        <p>
                                                            <?php echo $addressInfo['add_address1']?stripslashes($addressInfo['add_address1']).', ':''; ?>
                                                            <?php echo $addressInfo['add_address2']?stripslashes($addressInfo['add_address2']).', ':''; ?>
                                                            <?php echo $addressInfo['add_city']?stripslashes($addressInfo['add_city']).', ':''; ?>
                                                            <?php echo $addressInfo['add_state']?stripslashes($addressInfo['add_state']):''; ?> 
                                                            <strong><?php echo stripslashes($addressInfo['add_pincode']); ?> </strong>
                                                        </p>
                                                  	</div>
                                                </label>
                                                <div class="pr_check delivery-button <?php if($checked=='Y'): ?>active<?php endif; ?>">
                                                    <a href="<?php echo sessionData('SHELLIOS_FRONT_CURRENT_PATH'); ?>user/checkout/delivery-here/<?php echo $addressInfo['address_id']; ?>" title="Deliver Here">Deliver Here</a>
                                                </div>
                                            </div>
                                            <?php $ia++; endforeach; endif; ?>
                                        </div>
                                        <div class="add_newadd">
                                        	<button class="check_newadd" type="button">Add a new address</button>
                                            <div class="accshadow_box row">
                                                <form id="currentPageForm" name="currentPageForm" class="form-auth-small" method="post" action="">
                                                   <input type="hidden" name="CurrentDataID" id="CurrentDataID" value=""/>
                                                   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group <?php if(form_error('add_name')): ?>error<?php endif; ?>">
                                                            <input type="text" name="add_name" id="add_name" value="<?php if(set_value('add_name')): echo set_value('add_name'); endif; ?>" placeholder="Name" class="form-control required"/>
                                                            <?php if(form_error('add_name')): ?>
                                                                <p><?php echo form_error('add_name'); ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group <?php if(form_error('add_phone')): ?>error<?php endif; ?>">
                                                            <input type="text" name="add_phone" id="add_phone" value="<?php if(set_value('add_phone')): echo set_value('add_phone'); endif; ?>" placeholder="Phone" class="form-control required"/>
                                                            <?php if(form_error('add_phone')): ?>
                                                                <p><?php echo form_error('add_phone'); ?></p>
                                                            <?php endif; if($mobileerror):  ?>
                                                                <p><?php echo $mobileerror; ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group <?php if(form_error('add_address1')): ?>error<?php endif; ?>">
                                                            <input type="text" name="add_address1" id="add_address1" value="<?php if(set_value('add_address1')): echo set_value('add_address1'); endif; ?>" placeholder="Address 1" class="form-control required"/>
                                                            <?php if(form_error('add_address1')): ?>
                                                                <p><?php echo form_error('add_address1'); ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group <?php if(form_error('add_address2')): ?>error<?php endif; ?>">
                                                            <input type="text" name="add_address2" id="add_address2" value="<?php if(set_value('add_address2')): echo set_value('add_address2'); endif; ?>" placeholder="Address 2" class="form-control"/>
                                                            <?php if(form_error('add_address2')): ?>
                                                                <p><?php echo form_error('add_address2'); ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                       <div class="form-group <?php if(form_error('add_city')): ?>error<?php endif; ?>">
                                                            <input type="text" name="add_city" id="add_city" value="<?php if(set_value('add_city')): echo set_value('add_city'); endif; ?>" placeholder="City" class="form-control required"/>
                                                            <?php if(form_error('add_city')): ?>
                                                                <p><?php echo form_error('add_city'); ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group <?php if(form_error('add_state')): ?>error<?php endif; ?>">
                                                            <input type="text" name="add_state" id="add_state" value="<?php if(set_value('add_state')): echo set_value('add_state'); endif; ?>" placeholder="State" class="form-control required"/>
                                                            <?php if(form_error('add_state')): ?>
                                                                <p><?php echo form_error('add_state'); ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group <?php if(form_error('add_pincode')): ?>error<?php endif; ?>">
                                                            <input type="text" name="add_pincode" id="add_pincode" value="<?php if(set_value('add_pincode')): echo set_value('add_pincode'); endif; ?>" placeholder="Pincode" class="form-control required"/>
                                                            <?php if(form_error('add_pincode')): ?>
                                                                <p><?php echo form_error('add_pincode'); ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group <?php if(form_error('add_type')): ?>error<?php endif; ?>">
                                                            <?php if(set_value('add_type')): $addtype = set_value('add_type'); else: $addtype = ''; endif; ?>
                                                            <select name="add_type" id="add_type" class="form-control required">
                                                                <option value="">Select Address Type</option>
                                                                <option value="Home" <?php if($addtype == 'Home'): echo'selected="selected"'; endif; ?>>Home</option>
                                                                <option value="Official" <?php if($addtype == 'Official'): echo'selected="selected"'; endif; ?>>Official</option>
                                                            </select>
                                                            <?php if(form_error('add_type')): ?>
                                                                <p><?php echo form_error('add_type'); ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                  	<div class="col-md-12 col-sm-12 col-xs-12">
                                                      	<div class="form-group">
                                                        	<div class="cen_btn">
                                                            	<div class="save_b"><input type="submit" name="currentPageFormSubmit" id="currentPageFormSubmit" value="Save"></div>
                                                                <div class="save_b"><input type="button" name="hideAddressForm" id="hideAddressForm" value="Cancel"></div>
                                                        	</div>
                                                      	</div>
                                                  	</div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="<?php if(sessionData('checkout_tab') != 'Billing'):?>disabled<?php endif; ?>">
                                	<div class="wick_head check-list-mob3">
                                    	<span><i>3</i>Billing</span>
                                    </div>
                                    <div class="wick_body check-list-open3">
                                    	<h4>Review Order</h4>
                                        <div class="checkput-cart-data-div" id="allCartDataDiv">&nbsp;</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="car_loader"><img style="-webkit-user-select: none;" src="{ASSET_FRONT_URL}image/loadingimg.gif"></div>
</section>
<script type="text/javascript">
    $(document).on("click",'.old_addreslist input[type="radio"]',function(){
        $(this).closest(".old_addreslist").find(".delivery-button").removeClass("active");
        $(this).parent().next(".delivery-button").addClass("active");
    });
    $(document).on("click",".add_newadd .check_newadd",function (){
        $(this).closest(".add_newadd").prev(".old_addreslist").toggleClass("slideOutUp");
        $(this).next(".accshadow_box").toggleClass("slideOutUp"); 
        $(this).hide();
    });
    $(document).on("click",".add_newadd #hideAddressForm",function (){
        $(this).closest(".add_newadd").prev(".old_addreslist").toggleClass("slideOutUp");
        $(this).closest(".add_newadd").find(".accshadow_box").toggleClass("slideOutUp");
        $(this).closest(".add_newadd").find(".check_newadd").show();
    });  
    $(function(){
        <?php if(sessionData('checkout_tab')=='Address' && $formError=='YES'): ?>
            $(".add_newadd .check_newadd").closest(".add_newadd").prev(".old_addreslist").toggleClass("slideOutUp");
            $(".add_newadd .check_newadd").next(".accshadow_box").toggleClass("slideOutUp"); 
            $(".add_newadd .check_newadd").hide();
        <?php endif; ?>
    })
    $(document).on('click','.wick_body #changeAddressButton',function(){
        $(this).closest('.wick_body').hide();
        $(this).closest('.wick_body').next('.wick_body').show();
    });
</script>
<script type="text/javascript">
    $(function(){
        getAllcartData();
    });

    $(document).on('click','#allCartDataDiv .cart_qty a.qty-minus',function(e){
        e.preventDefault();
        var curObj          =   $(this);
        var inputObj        =   curObj.closest("div.col-qty").find("input");
        var value           =   parseInt(inputObj.val());
        if (value > 1) value = value - 1; else value = 1;
        inputObj.val(value);
        var cartId          =   curObj.closest("div.col-qty").attr('data');
        plusMinusQuantity(cartId,value);
    });

    $(document).on('click','#allCartDataDiv .cart_qty a.qty-plus',function(e){
        e.preventDefault();
        var curObj          =   $(this);
        var inputObj        =   curObj.closest("div.col-qty").find("input");
        var value           =   parseInt(inputObj.val());
        if (value < 100) value = value + 1; else  value = 100;
        inputObj.val(value);
        var cartId          =   curObj.closest("div.col-qty").attr('data');
        plusMinusQuantity(cartId,value);
    });

    $(document).on('blur','#allCartDataDiv .cart_qty input[type="numeric"]',function(e){
        e.preventDefault();
        var curObj          =   $(this);
        var inputObj        =   curObj.closest("div.col-qty").find("input");
        var value           =   parseInt(inputObj.val());
        if(value < 1 || isNaN(value)) value = 1; else if (value > 100)  value = 100;
        inputObj.val(value);
        var cartId          =   curObj.closest("div.col-qty").attr('data');
        plusMinusQuantity(cartId,value);
    });

    function plusMinusQuantity(cartId,quantity){
        $('.car_loader').show();
        $.ajax({
            type: 'post',
             url: FRONTSITEURL+'user/cart/plusminusquantity',
            data: {cartId:cartId,quantity:quantity},
         success: function(response){ 
                getHeaderCartCount();
                getAllcartData();
            }
        });
    }

    $(document).on('click','#allCartDataDiv span.deleting',function(e){
        e.preventDefault();  
        var curObj          =   $(this);
        var cartId          =   $(this).attr('data');
        if($('#placeOrderForm #totalCartProduct').val()>1){
            if(confirm("Are you sure to delete this product!")){ 
                deleteProdFromCart(cartId);
            }
        } else {
            var message     =   '<span>Your checkout has no items.&nbsp;&nbsp;<a href="<?php echo $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'); ?>user/cart" class="btn btn-default">Go to Cart</a></span>';
            alertMessageModelPopup(message,'Warning','NO');
        }
    });

    function deleteProdFromCart(cartId){
        $('.car_loader').show();
        $.ajax({
            type: 'post',
             url: FRONTSITEURL+'user/cart/deleteprodfromcart',
            data: {cartId:cartId},
         success: function(response){ 
                getHeaderCartCount();
                getAllcartData();
            }
        });
    }

    function getAllcartData(){
        $('.car_loader').show();
        var userId      =   '<?php echo $userId; ?>';
        var cookieId    =   '<?php echo $cookieId; ?>';

        $.ajax({
            type: 'post',
             url: FRONTSITEURL+'user/cart/getcheckoutcartdata',
            data: {userId:userId,cookieId:cookieId},
         success: function(response){ 
                $('#allCartDataDiv').html(response.result.cartData);
                $('.car_loader').hide();
            }
        });
    }
</script>