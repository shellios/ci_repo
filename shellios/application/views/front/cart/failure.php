<section class="bred_top">
    <div class="container">
      	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	          	<li class="breadcrumb-item"><a href="{FRONT_SITE_URL}">Home</a></li>
	          	<li class="breadcrumb-item active" aria-current="page">Payment Failure</li>
	        </ol>
      	</nav>
    </div>
</section>
<section class="accoutbase">
    <div class="container">
      	<div class="col-md-12 col-sm-12 col-xs-12 whitebadse">
	        <section class="terms-use about row">
		        <div class="col-md-12 col-sm-12 col-xs-12 thankcar">
		            <div class="order_bg">
	                	<div class="order-icon"><img src="{ASSET_FRONT_URL}image/failure.png" /></div>
	                    <div class="order_text"><h2>!!Sorry</h2></div>
	                    <div class="order-bott"><p>Your order is cancel. Please try again.</p></div>
	                    <div class="order-id"><p>Your Transaction Number is</p><span>#<?php echo $txnId; ?></span></div>
	                    <div class="back_h"><a href="{FRONT_SITE_URL}">Back to Home</a></div>
	                </div>
		         </div>
	        </section>
      	</div>
    </div>
</section>
<script>
	setTimeout("window.location.href=\"<?php echo $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'); ?>\";", 30000);
</script>