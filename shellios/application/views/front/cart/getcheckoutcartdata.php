<form id="placeOrderForm" name="placeOrderForm" class="form-auth-small" method="post" action="">
   <input type="hidden" name="CurrentDataID" id="CurrentDataID" value=""/>
   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
   <input type="hidden" name="totalCartProduct" id="totalCartProduct" value="<?php echo count($cartData); ?>"/>
    <div class="cehck_order">
        <?php if($cartData <> ""): 
                 $i=0;
                $subTotal           =   0.00;
                $gstTotal           =   0.00;
                $installTotal       =   0.00;
                $sellerIds          =   array();
                foreach($cartData as $cartInfo):
                    $prodImage      =   $this->front_model->getProductImage($cartInfo['prod_id'],'1'); 
                    $prodPrice      =   $cartInfo['prod_rmp'];
                    $prodQty        =   $cartInfo['quantity'];
                    $prodGst        =   $cartInfo['prod_gst'];

                    $productTotal       =   ($prodPrice*$prodQty);
                    $subTotal           =   ($subTotal+$productTotal);
                    $gstTotal           =   ($gstTotal+($prodGst*$prodQty));
                    if(!in_array($cartInfo['seller_id'],$sellerIds)):
                        array_push($sellerIds,$cartInfo['seller_id']);
                        $insCharge      =   $cartInfo['seller_installation_charges']?$cartInfo['seller_installation_charges']:'0.00';
                        $installTotal   =   ($installTotal+$insCharge);
                    endif;
        ?>
        <div class="r_oredr">
            <div class="roimg">
                <a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$cartInfo['prod_cate_slug'],$cartInfo['prod_sub_cate_slug'],$cartInfo['prod_slug']); ?>">
                    <img src="<?php echo showProductImage($prodImage[0]['prod_image'],'thumb'); ?>" class="img-responsive" />
                </a>
            </div>
            <div class="ro_text">
                <h5>
                    <a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$cartInfo['prod_cate_slug'],$cartInfo['prod_sub_cate_slug'],$cartInfo['prod_slug']); ?>">
                        <?php echo htmlspecialchars_decode(stripslashes($cartInfo['prod_name'])); ?> 
                    </a>
                    <span class="deleting" data="<?php echo base64_encode($cartInfo['cart_id']); ?>"></span>
                </h5>                                                        
                <div class="or_plus">
                    <div class="cart_qty">
                        <div class="col col-qty layout-inline" data="<?php echo base64_encode($cartInfo['cart_id']); ?>">
                            <a href="javascript:void(0);" class="qty qty-minus <?php if($prodQty==1): echo 'not-active'; endif; ?>"><i class="fa fa-minus"></i></a>
                            <input type="numeric" name="quantity" id="quantity<?php echo $i; ?>" value="<?php echo $prodQty; ?>" />
                            <a href="javascript:void(0);" class="qty qty-plus <?php if($prodQty==100): echo 'not-active'; endif; ?>"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                    <p><i class="fa fa-rupee"></i> <?php echo displayPrice($prodPrice); ?></p>
                </div>
                <p><strong>Sub Total</strong> <i class="fa fa-rupee"></i> <?php echo displayPrice($productTotal); ?></p>
            </div>
        </div>
        <?php $i++; endforeach; endif; ?>
    </div>
    <div class="check_bill">
        <div class="cart_summry">
            <ul>
                <li>
                    <label for="installation_charges">
                        <input type="checkbox" name="installation_charges" id="installation_charges" value="YES" <?php if(sessionData('installation')=='YES'):?> checked="checked"<?php endif; ?>>
                        Installation Charges
                    </label>
                </li>
                <li>
                    Total<span class="sum_r"><i class="fa fa-rupee"></i> <?php echo displayPrice($subTotal); ?></span>
                </li>
                <li>
                    Tax (GST)<span class="sum_r"> <?php echo '18%';//$gstTotal; ?></span>
                </li>
                <li class="c_total">
                    Payable<span class="sum_r"><i class="fa fa-rupee"></i> <?php echo displayPrice($subTotal+(($subTotal*18)/100));//($subTotal+$gstTotal); ?></span>
                </li>
            </ul>
            <div class="comment_check">
                <div class="form-group">
                    <label>Comments:</label>
                    <textarea name="order_comments" id="order_comments" class="form-control" rows="3" placeholder=""></textarea>
                </div>
                <div class="form-group">
                    <select name="you_know" id="you_know" class="form-control required">
                        <option value="">Where did you hear about us?</option>
                        <option value="TV">By TV</option>
                        <option value="Newspaper">By Newspaper</option>
                       <option value="Facebook Add">By Facebook Add</option>
                    </select>
                </div>
            </div>
            <div class="pr_check">
                <div class="cen_btn">
                    <div class="save_b"><input type="submit" name="placeOrderFormSubmit" id="placeOrderFormSubmit" value="Place Order"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(function(){
        $("#placeOrderForm").validate({
            errorClass: "help-inline",
            errorElement: "p",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.form-group').removeClass('success');
                $(element).parents('.form-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.form-group').removeClass('error');
                $(element).parents('.form-group').addClass('success');
            }
        });
    });
</script>