<section class="banner">
	<div class="container-fluid">
		 <div class="banner_home">
		    <div class="banner_bg"> <a href="javascript:void(0);"><img src="<?php echo stripslashes($homeBanner['slider_image']); ?>" class="img-responsive" /></a> </div>
		    <div class="banner_over">
		      	<div class="bannre_btn"> <a href="javascript:void(0);"> <span class="btn_ico"> <img src="{ASSET_FRONT_URL}image/apple.png" /> </span> <span class="btn_t"> <small>available on</small> <strong>Apple Store</strong> </span> </a> <a href=""> <span class="btn_ico"> <img src="{ASSET_FRONT_URL}image/play.png" /> </span> <span class="btn_t"> <small>available on</small> <strong>Google Play</strong> </span> </a> </div>
		    </div>
		</div>
	</div>
</section>
<section class="urban_sec">
  	<div class="container">
    	<div class="col-md-12 col-sm-12 col-xs-12">
    		<?php  if($cateData <> ""): $ci=0; foreach($cateData as $cateInfo): ?>
        		<div class="col-md-2 col-sm-12 col-xs-12 <?php if($ci==0):?>col-md-offset-1 col-sm-offset-1<?php endif; ?>">
            	<div class="urban_img">
                	<a href="{FRONT_SITE_URL}product-listing/<?php echo $cateInfo['prod_cate_slug']; ?>">
                		<?php if($cateInfo['prod_cate_image']): ?>
	                    <div class="u_img">
	                    	<img src="<?php echo stripslashes($cateInfo['prod_cate_image']); ?>" class="img-responsive" />
	                    </div>
	                <?php endif; ?>
	                    <h3><?php echo stripslashes($cateInfo['prod_cate_name']); ?></h3>
                    </a>
                </div>
            	</div>
            <?php $ci++; endforeach; endif; ?>
        </div>
    </div>
</section>
<?php if($mostPopularData <> ""): ?>
<section class="smilar_p">
    <div class="container">
      	<div class="row">
        	<div class="smilar_head">
          		<h3>Most Popular </h3>
        	</div>
      	</div>
      	<div class="carousel-list">
      		<?php foreach($mostPopularData as $mostPopularInfo): 
      				$prodImage  	= 	$this->front_model->getProductImage($mostPopularInfo['prod_id'],'1'); 
      				$wishlist  		= 	$this->front_model->checkInWishlist(sessionData('SHELLIOS_USER_ID'),$mostPopularInfo['prod_id']); 
      				$ratingData 	=	$this->front_model->getRatingbyProductId($mostPopularInfo['prod_id']); 
      		?>
		        <div class="carousel-cell">
		          	<div class="col-md-12 col-sm-12 col-xs-12">
			            <div class="list_box">
				            <div class="pc_box">
				                <div class="p_img"> 
				                	<a href="javascript:void(0);">
				                		<img src="<?php echo showProductImage($prodImage[0]['prod_image'],'medium'); ?>" class="img-responsive"/>
				                	</a> 
				                	<a href="javascript:void(0);" class="atc_btn add-to-mycart" data="<?php echo base64_encode($mostPopularInfo['prod_id']); ?>">Add to Cart</a> 
				                </div>
				                <span class="add_wish">
				                	<?php if($wishlist == 'Y'): ?>
				                		<a href="javascript:void(0);" class="add-to-wishlist active" data="<?php echo base64_encode($mostPopularInfo['prod_id']); ?>"><i class="fa fa-heart"></i></a>
				                	<?php else: ?>
				                		<a href="javascript:void(0);" class="add-to-wishlist" data="<?php echo base64_encode($mostPopularInfo['prod_id']); ?>"><i class="fa fa-heart"></i></a>
				                	<?php endif; ?>
				                </span> 
				            </div>
				            <div class="p_data">
				            	<p>
				            		<a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$mostPopularInfo['prod_cate_slug'],$mostPopularInfo['prod_sub_cate_slug'],$mostPopularInfo['prod_slug']); ?>">
				            			<?php echo characterLimit($mostPopularInfo['prod_name'],25); ?>	
				            		</a>
				            	</p>
				            	<p>
				            		<?php echo htmlspecialchars_decode(stripslashes($mostPopularInfo['prod_brand_name'])); ?>	
				            	</p>
				                <?php /* ?><div class="rating"> 
				                	<?php echo showProductRating($ratingData['rating']); ?>
				                </div><?php */ ?>
				                <div class="pro_price"> 
				                	<span class="act_pri"><i class="fa fa-rupee"></i> <?php echo displayPrice($mostPopularInfo['prod_rmp']); ?></span>
				                </div>
				            </div>
			            </div>
		          	</div>
		        </div>
		    <?php endforeach; ?>
      	</div>
    </div>
</section>
<?php endif; ?>
<?php if($recentViewData <> ""): ?>
<section class="smilar_p">
    <div class="container">
		<div class="row">
			<div class="smilar_head">
				<h3>Recently Viewed</h3>
			</div>
		</div>
      	<div class="carousel-list">
      		<?php foreach($recentViewData as $recentViewInfo): 
      				$prodImage  	= 	$this->front_model->getProductImage($recentViewInfo['prod_id'],'1'); 
      				$wishlist  		= 	$this->front_model->checkInWishlist(sessionData('SHELLIOS_USER_ID'),$recentViewInfo['prod_id']); 
      				$ratingData 	=	$this->front_model->getRatingbyProductId($recentViewInfo['prod_id']); 
      		?>
				<div class="carousel-cell">
		          	<div class="col-md-12 col-sm-12 col-xs-12">
			            <div class="list_box">
				            <div class="pc_box">
				                <div class="p_img"> 
				                	<a href="javascript:void(0);">
				                		<img src="<?php echo showProductImage($prodImage[0]['prod_image'],'medium'); ?>" class="img-responsive"/>
				                	</a> 
				                	<a href="javascript:void(0);" class="atc_btn add-to-mycart" data="<?php echo base64_encode($recentViewInfo['prod_id']); ?>">Add to Cart</a> 
				                </div>
				                <span class="add_wish">
				                	<?php if($wishlist == 'Y'): ?>
				                		<a href="javascript:void(0);" class="add-to-wishlist active" data="<?php echo base64_encode($recentViewInfo['prod_id']); ?>"><i class="fa fa-heart"></i></a>
				                	<?php else: ?>
				                		<a href="javascript:void(0);" class="add-to-wishlist" data="<?php echo base64_encode($recentViewInfo['prod_id']); ?>"><i class="fa fa-heart"></i></a>
				                	<?php endif; ?>
				                </span> 
				            </div>
				            <div class="p_data">
				            	<p>
				            		<a href="<?php echo productDetailPageLink(sessionData('SHELLIOS_FRONT_CURRENT_PATH'),$recentViewInfo['prod_cate_slug'],$recentViewInfo['prod_sub_cate_slug'],$recentViewInfo['prod_slug']); ?>">
				            			<?php echo characterLimit($recentViewInfo['prod_name'],25); ?>	
				            		</a>
				            	</p>
				            	<p>
				            		<?php echo htmlspecialchars_decode(stripslashes($recentViewInfo['prod_brand_name'])); ?>	
				            	</p>
				                <?php /* ?><div class="rating"> 
				                	<?php echo showProductRating($ratingData['rating']); ?>
				                </div><?php */ ?>
				                <div class="pro_price"> 
				                	<span class="act_pri"><i class="fa fa-rupee"></i> <?php echo displayPrice($recentViewInfo['prod_rmp']); ?></span>
				                </div>
				            </div>
			            </div>
		          	</div>
		        </div>
			<?php endforeach; ?>
      	</div>
    </div>
</section>
<?php endif; ?>
<?php if($blogData <> ""): ?>
<section class="smilar_p blog-section">
    <div class="container">
		<div class="row">
			<div class="smilar_head">
				<h3>Blog</h3>
			</div>
		</div>
	    <div class="carousel-list">
	    	<?php foreach($blogData as $blogInfo): ?>
		        <div class="carousel-cell">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="blog_box"> 
							<a href="<?php echo $blogInfo['slug']; ?>" target="_blank">
								<div class="blog_img img_big"> <img src="<?php echo $blogInfo['image']; ?>" class="img-responsive" /> </div>
								<div class="blog_text">
									<h3><span><?php echo $blogInfo['title']; ?></span></h3>
									<?php /* ?><div class="blog_date">
										<div class="blog_by">by Linda Marcell </div>
									</div><?php */ ?>
									<div class="read_btn">Read more</div>
								</div>
							</a> 
						</div>
					</div>
		        </div>
		    <?php endforeach; ?>
      	</div>
    </div>
</section>
<?php endif; ?>
<?php if($testimonialData <> ""): ?>
<section class="testimonial">
	<div class="container">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="h_head center_head">
            	<h1><span>Testimonials</span></h1>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" > 
        		<div id="testiCarousel" class="carousel slide" data-ride="carousel">
	                <ol class="carousel-indicators">
	                  	<?php for($tes=0; $tes < count($testimonialData); $tes++): ?>
							<li data-target="#testiCarousel" data-slide-to="<?php echo $tes; ?>" class="<?php if($tes==0):?>active<?php endif; ?>"></li>
						<?php endfor; ?>
	                </ol>
	                <div class="carousel-inner testimonial_new">
	                	<?php $tes=0; foreach($testimonialData as $testimonialInfo): ?>
		                  	<div class="item <?php if($tes==0):?>active<?php endif; ?>">
		                  	  	<div class="item_tu">
		                            <div class="test_content row">
		                            	<div class="tes_img col-md-5 col-sm-5 col-xs-12">
		                                    <div class="i_img">
		                                    	<img src="<?php echo stripslashes($testimonialInfo['image']); ?>" class="img-responsive"/>
		                                     </div>
		                                </div>
		                            	<div class="testi_detail col-md-7 col-sm-7 col-xs-12">
		                                    <span class="tes_detil">
		                                        <small><?php echo stripslashes($testimonialInfo['name']); ?></small>
		                                        <strong><?php echo stripslashes($testimonialInfo['designation']); ?></strong>
		                                    </span>
		                                    <p><?php echo stripslashes($testimonialInfo['content']); ?></p>
		                                </div>
		                         	</div>
		                     	</div>
		                  	</div>
	                  	<?php $tes++; endforeach; ?>
	                </div>
              	</div>
            </div>
            <a class="left carousel-control" href="#testiCarousel" data-slide="prev">
               <img src="{ASSET_FRONT_URL}image/left.png" />
               <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#testiCarousel" data-slide="next">
               <img src="{ASSET_FRONT_URL}image/right.png" />
               <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
<?php endif; ?>