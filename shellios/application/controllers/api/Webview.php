<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webview extends CI_Controller {

	public function  __construct() 
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('frontauth_model','front_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'front');
		$this->load->helper('front');
	} 

	/* * *********************************************************************
	 * * Function name : cancellationsandrefunds
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for cancellations and refunds
	 * * Date : 09 OCTOBER 2018
	 * * **********************************************************************/
	public function cancellationsandrefunds()
	{	
		$data['error'] 						= 	'';

		$data['CMSDATA']					=	$this->front_model->getCMSData('CancellationsandRefunds');

		$this->layouts->set_title($data['CMSDATA']['seo_title']);
		$this->layouts->set_description($data['CMSDATA']['seo_description']);
		$this->layouts->set_keyword($data['CMSDATA']['seo_keyword']);
		$this->layouts->web_view('webview/cancelrefund',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : disclaimer
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for disclaimer
	 * * Date : 09 OCTOBER 2018
	 * * **********************************************************************/
	public function disclaimer()
	{	
		$data['error'] 						= 	'';

		$data['CMSDATA']					=	$this->front_model->getCMSData('Disclaimer');

		$this->layouts->set_title($data['CMSDATA']['seo_title']);
		$this->layouts->set_description($data['CMSDATA']['seo_description']);
		$this->layouts->set_keyword($data['CMSDATA']['seo_keyword']);
		$this->layouts->web_view('webview/disclaimer',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : termsofuse
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for termsofuse
	 * * Date : 09 OCTOBER 2018
	 * * **********************************************************************/
	public function termsofuse()
	{	
		$data['error'] 						= 	'';

		$data['CMSDATA']					=	$this->front_model->getCMSData('TermsofUse');

		$this->layouts->set_title($data['CMSDATA']['seo_title']);
		$this->layouts->set_description($data['CMSDATA']['seo_description']);
		$this->layouts->set_keyword($data['CMSDATA']['seo_keyword']);
		$this->layouts->web_view('webview/termsofuse',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : aboutus
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for aboutus
	 * * Date : 09 OCTOBER 2018
	 * * **********************************************************************/
	public function aboutus()
	{	
		$data['error'] 						= 	'';

		$data['CMSDATA']					=	$this->front_model->getCMSData('AboutUs');

		$this->layouts->set_title($data['CMSDATA']['seo_title']);
		$this->layouts->set_description($data['CMSDATA']['seo_description']);
		$this->layouts->set_keyword($data['CMSDATA']['seo_keyword']);
		$this->layouts->web_view('webview/aboutus',array(),$data);
	}
}