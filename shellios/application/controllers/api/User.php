<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	var $postdata;
	var $user_agent;
	var $request_url;  
	var $method_name;
	
	public function  __construct() 
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('api/user_model','emailtemplate_model','sms_model','notification_model'));
		$this->lang->load('statictext','api');
		$this->load->helper('api/user');
		
		$this->user_agent 		= 	$_SERVER['HTTP_USER_AGENT'];
		$this->request_url 		= 	$_SERVER['REDIRECT_URL'];
		$this->method_name 		= 	$_SERVER['REDIRECT_QUERY_STRING'];
	} 
	
	/** *********************************************************************
	* * Function name : signup
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for signup
	* * Date : 23 MAY 2019
	* * **********************************************************************/
	public function signup()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):
			if($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			else:
				$userPhone					=	trim($this->input->post('userPhone'));
				$userDevice					=	trim($this->input->post('deviceId'));
				$deviceType					=	trim($this->input->post('deviceType'));
				
				$userQuery					=	"SELECT id, user_id, device_type, browse_type, user_phone, token_no FROM ".getTablePrefix()."users WHERE (user_phone = '".$userPhone."') ";
				$userData					=	$this->common_model->getDataByQuery('single',$userQuery);
				
				if($userData <> ""):
					$alparam['token_no']		=	md5($userPhone.date('Y-m-d'));
					$alparam['user_otp']		=	generateRandomString(4,'n');
					$alparam['user_last_login']	=	currentDateTime();
					$alparam['update_date']		=	currentDateTime();
					$alparam['user_last_login_ip']	=	$_SERVER['REMOTE_ADDR'];
					$alparam['device_id']		=	$userDevice;
					$alparam['device_type']		=	$deviceType;
					$this->common_model->editData('users',$alparam,'user_id',$userData['user_id']);
					$returnData['userPhone']	=	$userPhone;
					$returnData['userOtp']		=	$alparam['user_otp'];
					$this->sms_model->sendOtpSmsToUser($userPhone,$alparam['user_otp']);
					
					$result['userData']				=	$returnData;
					echo outPut(lang('SUCESS_STATUS'),lang('SEND_OTP_TO_RMN'),$result);
				else:
					$param['user_type']		=	'register';
					$param['network_id']	=	'';
					$param['device_id']		=	$userDevice;
					$param['device_type']	=	$deviceType;
					$param['browse_type']	=	'app';
					$param['user_phone']	=	addslashes($userPhone);
					$param['user_phone_verify']	=	'N';
					$param['user_otp']		=	generateRandomString(4,'n');
					$param['token_no']      =    md5($userPhone.date('Y-m-d'));
					$param['creation_date']	=	currentDateTime();
					$param['user_last_login']=	currentDateTime();
					$param['update_date']	=	currentDateTime();
					$param['user_last_login_ip']	=	$_SERVER['REMOTE_ADDR'];
					$param['status']		=	'I';
					$userId					=	$this->common_model->addData('users',$param);
					$uParam['encrypt_id']	=	manojEncript($userId);
					$uParam['user_id']		=	generateUniqueId($userId);
					$this->common_model->editData('users',$uParam,'id',$userId);
					$returnData['userPhone']	=	$userPhone;
					$returnData['userOtp']		=	$param['user_otp'];
					$this->sms_model->sendOtpSmsToUser($userPhone,$param['user_otp']);
					$result['userData']			=	$returnData;
					echo outPut(lang('SUCESS_STATUS'),lang('SEND_OTP_TO_RMN'),$result);
				endif;		
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	/** ********************************************************************* 
	* * Function name : otpverification
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for otp verification
	* * Date : 23 MAY 2019
	* * **********************************************************************/
	public function otpverification()
	{ 
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):
			if($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			elseif($this->input->post('userOtp') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('OTP_EMPTY'),$result);
			else:
				$userPhone					=	trim($this->input->post('userPhone'));
				$userOtp					=	trim($this->input->post('userOtp'));
				$deviceId					=	trim($this->input->post('deviceId'));
				$deviceType					=	trim($this->input->post('deviceType'));				

				$userQuery					=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND user_otp = '".$userOtp."'";
				$userData					=	$this->common_model->getDataByQuery('single',$userQuery);
				if($userData <> ""):  
					if($userData['status'] == 'B'):
						echo outPut(lang('ERROR_STATUS'),lang('ACCOUNT_BLOCK'),$result);
					elseif($userData['status'] == 'D'):
						echo outPut(lang('ERROR_STATUS'),lang('ACCOUNT_DELETE'),$result);
					else:
						$param['user_phone_verify']	=	'Y';
						$param['user_otp']			=	'';
						$param['device_id']			=	$deviceId;
						$param['device_type']		=	$deviceType;
						$param['status']			=	'A';
						$param['user_last_login']	=	currentDateTime();
						$param['update_date']		=	currentDateTime();
						$param['user_last_login_ip']	=	$_SERVER['REMOTE_ADDR'];
						$this->common_model->editData('users',$param,'user_id',$userData['user_id']);

						$returnData['userId']		=	$userData['user_id']?$userData['user_id']:'';
						$returnData['userName']		=	$userData['user_name']?stripslashes($userData['user_name']):'';
						$returnData['userEmail']	=	$userData['user_email']?stripslashes($userData['user_email']):'';
						$returnData['userPhone']	=	$userData['user_phone']?stripslashes($userData['user_phone']):'';
						$returnData['userImage']	=	$userData['user_image']?$userData['user_image']:'';
						$returnData['userLastLogin']=	$userData['user_last_login']?date('M, d Y',strtotime($userData['user_last_login'])):'';
						$returnData['userTokenNo']	=	$userData['token_no']?$userData['token_no']:'';
						
						$result['userData']			=	$returnData;						
						if($userData['status'] == 'I'):
							echo outPut(lang('SUCESS_STATUS'),lang('VERIFY_OTP'),$result);
						elseif($userData['status'] == 'A'):
							echo outPut(lang('SUCESS_STATUS'),lang('LOGIN_SUCCESSFULLY'),$result);
						endif;
					endif;
				else:
					echo outPut(lang('ERROR_STATUS'),lang('OTP_INCORRECT'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	/** *********************************************************************
	* * Function name : updateaccount
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for updateaccount
	* * Date : 23 MAY 2019
	* * **********************************************************************/
	public function updateaccount()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result);  
			elseif($this->input->post('userName') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('NAME_EMPTY'),$result);
			elseif($this->input->post('userEmail') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('EMAIL_EMPTY'),$result);
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			else:
				$token_no						=	trim($this->input->post('tokenNo'));
				$userPhone						=	trim($this->input->post('userPhone'));
				$userName						=	trim($this->input->post('userName'));
				$userEmail						=	trim($this->input->post('userEmail'));
				$deviceId						=	trim($this->input->post('deviceId'));
				$deviceType						=	trim($this->input->post('deviceType'));	
				
			    $userQuery						=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$token_no."'";
				$userData						=	$this->common_model->getDataByQuery('single',$userQuery);
				if($userData <> ""):
					$param['user_name']				=	addslashes($userName);
					$param['user_email']			=	addslashes($userEmail);
					$param['user_last_login']		=	currentDateTime();
					$param['update_date']			=	currentDateTime();
					$param['user_last_login_ip']	=	$_SERVER['REMOTE_ADDR'];
					$param['device_id']				=	$deviceId;
					$param['device_type']			=	$deviceType;
					$this->common_model->editData('users',$param,'user_id',$userData['user_id']);

					$user1Query					=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$token_no."'";
					$user1Data						=	$this->common_model->getDataByQuery('single',$user1Query);

					$returnData['userId']			=	$user1Data['user_id']?$user1Data['user_id']:'';
					$returnData['userName']			=	$user1Data['user_name']?stripslashes($user1Data['user_name']):'';
					$returnData['userEmail']		=	$user1Data['user_email']?stripslashes($user1Data['user_email']):'';
					$returnData['userPhone']		=	$user1Data['user_phone']?stripslashes($user1Data['user_phone']):'';
					$returnData['userImage']		=	$user1Data['user_image']?$user1Data['user_image']:'';
					$returnData['userLastLogin']	=	$user1Data['user_last_login']?date('M, d Y',strtotime($user1Data['user_last_login'])):'';
					$returnData['userTokenNo']		=	$user1Data['token_no']?$user1Data['token_no']:'';

					$result['userData']				=	$returnData;
					echo outPut(lang('SUCESS_STATUS'),lang('PROFILE_UPDATE_SUCCESS'),$result);
				else:
					echo outPut(lang('ERROR_STATUS'),lang('INVALID_MOBILE'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}

	/** ********************************************************************* 
	* * Function name : updateprofileimage
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for update profile image
	* * Date : 24 MAY 2019
	* * **********************************************************************/
	public function updateprofileimage()
	{ 
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 						= 	array();
		$returnData 					= 	array();
		if(requestAuthenticate(APIKEY)):
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result);   
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('USERID_EMPTY'),$result);
			elseif($_FILES['userImage']['name'] == ''):
				echo outPut(lang('ERROR_STATUS'),lang('USERIMG_EMPTY'),$result);
			else:
				$userPhone					=	trim($this->input->post('userPhone'));
				$token_no					=	trim($this->input->post('tokenNo'));
				
				$userQuery					=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$token_no."'";
				$userData					=	$this->common_model->getDataByQuery('single',$userQuery);
				
				if($userData <> ""):					
					if($_FILES['userImage']['name']):
						$ufileName				= 	$_FILES['userImage']['name'];
						$utmpName				= 	$_FILES['userImage']['tmp_name'];
						$ufileExt         		= 	pathinfo($ufileName);
						$unewFileName 			= 	time().'.'.$ufileExt['extension'];
						$this->load->library("upload_crop_img");
						$uimageLink				=	$this->upload_crop_img->_upload_image_from_app($ufileName,$utmpName,$unewFileName,'userImage','');
						$param['user_image']	=	$uimageLink;							
						$this->common_model->editData('users',$param,'user_id',$userData['user_id']);
					endif;				
					$user1Query				    =	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$token_no."'";					
					$user1Data				    =	$this->common_model->getDataByQuery('single',$user1Query);	
					
					$returnData['userId']			=	$user1Data['user_id']?$user1Data['user_id']:'';
					$returnData['userName']			=	$user1Data['user_name']?stripslashes($user1Data['user_name']):'';
					$returnData['userEmail']		=	$user1Data['user_email']?stripslashes($user1Data['user_email']):'';
					$returnData['userPhone']		=	$user1Data['user_phone']?stripslashes($user1Data['user_phone']):'';
					$returnData['userImage']		=	$user1Data['user_image']?$user1Data['user_image']:'';
					$returnData['userLastLogin']	=	$user1Data['user_last_login']?date('M, d Y',strtotime($user1Data['user_last_login'])):'';
					$returnData['userTokenNo']		=	$user1Data['token_no']?$user1Data['token_no']:'';

					$result['userData']			=	$returnData;			
					echo outPut(lang('SUCESS_STATUS'),lang('PROFILE_UPDATE_SUCCESS'),$result);					
				else:
					echo outPut(lang('ERROR_STATUS'),lang('INVALID_MOBILE'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	/** *********************************************************************
	* * Function name : sociallogin
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for social login
	* * Date : 24 MAY 2019
	* * **********************************************************************/
	public function sociallogin()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):  
			if($this->input->post('userName') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('NAME_EMPTY'),$result);
			elseif($this->input->post('userEmail') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('EMAIL_EMPTY'),$result);
			elseif($this->input->post('loginType') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('LOGINTYPE_EMPTY'),$result);
			elseif(!in_array($this->input->post('loginType'), array('facebook','gplus'))):
				echo outPut(lang('ERROR_STATUS'),lang('LOGIN_TYPE_INCORRECT'),$result);
			elseif($this->input->post('networkId') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('NETWORKID_EMPTY'),$result);
			else:
				$userName					=	trim($this->input->post('userName'));
				$userEmail					=	trim($this->input->post('userEmail'));
				$loginType					=	trim($this->input->post('loginType'));
				$networkId					=	trim($this->input->post('networkId'));
				$deviceId					=	trim($this->input->post('deviceId'));
				$deviceType					=	trim($this->input->post('deviceType'));
				
				$userQuery					=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, user_phone_verify, status FROM ".getTablePrefix()."users WHERE user_email = '".$userEmail."'";
				$userData					=	$this->common_model->getDataByQuery('single',$userQuery);
				if($userData <> ""):
					if($userData['status'] == 'B'):
						echo outPut(lang('ERROR_STATUS'),lang('ACCOUNT_BLOCK'),$result);
					elseif($userData['status'] == 'D'):
						echo outPut(lang('ERROR_STATUS'),lang('ACCOUNT_DELETE'),$result);
					elseif($userData['status'] == 'I'):
						$returnData['userEmail']	=	$userData['user_email'];
						$returnData['userTokenNo']	=	$userData['token_no'];
						$result['socialLoginData']	=	$returnData;
						echo outPut(lang('SUCESS_STATUS'),lang('ACCOUNT_INACTIVE'),$result);
					else:
						if($userData['user_phone_verify'] == 'N'):
							$returnData['userEmail']	=	$userData['user_email'];
							$returnData['userTokenNo']	=	$userData['token_no'];
							$result['socialLoginData']	=	$returnData;
							echo outPut(lang('SUCESS_STATUS'),lang('ACCOUNT_INACTIVE'),$result);
						else:
							$uParam['user_type']	=	$loginType;
							$uParam['network_id']	=	$networkId;
							$uParam['device_id']	=	$deviceId;
							$uParam['device_type']	=	$deviceType;
							$uParam['user_name']	=	addslashes($userName);
							$uParam['token_no']		=	md5($userEmail.date('Y-m-d'));
							$uParam['user_last_login']=	currentDateTime();
							$uParam['update_date']	=	currentDateTime();
							$uParam['user_last_login_ip']	=	$_SERVER['REMOTE_ADDR'];
							$this->common_model->editData('users',$uParam,'user_id',$userData['user_id']);
							
							$user1Query				=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_email = '".$userEmail."' AND status = 'A'";
							$user1Data				=	$this->common_model->getDataByQuery('single',$user1Query);

							$returnData['userId']	=	$user1Data['user_id']?$user1Data['user_id']:'';
							$returnData['userName']	=	$user1Data['user_name']?stripslashes($user1Data['user_name']):'';
							$returnData['userEmail']=	$user1Data['user_email']?stripslashes($user1Data['user_email']):'';
							$returnData['userPhone']=	$user1Data['user_phone']?stripslashes($user1Data['user_phone']):'';
							$returnData['userImage']=	$user1Data['user_image']?$user1Data['user_image']:'';
							$returnData['userLastLogin']=	$user1Data['user_last_login']?date('M, d Y',strtotime($user1Data['user_last_login'])):'';
							$returnData['userTokenNo']=	$user1Data['token_no']?$user1Data['token_no']:'';

							$result['userData']		=	$returnData;
							echo outPut(lang('SUCESS_STATUS'),lang('USER_PROFILE_UPDATE_SUCCESS'),$result);
						endif;
					endif;
				else:
					$param['user_type']		=	$loginType;
					$param['network_id']	=	$networkId;
					$param['device_id']		=	$deviceId;
					$param['device_type']	=	$deviceType;
					$param['browse_type']	=	'app';
					$param['user_name']		=	addslashes($userName);
					$param['user_email']	=	addslashes($userEmail);
					$param['user_phone_verify']	=	'N';
					$param['token_no']      =    md5($userEmail.date('Y-m-d'));
					$param['creation_date']	=	currentDateTime();
					$param['user_last_login']=	currentDateTime();
					$param['update_date']	=	currentDateTime();
					$param['user_last_login_ip']=	$_SERVER['REMOTE_ADDR'];
					$param['status']		=	'I';
					$lastUserId				=	$this->common_model->addData('users',$param);
					$uParam['encrypt_id']	=	manojEncript($lastUserId);
					$uParam['user_id']		=	generateUniqueId($lastUserId);
					$this->common_model->editData('users',$uParam,'id',$lastUserId);
					
					$returnData['userEmail']	=	$userEmail;
					$returnData['userTokenNo']	=	$param['token_no'];
					$result['socialLoginData']	=	$returnData;
					echo outPut(lang('SUCESS_STATUS'),lang('NUMBER_INPUT'),$result);
				endif;	
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	/** *********************************************************************
	* * Function name : socialsignupactivation
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for socialsignupactivation
	* * Date : 24 MAY 2019
	* * **********************************************************************/
	public function socialsignupactivation()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result); 
			elseif($this->input->post('userEmail') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('EMAIL_EMPTY'),$result);
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			else:
				$tokenNo					=	trim($this->input->post('tokenNo'));
				$userEmail					=	trim($this->input->post('userEmail'));
				$userPhone					=	trim($this->input->post('userPhone'));
				$userDevice					=	trim($this->input->post('deviceId'));
				$deviceType					=	trim($this->input->post('deviceType'));
				
				$user2Query					=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, user_phone_verify, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."'";
				$user2Data					=	$this->common_model->getDataByQuery('single',$user2Query);
				if($user2Data == ''):
					$userQuery				=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, user_phone_verify, status FROM ".getTablePrefix()."users WHERE user_email = '".$userEmail."' AND token_no = '".$tokenNo."'";
				
					$userData				=	$this->common_model->getDataByQuery('single',$userQuery);
					if($userData <> ""):
						$alparam['user_otp']		=	generateRandomString(4,'n');
						$alparam['update_date']		=	currentDateTime();
						$alparam['user_phone']		=	$userPhone;
						$alparam['device_id']		=	$userDevice;
						$alparam['device_type']		=	$deviceType;
						$this->common_model->editData('users',$alparam,'user_id',$userData['user_id']);
						$returnData['userPhone']	=	$userPhone;
						$returnData['userOtp']		=	$alparam['user_otp'];
						$this->sms_model->sendOtpSmsToUser($userPhone,$alparam['user_otp']);
						
						$result['userData']				=	$returnData;
						echo outPut(lang('SUCESS_STATUS'),lang('SEND_OTP_TO_RMN'),$result);
					else:
						echo outPut(lang('ERROR_STATUS'),lang('CREDENTIAL_ERROR'),$result);
					endif;		
				else:
					echo outPut(lang('ERROR_STATUS'),lang('ALREADY_REGISTER'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}

	/** *********************************************************************
	* * Function name : feedback
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for Send Feedback
	* * Date : 24 MAY 2019
	* * **********************************************************************/
	public function feedback()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)): 
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result); 
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			elseif($this->input->post('feedbackMessage') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MESSAGE_EMPTY'),$result);
			else:
				$tokenNo					=	trim($this->input->post('tokenNo'));
				$userPhone					=	trim($this->input->post('userPhone'));
				$feedbackMessage			=	trim($this->input->post('feedbackMessage'));
				
				$userQuery						=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$tokenNo."'";
				$userData						=	$this->common_model->getDataByQuery('single',$userQuery);
				
				if($userData <> ""):
					$param['userPhone']			= 	$userPhone;
					$param['message']			= 	$feedbackMessage;
					$param['creation_date']		=	currentDateTime();
					$param['status']			=	'Y';
					$lastInsertId				=	$this->common_model->addData('feedback',$param);
					$Uparam['encrypt_id']		=	generateUniqueId($lastInsertId);
					$Uwhere['id']				=	$lastInsertId;
					$this->common_model->editDataByMultipleCondition('feedback',$Uparam,$Uwhere);
					//$this->sms_model->contactUsSmsToAdmin();
					//$this->emailtemplate_model->ContactUsMailToAdmin($param);
					$returnData['FeedbackMessage']		=	$feedbackMessage;
					$result['FeedbackData']			=	$returnData;
					echo outPut(lang('SUCESS_STATUS'),lang('FEEDBACK_SUCCESS'),$result);
				else:
					echo outPut(lang('ERROR_STATUS'),lang('INVALID_MOBILE'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	/** *********************************************************************
	* * Function name : Faq
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for Send Faq
	* * Date : 25 MAY 2019
	* * **********************************************************************/
	public function faq()
	{	 
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$returnallData	=	array();
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):  
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result); 
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			else:
				$tokenNo					=	trim($this->input->post('tokenNo'));
				$userPhone					=	trim($this->input->post('userPhone'));
				
				$userQuery						=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$tokenNo."'";
				$userData						=	$this->common_model->getDataByQuery('single',$userQuery);
				if($userData <> ""):
					$user1Query					=	"SELECT id,questions,answer FROM ".getTablePrefix()."faq WHERE status = 'A'";
					$user1Data					=	$this->common_model->getDataByQuery('multiple',$user1Query);
					foreach($user1Data as $user1Info):
						$returnData['id']			=	$user1Info['id'];
						$returnData['questions']	=	$user1Info['questions'];
						$returnData['answer']		=	$this->replace_p_tags($user1Info['answer']);
						array_push($returnallData,$returnData);
					endforeach;
					if($user1Data<>""):
						$result['FaqquestionsanswerList']			=	$returnallData;						
						echo outPut(lang('SUCESS_STATUS'),lang('FAQ_LIST'),$result);
					else:
						echo outPut(lang('ERROR_STATUS'),lang('ERROR_MESSAGE'),$result);
					endif;
				else:
					echo outPut(lang('ERROR_STATUS'),lang('INVALID_MOBILE'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	public function replace_p_tags($text='')
	{
		$prearray	=	array('<p>','</p>');
		$posarray	=	array('','');
		return str_replace($prearray,$posarray,$text);
	}
	
	/***********************************************************************
	** Function name : replace_in_array
	** Developed By : Ravi Kumar
	** Purpose  : This function used for replace in array
	** Date : 28 FEBRUARY 2019
	************************************************************************/
	/*function replace_in_array($find, $replace, &$array) {
		    array_walk_recursive($array, function(&$array) use($find, $replace) {
		        if($array && $find && strpos($array,$find)) {
		             $array= str_replace($find, $replace, $array);
		        }
		    });
		    return $array;
		}*/
	
	/** *********************************************************************
	* * Function name : productlist
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for productlist
	* * Date : 28 MAY 2019
	* * **********************************************************************/
	/*public function productlist()
	{	//print_r(APIKEY); die;
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):  
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result); 
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			else:
				$tokenNo					=	trim($this->input->post('tokenNo'));
				$userPhone					=	trim($this->input->post('userPhone'));
				
				$userQuery						=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$tokenNo."'";
				$userData						=	$this->common_model->getDataByQuery('single',$userQuery);
				//print_r($userData); die;
				if($userData <> ""):
					$user1Query					=	"SELECT id, prod_id, prod_brand_name, prod_sr_no FROM ".getTablePrefix()."product WHERE status = 'A' AND userphone = ''";
					//echo $user1Query;die;
					$user1Data					=	$this->common_model->getDataByQuery('multiple',$user1Query);
					//print_r($user1Data); die;
					if($user1Data<>""):
						$result['ProductList']			=	$user1Data;						
						echo outPut(lang('SUCESS_STATUS'),lang('PRODUCT_LIST'),$result);
					else:
						echo outPut(lang('ERROR_STATUS'),lang('ERROR_MESSAGE'),$result);
					endif;
				else:
					echo outPut(lang('ERROR_STATUS'),lang('INVALID_MOBILE'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}*/
	
	/** *********************************************************************
	* * Function name : productlinking
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for productlinking
	* * Date : 28 MAY 2019
	* * **********************************************************************/
	public function productlinking()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):  
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result); 
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			elseif($this->input->post('prodSrNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('PRODUCT_SR_EMPTY'),$result);
			else:
				$tokenNo					=	trim($this->input->post('tokenNo'));
				$userPhone					=	trim($this->input->post('userPhone'));
				$prodSrNo					=	trim($this->input->post('prodSrNo'));
				
				$userQuery					=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$tokenNo."'";
				$userData					=	$this->common_model->getDataByQuery('single',$userQuery);
				
				if($userData <> ""):
					$user1Query				=	"SELECT id, prod_id, prod_brand_name, prod_sr_no FROM ".getTablePrefix()."product WHERE prod_sr_no = '".$prodSrNo."' AND status = 'A' AND userphone = ''";
					$user1Data				=	$this->common_model->getDataByQuery('single',$user1Query);
					if($user1Data <> ""):
						$alparam['productstatus']		=	"Sold";
						//$alparam['purchagedate']		=	currentDateTime();
						$alparam['userphone']			=	$userPhone;
						$this->common_model->editData('product',$alparam,'prod_sr_no',$user1Data['prod_sr_no']);
						$user2Query			=	"SELECT id, prod_id, prod_brand_name, prod_sr_no, prod_color, prod_material, prod_warranty, purchagedate, prod_image FROM ".getTablePrefix()."product WHERE status = 'A' AND userphone = '".$userPhone."'";
						$user2Data			=	$this->common_model->getDataByQuery('multiple',$user2Query);
						$result['ProductLinkedList']			=	$user2Data;	
						echo outPut(lang('SUCESS_STATUS'),lang('PRODUCT_LINKED'),$result);
					else:
						echo outPut(lang('ERROR_STATUS'),lang('PRODUCT_ALREADY_LINKED'),$result);
					endif;
				else:
					echo outPut(lang('ERROR_STATUS'),lang('INVALID_MOBILE'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	/** *********************************************************************
	* * Function name : linkedproductlist
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for linkedproductlist
	* * Date : 29 MAY 2019
	* * **********************************************************************/
	public function linkedproductlist()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):  
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result); 
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			else:
				$tokenNo					=	trim($this->input->post('tokenNo'));
				$userPhone					=	trim($this->input->post('userPhone'));
				
				$userQuery					=	"SELECT encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$tokenNo."'";
				$userData					=	$this->common_model->getDataByQuery('single',$userQuery);
				
				if($userData <> ""):
					$user1Query				=	"SELECT id, prod_id, prod_brand_name, prod_sr_no, prod_color, prod_material, prod_warranty, purchagedate, prod_image FROM ".getTablePrefix()."product WHERE status = 'A' AND userphone = '".$userPhone."'";
					$user1Data				=	$this->common_model->getDataByQuery('multiple',$user1Query);
					if($user1Data <> ""):
						$result['ProductLinkedList']			=	$user1Data;	
						echo outPut(lang('SUCESS_STATUS'),lang('ADD_PRODUCT_LIST'),$result);
					else:
						echo outPut(lang('ERROR_STATUS'),lang('PRODUCT_ID'),$result);
					endif;
				else:
					echo outPut(lang('ERROR_STATUS'),lang('INVALID_MOBILE'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	/** *********************************************************************
	* * Function name : servicerequest
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for servicerequest
	* * Date : 29 MAY 2019
	* * **********************************************************************/
	public function servicerequest()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):  
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result); 
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			elseif($this->input->post('prodSrNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('PRODUCT_SR_EMPTY'),$result);
			elseif($this->input->post('Id') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('PRODUCT_EMPTY'),$result);
			else:
				$tokenNo					=	trim($this->input->post('tokenNo'));
				$userPhone					=	trim($this->input->post('userPhone'));
				$prodSrNo					=	trim($this->input->post('prodSrNo'));
				$productId					=	trim($this->input->post('Id'));

				$userQuery					=	"SELECT id, encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$tokenNo."'";
				$userData					=	$this->common_model->getDataByQuery('single',$userQuery);
				
				if($userData <> ""):
					$user1Query				=	"SELECT id, prod_id FROM ".getTablePrefix()."product WHERE prod_sr_no = '".$prodSrNo."' AND id = '".$productId."' AND status = 'A'";
					$user1Data				=	$this->common_model->getDataByQuery('single',$user1Query);
					if($user1Data <> ""):
						$user2Query				=	"SELECT id, prod_id FROM ".getTablePrefix()."product WHERE userphone = '".$userPhone."' AND prod_sr_no = '".$prodSrNo."' AND id = '".$productId."' AND status = 'A'";
						$user2Data				=	$this->common_model->getDataByQuery('single',$user2Query);
						
						if($user2Data <> ""):
							$user3Query			=	"SELECT id, service_request_status FROM ".getTablePrefix()."service_request WHERE  userPhone = '".$userPhone."' AND product_sr_no = '".$prodSrNo."' AND service_request_status != 'Closed' AND status = 'A'";
							$user3Data			=	$this->common_model->getDataByQuery('single',$user3Query);
							if($user3Data <> ""):
								if($user3Data['service_request_status'] == 'Created'):
									$user4Query			=	"SELECT sr.id, sr.srv_req_id, sr.user_ID, sr.product_id, sr.product_sr_no, sr.userPhone, sr.service_request_date, sr.service_pending_date, sr.service_closed_date, sr.service_request_status, p.prod_brand_name, p.prod_image FROM ".getTablePrefix()."service_request AS sr LEFT JOIN ".getTablePrefix()."product AS p ON sr.product_sr_no = p.prod_sr_no WHERE sr.userPhone = '".$userPhone."' AND sr.status = 'A'";
									
									$user4Data			=	$this->common_model->getDataByQuery('multiple',$user4Query);
									$result['serviceRequestList']			=	$user4Data;
									echo outPut(lang('SUCESS_STATUS'),lang('ALREADY_SERVICE_REQUEST'),$result);
								elseif($user3Data['service_request_status'] == 'Pending'):
									$user4Query			=	"SELECT sr.id, sr.srv_req_id, sr.user_ID, sr.product_id, sr.product_sr_no, sr.userPhone, sr.service_request_date, sr.service_pending_date, sr.service_closed_date, sr.service_request_status, p.prod_brand_name, p.prod_image FROM ".getTablePrefix()."service_request AS sr LEFT JOIN ".getTablePrefix()."product AS p ON sr.product_sr_no = p.prod_sr_no WHERE sr.userPhone = '".$userPhone."' AND sr.status = 'A'";
									
									$user4Data			=	$this->common_model->getDataByQuery('multiple',$user4Query);
									$result['serviceRequestList']			=	$user4Data;
									echo outPut(lang('SUCESS_STATUS'),lang('ALREADY_SERVICE_REQUEST'),$result);
								else:
									$param['user_ID']			= 	$userData['id'];
									$param['userPhone']			= 	$userPhone;
									$param['product_id']		= 	$productId;
									$param['product_sr_no']		= 	$prodSrNo;
									$param['service_request_date']=	currentDateTime();
									$param['creation_date']		=	currentDateTime();
									$param['status']			=	'A';
									$param['service_request_status '] = 'Created';
									$lastInsertId				=	$this->common_model->addData('service_request',$param);				
									$Uparam['encrypt_id']		=	manojEncript($lastInsertId);
									$Uparam['srv_req_id']		=	generateUniqueId($lastInsertId);
									$Uwhere['id']				=	$lastInsertId;
									$this->common_model->editDataByMultipleCondition('service_request',$Uparam,$Uwhere);
									//$this->sms_model->contactUsSmsToAdmin();
									//$this->emailtemplate_model->ContactUsMailToAdmin($param);
									//$returnData['serviceRequestId']				=	$Uparam['srv_req_id'];
									$user4Query			=	"SELECT sr.id, sr.srv_req_id, sr.user_ID, sr.product_id, sr.product_sr_no, sr.userPhone, sr.service_request_date, sr.service_pending_date, sr.service_closed_date, sr.service_request_status, p.prod_brand_name, p.prod_image FROM ".getTablePrefix()."service_request AS sr LEFT JOIN ".getTablePrefix()."product AS p ON sr.product_sr_no = p.prod_sr_no WHERE sr.userPhone = '".$userPhone."' AND sr.status = 'A'";
									
									$user4Data			=	$this->common_model->getDataByQuery('multiple',$user4Query);
									$result['serviceRequestList']			=	$user4Data;
									echo outPut(lang('SUCESS_STATUS'),lang('SELLERREQUEST_SUCCESS'),$result);
								endif;
							else:
								$param['user_ID']			= 	$userData['id'];
								$param['userPhone']			= 	$userPhone;
								$param['product_id']		= 	$productId;
								$param['product_sr_no']		= 	$prodSrNo;
								$param['service_request_date']=	currentDateTime();
								$param['creation_date']		=	currentDateTime();
								$param['status']			=	'A';
								$param['service_request_status '] = 'Created';
								$lastInsertId				=	$this->common_model->addData('service_request',$param);				
								$Uparam['encrypt_id']		=	manojEncript($lastInsertId);
								$Uparam['srv_req_id']		=	generateUniqueId($lastInsertId);
								$Uwhere['id']				=	$lastInsertId;
								$this->common_model->editDataByMultipleCondition('service_request',$Uparam,$Uwhere);
								//$this->sms_model->contactUsSmsToAdmin();
								//$this->emailtemplate_model->ContactUsMailToAdmin($param);
								//$returnData['serviceRequestId']				=	$Uparam['srv_req_id'];
								$user4Query			=	"SELECT sr.id, sr.srv_req_id, sr.user_ID, sr.product_id, sr.product_sr_no, sr.userPhone, sr.service_request_date, sr.service_pending_date, sr.service_closed_date, sr.service_request_status, p.prod_brand_name, p.prod_image FROM ".getTablePrefix()."service_request AS sr LEFT JOIN ".getTablePrefix()."product AS p ON sr.product_sr_no = p.prod_sr_no WHERE sr.userPhone = '".$userPhone."' AND sr.status = 'A'";
								
								$user4Data			=	$this->common_model->getDataByQuery('multiple',$user4Query);
								$result['serviceRequestList']			=	$user4Data;
								echo outPut(lang('SUCESS_STATUS'),lang('SELLERREQUEST_SUCCESS'),$result);
							endif;
						else:
							echo outPut(lang('ERROR_STATUS'),lang('PRODUCT_DIFFRENT'),$result);
						endif;
					else:
						echo outPut(lang('ERROR_STATUS'),lang('INVALID_PSERIAL_ID'),$result);
					endif;
				else:
					echo outPut(lang('ERROR_STATUS'),lang('INVALID_MOBILE'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	/** *********************************************************************
	* * Function name : getlistserreq
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for getlistserreq
	* * Date : 30 MAY 2019
	* * **********************************************************************/
	public function getlistserreq()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):  
			if($this->input->post('tokenNo') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('TOKEN_NO'),$result); 
			elseif($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			else:
				$tokenNo					=	trim($this->input->post('tokenNo'));
				$userPhone					=	trim($this->input->post('userPhone'));
				
				$userQuery					=	"SELECT id, encrypt_id, user_id, user_type, device_id, browse_type, token_no, user_name, user_email, user_phone, user_address, user_image, user_last_login, status FROM ".getTablePrefix()."users WHERE user_phone = '".$userPhone."' AND status = 'A' AND token_no = '".$tokenNo."'";
				$userData					=	$this->common_model->getDataByQuery('single',$userQuery);
				if($userData <> ""):
					$user1Query			=	"SELECT sr.id, sr.srv_req_id, sr.user_ID, sr.product_id, sr.product_sr_no, sr.userPhone, sr.service_request_date, sr.service_pending_date, sr.service_closed_date, sr.service_request_status, p.prod_brand_name, p.prod_image FROM ".getTablePrefix()."service_request AS sr LEFT JOIN ".getTablePrefix()."product AS p ON sr.product_sr_no = p.prod_sr_no WHERE sr.userPhone = '".$userPhone."' AND sr.status = 'A'";
									
					$user1Data			=	$this->common_model->getDataByQuery('multiple',$user1Query);
					if($userData <> ""):
						$result['serviceRequestList']			=	$user1Data;
						echo outPut(lang('SUCESS_STATUS'),lang('GET_SERVICE_REQUEST'),$result);
					else:
						echo outPut(lang('ERROR_STATUS'),lang('SERVICE_REQUEST_ERROR'),$result);
					endif;
				else:
					echo outPut(lang('ERROR_STATUS'),lang('INVALID_MOBILE'),$result);
				endif;
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
	
	/** *********************************************************************
	* * Function name : retryotp
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for retry otp
	* * Date : 30 MAY 2019
	* * **********************************************************************/
	public function retryotp()
	{	
		$this->generatelogs->putLog('APP',logOutPut($_POST));
		$result 							= 	array();
		$returnData 						= 	array();
		if(requestAuthenticate(APIKEY)):
			if($this->input->post('userPhone') == ''):
				echo outPut(lang('ERROR_STATUS'),lang('MOBILE_EMPTY'),$result);
			else:
				$userPhone					=	trim($this->input->post('userPhone'));
				
				$userQuery					=	"SELECT id, user_id, device_type, browse_type, user_phone, token_no FROM ".getTablePrefix()."users WHERE (user_phone = '".$userPhone."') ";
				$userData					=	$this->common_model->getDataByQuery('single',$userQuery);
				if($userData <> ""):
					$returnData['userPhone']	=	$userPhone;
					$this->sms_model->sendOtpRetrySms($userPhone);
					
					$result['userData']				=	$returnData;
					echo outPut(lang('SUCESS_STATUS'),lang('SEND_OTP_TO_RMN'),$result);
				else:
					echo outPut(lang('ERROR_STATUS'),lang('USER_NOT_REGISTER'),$result);
				endif;		
			endif;
		else:
			echo outPut(lang('ERROR_STATUS'),lang('INVALID_APIKEY'),$result);
		endif;
	}
}