<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminservicerequest extends CI_Controller {

	public function  __construct()  
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('adminauth_model','admin_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'admin');
		$this->load->helper('admin');
	} 
	
	/* * *********************************************************************
	 * * Function name : customer
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for customer
	 * * Date : 22 NOVEMVER 2018
	 * * **********************************************************************/
	public function index()
	{	
		$this->adminauth_model->authCheck('admin','view_data');
		$this->adminauth_model->getPermissionType($data); 
		$data['error'] 						= 	'';
		$data['activeMenu'] 				= 	'adminservicerequest';
		$data['activeSubMenu'] 				= 	'adminservicerequest';
		
		if($this->input->get('searchValue')):
			$sValue							=	$this->input->get('searchValue');
			$whereCon['like']		 		= 	"(ur.userPhone LIKE '%".$sValue."%' 
			                   					  OR ur.product_sr_no LIKE '%".$sValue."%'
			                   					  )";
			$data['searchValue'] 			= 	$sValue;
		else:
			$whereCon['like']		 		= 	"";
			$data['searchValue'] 			= 	'';
		endif;
		
		$whereCon['where']		 			= 	"";		
		$shortField 						= 	'ur.id ASC';
		
		$baseUrl 							= 	$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index';
		$this->session->set_userdata('servicerequestAdminData',currentFullUrl());
		$qStringdata						=	explode('?',currentFullUrl());
		$suffix								= 	$qStringdata[1]?'?'.$qStringdata[1]:'';
		$tblName 							= 	'service_request as ur';
		$con 								= 	'';
		$totalRows 							= 	$this->admin_model->selectservicerequestData('count',$tblName,$whereCon,$shortField,'0','0');
		
		if($this->input->get('showLength') == 'All'):
			$perPage	 					= 	$totalRows;
			$data['perpage'] 				= 	$this->input->get('showLength');  
		elseif($this->input->get('showLength')):
			$perPage	 					= 	$this->input->get('showLength'); 
			$data['perpage'] 				= 	$this->input->get('showLength'); 
		else:
			$perPage	 					= 	SHOW_NO_OF_DATA;
			$data['perpage'] 				= 	SHOW_NO_OF_DATA; 
		endif;
		$uriSegment 						= 	getUrlSegment();
	    $data['PAGINATION']					=	adminPagination($baseUrl,$suffix,$totalRows,$perPage,$uriSegment);

       if ($this->uri->segment(getUrlSegment())):
           $page = $this->uri->segment(getUrlSegment());
       else:
           $page = 0;
       endif;
		
		$data['forAction'] 					= 	$baseUrl; 
		if($totalRows):
			$first							=	($page)+1;
			$data['first']					=	$first;
			$last							=	(($page)+$data['perpage'])>$totalRows?$totalRows:(($page)+$data['perpage']);
			$data['noOfContent']			=	'Showing '.$first.'-'.$last.' of '.$totalRows.' items';
		else:
			$data['first']					=	1;
			$data['noOfContent']			=	'';
		endif;
		
		$data['ALLDATA'] 					= 	$this->admin_model->selectservicerequestData('data',$tblName,$whereCon,$shortField,$perPage,$page); 
		$this->layouts->set_title('Manage Customer Details');
		$this->layouts->admin_view('admin/servicerequest/index',array(),$data);
	}	// END OF FUNCTION
	/* * *********************************************************************
	 * * Function name : addeditdata
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for add edit data
	 * * Date : 29 APRIL 2019
	 * * **********************************************************************/
	public function addeditdata($editId='')
	{		
		$data['error'] 				= 	'';
		$data['activeMenu'] 				= 	'servicerequest';
		$data['activeSubMenu'] 				= 	'servicerequest';
		
		if($editId):
			$this->adminauth_model->authCheck('admin','edit_data');
			$data['EDITDATA']		=	$this->common_model->getDataByParticularField('service_request','srv_req_id',$editId);
		else:
			$this->adminauth_model->authCheck('admin','add_data');
		endif;
		
		if($this->input->post('SaveChanges')): //echo "<pre>"; print_r($_POST); die;
			$error					=	'NO';
			$this->form_validation->set_rules('userstatus', 'user Status', 'trim');
			if($this->form_validation->run() && $error == 'NO'):   
				$param['service_request_status']				= 	addslashes($this->input->post('userstatus'));
				if($this->input->post('CurrentIdForUnique') ==''): 
					$param['creation_date']		=	currentDateTime();
					$param['created_by']		=	$this->session->userdata('SHELLIOS_ADMIN_ID');
					$param['status']			=	'A';
					$lastInsertId				=	$this->common_model->addData('service_request',$param);
					$Uparam['encrypt_id']		=	manojEncript($lastInsertId);
					$Uparam['srv_req_id']		=	generateUniqueId($lastInsertId);
					$Uwhere['id']				=	$lastInsertId;
					$this->common_model->editDataByMultipleCondition('service_request',$Uparam,$Uwhere);
					$this->session->set_flashdata('alert_success',lang('addsuccess'));
				else:   
					$sellerId					=	$this->input->post('CurrentIdForUnique');
					//$param['service_request_date']		=	currentDateTime();
					if($param['service_request_status'] == Pending):
						$param['service_pending_date']		=	currentDateTime();
					elseif($param['service_request_status'] == Closed):
						$param['service_closed_date']		=	currentDateTime();
					endif;
					$param['update_date']		=	currentDateTime();
					$param['updated_by']		=	$this->session->userdata('SHELLIOS_ADMIN_ID');
					$this->common_model->editData('service_request',$param,'srv_req_id',$sellerId);
					$this->session->set_flashdata('alert_success',lang('updatesuccess')); 
				endif;
				redirect(correctLink('servicerequestAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
			endif;
		endif;
		$this->layouts->set_title('Edit service request');
		$this->layouts->admin_view('admin/servicerequest/addeditdata',array(),$data);
	}	// END OF FUNCTION

	/* * *********************************************************************
	 * * Function name : viewcustomer
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for view customer details
	 * * Date : 22 NOVEMVER 2018
	 * * **********************************************************************/
	public function viewcustomer($editId='')
	{		
		$data['error'] 				= 	'';
		$data['activeMenu'] 		= 	'adminuser';
		$data['activeSubMenu'] 		= 	'adminuser';
		
		if($editId):
			$this->adminauth_model->authCheck('admin','edit_data');
			//$data['VIEWDATA']		=	$this->common_model->getDataByParticularField('service_request','srv_req_id',$editId);
			
			$kycQuery				=	"SELECT user.user_name, user.user_email, user.user_phone,prod.prod_brand_name,prod.prod_sr_no,sr.service_request_date,sr.service_pending_date,sr.service_closed_date,sr.service_request_status  FROM ".getTablePrefix()."service_request as sr
										LEFT JOIN ".getTablePrefix()."product as prod ON sr.product_id=prod.id
										LEFT JOIN ".getTablePrefix()."users as user ON sr.user_ID=user.id
										 WHERE srv_req_id = '".$editId."'"; 
			$data['VIEWDATA']	=	$this->common_model->getDataByQuery('single',$kycQuery); 
			//echo "<pre>"; print_r($data['VIEWDATA']); die; 
		else:
			redirect(correctLink('servicerequestAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
		endif;
		
		$this->layouts->set_title('View Service Request Details');
		$this->layouts->admin_view('admin/servicerequest/viewcustomer',array(),$data);
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : changestatus
	** Developed By : Manoj Kumar
	** Purpose  : This function used for change status
	** Date : 22 NOVEMVER 2018
	************************************************************************/
	function changestatus($changeStatusId='',$statusType='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$param['status']		=	$statusType;
		$this->common_model->editData('service_request',$param,'srv_req_id',$changeStatusId);
		
		$this->session->set_flashdata('alert_success',lang('statussuccess'));
		
		redirect(correctLink('servicerequestAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
	
	/***********************************************************************
	** Function name : deleteData
	** Developed By : Ravi Kumar
	** Purpose  : This function used for delete data
	** Date : 30 MAY 2019
	************************************************************************/
	function deleteData($deleteId='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$this->common_model->deleteParticularData('service_request','srv_req_id',$deleteId);
		
		$this->session->set_flashdata('alert_success',lang('deletesuccess'));
		
		redirect(correctLink('servicerequestAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
}
