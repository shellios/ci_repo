<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminfeedback extends CI_Controller {

	public function  __construct()  
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('adminauth_model','admin_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'admin');
		$this->load->helper('admin');
	} 
	
	/* * *********************************************************************
	 * * Function name : customer
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for customer
	 * * Date : 22 NOVEMVER 2018
	 * * **********************************************************************/
	public function index()
	{	
		$this->adminauth_model->authCheck('admin','view_data');
		$this->adminauth_model->getPermissionType($data); 
		$data['error'] 						= 	'';
		$data['activeMenu'] 				= 	'adminfeedback';
		$data['activeSubMenu'] 				= 	'adminfeedback';
		
		if($this->input->get('searchValue')):
			$sValue							=	$this->input->get('searchValue');
			$whereCon['like']		 		= 	"(feed.message LIKE '%".$sValue."%' 
												  OR feed.userPhone LIKE '%".$sValue."%')";
			$data['searchValue'] 			= 	$sValue;
		else:
			$whereCon['like']		 		= 	"";
			$data['searchValue'] 			= 	'';
		endif;
		
		$whereCon['where']		 			= 	"";		
		$shortField 						= 	'feed.id ASC';
		
		$baseUrl 							= 	$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index';
		$this->session->set_userdata('userfeedbackAdminData',currentFullUrl());
		$qStringdata						=	explode('?',currentFullUrl());
		//$qStringdata						=	explode('/',currentFullUrl());
		//echo "<pre>"; print_r($qStringdata); die;
		$suffix								= 	$qStringdata[1]?'?'.$qStringdata[1]:'';
		$tblName 							= 	'feedback as feed';
		$con 								= 	'';
		$totalRows 							= 	$this->admin_model->selectCustomerfeedbackData('count',$tblName,$whereCon,$shortField,'0','0');
		
		if($this->input->get('showLength') == 'All'):
			$perPage	 					= 	$totalRows;
			$data['perpage'] 				= 	$this->input->get('showLength');  
		elseif($this->input->get('showLength')):
			$perPage	 					= 	$this->input->get('showLength'); 
			$data['perpage'] 				= 	$this->input->get('showLength'); 
		else:
			$perPage	 					= 	SHOW_NO_OF_DATA;
			$data['perpage'] 				= 	SHOW_NO_OF_DATA; 
		endif;
		$uriSegment 						= 	getUrlSegment();
		//echo $baseUrl; echo "<hr>"; echo $suffix; echo "<hr>"; echo $totalRows; echo "<hr>"; echo $perPage; echo "<hr>"; echo $uriSegment; echo "<hr>";die;
	    $data['PAGINATION']					=	adminPagination($baseUrl,$suffix,$totalRows,$perPage,$uriSegment);
		//print_r($data['PAGINATION']); die;

       if ($this->uri->segment(getUrlSegment())):
           $page = $this->uri->segment(getUrlSegment());
       else:
           $page = 0;
       endif;
		//print_r($this->uri->segment(getUrlSegment())); die;
		$data['forAction'] 					= 	$baseUrl; 
		if($totalRows):
			$first							=	($page)+1;
			$data['first']					=	$first;
			$last							=	(($page)+$data['perpage'])>$totalRows?$totalRows:(($page)+$data['perpage']);
			$data['noOfContent']			=	'Showing '.$first.'-'.$last.' of '.$totalRows.' items';
		else:
			$data['first']					=	1;
			$data['noOfContent']			=	'';
		endif;
		
		$data['ALLDATA'] 					= 	$this->admin_model->selectCustomerfeedbackData('data',$tblName,$whereCon,$shortField,$perPage,$page); 
		//echo "<pre>"; print_r($data['ALLDATA']); die;
		$this->layouts->set_title('Manage Customer Feedback');
		$this->layouts->admin_view('admin/Userfeedback/index',array(),$data);
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : viewcustomer
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for view customer details
	 * * Date : 22 NOVEMVER 2018
	 * * **********************************************************************/
	public function viewcustomerfeedback($editId='')
	{		
		$data['error'] 				= 	'';
		$data['activeMenu'] 		= 	'adminfeedback';
		$data['activeSubMenu'] 		= 	'adminfeedback';
		if($editId):
			$this->adminauth_model->authCheck('admin','edit_data');
			$data['VIEWDATA']		=	$this->common_model->getcustomerfeedbackdata($editId);
		//echo "<pre>"; print_r($data['VIEWDATA']); die;
		else:
			redirect(correctLink('userfeedbackAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
		endif;
		
		$this->layouts->set_title('View Customer Details');
		$this->layouts->admin_view('admin/Userfeedback/viewcustomerfeedback',array(),$data);
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : changestatus
	** Developed By : Ashish UMrao
	** Purpose  : This function used for change status
	** Date : 22 NOVEMVER 2018
	************************************************************************/
	function changestatus($changeStatusId='',$statusType='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$param['status']		=	$statusType;
		$this->common_model->editData('feedback',$param,'id',$changeStatusId);
		
		$this->session->set_flashdata('alert_success',lang('statussuccess'));
		
		redirect(correctLink('userfeedbackAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
	
	/***********************************************************************
	** Function name : deleteData
	** Developed By : Ravi Kumar
	** Purpose  : This function used for delete data
	** Date : 30 MAY 2019
	************************************************************************/
	function deleteData($deleteId='')
	{  //print_r($deleteId); die;
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$this->common_model->deleteParticularData('feedback','id',$deleteId);
		
		$this->session->set_flashdata('alert_success',lang('deletesuccess'));
		
		redirect(correctLink('userfeedbackAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
}
