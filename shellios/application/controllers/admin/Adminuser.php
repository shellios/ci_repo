<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminuser extends CI_Controller {

	public function  __construct()  
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('adminauth_model','admin_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'admin');
		$this->load->helper('admin');
	} 
	
	/* * *********************************************************************
	 * * Function name : customer
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for customer
	 * * Date : 22 NOVEMVER 2018
	 * * **********************************************************************/
	public function index()
	{	
		$this->adminauth_model->authCheck('admin','view_data');
		$this->adminauth_model->getPermissionType($data); 
		$data['error'] 						= 	'';
		$data['activeMenu'] 				= 	'adminuser';
		$data['activeSubMenu'] 				= 	'adminuser';
		
		if($this->input->get('searchValue')):
			$sValue							=	$this->input->get('searchValue');
			$whereCon['like']		 		= 	"(usr.user_name LIKE '%".$sValue."%' 
			                   					  OR usr.user_email LIKE '%".$sValue."%' 
												  OR usr.user_phone LIKE '%".$sValue."%')";
			$data['searchValue'] 			= 	$sValue;
		else:
			$whereCon['like']		 		= 	"";
			$data['searchValue'] 			= 	'';
		endif;
		
		$whereCon['where']		 			= 	"";		
		$shortField 						= 	'usr.user_name ASC';
		
		$baseUrl 							= 	$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index';
		$this->session->set_userdata('userAdminData',currentFullUrl());
		$qStringdata						=	explode('?',currentFullUrl());
		$suffix								= 	$qStringdata[1]?'?'.$qStringdata[1]:'';
		$tblName 							= 	'users as usr';
		$con 								= 	'';
		$totalRows 							= 	$this->admin_model->selectCustomerData('count',$tblName,$whereCon,$shortField,'0','0');
		
		if($this->input->get('showLength') == 'All'):
			$perPage	 					= 	$totalRows;
			$data['perpage'] 				= 	$this->input->get('showLength');  
		elseif($this->input->get('showLength')):
			$perPage	 					= 	$this->input->get('showLength'); 
			$data['perpage'] 				= 	$this->input->get('showLength'); 
		else:
			$perPage	 					= 	SHOW_NO_OF_DATA;
			$data['perpage'] 				= 	SHOW_NO_OF_DATA; 
		endif;
		$uriSegment 						= 	getUrlSegment();
	    $data['PAGINATION']					=	adminPagination($baseUrl,$suffix,$totalRows,$perPage,$uriSegment);

       if ($this->uri->segment(getUrlSegment())):
           $page = $this->uri->segment(getUrlSegment());
       else:
           $page = 0;
       endif;
		
		$data['forAction'] 					= 	$baseUrl; 
		if($totalRows):
			$first							=	($page)+1;
			$data['first']					=	$first;
			$last							=	(($page)+$data['perpage'])>$totalRows?$totalRows:(($page)+$data['perpage']);
			$data['noOfContent']			=	'Showing '.$first.'-'.$last.' of '.$totalRows.' items';
		else:
			$data['first']					=	1;
			$data['noOfContent']			=	'';
		endif;
		
		$data['ALLDATA'] 					= 	$this->admin_model->selectCustomerData('data',$tblName,$whereCon,$shortField,$perPage,$page); 
		
		$this->layouts->set_title('Manage Customer Details');
		$this->layouts->admin_view('admin/users/index',array(),$data);
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : viewcustomer
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for view customer details
	 * * Date : 22 NOVEMVER 2018
	 * * **********************************************************************/
	public function viewcustomer($editId='')
	{		
		$data['error'] 				= 	'';
		$data['activeMenu'] 		= 	'adminuser';
		$data['activeSubMenu'] 		= 	'adminuser';
		
		if($editId):
			$this->adminauth_model->authCheck('admin','edit_data');
			$data['VIEWDATA']		=	$this->common_model->getDataByParticularField('users','user_id',$editId);
			
			$kycQuery				=	"SELECT * FROM ".getTablePrefix()."users 
										 WHERE user_id = '".$editId."'";  
			$data['ADDRESSDATA']	=	$this->common_model->getDataByQuery('multiple',$kycQuery); 
		else:
			redirect(correctLink('userAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
		endif;
		
		$this->layouts->set_title('View Customer Details');
		$this->layouts->admin_view('admin/users/viewcustomer',array(),$data);
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : changestatus
	** Developed By : Manoj Kumar
	** Purpose  : This function used for change status
	** Date : 22 NOVEMVER 2018
	************************************************************************/
	function changestatus($changeStatusId='',$statusType='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$param['status']		=	$statusType;
		$this->common_model->editData('users',$param,'user_id',$changeStatusId);
		
		$this->session->set_flashdata('alert_success',lang('statussuccess'));
		
		redirect(correctLink('userAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
	
	/***********************************************************************
	** Function name : deleteData
	** Developed By : Ravi Kumar
	** Purpose  : This function used for delete data
	** Date : 30 MAY 2019
	************************************************************************/
	function deleteData($deleteId='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$this->common_model->deleteParticularData('users','user_id',$deleteId);
		
		$this->session->set_flashdata('alert_success',lang('deletesuccess'));
		
		redirect(correctLink('userAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
}
