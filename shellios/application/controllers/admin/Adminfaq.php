<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminfaq extends CI_Controller {

	public function  __construct()  
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('adminauth_model','admin_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'admin');
		$this->load->helper('admin');
	} 
	
	/* * *********************************************************************
	 * * Function name : Admin faq
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for Admin faq
	 * * Date : 04 APRIL 2019
	 * * **********************************************************************/
	public function index()
	{	
		$this->adminauth_model->authCheck('admin','view_data');
		$data['error'] 						= 	'';
		$this->adminauth_model->getPermissionType($data); 
		$data['activeMenu'] 				= 	'adminfaq';
		$data['activeSubMenu'] 				= 	'adminfaq';
		
		if($this->input->get('searchValue')):
			$sValue							=	$this->input->get('searchValue');
			$whereCon['like']		 		= 	"(f.questions LIKE '%".$sValue."%')";
			$data['searchValue'] 			= 	$sValue;
		else:
			$whereCon['like']		 		= 	"";
			$data['searchValue'] 			= 	'';
		endif;
		
		$whereCon['where']		 			= 	"";		
		$shortField 						= 	'f.id ASC';
		
		$baseUrl 							= 	$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index';
		$this->session->set_userdata('FaqAdminData',currentFullUrl());
		$qStringdata						=	explode('?',currentFullUrl());
		$suffix								= 	$qStringdata[1]?'?'.$qStringdata[1]:'';
		$tblName 							= 	'faq as f';
		$con 								= 	'';
		$totalRows 							= 	$this->admin_model->selectfaqData('count',$tblName,$whereCon,$shortField,'0','0');
		
		if($this->input->get('showLength') == 'All'):
			$perPage	 					= 	$totalRows;
			$data['perpage'] 				= 	$this->input->get('showLength');  
		elseif($this->input->get('showLength')):
			$perPage	 					= 	$this->input->get('showLength'); 
			$data['perpage'] 				= 	$this->input->get('showLength'); 
		else:
			$perPage	 					= 	SHOW_NO_OF_DATA;
			$data['perpage'] 				= 	SHOW_NO_OF_DATA; 
		endif;
		$uriSegment 						= 	getUrlSegment();
	    $data['PAGINATION']					=	adminPagination($baseUrl,$suffix,$totalRows,$perPage,$uriSegment);

       if ($this->uri->segment(getUrlSegment())):
           $page = $this->uri->segment(getUrlSegment());
       else:
           $page = 0;
       endif;
		
		$data['forAction'] 					= 	$baseUrl; 
		if($totalRows):
			$first							=	($page)+1;
			$data['first']					=	$first;
			$last							=	(($page)+$data['perpage'])>$totalRows?$totalRows:(($page)+$data['perpage']);
			$data['noOfContent']			=	'Showing '.$first.'-'.$last.' of '.$totalRows.' items';
		else:
			$data['first']					=	1;
			$data['noOfContent']			=	'';
		endif;
		
		$data['ALLDATA'] 					= 	$this->admin_model->selectfaqData('data',$tblName,$whereCon,$shortField,$perPage,$page); 
		
		$this->layouts->set_title('Manage Faq');
		$this->layouts->admin_view('admin/adminfaq/index',array(),$data);
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : addeditdata
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for add edit data
	 * * Date : 04 April 2019
	 * * **********************************************************************/
	public function addeditdata($editId='')
	{		
		$data['error'] 				= 	'';
		$data['activeMenu'] 		= 	'adminfaq';
		$data['activeSubMenu'] 		= 	'adminfaq';
		
		if($editId):
			$this->adminauth_model->authCheck('admin','edit_data');
			$data['EDITDATA']		=	$this->common_model->getDataByParticularField('faq','user_id',$editId);
		else:
			$this->adminauth_model->authCheck('admin','add_data');
		endif;
		
		if($this->input->post('SaveChanges')):  //echo "<pre>"; print_r($_POST); die;
			$error					=	'NO';
			$this->form_validation->set_rules('title', 'Title', 'trim|required');
			$this->form_validation->set_rules('content', 'Description', 'trim|required');
			if($this->form_validation->run() && $error == 'NO'):  
				$param['questions']				= 	addslashes($this->input->post('title'));
				$param['answer']				= 	addslashes($this->input->post('content'));
				if($this->input->post('CurrentDataID') ==''):
					$param['creation_date']		=	currentDateTime();
					$param['created_by']		=	$this->session->userdata('SHELLIOS_ADMIN_ID');
					$param['status']			=	'A';
					$lastInsertId				=	$this->common_model->addData('faq',$param);
					$Uparam['encrypt_id']		=	manojEncript($lastInsertId);
					$Uparam['user_id']			=	generateUniqueId($lastInsertId);
					$Uwhere['id']				=	$lastInsertId;
					$this->common_model->editDataByMultipleCondition('faq',$Uparam,$Uwhere);

					$this->session->set_flashdata('alert_success',lang('addsuccess'));
				else:  
					$faqId					=	$this->input->post('CurrentDataID');
					$param['update_date']		=	currentDateTime();
					$param['updated_by']		=	$this->session->userdata('SHELLIOS_ADMIN_ID');
					$this->common_model->editData('faq',$param,'user_id',$faqId);
					$this->session->set_flashdata('alert_success',lang('updatesuccess')); 
				endif;
				
				redirect(correctLink('FaqAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
			endif;
		endif;
		
		$this->layouts->set_title('Edit FAQ Details');
		$this->layouts->admin_view('admin/adminfaq/addeditdata',array(),$data);
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : changestatus
	** Developed By : Ashish UMrao
	** Purpose  : This function used for change status
	** Date : 04 APRIL 2019
	************************************************************************/
	function changestatus($changeStatusId='',$statusType='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$param['status']		=	$statusType;
		$this->common_model->editData('faq',$param,'user_id',$changeStatusId);
		
		$this->session->set_flashdata('alert_success',lang('statussuccess'));
		
		redirect(correctLink('FaqAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
	/***********************************************************************
	** Function name : deleteData
	** Developed By : Ashish UMrao
	** Purpose  : This function used for delete data
	** Date : 05 APRIL 2019
	************************************************************************/
	function deleteData($deleteId='')
	{ //print_r($deleteId);die;
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$this->common_model->deleteParticularData('faq','user_id',$deleteId);
		
		$this->session->set_flashdata('alert_success',lang('deletesuccess'));
		
		redirect(correctLink('FaqAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
}
