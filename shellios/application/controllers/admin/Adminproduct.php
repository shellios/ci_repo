<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminproduct extends CI_Controller {

	public function  __construct()  
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('adminauth_model','admin_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'admin');
		$this->load->helper('admin');
	} 
	
	/* * *********************************************************************
	 * * Function name : car brand
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for car brand
	 * * Date : 14 AUGUSt 2018
	 * * **********************************************************************/
	public function index()
	{	
		$this->adminauth_model->authCheck('admin','view_data');
		$this->adminauth_model->getPermissionType($data); 
		$data['error'] 						= 	'';
		$data['activeMenu'] 				= 	'adminproduct';
		$data['activeSubMenu'] 				= 	'adminproduct';

		if($this->input->get('searchValue')):
			$sValue							=	$this->input->get('searchValue');
			$whereCon['like']		 		= 	"(p.prod_brand_name LIKE '%".$sValue."%'
												OR p.prod_sr_no LIKE '%".$sValue."%'
												OR p.prod_color LIKE '%".$sValue."%'
												OR p.userphone LIKE '%".$sValue."%')";
			$data['searchValue'] 			= 	$sValue;
		else:
			$whereCon['like']		 		= 	"";
			$data['searchValue'] 			= 	'';
		endif;
				
		$whereCon['where']		 			= 	"";		
		$shortField 						= 	'p.id ASC';
		
		$baseUrl 							= 	$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index';
		$this->session->set_userdata('productAdminData',currentFullUrl());
		$qStringdata						=	explode('?',currentFullUrl());
		$suffix								= 	$qStringdata[1]?'?'.$qStringdata[1]:'';
		$tblName 							= 	'product as p';
		$con 								= 	'';
		$totalRows 							= 	$this->admin_model->selectproductData('count',$tblName,$whereCon,$shortField,'0','0');
		
		if($this->input->get('showLength') == 'All'):
			$perPage	 					= 	$totalRows;
			$data['perpage'] 				= 	$this->input->get('showLength');  
		elseif($this->input->get('showLength')):
			$perPage	 					= 	$this->input->get('showLength'); 
			$data['perpage'] 				= 	$this->input->get('showLength'); 
		else:
			$perPage	 					= 	SHOW_NO_OF_DATA;
			$data['perpage'] 				= 	SHOW_NO_OF_DATA; 
		endif;
		$uriSegment 						= 	getUrlSegment();
	    $data['PAGINATION']					=	adminPagination($baseUrl,$suffix,$totalRows,$perPage,$uriSegment);

       if ($this->uri->segment(getUrlSegment())):
           $page = $this->uri->segment(getUrlSegment());
       else:
           $page = 0;
       endif;

		$data['forAction'] 					= 	$baseUrl; 
		if($totalRows):
			$first							=	($page)+1;
			$data['first']					=	$first;
			$last							=	(($page)+$data['perpage'])>$totalRows?$totalRows:(($page)+$data['perpage']);
			$data['noOfContent']			=	'Showing '.$first.'-'.$last.' of '.$totalRows.' items';
		else:
			$data['first']					=	1;
			$data['noOfContent']			=	'';
		endif;
		
		$data['ALLDATA'] 					= 	$this->admin_model->selectproductData('data',$tblName,$whereCon,$shortField,$perPage,$page); 
		$this->layouts->set_title('Manage Product');
		$this->layouts->admin_view('admin/adminproduct/index',array(),$data);
	}	// END OF FUNCTION
	/* * *********************************************************************
	 * * Function name : addeditdata
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for add edit data
	 * * Date : 29 APRIL 2019
	 * * **********************************************************************/
	public function addeditdata($editId='')
	{		
		$data['error'] 				= 	'';
		$data['activeMenu'] 				= 	'adminproduct';
		$data['activeSubMenu'] 				= 	'adminproduct';
		
		if($editId):
			$this->adminauth_model->authCheck('admin','edit_data');
			$data['EDITDATA']		=	$this->common_model->getDataByParticularField('product','prod_id',$editId);
		else:
			$this->adminauth_model->authCheck('admin','add_data');
		endif;
		$data['WARRANTY'] 				= 	$this->common_model->package_status();
		//echo "<pre>"; print_r($data['WARRENTY'] ); die;
		if($this->input->post('SaveChanges')): //echo "<pre>"; print_r($_POST); die;
			$error					=	'NO';
			$this->form_validation->set_rules('prod_brand_name', 'Brand Name', 'trim|required');
			if($this->input->post('CurrentIdForUnique') == ''):
				$this->form_validation->set_rules('prod_sr_no', 'Sr.No', 'trim|required|is_unique[product.prod_sr_no]');
			else:
				$this->form_validation->set_rules('prod_sr_no', 'Sr.No', 'trim|required');
			endif;
			$this->form_validation->set_rules('prod_color', 'Color', 'trim|required');
			$this->form_validation->set_rules('prod_material', 'Material', 'trim|required');
			$this->form_validation->set_rules('prod_warranty', 'Prod. Warranty', 'trim|required');
			$this->form_validation->set_rules('prod_image', 'prod image', 'trim');
			//$this->form_validation->set_rules('productstatus', 'prod Status', 'trim');
			$this->form_validation->set_rules('purchasedate', 'purchasedate', 'trim');
			if($this->form_validation->run() && $error == 'NO'):   
				$prodWarranty = $this->input->post('prod_warranty');
				$param['prod_brand_name']			= 	addslashes($this->input->post('prod_brand_name'));
				$param['prod_sr_no']				= 	addslashes($this->input->post('prod_sr_no'));
				$param['prod_color']				= 	addslashes($this->input->post('prod_color'));
				$param['prod_material']				= 	addslashes($this->input->post('prod_material'));
				$param['prod_warranty']				= 	addslashes($this->input->post('prod_warranty'));
				if($this->input->post('prod_image') <> ""):
					$param['prod_image']			= 	addslashes($this->input->post('prod_image'));
				else:
					$param['prod_image']			= 	base_url()."assets/front/image/puros-white.png";
				endif;
				//$param['productstatus']				= 	addslashes($this->input->post('productstatus'));
				
				
				
				if($this->input->post('CurrentIdForUnique') ==''): 
					$param['creation_date']		=	currentDateTime();
					$param['created_by']		=	$this->session->userdata('SHELLIOS_ADMIN_ID');
					$param['status']			=	'A';
					$lastInsertId				=	$this->common_model->addData('product',$param);
					$Uparam['encrypt_id']		=	manojEncript($lastInsertId);
					$Uparam['prod_id']			=	generateUniqueId($lastInsertId);
					$Uwhere['id']				=	$lastInsertId;
					$this->common_model->editDataByMultipleCondition('product',$Uparam,$Uwhere);
					$this->session->set_flashdata('alert_success',lang('addsuccess'));
				else:   
					$sellerId					=	$this->input->post('CurrentIdForUnique');
					$param['update_date']		=	currentDateTime();
					//$param['productstatustime']	=	currentDateTime();
					$param['purchagedate']	=	addslashes($this->input->post('purchasedate'));
					$param['updated_by']		=	$this->session->userdata('SHELLIOS_ADMIN_ID');
					//print_r($param); die;
					$this->common_model->editData('product',$param,'prod_id',$sellerId);
					$this->session->set_flashdata('alert_success',lang('updatesuccess')); 
				endif;
				redirect(correctLink('productAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
			endif;
		endif;
		
		$this->layouts->set_title('Edit Product Details');
		$this->layouts->admin_view('admin/adminproduct/addeditdata',array(),$data);
	}	// END OF FUNCTION

	
	/***********************************************************************
	** Function name : changeStatus
	** Developed By : Manoj Kumar
	** Purpose  : This function used for change status
	** Date : 14 AUGUSt 2018
	************************************************************************/
	function changeStatus($changeStatusId='',$statusType='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$param['status']		=	$statusType;
		$this->common_model->editData('product',$param,'prod_id',$changeStatusId);
		
		$this->session->set_flashdata('alert_success',lang('statussuccess'));
		
		redirect(correctLink('productAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}

	/***********************************************************************
	** Function name : deleteData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for delete data
	** Date : 14 AUGUSt 2018
	************************************************************************/
	function deleteData($deleteId='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$this->common_model->deleteParticularData('product','prod_id',$deleteId);
		
		$this->session->set_flashdata('alert_success',lang('deletesuccess'));
		
		redirect(correctLink('productAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}

	/***********************************************************************
	** Function name : prodAttImageUpload
	** Developed By : Ashish UMrao
	** Purpose  : This function used for prod Att Image Upload
	** Date : 30 APRIL 2019
	************************************************************************/
	function prodAttImageUpload()
	{ 
		if($this->input->post('imageData')): 
			$imageData				= 	$this->input->post('imageData');
			$imageName				= 	time().'.png';
			$imageFolder			=	'';
			$imageType				=	'productImage';
			$this->load->library("upload_crop_img");
			$returnFileName		=	$this->upload_crop_img->_upload_canvas_image($imageData,$imageName,$imageType,$imageFolder);
			if($returnFileName):
				echo $returnFileName; die;
			else:
				echo 'UPLODEERROR'; die;
			endif;
		else:
			echo 'UPLODEERROR'; die;
		endif;
	}	// END OF FUNCTION	
	
	/***********************************************************************
	** Function name : prodAttImageDelete
 	** Developed By : Ashish UMrao
	** Purpose  : This function used for prod Att Image Delete
	** Date : 30 APRIL 2019
	************************************************************************/
	function prodAttImageDelete()
	{  
		$imageName	=	$this->input->post('imageName'); 
		if($imageName):
			$this->load->library("upload_crop_img");
			$return	=	$this->upload_crop_img->_delete_image(trim($imageName)); 
		endif;
		echo '1'; die;
	}	// END OF FUNCTION	
}
