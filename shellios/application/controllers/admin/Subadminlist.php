<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subadminlist extends CI_Controller {

	public function  __construct()  
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('adminauth_model','admin_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'admin');
		$this->load->helper('admin');
	} 
	
	/* * *********************************************************************
	 * * Function name : subadmin
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for subadmin
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function index()
	{	
		$this->adminauth_model->authCheck('admin','view_data');
		$this->adminauth_model->getPermissionType($data); 
		$data['error'] 						= 	'';
		$data['activeMenu'] 				= 	'subadminlist';
		$data['activeSubMenu'] 				= 	'subadminlist';
		
		if($this->input->get('searchValue')):
			$sValue							=	$this->input->get('searchValue');
			$whereCon['like']		 		= 	"(adm.admin_name LIKE '%".$sValue."%' 
			                   					  OR adm.admin_display_name LIKE '%".$sValue."%' 
												  OR adm.admin_slug LIKE '%".$sValue."%' 
												  OR adm.admin_email_id LIKE '%".$sValue."%' 
												  OR adm.admin_mobile_number LIKE '%".$sValue."%' 
												  OR adm.admin_address LIKE '%".$sValue."%' 
												  OR adm.admin_locality LIKE '%".$sValue."%' 
												  OR adm.admin_city LIKE '%".$sValue."%' 
												  OR adm.admin_state LIKE '%".$sValue."%' 
												  OR adm.admin_zipcode LIKE '%".$sValue."%' 
												  OR adm.status LIKE '%".$sValue."%')";
			$data['searchValue'] 			= 	$sValue;
		else:
			$whereCon['like']		 		= 	"";
			$data['searchValue'] 			= 	'';
		endif;
		
		$whereCon['where']		 			= 	"admt.admin_metadata_key = '_admin_type' AND admt.admin_metadata_value = 'Sub admin'
												 AND admta.admin_metadata_key = '_admin_parent' AND admta.admin_metadata_value = '".sessionData('SHELLIOS_ADMIN_CURRENT_ID')."'";		
		$shortField 						= 	'adm.admin_name DESC, adm.admin_id DESC';
		
		$baseUrl 							= 	$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index';
		$this->session->set_userdata('subadminlistAdminData',currentFullUrl());
		$qStringdata						=	explode('?',currentFullUrl());
		$suffix								= 	$qStringdata[1]?'?'.$qStringdata[1]:'';
		$tblName 							= 	'admin as adm';
		$con 								= 	'';
		$totalRows 							= 	$this->admin_model->selectSubadminData('count',$tblName,$whereCon,$shortField,'0','0');
		
		if($this->input->get('showLength') == 'All'):
			$perPage	 					= 	$totalRows;
			$data['perpage'] 				= 	$this->input->get('showLength');  
		elseif($this->input->get('showLength')):
			$perPage	 					= 	$this->input->get('showLength'); 
			$data['perpage'] 				= 	$this->input->get('showLength'); 
		else:
			$perPage	 					= 	SHOW_NO_OF_DATA;
			$data['perpage'] 				= 	SHOW_NO_OF_DATA; 
		endif;
		$uriSegment 						= 	getUrlSegment();
	    $data['PAGINATION']					=	adminPagination($baseUrl,$suffix,$totalRows,$perPage,$uriSegment);

       if ($this->uri->segment(getUrlSegment())):
           $page = $this->uri->segment(getUrlSegment());
       else:
           $page = 0;
       endif;
		
		$data['forAction'] 					= 	$baseUrl; 
		if($totalRows):
			$first							=	($page)+1;
			$data['first']					=	$first;
			$last							=	(($page)+$data['perpage'])>$totalRows?$totalRows:(($page)+$data['perpage']);
			$data['noOfContent']			=	'Showing '.$first.'-'.$last.' of '.$totalRows.' items';
		else:
			$data['first']					=	1;
			$data['noOfContent']			=	'';
		endif;
		
		$data['ALLDATA'] 					= 	$this->admin_model->selectSubadminData('data',$tblName,$whereCon,$shortField,$perPage,$page); 
		
		$this->layouts->set_title('Manage Subadmin Details');
		$this->layouts->admin_view('admin/subadminlist/index',array(),$data);
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : addeditdata
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for add edit data
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function addeditdata($editId='')
	{		
		$data['error'] 				= 	'';
		$data['activeMenu'] 		= 	'subadminlist';
		$data['activeSubMenu'] 		= 	'subadminlist';
		
		if($editId):
			$this->adminauth_model->authCheck('admin','edit_data');
			$data['EDITDATA']		=	$this->common_model->getDataByParticularField('admin','encrypt_id',$editId);
			
			$DepartmentQuery		=	"SELECT admin_metadata_value FROM ".getTablePrefix()."admin_metadata 
										 WHERE admin_id = '".$editId."' AND admin_metadata_key = '_admin_department'";  
			$data['DEPARTMENTDATA']	=	$this->common_model->getDataByQuery('single',$DepartmentQuery); 
			$data['MODULEDATA'] 	= 	$this->adminauth_model->getPermission($editId);
		else:
			$this->adminauth_model->authCheck('admin','add_data');
		endif;
		
		$data['Modirectory'] 		= 	$this->adminauth_model->getModule(); 
		
		if($this->input->post('SaveChanges')):
			$error					=	'NO';
			$this->form_validation->set_rules('admin_department_id', 'Department', 'trim|required');
			$this->form_validation->set_rules('admin_name', 'Name', 'trim|required');
			$this->form_validation->set_rules('admin_display_name', 'Display name', 'trim|required');
			$this->form_validation->set_rules('admin_email_id', 'E-Mail', 'trim|required|valid_email|is_unique[admin.admin_email_id]');
			if($this->input->post('new_password')!= ''):
				$this->form_validation->set_rules('new_password', 'New password', 'trim|required|min_length[6]|max_length[25]');
				$this->form_validation->set_rules('conf_password', 'Confirm password', 'trim|required|min_length[6]|matches[new_password]');
			endif;
			
			$this->form_validation->set_rules('admin_mobile_number', 'Mobile number', 'trim|required|min_length[10]|max_length[15]|is_unique[admin.admin_mobile_number]');
			$adminmobile		=	str_replace(' ','',$this->input->post('admin_mobile_number'));
			if($this->input->post('admin_mobile_number') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$adminmobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$adminmobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please eneter correct number.';
				endif;
			endif;
			
			$this->form_validation->set_rules('admin_address', 'Address', 'trim|required');
			$this->form_validation->set_rules('admin_locality', 'Locality', 'trim');
			$this->form_validation->set_rules('admin_city', 'City', 'trim');
			$this->form_validation->set_rules('admin_state', 'State', 'trim');
			$this->form_validation->set_rules('admin_zipcode', 'Zipcode', 'trim');
			
			$pererror							=	'YES';
			if($data['Modirectory'] <> ""): 
			 	foreach($data['Modirectory'] as $MODinfo): 
					if($this->input->post('mainmodule'.$MODinfo['encrypt_id'])):
						$pererror				=	'NO';
					endif;
				endforeach;
				if($pererror == 'YES'):
					$error						=	'YES';
					$data['PERERROR'] 			= 	lang('PERERROR');
				endif;
			endif;  
			
			if($this->form_validation->run() && $error == 'NO'):   
			
				$param['admin_name']				= 	addslashes($this->input->post('admin_name'));
				$param['admin_display_name']		= 	addslashes($this->input->post('admin_display_name'));
				$param['admin_slug']				= 	strtolower(url_title(addslashes($this->input->post('admin_name'))));
				$param['admin_email_id']			= 	addslashes($this->input->post('admin_email_id'));
				
				if($this->input->post('new_password')):
					$NewPassword					=	$this->input->post('new_password');
					$param['admin_password']		= 	$this->adminauth_model->encriptPassword($NewPassword);
				endif;
				
				$param['admin_mobile_number']		= 	addslashes($this->input->post('admin_mobile_number'));
				$param['admin_address']				= 	addslashes($this->input->post('admin_address'));
				$param['admin_locality']			= 	addslashes($this->input->post('admin_locality'));
				$param['admin_city']				= 	addslashes($this->input->post('admin_city'));
				$param['admin_state']				= 	addslashes($this->input->post('admin_state'));
				$param['admin_zipcode']				= 	addslashes($this->input->post('admin_zipcode'));
				
				if($this->input->post('CurrentDataID') ==''):
					$param['creation_ip']		=	currentIp();
					$param['creation_date']		=	currentDateTime();
					$param['created_by']		=	$this->session->userdata('SHELLIOS_ADMIN_ID');
					$param['status']			=	'A';
					$lastInsertId				=	$this->common_model->addData('admin',$param);
					
					$Uparam['encrypt_id']		=	manojEncript($lastInsertId);
					$Uwhere['admin_id']			=	$lastInsertId;
					$this->common_model->editDataByMultipleCondition('admin',$Uparam,$Uwhere);
					
					$BRATparam['admin_id']				=	$Uparam['encrypt_id'];
					$BRATparam['admin_metadata_key']	=	'_admin_type';
					$BRATparam['admin_metadata_value']	=	'Sub admin';
					$this->common_model->addData('admin_metadata',$BRATparam);
					
					$BRALparam['admin_id']				=	$Uparam['encrypt_id'];	
					$BRALparam['admin_metadata_key']	=	'_admin_label';
					$BRALparam['admin_metadata_value']	=	'1';
					$this->common_model->addData('admin_metadata',$BRALparam);
					
					$BRALparam['admin_id']				=	$Uparam['encrypt_id'];
					$BRALparam['admin_metadata_key']	=	'_admin_department';
					$BRALparam['admin_metadata_value']	=	addslashes($this->input->post('admin_department_id'));
					$this->common_model->addData('admin_metadata',$BRALparam);
					
					$BRAPparam['admin_id']				=	$Uparam['encrypt_id'];
					$BRAPparam['admin_metadata_key']	=	'_admin_parent';
					$BRAPparam['admin_metadata_value']	=	sessionData('SHELLIOS_ADMIN_CURRENT_ID');
					$this->common_model->addData('admin_metadata',$BRAPparam);
					
					$subAdminId					=	$Uparam['encrypt_id'];
					
					$this->session->set_flashdata('alert_success',lang('addsuccess'));
				else:  
					$subAdminId					=	$this->input->post('CurrentDataID');
					$param['update_ip']			=	currentIp();
					$param['update_date']		=	currentDateTime();
					$param['updated_by']		=	$this->session->userdata('SHELLIOS_ADMIN_ID');
					$this->common_model->editData('admin',$param,'encrypt_id',$subAdminId);
					
					$AUparam['admin_metadata_value']	=	addslashes($this->input->post('admin_department_id'));
					$AUwhere['admin_id']				=	$subadminId;
					$AUwhere['admin_metadata_key']		=	'_admin_department';
					$this->common_model->editDataByMultipleCondition('admin_metadata',$AUparam,$AUwhere);
					
					$this->session->set_flashdata('alert_success',lang('updatesuccess')); 
				endif;
				
				if($data['Modirectory'] <> "" && $subAdminId): 
					$this->adminauth_model->deletePermissionData($subAdminId);
					foreach($data['Modirectory'] as $MODinfo):
						$mainmodperids		=	'';
						$mmc 				= 	$MODinfo['encrypt_id']; 
						if($this->input->post('mainmodule'.$mmc)):
							$MMParams['module_id']			=	$MODinfo['encrypt_id'];
							$MMParams['module_name']		=	$MODinfo['module_name'];
							$MMParams['module_display_name']=	$MODinfo['module_display_name'];
							$MMParams['module_orders']		=	$MODinfo['module_orders'];
							$MMParams['module_icone']		=	$MODinfo['module_icone'];
							$MMParams['child_data']			=	$MODinfo['child_data'];
							$MMParams['admin_id']			=	$subAdminId;
							
							if($this->input->post('mainmodule_view_data'.$mmc)):
								$MMParams['view_data']		=	'Y';
							else:
								$MMParams['view_data']		=	'N';
							endif;
							if($this->input->post('mainmodule_add_data'.$mmc)):
								$MMParams['add_data']		=	'Y';
							else:
								$MMParams['add_data']		=	'N';
							endif;
							if($this->input->post('mainmodule_edit_data'.$mmc)):
								$MMParams['edit_data']		=	'Y';
							else:
								$MMParams['edit_data']		=	'N';
							endif;
							if($this->input->post('mainmodule_delete_data'.$mmc)):
								$MMParams['delete_data']	=	'Y';
							else:
								$MMParams['delete_data']	=	'N';
							endif;
							
							$MMlastInsertId					=	$this->common_model->addData('admin_module_permission',$MMParams);
							
							$UMMparam['encrypt_id']				=	manojEncript($MMlastInsertId);
							$UMMwhere['module_permission_id']	=	$MMlastInsertId;
							$this->common_model->editDataByMultipleCondition('admin_module_permission',$UMMparam,$UMMwhere);
							
							$mainModuleId					=	$UMMparam['encrypt_id'];
						endif;
						
						if($MODinfo['child_data'] == 'Y' && $mainModuleId):
							$childdata 						= 	$this->adminauth_model->getModuleChild($MODinfo['encrypt_id']);
							if($childdata <> ""): 
								foreach($childdata as $CDinfo):	
									 $cmc 					= 	$CDinfo['encrypt_id'];
									if($this->input->post('childmodule'.$mmc.'_'.$cmc)):
										$UMMUparam['child_data']		=	'Y';
										$UMMUwhere['encrypt_id']		=	$mainModuleId;
										$this->common_model->editDataByMultipleCondition('admin_module_permission',$UMMUparam,$UMMUwhere);
									
										$CMParams['permission_id']		=	$mainModuleId;
										$CMParams['module_id']			=	$CDinfo['encrypt_id'];
										$CMParams['module_name']		=	$CDinfo['module_name'];
										$CMParams['module_display_name']=	$CDinfo['module_display_name'];
										$CMParams['module_orders']		=	$CDinfo['module_orders'];
										$CMParams['admin_id']			=	$subAdminId;
										
										if($this->input->post('childmodule_view_data'.$mmc.'_'.$cmc)):
											$CMParams['view_data']		=	'Y';
										else:
											$CMParams['view_data']		=	'N';
										endif;
										if($this->input->post('childmodule_add_data'.$mmc.'_'.$cmc)):
											$CMParams['add_data']		=	'Y';
										else:
											$CMParams['add_data']		=	'N';
										endif;
										if($this->input->post('childmodule_edit_data'.$mmc.'_'.$cmc)):
											$CMParams['edit_data']		=	'Y';
										else:
											$CMParams['edit_data']		=	'N';
										endif;
										if($this->input->post('childmodule_delete_data'.$mmc.'_'.$cmc)):
											$CMParams['delete_data']	=	'Y';
										else:
											$CMParams['delete_data']	=	'N';
										endif;
										
										$CMlastInsertId					=	$this->common_model->addData('admin_module_child_permission',$CMParams);
							
										$UCMparam['encrypt_id']					=	manojEncript($CMlastInsertId);
										$UCMwhere['module_child_permission_id']	=	$CMlastInsertId;
										$this->common_model->editDataByMultipleCondition('admin_module_child_permission',$UCMparam,$UCMwhere);
									endif;
								endforeach;
							endif;
						endif;
					endforeach;
				endif;
				
				redirect(correctLink('subadminlistAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
			endif;
		endif;
		
		$this->layouts->set_title('Edit Subadmin Details');
		$this->layouts->admin_view('admin/subadminlist/addeditdata',array(),$data);
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : changestatus
	** Developed By : Manoj Kumar
	** Purpose  : This function used for change status
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function changestatus($changeStatusId='',$statusType='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$param['status']		=	$statusType;
		$this->common_model->editData('admin',$param,'encrypt_id',$changeStatusId);
		
		$this->session->set_flashdata('alert_success',lang('statussuccess'));
		
		redirect(correctLink('subadminlistAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
	
	/***********************************************************************
	** Function name : deleteData
	** Developed By : Ravi Kumar
	** Purpose  : This function used for delete data
	** Date : 30 MAY 2019
	************************************************************************/
	function deleteData($deleteId='')
	{  
		$this->adminauth_model->authCheck('admin','edit_data');
		
		$this->common_model->deleteParticularData('admin','admin_id',$deleteId);
		
		$this->session->set_flashdata('alert_success',lang('deletesuccess'));
		
		redirect(correctLink('productAdminData',$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index'));
	}
	
	/***********************************************************************
	** Function name : get_view_data
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get view data.
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function get_view_data()
	{
		$html					=	'';
		if($this->input->post('viewid')):
			$viewId					=	$this->input->post('viewid');
			$viewData				=	$this->common_model->getDataByParticularField('admin','encrypt_id',$viewId);
			 
			if($viewData <> ""):
			
				$html			.=	'<table class="table border-none">
									  <tbody>
										<tr>
										  <td>';
				$html			.=	'<table class="table border-none">
									  <thead>
										<tr>
										  <th align="left" colspan="2"><strong>Subadmin details</strong></th>
										</tr>
									  </thead>
									  <tbody><tr>
									  <td align="left" width="30%"><strong>Name</strong></td>
									  <td align="left" width="70%">'.stripslashes($viewData['admin_name']).'</td>
									</tr>
									<tr>
									  <td align="left" width="30%"><strong>Display Name</strong></td>
									  <td align="left" width="70%">'.stripslashes($viewData['admin_display_name']).'</td>
									</tr>
									<tr>
									  <td align="left" width="30%"><strong>Email Id</strong></td>
									  <td align="left" width="70%">'.stripslashes($viewData['admin_email_id']).'</td>
									</tr>
									<tr>
									  <td align="left" width="30%"><strong>Mobile Number</strong></td>
									  <td align="left" width="70%">'.stripslashes($viewData['admin_mobile_number']).'</td>
									</tr>
									<tr>
									  <td align="left" width="30%"><strong>Address</strong></td>
									  <td align="left" width="70%">'.stripslashes($viewData['admin_address']).'</td>
									</tr>
									<tr>
									  <td align="left" width="30%"><strong>Locality</strong></td>
									  <td align="left" width="70%">'.stripslashes($viewData['admin_locality']).'</td>
									</tr>
									<tr>
									  <td align="left" width="30%"><strong>City</strong></td>
									  <td align="left" width="70%">'.stripslashes($viewData['admin_city']).'</td>
									</tr>
									<tr>
									  <td align="left" width="30%"><strong>State</strong></td>
									  <td align="left" width="70%">'.stripslashes($viewData['admin_state']).'</td>
									</tr>
									<tr>
									  <td align="left" width="30%"><strong>Zipcode</strong></td>
									  <td align="left" width="70%">'.stripslashes($viewData['admin_zipcode']).'</td>
									</tr>
									<tr>
									  <td align="left" width="30%"><strong>Status</strong></td>
									  <td align="left" width="70%">'.showStatus($viewData['status']).'</td>
									</tr>
									</tbody>
										</table></td>
									</tr>';
				$html			.=	'</td>
										</tr></tbody>
									</table>';
			endif;
		endif;
		echo $html; die;
	}
}
