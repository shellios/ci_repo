<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function  __construct() 
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('adminauth_model','admin_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'admin');
		$this->load->helper('admin');
	} 
	
	/* * *********************************************************************
	 * * Function name : login
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for login
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function index()
	{	
		if($this->session->userdata('SHELLIOS_ADMIN_ID')) redirect($this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').'dashboard');
		$data['error'] 						= 	'';
		//$this->adminauth_model->checkAdminLoginCookie();
		
		/*-----------------------------------Login ---------------*/
		if($this->input->post('loginFormSubmit')):	
			//Set rules
			$this->form_validation->set_rules('userEmail', 'email', 'trim|required');
			$this->form_validation->set_rules('userPassword', 'password', 'trim|required');
			
			if($this->form_validation->run()):	
				$result		=	$this->adminauth_model->Authenticate($this->input->post('userEmail'));
				if($result):	
				//print_r($this->adminauth_model->decryptsPassword($result['admin_password'])); die;
					if($this->adminauth_model->decryptsPassword($result['admin_password']) != $this->input->post('userPassword')):
						$data['error'] = lang('invalidpassword');	
					elseif($result['status'] != 'A'):	
						$data['error'] = lang('accountblock');	
					else:	
						$ststusError	=	0;
						$departmentName	=	'';
						
						$ATQuery		=	"SELECT admin_metadata_value FROM ".getTablePrefix()."admin_metadata 
											 WHERE admin_id = '".$result['encrypt_id']."' AND admin_metadata_key = '_admin_type'";  
						$ATData			=	$this->common_model->getDataByQuery('single',$ATQuery); 
						
						$ALQuery		=	"SELECT admin_metadata_value FROM ".getTablePrefix()."admin_metadata 
											 WHERE admin_id = '".$result['encrypt_id']."' AND admin_metadata_key = '_admin_label'";  
						$ALData			=	$this->common_model->getDataByQuery('single',$ALQuery); 
						if($ATData['admin_metadata_value'] == 'Super admin' && $ALData['admin_metadata_value'] == '0'):
							
							$adminCurrentId				=	$result['encrypt_id'];
							$adminCurrentName			=	$result['admin_name'];
							$adminCurrentDisplayName	=	$result['admin_display_name'];
							$adminCurrentPath			=	base_url().'admin/'.$result['admin_slug'].'/';
							//echo "<pre>"; print_r($adminCurrentPath); die;
							$adminCurrentLogo			=	$result['admin_image'];
							
						elseif($ATData['admin_metadata_value'] == 'Sub admin' && $ALData['admin_metadata_value'] == '1'):
							
							$AdmQuery					=	"SELECT ad.encrypt_id, ad.admin_name, ad.admin_display_name, ad.admin_slug, ad.admin_image
												 	 	 	 FROM ".getTablePrefix()."admin_metadata as am LEFT JOIN ".getTablePrefix()."admin as ad ON am.admin_metadata_value= ad.encrypt_id
												 	 	 	 WHERE am.admin_id = '".$result['encrypt_id']."' AND am.admin_metadata_key = '_admin_parent'";
							$AdmData					=	$this->common_model->getDataByQuery('single',$AdmQuery); 
							
							$adminCurrentId				=	$AdmData['encrypt_id'];
							$adminCurrentName			=	$AdmData['admin_name'];
							$adminCurrentDisplayName	=	$AdmData['admin_display_name'];
							$adminCurrentPath			=	base_url().'admin/'.$AdmData['admin_slug'].'/';
							$adminCurrentLogo			=	$AdmData['admin_image'];
														
							$ADQuery					=	"SELECT admin_metadata_value FROM ".getTablePrefix()."admin_metadata 
													 	 	 WHERE admin_id = '".$result['encrypt_id']."' AND admin_metadata_key = '_admin_department'";  
							$ADData						=	$this->common_model->getDataByQuery('single',$ADQuery);
							$departmentName				=	$ADData['admin_metadata_value'];
						endif;
						
						if($ststusError == 1):	
							$data['error'] 			= 	lang('accountblock');	
						else:	
						 
							$this->session->set_userdata(array(
												'SHELLIOS_ADMIN_LOGGED_IN'=>true,
												'SHELLIOS_ADMIN_ID'=>$result['encrypt_id'],
												'SHELLIOS_ADMIN_NAME'=>$result['admin_name'],
												'SHELLIOS_ADMIN_DISLPLAY_NAME'=>$result['admin_display_name'],
												'SHELLIOS_ADMIN_SLUG'=>$result['admin_slug'],
												'SHELLIOS_ADMIN_EMAIL'=>$result['admin_email_id'],
												'SHELLIOS_ADMIN_MOBILE'=>$result['admin_mobile_number'],
												'SHELLIOS_ADMIN_EMPLOYEE_ID'=>$result['admin_employee_id'],
												'SHELLIOS_ADMIN_LOGO'=>$result['admin_image'],
												'SHELLIOS_ADMIN_ADDRESS'=>$result['admin_address'],
												'SHELLIOS_ADMIN_LOCALITY'=>$result['admin_locality'],
												'SHELLIOS_ADMIN_CITY'=>$result['admin_city'],
												'SHELLIOS_ADMIN_STATE'=>$result['admin_state'],
												'SHELLIOS_ADMIN_ZIPCODE'=>$result['admin_zipcode'],
												'SHELLIOS_ADMIN_TYPE'=>$ATData['admin_metadata_value'],
												'SHELLIOS_ADMIN_LABEL'=>$ALData['admin_metadata_value'],
												'SHELLIOS_ADMIN_DEPARTMENT'=>$departmentName,
												'SHELLIOS_ADMIN_CURRENT_ID'=>$adminCurrentId,
												'SHELLIOS_ADMIN_CURRENT_NAME'=>$adminCurrentName,
												'SHELLIOS_ADMIN_CURRENT_DISPLAY_NAME'=>$adminCurrentDisplayName,
												'SHELLIOS_ADMIN_CURRENT_PATH'=>$adminCurrentPath,
												'SHELLIOS_ADMIN_CURRENT_LOGO'=>$adminCurrentLogo,
												'SHELLIOS_ADMIN_LAST_LOGIN'=>$result['admin_last_login'].' ('.$result['admin_last_login_ip'].')'));
						
							$param['admin_last_login']		=	currentDateTime();
							$param['admin_last_login_ip']	=	currentIp();
					
							$this->common_model->editData('admin',$param,'encrypt_id',$result['encrypt_id']);
						
							setcookie('SHELLIOS_ADMIN_LOGIN_EMAIL',$result['admin_email_id'],time()+60*60*24*100,'/');
							
							redirect($adminCurrentPath.'dashboard');
						endif;
					endif;
				else:
					$data['error'] = lang('invalidlogindetails');
				endif;			
			endif;
		endif;
		
		/*-----------------------------------Forgot password ---------------*/
		if($this->input->post('recoverformSubmit')):	
			//Set rules
			$this->form_validation->set_rules('forgotEmail', 'email', 'trim|required|valid_email');
			
			if($this->form_validation->run()):	
				$result		=	$this->adminauth_model->Authenticate($this->input->post('forgotEmail'));
				//echo "<pre>"; print_r($result); die;
				if($result):
					$param['admin_password_otp']		=	generateRandomString(6,'n');
					$this->common_model->editData('admin',$param,'encrypt_id',$result['encrypt_id']);
							
					$this->emailtemplate_model->sendPasswordRecoverMail($result,$param);
					$data['forgotsuccess'] = lang('sendforgotpassmail');
				else:
					$data['forgoterror'] = lang('invalidemail');
				endif;
			endif;
		endif;
		
		$this->layouts->set_title('Login');
		$this->layouts->admin_view('admin/login',array(),$data,'login');
	}
	
	/* * *********************************************************************
	 * * Function name : password_recover
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for admin password recover
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function password_recover()
	{	
		if($this->session->userdata('SHELLIOS_ADMIN_ID')) redirect($this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').'dashboard');
		$data['error'] 						= 	'';
		//$this->adminauth_model->checkAdminLoginCookie();

		/*-----------------------------------recover password ---------------*/
		if($this->input->post('passwordRecoverFormSubmit')):	
			//Set rules
			$this->form_validation->set_rules('userOtp', 'otp', 'trim|required|min_length[6]|max_length[6]');
			$this->form_validation->set_rules('userPassword', 'New password', 'trim|required|min_length[6]|max_length[25]');
			$this->form_validation->set_rules('userConfPassword', 'Confirm password', 'trim|required|min_length[6]|matches[userPassword]');
			
			if($this->form_validation->run()):	
				$result		=	$this->adminauth_model->checkOTP($this->input->post('userOtp'));
				if($result):
					$param['admin_password']		=	$this->adminauth_model->encriptPassword($this->input->post('userPassword'));
					$param['admin_password_otp']	=	'';
					$this->common_model->editData('admin',$param,'encrypt_id',$result['encrypt_id']);
							
					$data['recoversuccess'] = lang('passrecoversuccess');
				else:
					$data['recovererror'] = lang('invalidotp');
				endif;
			endif;
		endif;
		
		$this->layouts->set_title('Password Recover');
		$this->layouts->admin_view('admin/password_recover',array(),$data,'login');
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : dashboard
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for admin dashboard
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function dashboard()
	{	//echo "<pre>"; print_r($this->session->userdata()); die;
		$this->adminauth_model->authCheck('admin');
		$data['error'] 						= 	'';
		$data['activeMenu'] 				= 	'Dashboard';

		$this->layouts->set_title('Dashboard');
		$this->layouts->admin_view('admin/dashboard',array(),$data);
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : profile
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for admin profile
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function profile()
	{	
		$this->adminauth_model->authCheck('admin');
		$data['error'] 						= 	'';
		$data['activeMenu'] 				= 	'';
		$data['activeSubMenu'] 				= 	'';
		
		$whereCon['where']			 		= 	'adm.encrypt_id ="'.$this->session->userdata('SHELLIOS_ADMIN_ID').'"';		
		$shortField 						= 	'adm.admin_id ASC';
		
		$this->load->library('pagination');
		$config['base_url'] 				= 	$this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').$this->router->fetch_class().'/index';
		$tblName 							= 	'admin as adm';
		$con 								= 	'';
		$config['total_rows'] 				= 	$this->admin_model->selectAdminData('count',$tblName,$whereCon,$shortField,'0','0');
		$config['per_page']	 				= 	10;
		$config['uri_segment'] 				= 	getUrlSegment();
       $this->pagination->initialize($config);

       if ($this->uri->segment(getUrlSegment())):
           $page = $this->uri->segment(getUrlSegment());
       else:
           $page = 0;
       endif;
		
		$data['ADMINDATA'] 					= 	$this->admin_model->selectAdminData('data',$tblName,$whereCon,$shortField,$config['per_page'],$page); 
		//print_r($data['ADMINDATA']); die;
		$this->layouts->set_title('Profile');
		$this->layouts->admin_view('admin/profile',array(),$data);
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : editprofile
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for admin editprofile
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function editprofile($editId='')
	{	
		/*$originalpath	=	'/var/www/html/Car-Tamaam/./assets/adminImage/1533970809.png';
		if(file_exists($originalpath)):
			@unlink($originalpath);
		endif;

		echo 'die'; die;*/
		$this->adminauth_model->authCheck('admin');
		$data['error'] 						= 	'';
		$data['activeMenu'] 				= 	'';
		$data['activeSubMenu'] 				= 	'';
		
		$data['profileuserdata']			=	$this->common_model->getDataByParticularField('admin','encrypt_id',$editId); 
		if($data['profileuserdata'] == ''):
			redirect($this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').'dashboard');
		endif;
		
		if($this->input->post('SaveChanges')):
			$error							=	'NO';
			$this->form_validation->set_rules('admin_name', 'Name', 'trim|required');
			$this->form_validation->set_rules('admin_display_name', 'Display name', 'trim|required');
			if(sessionData('SHELLIOS_ADMIN_TYPE') == 'Super admin'):
				$this->form_validation->set_rules('admin_slug', 'Slug url', 'trim|required|is_unique[admin.admin_slug]');
			endif;
			$this->form_validation->set_rules('admin_email_id', 'E-Mail', 'trim|required|valid_email|is_unique[admin.admin_email_id]');
			if($this->input->post('new_password')!= ''):
				$this->form_validation->set_rules('new_password', 'New password', 'trim|required|min_length[6]|max_length[25]');
				$this->form_validation->set_rules('conf_password', 'Confirm password', 'trim|required|min_length[6]|matches[new_password]');
			endif;
			
			$this->form_validation->set_rules('admin_mobile_number', 'Mobile number', 'trim|required|min_length[10]|max_length[15]|is_unique[admin.admin_mobile_number]');
			$testmobile		=	str_replace(' ','',$this->input->post('admin_mobile_number'));
			if($this->input->post('admin_mobile_number') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$testmobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$testmobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please eneter correct number';
				endif;
			endif;
			
			$this->form_validation->set_rules('admin_address', 'Address', 'trim|required');
			$this->form_validation->set_rules('admin_locality', 'Locality', 'trim');
			$this->form_validation->set_rules('admin_city', 'City', 'trim');
			$this->form_validation->set_rules('admin_state', 'State', 'trim');
			$this->form_validation->set_rules('admin_zipcode', 'Zipcode', 'trim');
			$this->form_validation->set_rules('admin_image', 'Image', 'trim');
			
			if($this->form_validation->run() && $error == 'NO'):
			 
				$param['admin_name']				= 	addslashes($this->input->post('admin_name'));
				$param['admin_display_name']		= 	addslashes($this->input->post('admin_display_name'));
				if(sessionData('SHELLIOS_ADMIN_TYPE') == 'Super admin'):
					$param['admin_slug']			= 	strtolower(url_title(addslashes($this->input->post('admin_slug'))));
				endif;
				$param['admin_email_id']			= 	addslashes($this->input->post('admin_email_id'));
				$param['admin_mobile_number']		= 	addslashes($this->input->post('admin_mobile_number'));
				$param['admin_address']				= 	addslashes($this->input->post('admin_address'));
				$param['admin_locality']			= 	addslashes($this->input->post('admin_locality'));
				$param['admin_city']				= 	addslashes($this->input->post('admin_city'));
				$param['admin_state']				= 	addslashes($this->input->post('admin_state'));
				$param['admin_zipcode']				= 	addslashes($this->input->post('admin_zipcode'));
				$param['admin_image']				= 	addslashes($this->input->post('admin_image'));
				
				if($this->input->post('new_password')):
					$NewPassword					=	$this->input->post('new_password');
					$param['admin_password']		= 	$this->adminauth_model->encriptPassword($NewPassword);
				endif;
				
				$param['update_date']				=	currentDateTime();
				$param['updated_by']				=	$this->session->userdata('SHELLIOS_ADMIN_ID');
				$this->common_model->editData('admin',$param,'encrypt_id',$this->input->post('CurrentDataID'));
				
				$result		=	$this->adminauth_model->Authenticate($param['admin_email_id']);
				if($result):
					
					$ATQuery		=	"SELECT admin_metadata_value FROM ".getTablePrefix()."admin_metadata 
										 WHERE admin_id = '".$result['encrypt_id']."' AND admin_metadata_key = '_admin_type'";  
					$ATData			=	$this->common_model->getDataByQuery('single',$ATQuery); 
					
					$ALQuery		=	"SELECT admin_metadata_value FROM ".getTablePrefix()."admin_metadata 
										 WHERE admin_id = '".$result['encrypt_id']."' AND admin_metadata_key = '_admin_label'";  
					$ALData			=	$this->common_model->getDataByQuery('single',$ALQuery); 
					
					if($ATData['admin_metadata_value'] == 'Super admin' && $ALData['admin_metadata_value'] == '0'):
						
						$adminCurrentId				=	$result['encrypt_id'];
						$adminCurrentName			=	$result['admin_name'];
						$adminCurrentDisplayName	=	$result['admin_display_name'];
						$adminCurrentPath			=	base_url().'admin/'.$result['admin_slug'].'/';
						$adminCurrentLogo			=	$result['admin_image'];
						
						$this->session->set_userdata(array('SHELLIOS_ADMIN_CURRENT_ID'=>$adminCurrentId,
														   'SHELLIOS_ADMIN_CURRENT_NAME'=>$adminCurrentName,
														   'SHELLIOS_ADMIN_CURRENT_DISPLAY_NAME'=>$adminCurrentDisplayName,
														   'SHELLIOS_ADMIN_CURRENT_PATH'=>$adminCurrentPath,
														   'SHELLIOS_ADMIN_CURRENT_LOGO'=>$adminCurrentLogo));
					endif;
					
					$this->session->set_userdata(array('SHELLIOS_ADMIN_NAME'=>$result['admin_name'],
													   'SHELLIOS_ADMIN_DISLPLAY_NAME'=>$result['admin_display_name'],
													   'SHELLIOS_ADMIN_SLUG'=>$result['admin_slug'],
													   'SHELLIOS_ADMIN_EMAIL'=>$result['admin_email_id'],
													   'SHELLIOS_ADMIN_MOBILE'=>$result['admin_mobile_number'],
													   'SHELLIOS_ADMIN_EMPLOYEE_ID'=>$result['admin_employee_id'],
													   'SHELLIOS_ADMIN_LOGO'=>$result['admin_image'],
													   'SHELLIOS_ADMIN_ADDRESS'=>$result['admin_address'],
													   'SHELLIOS_ADMIN_LOCALITY'=>$result['admin_locality'],
													   'SHELLIOS_ADMIN_CITY'=>$result['admin_city'],
													   'SHELLIOS_ADMIN_STATE'=>$result['admin_state'],
													   'SHELLIOS_ADMIN_ZIPCODE'=>$result['admin_zipcode']));
											
					setcookie('SHELLIOS_ADMIN_LOGIN_EMAIL',$result['admin_email_id'],time()+60*60*24*100,'/');
					
					$this->session->set_flashdata('alert_success',lang('updatesuccess'));
					redirect($this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').'profile');
				endif;
			endif;
		endif;
		
		$this->layouts->set_title('Edit Profile');
		$this->layouts->admin_view('admin/editprofile',array(),$data);
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : firstImageUpload
	** Developed By : Manoj Kumar
	** Purpose  : This function used for first Image Upload
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function firstImageUpload()
	{ 
		if($this->input->post('imageData')): 
			$imageData				= 	$this->input->post('imageData');
			$imageName				= 	time().'.png';
			$imageFolder			=	'';
			
			$imageType				=	'adminImage';

			$this->load->library("upload_crop_img");
			$returnFileName		=	$this->upload_crop_img->_upload_canvas_image($imageData,$imageName,$imageType,$imageFolder);
			if($returnFileName):
				echo $returnFileName; die;
			else:
				echo 'UPLODEERROR'; die;
			endif;
		else:
			echo 'UPLODEERROR'; die;
		endif;
	}	// END OF FUNCTION	
	
	/***********************************************************************
	** Function name : firstImageDelete
 	** Developed By : Manoj Kumar
	** Purpose  : This function used for first Image Delete
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function firstImageDelete()
	{  
		$imageName	=	$this->input->post('imageName'); 
		if($imageName):
			$this->load->library("upload_crop_img");
			$return	=	$this->upload_crop_img->_delete_image($imageName); 
		endif;
		echo '1'; die;
	}	// END OF FUNCTION	
	
	/***********************************************************************
	** Function name : logout
	** Developed By : Manoj Kumar
	** Purpose  : This function used for logout
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function logout()
	{ //echo "<pre>"; print_r($this->session->userdata()); die;
		setcookie('SHELLIOS_ADMIN_LOGIN_EMAIL','',time()-60*60*24*100,'/');
		setcookie('SHELLIOS_ADMIN_REFERENCE_PAGES','',time()-60*60*24*100,'/');
		$this->session->unset_userdata(array('SHELLIOS_ADMIN_LOGGED_IN',
											 'SHELLIOS_ADMIN_ID',
											 'SHELLIOS_ADMIN_NAME',
											 'SHELLIOS_ADMIN_DISLPLAY_NAME',
											 'SHELLIOS_ADMIN_SLUG',
											 'SHELLIOS_ADMIN_EMAIL',
											 'SHELLIOS_ADMIN_MOBILE',
											 'SHELLIOS_ADMIN_EMPLOYEE_ID',
											 'SHELLIOS_ADMIN_LOGO',
											 'SHELLIOS_ADMIN_ADDRESS',
											 'SHELLIOS_ADMIN_LOCALITY',
											 'SHELLIOS_ADMIN_CITY',
											 'SHELLIOS_ADMIN_STATE',
											 'SHELLIOS_ADMIN_ZIPCODE',
											 'SHELLIOS_ADMIN_TYPE',
											 'SHELLIOS_ADMIN_LABEL',
											 'SHELLIOS_ADMIN_DEPARTMENT',
											 'SHELLIOS_ADMIN_CURRENT_ID',
											 'SHELLIOS_ADMIN_CURRENT_NAME',
											 'SHELLIOS_ADMIN_CURRENT_DISPLAY_NAME',
											 'SHELLIOS_ADMIN_CURRENT_PATH',
											 'SHELLIOS_ADMIN_CURRENT_LOGO',
											 'SHELLIOS_ADMIN_LAST_LOGIN'));
		redirect(base_url().'admin');
	}	// END OF FUNCTION
}