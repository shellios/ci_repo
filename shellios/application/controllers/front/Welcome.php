<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function  __construct() 
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('frontauth_model','front_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'front');
		$this->load->helper('front');
		$this->session->set_userdata('SHELLIOS_FRONT_CURRENT_PATH',base_url());
		$this->session->unset_userdata(array('installation','cur_address_id','checkout_tab','currentOrderId','buynow_prod_id','buynow_quantity','buynow_checkout_tab','buynow_address_id','buynow_installation','currentTranId'));
	} 

	/* * *********************************************************************
	 * * Function name : index
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for home page
	 * * Date : 05 OCTOBER 2018
	 * * **********************************************************************/
	public function index()
	{	
		echo "Welcome to Shellios"; die;
		
	}

	

	
}