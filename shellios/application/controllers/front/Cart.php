<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function  __construct() 
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('frontauth_model','front_model','emailtemplate_model','sms_model','notification_model'));
		$this->lang->load('statictext', 'front');
		$this->load->helper('front');
		$this->session->set_userdata('SHELLIOS_FRONT_CURRENT_PATH',base_url());
	} 

	/***********************************************************************
	** Function name : addtomycart
	** Developed By : Manoj Kumar
	** Purpose  : This function used for addtomycart page
	** Date : 22 OCTOBER 2018
	************************************************************************/
	public function addtomycart()
	{
		$data 								=	array('success'=>0,'message'=>lang('INVALID_ACCESS'),'result'=>'');
		if($this->input->post('cookieId') && $this->input->post('productId')):
			$cookieId						=	base64_decode($this->input->post('cookieId'));
			$userId							=	$this->input->post('userId')?base64_decode($this->input->post('userId')):'';
			$productId						=	base64_decode($this->input->post('productId'));
			
			if($userId):
				$addParam['user_id']		=	$userId;
			else:
				$addParam['cookie_id']		=	$cookieId;
			endif;
			$addParam['prod_id']			=	$productId;
			
			$checkCart						=	$this->front_model->checkInCart($addParam['user_id'],$addParam['cookie_id'],$addParam['prod_id']);
			if($checkCart == 'Y'):
				$productCountInCart			=	$this->front_model->getProductCountInCart($addParam['user_id'],$addParam['cookie_id']);
				$data 						=	array('success'=>2,'message'=>lang('ALREADY_IN_CART'),'result'=>'<span class="cart_item">'.$productCountInCart.'</span>');
			else:
				$addParam['quantity']		=	1;
				$addParam['browse_type']	=	'web';
				$addParam['creation_date']	=	currentDateTime();
				$calastInsertId				=	$this->common_model->addData('cart',$addParam);

				$cUparam['encrypt_id']		=	manojEncript($calastInsertId);
				$cUparam['cart_id']			=	generateUniqueId($calastInsertId);
				$cUwhere['id']				=	$calastInsertId;
				$this->common_model->editDataByMultipleCondition('cart',$cUparam,$cUwhere);

				$productCountInCart			=	$this->front_model->getProductCountInCart($addParam['user_id'],$addParam['cookie_id']);

				$data 						=	array('success'=>1,'message'=>lang('ADDTOCART_SUCCESS'),'result'=>'<span class="cart_item">'.$productCountInCart.'</span>');
			endif;			
		endif;
		header('Content-type: application/json');
		echo json_encode($data);
	}

	/***********************************************************************
	** Function name : getfrommycart
	** Developed By : Manoj Kumar
	** Purpose  : This function used for getfrommycart page
	** Date : 22 OCTOBER 2018
	************************************************************************/
	public function getfrommycart()
	{
		if($this->input->post('cookieId')):
			$cookieId					=	base64_decode($this->input->post('cookieId'));
			$userId						=	$this->input->post('userId')?base64_decode($this->input->post('userId')):'';
			
			$productCountInCart			=	$this->front_model->getProductCountInCart($userId,$cookieId);
			if($productCountInCart > 0):
				$data 					=	array('success'=>1,'message'=>'','result'=>'<span class="cart_item">'.$productCountInCart.'</span>');
			else:
				$data 					=	array('success'=>0,'message'=>'','result'=>'');
			endif;
		endif;
		header('Content-type: application/json');
		echo json_encode($data);
	}

	/* * *********************************************************************
	 * * Function name : cartview
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for view cart
	 * * Date : 15 OCTOBER 2018
	 * * **********************************************************************/
	public function cartview()
	{	
		$data['error'] 				= 	'';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$this->session->unset_userdata(array('installation','cur_address_id','checkout_tab','currentOrderId','toalPayAmount','paymentAddressId','buynow_prod_id','buynow_quantity','buynow_checkout_tab','buynow_address_id','buynow_installation','currentTranId'));
		$data['userId'] 			= 	sessionData('SHELLIOS_USER_ID');
		$data['cookieId'] 			= 	get_cookie('currentCartCookie');

		$this->layouts->set_title('CarTamaam');
		$this->layouts->set_description('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->set_keyword('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->front_view('front/cart/cartview',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : getcartdata
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get cart data
	 * * Date : 26 OCTOBER 2018
	 * * **********************************************************************/
	public function getcartdata()
	{	
		if($this->input->post('userId') || $this->input->post('cookieId')):
			$data['error'] 				= 	'';

			$data['userId'] 			= 	$this->input->post('userId');
			$data['cookieId'] 			= 	$this->input->post('cookieId');		
			$data['cartData']			=	$this->front_model->getCartData($data['userId'],$data['cookieId']);

			$JSONData['cartData']		=	$this->load->view('front/cart/getCartData',$data,TRUE);
			$returnData 				=	array('success'=>1,'message'=>'Success','result'=>$JSONData);

			header('Content-type: application/json');
			echo json_encode($returnData);
		endif;
	}

	/* * *********************************************************************
	 * * Function name : getcheckoutcartdata
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get checkout cart data
	 * * Date : 29 OCTOBER 2018
	 * * **********************************************************************/
	public function getcheckoutcartdata()
	{	
		if($this->input->post('userId') || $this->input->post('cookieId')):
			$data['error'] 				= 	'';

			$data['userId'] 			= 	$this->input->post('userId');
			$data['cookieId'] 			= 	$this->input->post('cookieId');		
			$data['cartData']			=	$this->front_model->getCartData($data['userId'],$data['cookieId']);

			$JSONData['cartData']		=	$this->load->view('front/cart/getcheckoutcartdata',$data,TRUE);
			$returnData 				=	array('success'=>1,'message'=>'Success','result'=>$JSONData);

			header('Content-type: application/json');
			echo json_encode($returnData);
		endif;
	}

	/* * *********************************************************************
	 * * Function name : plusminusquantity
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for plus minus quantity
	 * * Date : 26 OCTOBER 2018
	 * * **********************************************************************/
	public function plusminusquantity()
	{	
		if($this->input->post('cartId') && $this->input->post('quantity')):

			$cUparam['quantity']		=	$this->input->post('quantity');
			$cUwhere['cart_id']			=	base64_decode($this->input->post('cartId'));
			$this->common_model->editDataByMultipleCondition('cart',$cUparam,$cUwhere);

			$returnData 				=	array('success'=>1,'message'=>'Success','result'=>'');

			header('Content-type: application/json');
			echo json_encode($returnData);
		endif;
	}

	/* * *********************************************************************
	 * * Function name : deleteprodfromcart
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for delete prod from cart
	 * * Date : 26 OCTOBER 2018
	 * * **********************************************************************/
	public function deleteprodfromcart()
	{	
		if($this->input->post('cartId')):
			
			$dparams['cart_id']			=	base64_decode($this->input->post('cartId'));
			$this->common_model->deleteByMultipleCondition('cart',$dparams);

			$returnData 				=	array('success'=>1,'message'=>'Success','result'=>'');

			header('Content-type: application/json');
			echo json_encode($returnData);
		endif;
	}

	/* * *********************************************************************
	 * * Function name : checkout
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for checkout
	 * * Date : 29 OCTOBER 2018
	 * * **********************************************************************/
	public function checkout($segmentOne='',$segmentTwo='')
	{	
		$data['error'] 				= 	'';
		$data['formError']			=	'NO';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$this->session->unset_userdata(array('currentOrderId','toalPayAmount','paymentAddressId','buynow_prod_id','buynow_quantity','buynow_checkout_tab','buynow_address_id','buynow_installation','currentTranId'));
		$data['userId'] 			= 	sessionData('SHELLIOS_USER_ID');
		$data['cookieId'] 			= 	get_cookie('currentCartCookie');

		$data['cartData']			=	$this->front_model->getCartData($data['userId'],$data['cookieId']);
		if(!$data['cartData']):
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'));
		endif;

		if($segmentOne=='installation' && $segmentTwo=='YES'):  
			$this->session->set_userdata('installation','YES'); 
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/checkout');
		endif;
		
		if($segmentOne=='deliveryhere' && $segmentTwo!=''):
			$this->session->set_userdata('checkout_tab','Billing');
			$this->session->set_userdata('cur_address_id',$segmentTwo);
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/checkout');
		endif;

		if(!sessionData('checkout_tab')):
			if($data['userId']):
				$this->session->set_userdata('checkout_tab','Address');
			else:
				$this->session->set_userdata('checkout_tab','Login');
			endif;
		endif;
		$data['referalUrl']			=	base64_encode(uri_string());

		$data['addressData']		=	$this->front_model->getAddressData($data['userId']);
		
		if(sessionData('checkout_tab')=='Billing'):
			$data['curAddData']		=	$this->common_model->getDataByParticularField('users_address','address_id',sessionData('cur_address_id'));
		endif;

		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';
			$data['formError']		=	'YES';
			$this->form_validation->set_rules('add_name', 'Name', 'trim|required');
			$this->form_validation->set_rules('add_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('add_phone'));
			if($this->input->post('add_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('add_address1', 'Address 1', 'trim|required');
			$this->form_validation->set_rules('add_address2', 'Address 2', 'trim');
			$this->form_validation->set_rules('add_city', 'City', 'trim|required');
			$this->form_validation->set_rules('add_state', 'State', 'trim|required');
			$this->form_validation->set_rules('add_pincode', 'Pincode', 'trim|required');
			$this->form_validation->set_rules('add_type', 'Address Type', 'trim|required');

			if($this->form_validation->run() && $error == 'NO'): 
				$data['formError']			=	'NO';

				$param['add_name']			= 	addslashes($this->input->post('add_name'));
				$param['add_phone']			= 	addslashes($this->input->post('add_phone'));
				$param['add_address1']		= 	addslashes($this->input->post('add_address1'));
				$param['add_address2']		= 	addslashes($this->input->post('add_address2'));
				$param['add_city']			= 	addslashes($this->input->post('add_city'));
				$param['add_state']			= 	addslashes($this->input->post('add_state'));
				$param['add_pincode']		= 	addslashes($this->input->post('add_pincode'));
				$param['add_type']			= 	addslashes($this->input->post('add_type'));
				
				$param['user_id']			=	$data['userId'];
				$param['add_default']		=	'N';
				$param['creation_date']		=	currentDateTime();
				$param['status']			=	'Y';
				$alastInsertId				=	$this->common_model->addData('users_address',$param);
				
				$Uparam['encrypt_id']		=	manojEncript($alastInsertId);
				$Uparam['address_id']		=	generateUniqueId($alastInsertId);
				$Uwhere['id']				=	$alastInsertId;
				$this->common_model->editDataByMultipleCondition('users_address',$Uparam,$Uwhere);
				$this->session->set_flashdata('alert_success',lang('addsuccess'));

				$this->session->set_userdata('checkout_tab','Address');
				$this->session->set_userdata('cur_address_id',$Uparam['address_id']);

				redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/checkout');
			endif;
		endif;

		if($this->input->post('placeOrderFormSubmit')):
			if($this->input->post('you_know')):
				if($this->input->post('installation_charges')== 'YES'):
					$this->session->set_userdata('installation','YES');
				else:
					$this->session->unset_userdata('installation');
				endif;
				$order_comments			= 	addslashes($this->input->post('order_comments'));
				$you_know				= 	addslashes($this->input->post('you_know'));

				$ordAddData				=	$this->common_model->getDataByParticularField('users_address','address_id',sessionData('cur_address_id'));

				if($data['cartData'] <> ""):  
					$i=0;	  
					$toalPayAmount		=	0;
					$sessionId 			= 	getSessionForOrder();
					$orderIds			=	$this->front_model->getAllOrderIdBySeller($data['userId']);	
					
					foreach($data['cartData'] as $prodInfo):

						$params['user_id']				=	$data['userId'];
						$params['seller_id']			=	$prodInfo['seller_id'];
						$params['prod_id']				=	$prodInfo['prod_id'];

						$params['session_id']			=	$sessionId;
						$params['order_id']				=	$orderIds[$prodInfo['seller_id']];
						$params['quantity']				=	$prodInfo['quantity'];
						$params['amount']				=	$prodInfo['prod_rmp'];
						$totalAmount 					=	($params['quantity']*$params['amount']);
						$params['total_amount']			=	($totalAmount+(($totalAmount*18)/100));//($params['quantity']*$params['amount']);
						$params['gst_amount']			=	'18';//$prodInfo['prod_gst'];

						$toalPayAmount 					=	($toalPayAmount+$params['total_amount']);

						if(sessionData('installation')=='YES'):
							$params['installation_charges']	=	$prodInfo['seller_installation_charges'];
						endif;
						
						$params['shipping_name']		=	$ordAddData['add_name'];
						$params['shipping_phone']		=	$ordAddData['add_phone'];
						$params['shipping_address1']	=	$ordAddData['add_address1'];
						$params['shipping_address2']	=	$ordAddData['add_address2'];
						$params['shipping_city']		=	$ordAddData['add_city'];
						$params['shipping_state']		=	$ordAddData['add_state'];
						$params['shipping_pincode']		=	$ordAddData['add_pincode'];
						$params['shipping_add_type']	=	$ordAddData['add_type'];

						$params['order_comments']		=	$order_comments;
						$params['you_know']				=	$you_know;

						$params['seller_accepted']		=	'Sent';
						$params['seller_remark']		=	'';
						$params['seller_action_date']	=	'1970-01-01 00:00:00';

						$params['payment_status']		=	'0';
						$params['payment_type']			=	'Razorpay';
						$params['tran_id']				=	'';
						$params['browse_type']			=	'Confirmed';
						$params['browse_type']			=	'web';
						$params['order_date']			=	currentDateTime();

						$orderId 	=	$this->common_model->addData('orders',$params);

						$uParam['encrypt_id']			=	manojEncript($orderId);
						$this->common_model->editData('orders',$uParam,'id',$orderId);
						$i++;
					endforeach;

					$dparams['user_id']					=	$data['userId'];
					$this->common_model->deleteByMultipleCondition('cart',$dparams);

					$this->session->set_userdata('currentOrderId',$sessionId);
					$this->session->set_userdata('toalPayAmount',$toalPayAmount);
					$this->session->set_userdata('paymentAddressId',sessionData('cur_address_id'));

					$this->session->unset_userdata(array('installation','cur_address_id','checkout_tab'));

					redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/payment');
				endif; 
			endif;
		endif;

		$this->layouts->set_title('CarTamaam');
		$this->layouts->set_description('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->set_keyword('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->front_view('front/cart/checkout',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : getbuynowcheckoutdata
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get get buy now check out data
	 * * Date : 30 OCTOBER 2018
	 * * **********************************************************************/
	public function getbuynowcheckoutdata()
	{	
		if($this->input->post('prodId')):
			$data['error'] 				= 	'';

			$data['userId'] 			= 	$this->input->post('userId');
			$data['prodId'] 			= 	$this->input->post('prodId');	
			$data['prodQty'] 			= 	sessionData('buynow_quantity');		
			$data['cartData']			=	$this->front_model->getBuyNowCheckoutData($data['prodId']);

			$JSONData['cartData']		=	$this->load->view('front/cart/getbuynowcheckoutdata',$data,TRUE);
			$returnData 				=	array('success'=>1,'message'=>'Success','result'=>$JSONData);

			header('Content-type: application/json');
			echo json_encode($returnData);
		endif;
	}

	/* * *********************************************************************
	 * * Function name : plusminusbuynowquantity
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for plus minus buy now quantity
	 * * Date : 30 OCTOBER 2018
	 * * **********************************************************************/
	public function plusminusbuynowquantity()
	{	
		if($this->input->post('prodId') && $this->input->post('quantity')):

			$data['userId']		=	$this->input->post('userId');
			$data['prodId']		=	$this->input->post('prodId');
			$data['quantity']	=	$this->input->post('quantity');
			$this->session->set_userdata('buynow_quantity',$data['quantity']);

			$returnData 			=	array('success'=>1,'message'=>'Success','result'=>'');

			header('Content-type: application/json');
			echo json_encode($returnData);
		endif;
	}

	/* * *********************************************************************
	 * * Function name : buynowcheckout
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for buy now checkout
	 * * Date : 30 OCTOBER 2018
	 * * **********************************************************************/
	public function buynowcheckout($segmentOne='',$segmentTwo='')
	{	
		$data['error'] 				= 	'';
		$data['formError']			=	'NO';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$this->session->unset_userdata(array('installation','cur_address_id','checkout_tab','currentOrderId','toalPayAmount','paymentAddressId','currentTranId'));
		$data['userId'] 			= 	sessionData('SHELLIOS_USER_ID');
		$data['prodId'] 			= 	sessionData('buynow_prod_id');
		$data['prodQty'] 			= 	sessionData('buynow_quantity');
		//manvendra@cartamaam.com

		if($segmentOne=='productId' && $segmentTwo!=''):  
			$this->session->set_userdata('buynow_prod_id',$segmentTwo); 
			$this->session->set_userdata('buynow_quantity',1); 
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/buy-now-checkout');
		endif;

		if(!sessionData('buynow_prod_id')):
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'));
		endif;

		if($segmentOne=='deliveryhere' && $segmentTwo!=''):
			$this->session->set_userdata('buynow_checkout_tab','Billing');
			$this->session->set_userdata('buynow_address_id',$segmentTwo);
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/buy-now-checkout');
		endif;

		if(!sessionData('buynow_checkout_tab')):
			if($data['userId']):
				$this->session->set_userdata('buynow_checkout_tab','Address');
			else:
				$this->session->set_userdata('buynow_checkout_tab','Login');
			endif;
		endif;

		$data['referalUrl']			=	base64_encode(uri_string());

		$data['addressData']		=	$this->front_model->getAddressData($data['userId']);
		
		if(sessionData('buynow_checkout_tab')=='Billing'):
			$data['curAddData']		=	$this->common_model->getDataByParticularField('users_address','address_id',sessionData('buynow_address_id'));
		endif;

		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';
			$data['formError']		=	'YES';
			$this->form_validation->set_rules('add_name', 'Name', 'trim|required');
			$this->form_validation->set_rules('add_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('add_phone'));
			if($this->input->post('add_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('add_address1', 'Address 1', 'trim|required');
			$this->form_validation->set_rules('add_address2', 'Address 2', 'trim');
			$this->form_validation->set_rules('add_city', 'City', 'trim|required');
			$this->form_validation->set_rules('add_state', 'State', 'trim|required');
			$this->form_validation->set_rules('add_pincode', 'Pincode', 'trim|required');
			$this->form_validation->set_rules('add_type', 'Address Type', 'trim|required');

			if($this->form_validation->run() && $error == 'NO'): 
				$data['formError']			=	'NO';

				$param['add_name']			= 	addslashes($this->input->post('add_name'));
				$param['add_phone']			= 	addslashes($this->input->post('add_phone'));
				$param['add_address1']		= 	addslashes($this->input->post('add_address1'));
				$param['add_address2']		= 	addslashes($this->input->post('add_address2'));
				$param['add_city']			= 	addslashes($this->input->post('add_city'));
				$param['add_state']			= 	addslashes($this->input->post('add_state'));
				$param['add_pincode']		= 	addslashes($this->input->post('add_pincode'));
				$param['add_type']			= 	addslashes($this->input->post('add_type'));
				
				$param['user_id']			=	$data['userId'];
				$param['add_default']		=	'N';
				$param['creation_date']		=	currentDateTime();
				$param['status']			=	'Y';
				$alastInsertId				=	$this->common_model->addData('users_address',$param);
				
				$Uparam['encrypt_id']		=	manojEncript($alastInsertId);
				$Uparam['address_id']		=	generateUniqueId($alastInsertId);
				$Uwhere['id']				=	$alastInsertId;
				$this->common_model->editDataByMultipleCondition('users_address',$Uparam,$Uwhere);
				$this->session->set_flashdata('alert_success',lang('addsuccess'));

				$this->session->set_userdata('buynow_checkout_tab','Address');
				$this->session->set_userdata('buynow_address_id',$Uparam['address_id']);

				redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/buy-now-checkout');
			endif;
		endif;

		if($this->input->post('placeOrderFormSubmit')):
			if($this->input->post('you_know')):
				if($this->input->post('installation_charges')== 'YES'):
					$this->session->set_userdata('buynow_installation','YES');
				else:
					$this->session->unset_userdata('buynow_installation');
				endif;
				$order_comments			= 	addslashes($this->input->post('order_comments'));
				$you_know				= 	addslashes($this->input->post('you_know'));

				$ordAddData				=	$this->common_model->getDataByParticularField('users_address','address_id',sessionData('buynow_address_id'));

				$cartData				=	$this->front_model->getBuyNowCheckoutData($data['prodId']);

				if($cartData <> ""):    
					$toalPayAmount		=	0;
					$sessionId 			= 	getSessionForOrder();
					$orderId			=	getRandomOrderId();
					
					$params['user_id']				=	$data['userId'];
					$params['seller_id']			=	$cartData['seller_id'];
					$params['prod_id']				=	$cartData['prod_id'];

					$params['session_id']			=	$sessionId;
					$params['order_id']				=	$orderId;
					$params['quantity']				=	$data['prodQty'];
					$params['amount']				=	$cartData['prod_rmp'];
					$totalAmount 					=	($params['quantity']*$params['amount']);
					$params['total_amount']			=	($totalAmount+(($totalAmount*18)/100));//($params['quantity']*$params['amount']);
					$params['gst_amount']			=	'18';//$cartData['prod_gst'];

					$toalPayAmount 					=	($toalPayAmount+$params['total_amount']);

					if(sessionData('buynow_installation')=='YES'):
						$params['installation_charges']	=	$cartData['seller_installation_charges'];
					endif;
					
					$params['shipping_name']		=	$ordAddData['add_name'];
					$params['shipping_phone']		=	$ordAddData['add_phone'];
					$params['shipping_address1']	=	$ordAddData['add_address1'];
					$params['shipping_address2']	=	$ordAddData['add_address2'];
					$params['shipping_city']		=	$ordAddData['add_city'];
					$params['shipping_state']		=	$ordAddData['add_state'];
					$params['shipping_pincode']		=	$ordAddData['add_pincode'];
					$params['shipping_add_type']	=	$ordAddData['add_type'];

					$params['order_comments']		=	$order_comments;
					$params['you_know']				=	$you_know;

					$params['seller_accepted']		=	'Sent';
					$params['seller_remark']		=	'';
					$params['seller_action_date']	=	'1970-01-01 00:00:00';

					$params['payment_status']		=	'0';
					$params['payment_type']			=	'Razorpay';
					$params['tran_id']				=	'';
					$params['browse_type']			=	'Confirmed';
					$params['browse_type']			=	'web';
					$params['order_date']			=	currentDateTime();

					$orderId 						=	$this->common_model->addData('orders',$params);

					$uParam['encrypt_id']			=	manojEncript($orderId);
					$this->common_model->editData('orders',$uParam,'id',$orderId);

					$this->session->set_userdata('currentOrderId',$sessionId);
					$this->session->set_userdata('toalPayAmount',$toalPayAmount);
					$this->session->set_userdata('paymentAddressId',sessionData('buynow_address_id'));

					$this->session->unset_userdata(array('buynow_prod_id','buynow_quantity','buynow_checkout_tab','buynow_address_id','buynow_installation'));

					redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/payment');
				endif; 
			endif;
		endif;

		$this->layouts->set_title('CarTamaam');
		$this->layouts->set_description('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->set_keyword('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->front_view('front/cart/buynowcheckout',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : payment
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for payment
	 * * Date : 31 OCTOBER 2018
	 * * **********************************************************************/
	public function payment()
	{	
		$data['error'] 					= 	'';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$this->session->unset_userdata('currentTranId');

		if(!sessionData('currentOrderId') || !sessionData('toalPayAmount')):
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'));
		endif;
		$data['ordAddData']				=	$this->common_model->getDataByParticularField('users_address','address_id',sessionData('paymentAddressId'));

		$data['successUrl']				=	$this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH')."user/payment-success";
		$data['rozPayUrl']				=	ROZ_PAY_URL;
		$data['keyId']					=	ROZ_PAY_KEY_ID;
		$data['buttonText']				=	ROZ_PAY_BUTTON_TEXT;
		$data['siteName']				=	ROZ_PAY_SITE_NAME;
		$data['siteDesc']				=	ROZ_PAY_SITE_DESC;
		$data['siteLogo']				=	base_url().ROZ_PAY_SITE_LOGO;
		$data['themeColor']				=	ROZ_PAY_THEME_COLOR;
		
		$data['payAmount']				=	(sessionData('toalPayAmount')*100);

		$data['userId'] 				= 	sessionData('SHELLIOS_USER_ID');
		$data['userName'] 				= 	sessionData('SHELLIOS_USER_NAME');
		$data['userEmail'] 				= 	sessionData('SHELLIOS_USER_EMAIL');
		$data['userPhone'] 				= 	sessionData('SHELLIOS_USER_PHONE');

		$data['orderId']				=	sessionData('currentOrderId');

		$this->layouts->set_title('CarTamaam');
		$this->layouts->set_description('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->set_keyword('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->front_view('front/cart/payment',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : paymentsuccess
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for payment success
	 * * Date : 30 NOVEMBER 2018
	 * * **********************************************************************/
	public function paymentsuccess()
	{	//echo '<pre>'; print_r($_POST); die;
		$data['error'] 					= 	'';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$this->session->unset_userdata('currentTranId');

		if(!sessionData('currentOrderId') || !sessionData('toalPayAmount')):
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'));
		endif;
		$data['userId'] 				= 	sessionData('SHELLIOS_USER_ID');
		$data['userPhone'] 				= 	sessionData('SHELLIOS_USER_PHONE');

		$data['orderId']				=	sessionData('currentOrderId');
		if($_REQUEST['razorpay_payment_id'] != ''):
			$data['paymentStatus']			=	1;
			$data['txnId']					=	$_REQUEST['razorpay_payment_id'];
		else:
			$data['paymentStatus']			=	2;
			$data['txnId']					=	'fail_'.generateRandomString(15);
		endif;

		$orderData   					=	$this->front_model->getOrderData($data['userId'],$data['orderId']);

		if($data['paymentStatus'] == 1):
			$uparams['payment_status']	=	'1';
			$uparams['tran_id']			=	$data['txnId'];

			$uwhere['user_id']			=	$data['userId'];
			$uwhere['session_id']		=	$data['orderId'];
			$uwhere['payment_status']	=	'0';
			$uwhere['browse_type']		=	'web';
			$this->common_model->editDataByMultipleCondition('orders',$uparams,$uwhere);

			//////////////////		USER 	/////////////////
			$this->sms_model->sendPaymentSuccessSmsToUser($data['userPhone']);
			$this->notification_model->sendPaymentSuccessNotificationToUser($data['userPhone'],$data['orderId']);
			$this->emailtemplate_model->sendPaymentSuccessMailToUser($data['userId'],$data['orderId']);

			////////////////////		SELLER 	/////////////////
			$this->sms_model->sendPaymentSuccessSmsToSeller($orderData[0]['seller_id']);
			//$this->notification_model->sendPaymentSuccessNotificationToSeller($orderData['seller_id'],$orderId);
			$this->emailtemplate_model->sendPaymentSuccessMailToSeller($data['userId'],$data['orderId']);

			////////////////////		ADMIN 	/////////////////
			$this->emailtemplate_model->sendPaymentSuccessMailToAdmin($data['userId'],$data['orderId']);

			$this->session->unset_userdata(array('currentOrderId','toalPayAmount','paymentAddressId'));
			$this->session->set_userdata('currentTranId',$data['txnId']);

			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/payment/thanks');

		elseif($data['paymentStatus'] == 2):
			$uparams['payment_status']	=	'2';
			$uparams['tran_id']			=	$data['txnId'];

			$uwhere['user_id']			=	$data['userId'];
			$uwhere['session_id']		=	$data['orderId'];
			$uwhere['payment_status']	=	'0';
			$uwhere['browse_type']		=	'web';
			$this->common_model->editDataByMultipleCondition('orders',$uparams,$uwhere);

			//////////////////		USER 	/////////////////
			$this->sms_model->sendPaymentFalureSmsToUser($data['userPhone']);
			$this->notification_model->sendPaymentFalureNotificationToUser($data['userPhone'],$data['orderId']);

			$this->session->unset_userdata(array('currentOrderId','toalPayAmount','paymentAddressId'));
			$this->session->set_userdata('currentTranId',$data['txnId']);

			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/payment/failure');

		endif;
	}

	/* * *********************************************************************
	 * * Function name : thanks
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for payment success
	 * * Date : 31 OCTOBER 2018
	 * * **********************************************************************/
	public function thanks()
	{	
		$data['error'] 					= 	'';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$data['txnId']					=	sessionData('currentTranId');

		$this->layouts->set_title('CarTamaam');
		$this->layouts->set_description('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->set_keyword('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->front_view('front/cart/thanks',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : failure
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for payment failure
	 * * Date : 31 OCTOBER 2018
	 * * **********************************************************************/
	public function failure()
	{	
		$data['error'] 					= 	'';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$data['txnId']					=	sessionData('currentTranId');

		$this->layouts->set_title('CarTamaam');
		$this->layouts->set_description('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->set_keyword('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->front_view('front/cart/failure',array(),$data);
	}
}