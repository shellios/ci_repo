<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function  __construct() 
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('frontauth_model','front_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'front');
		$this->load->helper('front');
		$this->session->set_userdata('SHELLIOS_FRONT_CURRENT_PATH',base_url());
		$this->session->unset_userdata(array('installation','cur_address_id','checkout_tab','currentOrderId','buynow_prod_id','buynow_quantity','buynow_checkout_tab','buynow_address_id','buynow_installation','currentTranId'));
	} 

	/* * *********************************************************************
	 * * Function name : listing
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for product listing
	 * * Date : 24 OCTOBER 2018
	 * * **********************************************************************/
	public function listing($category='')
	{	
		$data['error'] 						= 	'';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');
		$data['category'] 					= 	$category;
		$data['categorySlug'] 				= 	de_url_title($category);
		$data['categoryData']				= 	$this->front_model->getProductCategoryBySlug($data['category']); 
		if($data['categoryData'] == ""):
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'));
		endif;

		$data['carBrandData']				= 	$this->front_model->getCarBrand(); 
		$data['subCategoryData']			= 	$this->front_model->getProductSubCategory($data['categoryData']['prod_cate_id']); 
		$data['priceData']					= 	priceRangeArray(); 

		$this->layouts->set_title('CarTamaam');
		$this->layouts->set_description('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->set_keyword('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->front_view('front/product/listing',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : getcerbrandmodel
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get cer brand model
	 * * Date : 25 OCTOBER 2018
	 * * **********************************************************************/
	public function getcerbrandmodel()
	{	
		if($this->input->post('carBrandId')):
			$data['error'] 						= 	'';

			$data['carBrandId'] 				= 	$this->input->post('carBrandId');	
			$data['carBrandModelData']			=	$this->front_model->getCarBrandModel($data['carBrandId']);
			
			if($data['carBrandModelData'] <> ""):
				$JSONData['carBrandModelData']	=	$this->load->view('front/product/getCarBrandModel',$data,TRUE);
				$returnData 					=	array('success'=>1,'message'=>'Success','result'=>$JSONData);
			else:
				$JSONData['carBrandModelData']	=	$this->load->view('front/product/getCarBrandModel',$data,TRUE);
				$returnData 					=	array('success'=>0,'message'=>'Error','result'=>$JSONData);
			endif;

			header('Content-type: application/json');
			echo json_encode($returnData);
		endif;
	}

	/* * *********************************************************************
	 * * Function name : getproductlist
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get product list
	 * * Date : 24 OCTOBER 2018
	 * * **********************************************************************/
	public function getproductlist()
	{	
		if($this->input->post('categoryId') && $this->input->post('shortBy') && $this->input->post('showData')):
			$data['error'] 						= 	'';
			$data['carBrandModel'] 				= 	array();

			$data['categoryId'] 				= 	$this->input->post('categoryId');
			$data['carBrand']					=	$this->input->post('carBrand');
			if($this->input->post('carBrand')):
				$carBrand						=	$this->input->post('carBrand');
				foreach($carBrand as $carBrandInfo):
					if($this->input->post('carBrandModel'.$carBrandInfo) && $this->input->post('carBrandModel'.$carBrandInfo) != 'ALL'):
						$carBrandModel			=	explode('_____',$this->input->post('carBrandModel'.$carBrandInfo));
						$data['carBrandModel']	=	array_merge($data['carBrandModel'],$carBrandModel);
					endif;
				endforeach;
			endif;
			$data['subCategory']				=	$this->input->post('subCategory');
			$data['priceData']					=	$this->input->post('priceData');
			$data['showFrom']					=	0;
			$data['showTo'] 					= 	0;
			$data['action']						=	'count';

			$data['totalRows']					= 	$this->front_model->getProductListData($data);

			$data['shortBy']					=	$this->input->post('shortBy');
			$data['showData']					=	$this->input->post('showData');
			$data['showFrom']					=	$this->input->post('noOfStart');
			$data['showTo'] 					= 	$this->input->post('noOfEnd');
			$data['action']						=	'data';

			$data['prodData']					=	$this->front_model->getProductListData($data);
			
			if($data['totalRows'] > $data['showFrom']):
				$noOfStart						=	$data['showFrom']+$data['showTo'];
				$noOfEnd						=	$data['showTo'];
			else:
				$noOfStart						=	$data['showFrom'];
				$noOfEnd						=	$data['showTo'];	
			endif;
			$JSONData['noOfStart']				=	$noOfStart;
			$JSONData['noOfEnd']				=	$noOfEnd;
			$JSONData['noOfTotal']				=	$data['totalRows'];	
			
			if($data['prodData'] <> ""):
				$JSONData['prodData']			=	$this->load->view('front/product/getProductListData',$data,TRUE);
				$returnData 					=	array('success'=>1,'message'=>'Success','result'=>$JSONData);
			else:
				$JSONData['prodData']			=	$this->load->view('front/product/getNoProductData',$data,TRUE);
				$returnData 					=	array('success'=>0,'message'=>'Error','result'=>$JSONData);
			endif;

			header('Content-type: application/json');
			echo json_encode($returnData);
		endif;
	}

	/* * *********************************************************************
	 * * Function name : details
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for product details
	 * * Date : 25 OCTOBER 2018
	 * * **********************************************************************/
	public function details($category='',$subCategory='',$prodct='')
	{	
		$data['error'] 						= 	'';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$data['raviewFormError']			=	'NO';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');
		$data['category'] 					= 	$category;
		$data['categorySlug'] 				= 	de_url_title($category);
		$data['categoryData']				= 	$this->front_model->getProductCategoryBySlug($data['category']); 
		$data['subCategory'] 				= 	$subCategory;
		$data['subCategorySlug'] 			= 	de_url_title($subCategory);
		$data['subCategoryData']			= 	$this->front_model->getProductSubCategoryBySlug($data['subCategory']); 
		$data['product'] 					= 	$prodct;
		$data['productSlug'] 				= 	de_url_title($prodct);
		$data['productData']				= 	$this->front_model->getProductBySlug($data['product']); 
		if($data['categoryData'] == "" || $data['subCategoryData'] == "" || $data['productData'] == ""):
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'));
		endif;

		$data['prodImage']  				= 	$this->front_model->getProductImage($data['productData']['prod_id'],'3'); 
		$data['wishlist']  					= 	$this->front_model->checkInWishlist(sessionData('SHELLIOS_USER_ID'),$data['productData']['prod_id']); 
      	$data['ratingData'] 				=	$this->front_model->getRatingbyProductId($data['productData']['prod_id']); 
      	$data['checkInOrder']  				= 	$this->front_model->checkProductInOrder(sessionData('SHELLIOS_USER_ID'),$data['productData']['prod_id']); 
      	$data['reviewData'] 				=	$this->front_model->getRatingReviewbyProductId($data['productData']['prod_id']); 

      	$data['similarProductData'] 		=	$this->front_model->getSimilarProducts($data['productData']['prod_cate_id'],$data['productData']['prod_sub_cate_id'],$data['productData']['prod_id']);

      	$recViewPro 					=	$this->front_model->checkProductInRecentView(get_cookie('recentViewCookie'),$data['productData']['prod_id']); 
      	if($recViewPro == 'N'): 
      		$RVparam['cookie_id']		=	get_cookie('recentViewCookie');
			$RVparam['prod_id']			=	$data['productData']['prod_id'];
			$RVparam['creation_date']	=	currentDateTime();
			$RVparam['status']			=	'Y';
			$RVlastInsertId				=	$this->common_model->addData('recently_viewed',$RVparam);
			
			$RVuParam['encrypt_id']		=	manojEncript($RVlastInsertId);
			$RVuParam['viewed_id']		=	generateUniqueId($RVlastInsertId);
			$this->common_model->editData('recently_viewed',$RVuParam,'id',$RVlastInsertId);
      	endif;

      	if($this->input->post('currentPageFormSubmit')):  
			$error							=	'NO';
			$data['raviewFormError']		=	'YES';
			$this->form_validation->set_rules('rating', 'Rating', 'trim|required');
			$this->form_validation->set_rules('heading', 'Heading', 'trim|required');
			$this->form_validation->set_rules('content', 'Content', 'trim|required');

			if($this->form_validation->run() && $error == 'NO'): 

				$param['user_id']			=	$data['userId'];
				$param['prod_id']			=	$data['productData']['prod_id'];
				$param['rating']			=	addslashes(trim($this->input->post('rating')));
				$param['heading']			=	addslashes(trim($this->input->post('heading')));
				$param['content']			=	addslashes(trim($this->input->post('content')));
				$param['browse_type']		=	'web';
				$param['creation_date']		=	currentDateTime();
				$lastInsertId				=	$this->common_model->addData('prod_user_review',$param);
				
				$uParam['encrypt_id']		=	manojEncript($lastInsertId);
				$uParam['review_id']		=	generateUniqueId($lastInsertId);
				$this->common_model->editData('prod_user_review',$uParam,'id',$lastInsertId);

				$this->session->set_flashdata('alert_success',lang('GIVEREVIEW_SUCCESS')); 
				redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'product-details/'.$data['category'].'/'.$data['subCategory'].'/'.$data['product']);
			endif;
		endif;

		$this->layouts->set_title('CarTamaam');
		$this->layouts->set_description('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->set_keyword('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->front_view('front/product/details',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : search
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for product search
	 * * Date : 01 NOVEMBER 2018
	 * * **********************************************************************/
	public function search()
	{	
		$data['error'] 						= 	'';
		$this->frontauth_model->checkOnlyUserLoginCookie();
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');
		$data['searchText'] 				= 	$this->input->get('searchText');
		if($data['searchText'] == ""):
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'));
		endif;

		$data['prodData']					=	$this->front_model->getSearchProductListData($data['searchText'],'data',100);

		//echo '<pre>'; print_r($data); die;
		$this->layouts->set_title('CarTamaam');
		$this->layouts->set_description('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->set_keyword('CarTamaam - Car Accessories At Your Door Step!');
		$this->layouts->front_view('front/product/search',array(),$data);
	}
}