<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function  __construct() 
	{ 
		parent:: __construct();
		error_reporting(E_ALL ^ E_NOTICE);  
		$this->load->model(array('frontauth_model','front_model','emailtemplate_model','sms_model'));
		$this->lang->load('statictext', 'front');
		$this->load->helper('front');
		$this->session->set_userdata('SHELLIOS_FRONT_CURRENT_PATH',base_url());
		$this->session->unset_userdata(array('cur_address_id','checkout_tab','currentOrderId','buynow_checkout_tab','buynow_address_id','buynow_installation','currentTranId'));
	} 

	/* * *********************************************************************
	 * * Function name : signup
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user signup
	 * * Date : 11 OCTOBER 2018
	 * * **********************************************************************/
	public function signup($loginReferalUrl='')
	{	
		$this->session->unset_userdata(array('SHELLIOS_REGISTER_MOBILE','SHELLIOS_ERROR_USER_ID','SHELLIOS_ERROR_USER_PHONE','SHELLIOS_FORGOT_USER_PHONE'));
		if($this->session->userdata('SHELLIOS_USER_ID')) redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-account');
		$data['error'] 						= 	'';
		$this->frontauth_model->checkUserLoginCookie();

		if($loginReferalUrl):
			$this->session->set_userdata('SHELLIOS_LOGIN_REF_URL',$loginReferalUrl);
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/signup');
		endif;

		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';
			$this->form_validation->set_rules('user_name', 'Name', 'trim|required');
			$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]|is_unique[users.user_phone]');
			$usermobile		=	str_replace(' ','',$this->input->post('user_phone'));
			if($this->input->post('user_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('user_email', 'E-Mail', 'trim|required|valid_email|is_unique[users.user_email]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[25]');
			$this->form_validation->set_rules('conf_password', 'Confirm password', 'trim|required|min_length[6]|matches[password]');

			if($this->form_validation->run() && $error == 'NO'): 

				$userPassword 				=	trim($this->input->post('password'));

				$param['user_type']			=	'register';
				$param['network_id']		=	'';
				$param['device_id']			=	'';
				$param['browse_type']		=	'web';
				$param['user_name']			=	addslashes(trim($this->input->post('user_name')));
				$param['user_email']		=	addslashes(trim($this->input->post('user_email')));
				$param['user_phone']		=	addslashes(trim($this->input->post('user_phone')));
				$param['user_phone_verify']	=	'N';
				$param['user_password']		=	$this->frontauth_model->encriptPassword($userPassword);
				$param['user_otp']			=	generateRandomString(4,'n');
				$param['creation_date']		=	currentDateTime();
				$param['status']			=	'I';
				$userId						=	$this->common_model->addData('users',$param);
				
				$uParam['encrypt_id']		=	manojEncript($userId);
				$uParam['user_id']			=	generateUniqueId($userId);
				$this->common_model->editData('users',$uParam,'id',$userId);

				$this->sms_model->sendOtpSmsToUser($param['user_phone'],$param['user_otp']);

				$this->emailtemplate_model->UserRegistrationMailToUser($uParam['user_id'],$userPassword);
				$this->emailtemplate_model->UserRegistrationMailToAdmin($uParam['user_id']);

				$this->session->set_userdata('SHELLIOS_REGISTER_MOBILE',$param['user_phone']);

				$this->session->set_flashdata('alert_success',lang('SEND_OTP_TO_RMN')); 
				redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/otp-verification');
			endif;
		endif;

		$this->layouts->set_title('Signup');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/signup',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : otpverification
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user otp verification
	 * * Date : 11 OCTOBER 2018
	 * * **********************************************************************/
	public function otpverification()
	{	
		$data['error'] 						= 	'';
		$data['formError'] 					=	''; 	

		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';
			$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('user_phone'));
			if($this->input->post('user_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('user_otp', 'OTP', 'trim|required');

			if($this->form_validation->run() && $error == 'NO'): 

				$userData		=	$this->frontauth_model->checkOTP(trim($this->input->post('user_phone')),trim($this->input->post('user_otp')));
				if($userData <> ""):  
					if($userData['status'] == 'I'):
						$param['user_phone_verify']	=	'Y';
						$param['user_otp']			=	'';
						$param['status']			=	'A';
						$this->common_model->editData('users',$param,'user_id',$userData['user_id']);

						$this->session->unset_userdata('SHELLIOS_REGISTER_MOBILE');

						$this->session->set_flashdata('alert_success',lang('VERIFY_OTP')); 
						redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/login');
					else:
						$data['formError'] 		= 	lang('ACCOUNT_BLOCKED');
					endif;
				else:
					$data['formError'] 			= 	lang('OTP_INCORRECT');
				endif;
			endif;
		endif;

		$this->layouts->set_title('OTP Verification');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/otpverification',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : login
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user login
	 * * Date : 11 OCTOBER 2018
	 * * **********************************************************************/
	public function login($loginReferalUrl='')
	{	
		$this->session->unset_userdata(array('SHELLIOS_REGISTER_MOBILE','SHELLIOS_ERROR_USER_ID','SHELLIOS_ERROR_USER_PHONE','SHELLIOS_FORGOT_USER_PHONE'));
		$this->session->unset_userdata(array('SHELLIOS_NOTVERIFY_MOBILE','SHELLIOS_REMEMBER_ME','SHELLIOS_REMEMBER_MOBILE','SHELLIOS_REMEMBER_PASSWORD'));
		if($this->session->userdata('SHELLIOS_USER_ID')) redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-account');
		$data['error'] 						= 	'';
		$this->frontauth_model->checkUserLoginCookie();

		if($loginReferalUrl):
			$this->session->set_userdata('SHELLIOS_LOGIN_REF_URL',$loginReferalUrl);
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/login');
		endif;

		if($this->input->post('currentPageFormSubmit')):  
			$error					=	'NO';
			$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('user_phone'));
			if($this->input->post('user_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Enter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('terms', 'Terms', 'trim|required');

			if($this->form_validation->run() && $error == 'NO'):  

				$result		=	$this->frontauth_model->Authenticate(trim($this->input->post('user_phone')));
				if($result):	
					if($this->frontauth_model->decryptsPassword($result['user_password']) != trim($this->input->post('password'))):
						$data['formError'] = lang('invalidpassword');
					elseif($result['status'] == 'A'):
						if($result['user_phone_verify'] == 'Y'):
							$remember_me		=	$this->input->post('remember_me');
							$user_phone			=	$this->input->post('user_phone');
							$password			=	$this->input->post('password');
							$this->afterloginProcess($result,$remember_me,$user_phone,$password);
						else:
							if($this->input->post('remember_me') == 'YES'):
								$this->session->set_userdata(array('SHELLIOS_NOTVERIFY_MOBILE'=>$result['user_phone'],
								 									'SHELLIOS_REMEMBER_ME'=>'YES',
								 									'SHELLIOS_REMEMBER_MOBILE'=>$this->input->post('user_phone'),
								 									'SHELLIOS_REMEMBER_PASSWORD'=>$this->input->post('password')));
							else:

								$this->session->set_userdata(array('SHELLIOS_NOTVERIFY_MOBILE'=>$result['user_phone'],
								 									'SHELLIOS_REMEMBER_ME'=>'NO',
								 									'SHELLIOS_REMEMBER_MOBILE'=>$this->input->post('user_phone'),
								 									'SHELLIOS_REMEMBER_PASSWORD'=>$this->input->post('password')));
							endif;

							$uParam['user_otp']	=	generateRandomString(4,'n');
							$this->common_model->editData('users',$uParam,'user_id',$result['user_id']);

							$this->sms_model->sendOtpSmsToUser($result['user_phone'],$uParam['user_otp']);

							$this->session->set_flashdata('alert_success',lang('SEND_OTP_TO_RMN')); 
							redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/mobile-verify');
						endif;
					elseif($result['status'] == 'I'):
						if($this->input->post('remember_me') == 'YES'):
							$this->session->set_userdata(array('SHELLIOS_NOTVERIFY_MOBILE'=>$result['user_phone'],
							 									'SHELLIOS_REMEMBER_ME'=>'YES',
							 									'SHELLIOS_REMEMBER_MOBILE'=>$this->input->post('user_phone'),
							 									'SHELLIOS_REMEMBER_PASSWORD'=>$this->input->post('password')));
						else:

							$this->session->set_userdata(array('SHELLIOS_NOTVERIFY_MOBILE'=>$result['user_phone'],
							 									'SHELLIOS_REMEMBER_ME'=>'NO',
							 									'SHELLIOS_REMEMBER_MOBILE'=>$this->input->post('user_phone'),
							 									'SHELLIOS_REMEMBER_PASSWORD'=>$this->input->post('password')));
						endif;

						$uParam['user_otp']	=	generateRandomString(4,'n');
						$this->common_model->editData('users',$uParam,'user_id',$result['user_id']);

						$this->sms_model->sendOtpSmsToUser($result['user_phone'],$uParam['user_otp']);

						$this->session->set_flashdata('alert_success',lang('SEND_OTP_TO_RMN')); 
						redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/mobile-verify');
					else:
						$data['formError'] = lang('accountblock');	
					endif;
				else:
					$data['formError'] = lang('invalidlogindetails');
				endif;
			endif;
		endif;

		$this->layouts->set_title('Login');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/login',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : mobileverify
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user mobile verify
	 * * Date : 31 OCTOBER 2018
	 * * **********************************************************************/
	public function mobileverify()
	{	
		$data['error'] 						= 	'';
		$data['formError'] 					=	''; 	

		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';
			$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('user_phone'));
			if($this->input->post('user_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('user_otp', 'OTP', 'trim|required');

			if($this->form_validation->run() && $error == 'NO'): 

				$userData		=	$this->frontauth_model->checkOTP(trim($this->input->post('user_phone')),trim($this->input->post('user_otp')));
				if($userData <> ""):  
					if($userData['status'] == 'A' || $userData['status'] == 'I'):
						$param['user_phone_verify']	=	'Y';
						$param['user_otp']			=	'';
						$param['status']			=	'A';
						$this->common_model->editData('users',$param,'user_id',$userData['user_id']);

						$remember_me		=	$this->session->userdata('SHELLIOS_REMEMBER_ME');
						$user_phone			=	$this->session->userdata('SHELLIOS_REMEMBER_MOBILE');
						$password			=	$this->session->userdata('SHELLIOS_REMEMBER_PASSWORD');

						$this->session->unset_userdata(array('SHELLIOS_REGISTER_MOBILE',
															 'SHELLIOS_NOTVERIFY_MOBILE',
															 'SHELLIOS_REMEMBER_ME',
															 'SHELLIOS_REMEMBER_MOBILE',
															 'SHELLIOS_REMEMBER_PASSWORD'));

						$this->afterloginProcess($userData,$remember_me,$user_phone,$password);

					else:
						$data['formError'] 		= 	lang('ACCOUNT_BLOCKED');
					endif;
				else:
					$data['formError'] 			= 	lang('OTP_INCORRECT');
				endif;
			endif;
		endif;

		$this->layouts->set_title('Mobile Verification');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/mobileverify',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : afterloginProcess
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for after login Process
	 * * Date : 31 OCTOBER 2018
	 * * **********************************************************************/
	public function afterloginProcess($result=array(),$remember_me='',$user_phone='',$password='')
	{
		$this->session->set_userdata(array(
							'SHELLIOS_USER_LOGGED_IN'=>true,
							'SHELLIOS_USER_ID'=>$result['user_id'],
							'SHELLIOS_USER_TYPE'=>$result['user_type'],
							'SHELLIOS_USER_BROWSE_TYPE'=>$result['browse_type'],
							'SHELLIOS_USER_NAME'=>$result['user_name'],
							'SHELLIOS_USER_EMAIL'=>$result['user_email'],
							'SHELLIOS_USER_PHONE'=>$result['user_phone'],
							'SHELLIOS_USER_IMAGE'=>$result['user_image'],
							'SHELLIOS_USER_LAST_LOGIN'=>$result['user_last_login'].' ('.$result['user_last_login_ip'].')'));
		
		$param['user_type']				=	'register';
		$param['browse_type']			=	'web';
		$param['user_last_login']		=	currentDateTime();
		$param['user_last_login_ip']	=	currentIp();

		$this->common_model->editData('users',$param,'user_id',$result['user_id']);
	
		setcookie('SHELLIOS_USER_PHONE',$result['user_phone'],time()+60*60*24*100,'/');

		if($remember_me == 'YES'):
			 setcookie("ct_username",$user_phone,time()+60*60*24*100,'/');
			 setcookie("ct_password",$password,time()+60*60*24*100,'/');
			 setcookie("ct_remember_me",'YES',time()+60*60*24*100,'/');
		else:
			 setcookie("ct_username",$user_phone,time()-60*60*24*100,'/');
			 setcookie("ct_password",$password,time()-60*60*24*100,'/');
			 setcookie("ct_remember_me",'YES',time()-60*60*24*100,'/');
		endif;

		$this->changeCartDataCookieToUser($this->session->userdata('SHELLIOS_USER_ID'),get_cookie('currentCartCookie'));

		if($this->session->userdata('SHELLIOS_LOGIN_REF_URL')):
			$loginReferalUrl			=	explode('PRODUCTID',$this->session->userdata('SHELLIOS_LOGIN_REF_URL')); 
			if(strpos($loginReferalUrl[1],'TOWISHLIST')):	
				$productId					=	str_replace('ADDTOWISHLIST','',$loginReferalUrl[1]);
				$addParam['user_id']		=	$this->session->userdata('SHELLIOS_USER_ID');
				$addParam['prod_id']		=	base64_decode($productId);
				
				$checkWishlist				=	$this->front_model->checkInWishlist($addParam['user_id'],$addParam['prod_id']);
				if($checkWishlist == 'Y'):
					$dparams['user_id']			=	$addParam['user_id'];
					$dparams['prod_id']			=	$addParam['prod_id'];
					$this->common_model->deleteByMultipleCondition('wishlist',$dparams);
					$this->session->set_flashdata('alert_warning',lang('REMOVEFROMWISHLIST_SUCCESS'));
				else:
					$addParam['creation_date']	=	currentDateTime();
					$addParam['status']			=	'Y';
					$walastInsertId				=	$this->common_model->addData('wishlist',$addParam);

					$wUparam['encrypt_id']		=	manojEncript($walastInsertId);
					$wUparam['wishlist_id']		=	generateUniqueId($walastInsertId);
					$wUwhere['id']				=	$walastInsertId;
					$this->common_model->editDataByMultipleCondition('wishlist',$wUparam,$wUwhere);

					$this->session->set_flashdata('alert_success',lang('ADDTOWISHLIST_SUCCESS'));
				endif;
			endif;	
			if(strpos(base64_decode($loginReferalUrl[0]),'checkout')):
				$this->session->set_userdata('checkout_tab','Address');
			endif;	
			if(strpos(base64_decode($loginReferalUrl[0]),'buy-now-checkout')):
				$this->session->set_userdata('buynow_checkout_tab','Address');
			endif;					
			$this->session->unset_userdata('SHELLIOS_LOGIN_REF_URL','');
			if(base64_decode($loginReferalUrl[0]) == 'home'):
				redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH'));
			else:
				redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').base64_decode($loginReferalUrl[0]));
			endif;
		else:
			$this->session->set_flashdata('alert_success',lang('LOGIN_SUCCESSFULLY'));
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-account');
		endif;
	}

	/* * *********************************************************************
	 * * Function name : fbresponsedata
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for fb response data
	 * * Date : 12 OCTOBER 2018
	 * * **********************************************************************/
	public function fbresponsedata()
	{ 
	  	$returnUrl                     	=   $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/login';
	  	if($this->input->post('id') && $this->input->post('email') && $this->input->post('name')):
	  		$userData		=	$this->frontauth_model->getUserDataByEmail($this->input->post('email'));
		    if($userData <> ""):
				$param['user_type']		=	'facebook';
				$param['network_id']	=	$this->input->post('id');
				$param['device_id']		=	'';
				$param['browse_type']	=	'web';
				$param['user_name']		=	addslashes(trim($this->input->post('name')));
				$param['user_email']	=	addslashes(trim($this->input->post('email')));
				$param['user_image']    =   'http://graph.facebook.com/'.$this->input->post('id').'/picture?width=80&height=80';
				$param['update_date']	=	currentDateTime();
				$param['status']		=	'A';
				$this->common_model->editData('users',$param,'user_id',$userData['user_id']);

				$userId 		 		=	$userData['user_id'];
			else:
				$param['user_type']			=	'facebook';
				$param['network_id']		=	$this->input->post('id');
				$param['device_id']			=	'';
				$param['browse_type']		=	'web';
				$param['user_name']			=	addslashes(trim($this->input->post('name')));
				$param['user_email']		=	addslashes(trim($this->input->post('email')));
				$param['user_phone_verify']	=	'N';
				$param['user_image']    	=   'http://graph.facebook.com/'.$this->input->post('id').'/picture?width=80&height=80';
				$param['user_otp']			=	generateRandomString(4,'n');
				$param['creation_date']		=	currentDateTime();
				$param['status']			=	'A';
				$insertUserId				=	$this->common_model->addData('users',$param);
				
				$uParam['encrypt_id']		=	manojEncript($insertUserId);
				$uParam['user_id']			=	generateUniqueId($insertUserId);
				$this->common_model->editData('users',$uParam,'id',$insertUserId);

				$userId 					=	$uParam['user_id'];
			endif;	          

			$result						=	$this->frontauth_model->getUserDataByUserId($userId);
			if($result <> ""): 
				if($result['user_phone']):
					if($result['user_phone_verify'] == 'Y'):
						
						$returnUrl     	=   $this->aftersocialloginProcess($result);
					else:

						$this->session->set_userdata(array('SHELLIOS_ERROR_USER_ID'=>$result['user_id'],'SHELLIOS_ERROR_USER_PHONE'=>$result['user_phone']));

						$this->sms_model->sendOtpSmsToUser($result['user_phone'],$result['user_otp']);
						$this->session->set_flashdata('alert_success',lang('SEND_OTP_TO_RMN'));
						$returnUrl     	=   $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/verify-mobile';
					endif;
				else:

					$this->session->set_userdata('SHELLIOS_ERROR_USER_ID',$result['user_id']);

					$returnUrl     		=   $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/enter-mobile';
				endif;
			else:
				$this->session->set_flashdata('alert_success',lang('loginfailed'));
				$returnUrl     			=   $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/login';
			endif;
	  	endif;
	  	echo $returnUrl; die;
	}

	/* * *********************************************************************
	 * * Function name : gplusresponsedata
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for gplus response data
	 * * Date : 20 NOVEMBER 2018
	 * * **********************************************************************/
	public function gplusresponsedata()
	{ 
	  	$returnUrl                     	=   $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/login';
	  	if($this->input->post('id') && $this->input->post('email') && $this->input->post('name')):
	  		$userData		=	$this->frontauth_model->getUserDataByEmail($this->input->post('email'));
		    if($userData <> ""):
				$param['user_type']		=	'gplus';
				$param['network_id']	=	$this->input->post('id');
				$param['device_id']		=	'';
				$param['browse_type']	=	'web';
				$param['user_name']		=	addslashes(trim($this->input->post('name')));
				$param['user_email']	=	addslashes(trim($this->input->post('email')));
				$param['user_image']    =   addslashes(trim($this->input->post('image')));
				$param['update_date']	=	currentDateTime();
				$param['status']		=	'A';
				$this->common_model->editData('users',$param,'user_id',$userData['user_id']);

				$userId 		 		=	$userData['user_id'];
			else:
				$param['user_type']			=	'gplus';
				$param['network_id']		=	$this->input->post('id');
				$param['device_id']			=	'';
				$param['browse_type']		=	'web';
				$param['user_name']			=	addslashes(trim($this->input->post('name')));
				$param['user_email']		=	addslashes(trim($this->input->post('email')));
				$param['user_phone_verify']	=	'N';
				$param['user_image']    	=   addslashes(trim($this->input->post('image')));
				$param['user_otp']			=	generateRandomString(4,'n');
				$param['creation_date']		=	currentDateTime();
				$param['status']			=	'A';
				$insertUserId				=	$this->common_model->addData('users',$param);
				
				$uParam['encrypt_id']		=	manojEncript($insertUserId);
				$uParam['user_id']			=	generateUniqueId($insertUserId);
				$this->common_model->editData('users',$uParam,'id',$insertUserId);

				$userId 					=	$uParam['user_id'];
			endif;	          

			$result						=	$this->frontauth_model->getUserDataByUserId($userId);
			if($result <> ""): 
				if($result['user_phone']):
					if($result['user_phone_verify'] == 'Y'):
						
						$returnUrl     	=   $this->aftersocialloginProcess($result);
					else:

						$this->session->set_userdata(array('SHELLIOS_ERROR_USER_ID'=>$result['user_id'],'SHELLIOS_ERROR_USER_PHONE'=>$result['user_phone']));

						$this->sms_model->sendOtpSmsToUser($result['user_phone'],$result['user_otp']);
						$this->session->set_flashdata('alert_success',lang('SEND_OTP_TO_RMN'));
						$returnUrl     	=   $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/verify-mobile';
					endif;
				else:

					$this->session->set_userdata('SHELLIOS_ERROR_USER_ID',$result['user_id']);

					$returnUrl     		=   $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/enter-mobile';
				endif;
			else:
				$this->session->set_flashdata('alert_success',lang('loginfailed'));
				$returnUrl     			=   $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/login';
			endif;
	  	endif;
	  	echo $returnUrl; die;
	}

	/* * *********************************************************************
	 * * Function name : entermobile
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user enter mobile
	 * * Date : 12 OCTOBER 2018
	 * * **********************************************************************/
	public function entermobile()
	{	
		$data['error'] 						= 	'';

		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';	
			$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('user_phone'));
			if($this->input->post('user_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			
			if($this->form_validation->run() && $error == 'NO'):	

				$result				=		$this->frontauth_model->getUserDataByMobile(trim($this->input->post('user_phone')));
				if($result):
					if($result['user_id'] == $this->session->userdata('SHELLIOS_ERROR_USER_ID')):	

						$uparam['user_phone']		=	trim($this->input->post('user_phone'));
						$uparam['user_otp']			=	generateRandomString(4,'n');
						$this->common_model->editData('users',$uparam,'user_id',$result['user_id']);

						$this->session->set_userdata('SHELLIOS_ERROR_USER_PHONE',$uparam['user_phone']);

						$this->sms_model->sendOtpSmsToUser($uparam['user_phone'],$uparam['user_otp']);
						$this->session->set_flashdata('alert_success',lang('SEND_OTP_TO_RMN'));
						redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/verify-mobile');

					else:
						$data['formError'] = lang('mobilealreadyused');
					endif;
				else:

					$uparam['user_phone']		=	trim($this->input->post('user_phone'));
					$uparam['user_otp']			=	generateRandomString(4,'n');
					$this->common_model->editData('users',$uparam,'user_id',$this->session->userdata('SHELLIOS_ERROR_USER_ID'));

					$this->session->set_userdata('SHELLIOS_ERROR_USER_PHONE',$uparam['user_phone']);

					$this->sms_model->sendOtpSmsToUser($uparam['user_phone'],$uparam['user_otp']);
					$this->session->set_flashdata('alert_success',lang('SEND_OTP_TO_RMN'));
					redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/verify-mobile');

				endif;
			endif;
		endif;

		$this->layouts->set_title('Enter Mobile');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/entermobile',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : verifymobile
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user verify mobile
	 * * Date : 12 OCTOBER 2018
	 * * **********************************************************************/
	public function verifymobile()
	{	
		$data['error'] 						= 	'';
		$data['formError'] 					=	''; 	

		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';
			$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('user_phone'));
			if($this->input->post('user_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('user_otp', 'OTP', 'trim|required');

			if($this->form_validation->run() && $error == 'NO'): 

				$result		=	$this->frontauth_model->checkOTP(trim($this->input->post('user_phone')),trim($this->input->post('user_otp'))); 
				if($result <> ""):  
					$param['user_phone_verify']	=	'Y';
					$param['user_otp']			=	'';
					$param['status']			=	'A';
					$this->common_model->editData('users',$param,'user_id',$result['user_id']);

					$this->session->unset_userdata(array('SHELLIOS_ERROR_USER_ID','SHELLIOS_ERROR_USER_PHONE'));
		
					$returnUrl     	=   $this->aftersocialloginProcess($result);
					redirect($returnUrl);
				else:
					$data['formError'] 			= 	lang('OTP_INCORRECT');
				endif;
			endif;
		endif;

		$this->layouts->set_title('Moble Verification');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/verifymobile',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : aftersocialloginProcess
	 * * Developed By : Manoj Kumar 
	 * * Purpose  : This function used for after social login Process
	 * * Date : 20 NOVEMBER 2018
	 * * **********************************************************************/
	public function aftersocialloginProcess($result=array())
	{
		$this->session->set_userdata(array(
							'SHELLIOS_USER_LOGGED_IN'=>true,
							'SHELLIOS_USER_ID'=>$result['user_id'],
							'SHELLIOS_USER_TYPE'=>$result['user_type'],
							'SHELLIOS_USER_BROWSE_TYPE'=>$result['browse_type'],
							'SHELLIOS_USER_NAME'=>$result['user_name'],
							'SHELLIOS_USER_EMAIL'=>$result['user_email'],
							'SHELLIOS_USER_PHONE'=>$result['user_phone'],
							'SHELLIOS_USER_IMAGE'=>$result['user_image'],
							'SHELLIOS_USER_LAST_LOGIN'=>$result['user_last_login'].' ('.$result['user_last_login_ip'].')'));
		
		//$param['user_type']				=	'register';
		$param['browse_type']			=	'web';
		$param['user_last_login']		=	currentDateTime();
		$param['user_last_login_ip']	=	currentIp();

		$this->common_model->editData('users',$param,'user_id',$result['user_id']);
	
		setcookie('SHELLIOS_USER_PHONE',$result['user_phone'],time()+60*60*24*100,'/');

		$this->changeCartDataCookieToUser($this->session->userdata('SHELLIOS_USER_ID'),get_cookie('currentCartCookie'));

		if($this->session->userdata('SHELLIOS_LOGIN_REF_URL')):
			$loginReferalUrl			=	explode('PRODUCTID',$this->session->userdata('SHELLIOS_LOGIN_REF_URL')); 
			if(strpos($loginReferalUrl[1],'TOWISHLIST')):	
				$productId					=	str_replace('ADDTOWISHLIST','',$loginReferalUrl[1]);
				$addParam['user_id']		=	$this->session->userdata('SHELLIOS_USER_ID');
				$addParam['prod_id']		=	base64_decode($productId);
				
				$checkWishlist				=	$this->front_model->checkInWishlist($addParam['user_id'],$addParam['prod_id']);
				if($checkWishlist == 'Y'):
					$dparams['user_id']			=	$addParam['user_id'];
					$dparams['prod_id']			=	$addParam['prod_id'];
					$this->common_model->deleteByMultipleCondition('wishlist',$dparams);
					$this->session->set_flashdata('alert_warning',lang('REMOVEFROMWISHLIST_SUCCESS'));
				else:
					$addParam['creation_date']	=	currentDateTime();
					$addParam['status']			=	'Y';
					$walastInsertId				=	$this->common_model->addData('wishlist',$addParam);

					$wUparam['encrypt_id']		=	manojEncript($walastInsertId);
					$wUparam['wishlist_id']		=	generateUniqueId($walastInsertId);
					$wUwhere['id']				=	$walastInsertId;
					$this->common_model->editDataByMultipleCondition('wishlist',$wUparam,$wUwhere);

					$this->session->set_flashdata('alert_success',lang('ADDTOWISHLIST_SUCCESS'));
				endif;
			endif;	
			if(strpos(base64_decode($loginReferalUrl[0]),'checkout')):
				$this->session->set_userdata('checkout_tab','Address');
			endif;	
			if(strpos(base64_decode($loginReferalUrl[0]),'buy-now-checkout')):
				$this->session->set_userdata('buynow_checkout_tab','Address');
			endif;					
			$this->session->unset_userdata('SHELLIOS_LOGIN_REF_URL','');
			if(base64_decode($loginReferalUrl[0]) == 'home'):
				return $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH');
			else:
				return $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').base64_decode($loginReferalUrl[0]);
			endif;
		else:
			$this->session->set_flashdata('alert_success',lang('LOGIN_SUCCESSFULLY'));
			return $this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-account';
		endif;
	}

	/* * *********************************************************************
	 * * Function name : forgotpassword
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user forgot password
	 * * Date : 12 OCTOBER 2018
	 * * **********************************************************************/
	public function forgotpassword()
	{	
		$this->session->unset_userdata(array('SHELLIOS_REGISTER_MOBILE','SHELLIOS_ERROR_USER_ID','SHELLIOS_ERROR_USER_PHONE','SHELLIOS_FORGOT_USER_PHONE'));
		if($this->session->userdata('SHELLIOS_USER_ID')) redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-account');
		$data['error'] 						= 	'';
		$this->frontauth_model->checkUserLoginCookie();

		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';	
			$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('user_phone'));
			if($this->input->post('user_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			
			if($this->form_validation->run() && $error == 'NO'):	
				$result		=	$this->frontauth_model->Authenticate(trim($this->input->post('user_phone')));
				if($result):	
					if($result['status'] != 'A'):	
						$data['formError'] = lang('accountblock');		
					else:	
						$param['user_otp']		=	generateRandomString(4,'n');
						$this->common_model->editData('users',$param,'user_id',$result['user_id']);
						
						$this->sms_model->sendOtpSmsToUser($result['user_phone'],$param['user_otp']);

						$this->session->set_userdata('SHELLIOS_FORGOT_USER_PHONE',$result['user_phone']);

						$this->session->set_flashdata('alert_success',lang('SEND_OTP_TO_RMN')); 
						redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/reset-password');
					endif;
				else:
					$data['formError'] = lang('invalidmobile');
				endif;
			endif;
		endif;

		$this->layouts->set_title('Forgot Password');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/forgotpassword',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : resetpassword
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user reset password
	 * * Date : 12 OCTOBER 2018
	 * * **********************************************************************/
	public function resetpassword()
	{	
		$data['error'] 						= 	'';

		if($this->input->post('currentPageFormSubmit')):  
			$error					=	'NO';
			$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('user_phone'));
			if($this->input->post('user_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('user_otp', 'OTP', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[25]');
			$this->form_validation->set_rules('conf_password', 'Confirm password', 'trim|required|min_length[6]|matches[password]');

			if($this->form_validation->run() && $error == 'NO'): 

				$userData		=	$this->frontauth_model->checkOTP(trim($this->input->post('user_phone')),trim($this->input->post('user_otp')));
				if($userData <> ""):  
					if($userData['status'] == 'A'):

						$param['user_otp']		=	'';
						$param['user_password']	=	$this->frontauth_model->encriptPassword(trim($this->input->post('password')));
						$this->common_model->editData('users',$param,'user_id',$userData['user_id']);

						$this->session->unset_userdata('SHELLIOS_FORGOT_USER_PHONE');

						$this->session->set_flashdata('alert_success',lang('RESET_SUCCESSFULLY')); 
						redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/login');
					else:
						$data['formError'] 		= 	lang('ACCOUNT_BLOCKED');
					endif;
				else:
					$data['formError'] 			= 	lang('OTP_INCORRECT');
				endif;
			endif;
		endif;

		$this->layouts->set_title('Reset Password');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/resetpassword',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : logout
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user logout
	 * * Date : 11 OCTOBER 2018
	 * * **********************************************************************/
	public function logout()
	{	
		setcookie('SHELLIOS_USER_PHONE','',time()-60*60*24*100,'/');
		setcookie('SHELLIOS_USER_REFERENCE_PAGES','',time()-60*60*24*100,'/');
		$this->session->unset_userdata(array('SHELLIOS_USER_LOGGED_IN',
											 'SHELLIOS_USER_ID',
											 'SHELLIOS_USER_TYPE',
											 'SHELLIOS_USER_BROWSE_TYPE',
											 'SHELLIOS_USER_NAME',
											 'SHELLIOS_USER_EMAIL',
											 'SHELLIOS_USER_PHONE',
											 'SHELLIOS_USER_IMAGE',
											 'SHELLIOS_USER_LAST_LOGIN',
											 'SHELLIOS_LOGIN_REF_URL',
											 
											 'SHELLIOS_REGISTER_MOBILE',
											 'SHELLIOS_ERROR_USER_ID',
											 'SHELLIOS_ERROR_USER_PHONE',
											 'SHELLIOS_FORGOT_USER_PHONE',
											 'SHELLIOS_NOTVERIFY_MOBILE',
											 'SHELLIOS_REMEMBER_ME',
											 'SHELLIOS_REMEMBER_MOBILE',
											 'SHELLIOS_REMEMBER_PASSWORD',

											 'installation',
											 'checkout_tab',
											 'cur_address_id',
											 'currentOrderId',
											 'buynow_prod_id',
											 'buynow_quantity',
											 'buynow_checkout_tab',
											 'buynow_address_id',
											 'buynow_installation',
											 'currentTranId'));
		redirect(base_url().'user/login');
	}

	/* * *********************************************************************
	 * * Function name : myaccount
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user my account
	 * * Date : 13 OCTOBER 2018
	 * * **********************************************************************/
	public function myaccount()
	{	
		$this->frontauth_model->authCheck();
		$data['error'] 						= 	'';
		$data['leftMenu'] 					= 	'my-account';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');

		$data['addressData']				=	$this->front_model->getAddressData($data['userId'],2);

		$this->layouts->set_title('My Account');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/myaccount',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : accountinformation
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user account information
	 * * Date : 13 OCTOBER 2018
	 * * **********************************************************************/
	public function accountinformation()
	{	
		$this->frontauth_model->authCheck();
		$data['error'] 						= 	'';
		$data['leftMenu'] 					= 	'account-information';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');

		$data['userData']					=	$this->frontauth_model->getUserDataByUserId($data['userId']);
		
		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';
			$this->form_validation->set_rules('user_name', 'Name', 'trim|required');
			/*$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]|is_unique[users.user_phone]');
			$usermobile		=	str_replace(' ','',$this->input->post('user_phone'));
			if($this->input->post('user_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('user_email', 'E-Mail', 'trim|required|valid_email|is_unique[users.user_email]');
			*/
			if($this->form_validation->run() && $error == 'NO'): 
				$userId 					=	trim($this->input->post('CurrentDataID'));
				
				$param['user_name']			=	addslashes(trim($this->input->post('user_name')));
				//$param['user_email']		=	addslashes(trim($this->input->post('user_email')));
				//$param['user_phone']		=	addslashes(trim($this->input->post('user_phone')));
				$param['update_date']		=	currentDateTime();

				$this->common_model->editData('users',$param,'user_id',$userId);

				$this->session->set_userdata(array(
											'SHELLIOS_USER_NAME'=>$param['user_name'],
											'SHELLIOS_USER_EMAIL'=>$param['user_email'],
											'SHELLIOS_USER_PHONE'=>$param['user_phone']));
				
				setcookie('SHELLIOS_USER_PHONE',$param['user_phone'],time()+60*60*24*100,'/');

				if(get_cookie('ct_remember_me') == 'YES'):
					 setcookie("ct_username",$param['user_phone'],time()+60*60*24*100,'/');
					 setcookie("ct_remember_me",'YES',time()+60*60*24*100,'/');
				endif;

				$this->session->set_flashdata('alert_success',lang('updatesuccess')); 
				redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-account');
			endif;
		endif;

		$this->layouts->set_title('Account Information');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/accountinformation',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : changepassword
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user change password
	 * * Date : 13 OCTOBER 2018
	 * * **********************************************************************/
	public function changepassword()
	{	
		$this->frontauth_model->authCheck();
		$data['error'] 						= 	'';
		$data['leftMenu'] 					= 	'my-account';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');

		$data['userData']					=	$this->frontauth_model->getUserDataByUserId($data['userId']);
		$data['OLDPASSWORD']				=	$this->frontauth_model->decryptsPassword($data['userData']['user_password']);
		
		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';
			$this->form_validation->set_rules('old_password', 'Old password', 'trim');
			$this->form_validation->set_rules('current_password', 'Current password', 'trim|required|min_length[6]|matches[old_password]');
			$this->form_validation->set_rules('new_password', 'New password', 'trim|required|min_length[6]|max_length[25]');
			$this->form_validation->set_rules('conf_password', 'Confirm password', 'trim|required|min_length[6]|matches[new_password]');
			
			if($this->form_validation->run() && $error == 'NO'): 
			
				$NewPassword					=	$this->input->post('new_password');
				$param['user_password']			= 	$this->frontauth_model->encriptPassword($NewPassword);
				
				$userId							=	$this->input->post('CurrentDataID');
				$param['update_date']			=	currentDateTime();
				$this->common_model->editData('users',$param,'user_id',$userId);
				
				if(get_cookie('ct_remember_me') == 'YES'):
					 setcookie("ct_password",$NewPassword,time()+60*60*24*100,'/');
					 setcookie("ct_remember_me",'YES',time()+60*60*24*100,'/');
				endif;

				$this->session->set_flashdata('alert_success',lang('updatesuccess')); 
				redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-account');
			endif;
		endif;

		$this->layouts->set_title('Change Password');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/changepassword',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : addressbook
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user address book
	 * * Date : 13 OCTOBER 2018
	 * * **********************************************************************/
	public function addressbook()
	{	
		$this->frontauth_model->authCheck();
		$data['error'] 						= 	'';
		$data['leftMenu'] 					= 	'address-book';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');

		$data['addressData']				=	$this->front_model->getAddressData($data['userId']);

		$this->layouts->set_title('Address Book');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/addressbook',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : addeditaddress
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user add edit address
	 * * Date : 13 OCTOBER 2018
	 * * **********************************************************************/
	public function addeditaddress($addressId='')
	{	
		$this->frontauth_model->authCheck();
		$data['error'] 						= 	'';
		$data['leftMenu'] 					= 	'add-edit-address';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');
		$data['addressId'] 					= 	$addressId;

		if($data['addressId']):
			$data['EDITDATA']				=	$this->common_model->getDataByParticularField('users_address','address_id',$data['addressId']);
		endif;
		
		if($this->input->post('currentPageFormSubmit')):
			$error					=	'NO';
			$this->form_validation->set_rules('add_name', 'Name', 'trim|required');
			$this->form_validation->set_rules('add_phone', 'Phone', 'trim|required|min_length[10]|max_length[15]');
			$usermobile		=	str_replace(' ','',$this->input->post('add_phone'));
			if($this->input->post('add_phone') && !preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i',$usermobile)):
				if(!preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/",$usermobile)):
					$error						=	'YES';
					$data['mobileerror'] 		= 	'Please Eneter Correct Number.';
				endif;
			endif;
			$this->form_validation->set_rules('add_address1', 'Address 1', 'trim|required');
			$this->form_validation->set_rules('add_address2', 'Address 2', 'trim');
			$this->form_validation->set_rules('add_city', 'City', 'trim|required');
			$this->form_validation->set_rules('add_state', 'State', 'trim|required');
			$this->form_validation->set_rules('add_pincode', 'Pincode', 'trim|required');
			$this->form_validation->set_rules('add_type', 'Address Type', 'trim|required');

			if($this->form_validation->run() && $error == 'NO'): 
				
				$param['add_name']			= 	addslashes($this->input->post('add_name'));
				$param['add_phone']			= 	addslashes($this->input->post('add_phone'));
				$param['add_address1']		= 	addslashes($this->input->post('add_address1'));
				$param['add_address2']		= 	addslashes($this->input->post('add_address2'));
				$param['add_city']			= 	addslashes($this->input->post('add_city'));
				$param['add_state']			= 	addslashes($this->input->post('add_state'));
				$param['add_pincode']		= 	addslashes($this->input->post('add_pincode'));
				$param['add_type']			= 	addslashes($this->input->post('add_type'));
				
				if($this->input->post('CurrentDataID') ==''):
					$param['user_id']			=	$data['userId'];
					$param['add_default']		=	'N';
					$param['creation_date']		=	currentDateTime();
					$param['status']			=	'Y';
					$alastInsertId				=	$this->common_model->addData('users_address',$param);
					
					$Uparam['encrypt_id']		=	manojEncript($alastInsertId);
					$Uparam['address_id']		=	generateUniqueId($alastInsertId);
					$Uwhere['id']				=	$alastInsertId;
					$this->common_model->editDataByMultipleCondition('users_address',$Uparam,$Uwhere);
					$this->session->set_flashdata('alert_success',lang('addsuccess'));
				else:
					$addressId					=	$this->input->post('CurrentDataID');
					$param['update_date']		=	currentDateTime();
					$this->common_model->editData('users_address',$param,'address_id',$addressId);
					$this->session->set_flashdata('alert_success',lang('updatesuccess'));
				endif;

				redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/address-book');
			endif;
		endif;

		$this->layouts->set_title('Add Edit Address');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/addeditaddress',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : mywishlist
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user my wishlist
	 * * Date : 13 OCTOBER 2018
	 * * **********************************************************************/
	public function mywishlist()
	{	
		$this->frontauth_model->authCheck();
		$data['error'] 						= 	'';
		$data['leftMenu'] 					= 	'my-wishlist';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');

		$data['wishlistData']				=	$this->front_model->getWishlistData($data['userId']);


		$this->layouts->set_title('My Wishlist');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/mywishlist',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : myorder
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user my order
	 * * Date : 01 NOVEMBER 2018
	 * * **********************************************************************/
	public function myorder($type='',$ordId='')
	{	
		$this->frontauth_model->authCheck();
		$data['error'] 						= 	'';
		$data['leftMenu'] 					= 	'my-order';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');

		$data['orderList']					=	$this->getOrderListData($data['userId']);

		if($type=='cancelorder' && $ordId != ''):
			$param['order_status']			=	'Canceled';
			$param['canceled_date']			=	currentDateTime();
			$this->common_model->editData('orders',$param,'order_id',$ordId);

			////////////////////		USER 	/////////////////
			$this->emailtemplate_model->sendCancelOrderMailToUser($data['userId'],$ordId);

			////////////////////		SELLER 	/////////////////
			$this->emailtemplate_model->sendCancelOrderMailToSeller($data['userId'],$ordId);

			////////////////////		ADMIN 	/////////////////
			$this->emailtemplate_model->sendCancelOrderMailToAdmin($data['userId'],$ordId);

			$this->session->set_flashdata('alert_success',lang('CANCELORDER_SUCCESS')); 
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-order');
		endif;

		if($this->input->post('returnRequestFormSubmit')):
			
			$returnRequestDATA				=	explode('_____',$this->input->post('returnRequestId'));
			$orderId 						=	$returnRequestDATA[0];
			$prodId 						=	$returnRequestDATA[1];

			$Uparams['order_return_request']= 	'Y';
			$Uparams['order_return_reason']	=	addslashes($this->input->post('order_return_reason'));
			$Uparams['order_return_date']	=	currentDateTime();
			//$this->common_model->editData('orders',$param,'order_id',$returnRequestId);

			$Uwhere['user_id']		=	$data['userId'];
			$Uwhere['order_id']		=	$orderId;
			$Uwhere['prod_id']		=	$prodId;
			$this->common_model->editDataByMultipleCondition('orders',$Uparams,$Uwhere);

			////////////////////		USER 	/////////////////
			$this->emailtemplate_model->sendReturnRequestMailToUser($data['userId'],$orderId,$prodId);

			////////////////////		SELLER 	/////////////////
			$this->emailtemplate_model->sendReturnRequestMailToSeller($data['userId'],$orderId,$prodId);

			////////////////////		ADMIN 	/////////////////
			$this->emailtemplate_model->sendReturnRequestMailToAdmin($data['userId'],$orderId,$prodId);
			
			$this->session->set_flashdata('alert_success',lang('RETURNREQUEST_SUCCESS')); 
			redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-order');
		endif;

		$this->layouts->set_title('My Order');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/myorder',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : getOrderListData
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get Order List Data
	 * * Date : 01 NOVEMBER 2018
	 * * **********************************************************************/
	public function getOrderListData($userId='')
	{	
		$ordQuery				=	"SELECT ords.id, ords.session_id, ords.order_id, ords.quantity, ords.amount, ords.total_amount, ords.gst_amount, ords.installation_charges,
									 ords.shipping_name, ords.shipping_phone, ords.shipping_address1, ords.shipping_address2, ords.shipping_city, ords.shipping_state, ords.shipping_pincode, ords.shipping_add_type, 
									 ords.seller_accepted, ords.seller_remark, ords.seller_action_date, ords.order_status, ords.order_date, ords.order_return_request,
									 ords.packed_date, ords.shipped_date, ords.delivered_date, ords.canceled_date, ords.order_return_date,
									 prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
									 prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
									 prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
									 sell.seller_name, sell.seller_business_name, sell.seller_phone, sell.seller_email,
									 cate.prod_cate_slug, subcate.prod_sub_cate_slug
									 FROM ".getTablePrefix()."orders as ords
									 LEFT JOIN ".getTablePrefix()."product as prod ON ords.prod_id=prod.prod_id 
									 LEFT JOIN ".getTablePrefix()."product_details as prodet ON prod.prod_id=prodet.prod_id 
									 LEFT JOIN ".getTablePrefix()."seller as sell ON prod.seller_id=sell.seller_id
									 LEFT JOIN ".getTablePrefix()."prod_cate as cate ON prod.prod_cate_id=cate.prod_cate_id
									 LEFT JOIN ".getTablePrefix()."prod_sub_cate as subcate ON prod.prod_sub_cate_id=subcate.prod_sub_cate_id
									 WHERE ords.user_id = '".$userId."' AND ords.payment_status = '1'
									 AND prod.status = 'A' ORDER BY  ords.id DESC, ords.order_id ASC, ords.session_id ASC";
		$ordData				=	$this->common_model->getDataByQuery('multiple',$ordQuery);			
		if($ordData <> ""):  
			$i=0; $j=0; $k=0;$l=0;
			$sessionarray		=	array();
			$orderarray			=	array();
			foreach($ordData as $ordInfo):
				$changeJ		=	'N';
				if($l==0):
					array_push($sessionarray,$ordInfo['session_id']);
					array_push($orderarray,$ordInfo['order_id']);
				endif;
				if(!in_array($ordInfo['session_id'],$sessionarray)):	
					array_push($sessionarray,$ordInfo['session_id']);
					$i++; $j=0;  $k=0;
					$changeJ		=	'Y';
				endif;
				if(!in_array(trim($ordInfo['order_id']),$orderarray)):	
					array_push($orderarray,trim($ordInfo['order_id']));
					if($changeJ=='N'): $j++; echo 'aa '.$j; endif;  $k=0;   
				endif;	

					$wishlist 		=	$this->front_model->checkInWishlist($userData['user_id'],$ordInfo['prod_id']);
				$imgData 		=	$this->front_model->getProductImage($ordInfo['prod_id'],'1');
				$prodImage 		=	$imgData[0]['prod_image']?showProductImage(stripslashes($imgData[0]['prod_image']),'medium'):'';
				$ratingData 	=	$this->front_model->getRatingbyProductId($ordInfo['prod_id']);
				$rData[$i]['sessionId']												=	$ordInfo['session_id']?$ordInfo['session_id']:'';
				$rData[$i]['sessionData'][$j]['orderId']							=	$ordInfo['order_id']?$ordInfo['order_id']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['id']				=	$ordInfo['id']?$ordInfo['id']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productId']			=	$ordInfo['prod_id']?$ordInfo['prod_id']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productImage']		=	$prodImage;
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productName']		=	$ordInfo['prod_name']?htmlspecialchars_decode(stripslashes($ordInfo['prod_name'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productSKU']		=	$ordInfo['prod_sku']?htmlspecialchars_decode(stripslashes($ordInfo['prod_sku'])):'';
				//$rData[$i]['sessionData'][$j]['orderData'][$k]['productDesc']		=	$ordInfo['prod_desc']?htmlspecialchars_decode(stripslashes($ordInfo['prod_desc'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productWishlit']	=	$wishlist;
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productRating']		=	$ratingData['rating'];
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productRatingCount']=	$ratingData['count'];
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productBrandName']	=	$ordInfo['prod_brand_name']?htmlspecialchars_decode(stripslashes($ordInfo['prod_brand_name'])):'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['productSlug']		=	$ordInfo['prod_slug']?htmlspecialchars_decode(stripslashes($ordInfo['prod_slug'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productCateSlug']	=	$ordInfo['prod_cate_slug']?htmlspecialchars_decode(stripslashes($ordInfo['prod_cate_slug'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productSubCateSlug']=	$ordInfo['prod_sub_cate_slug']?htmlspecialchars_decode(stripslashes($ordInfo['prod_sub_cate_slug'])):'';

				//$rData[$i]['sessionData'][$j]['orderData'][$k]['productDate']					=	$ordInfo['order_date']?date('M, d Y',strtotime($ordInfo['order_date'])):'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['productOrderDate']				=	$ordInfo['order_date']!='1970-01-01 00:00:00'?$ordInfo['order_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productPackedDate']				=	$ordInfo['packed_date']!='1970-01-01 00:00:00'?$ordInfo['packed_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productShippedDate']			=	$ordInfo['shipped_date']!='1970-01-01 00:00:00'?$ordInfo['shipped_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productDeliveredDate']			=	$ordInfo['delivered_date']!='1970-01-01 00:00:00'?$ordInfo['delivered_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productcancelDate']				=	$ordInfo['canceled_date']!='1970-01-01 00:00:00'?$ordInfo['canceled_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productRequestDate']			=	$ordInfo['order_return_date']!='1970-01-01 00:00:00'?$ordInfo['order_return_date']:'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['productStatus']					=	$ordInfo['order_status']?stripslashes($ordInfo['order_status']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productReturnRequest']			=	$ordInfo['order_return_request']?stripslashes($ordInfo['order_return_request']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productQuantity']				=	$ordInfo['quantity']?stripslashes($ordInfo['quantity']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productSingleAmount']			=	$ordInfo['amount']?stripslashes($ordInfo['amount']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productTotalAmount']			=	$ordInfo['total_amount']?stripslashes($ordInfo['total_amount']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productGSTAmount']				=	'18%';//$ordInfo['gst_amount']?stripslashes($ordInfo['gst_amount']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productInstallationCharges']	=	$ordInfo['installation_charges']?stripslashes($ordInfo['installation_charges']):'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingName']					=	$ordInfo['shipping_name']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_name'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingPhone']					=	$ordInfo['shipping_phone']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_phone'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingAddress1']				=	$ordInfo['shipping_address1']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_address1'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingAddress2']				=	$ordInfo['shipping_address2']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_address2'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingCity']					=	$ordInfo['shipping_city']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_city'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingState']					=	$ordInfo['shipping_state']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_state'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingPincode']				=	$ordInfo['shipping_pincode']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_pincode'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingAddType']				=	$ordInfo['shipping_add_type']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_add_type'])):'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['selerName']						=	$ordInfo['seller_name']?htmlspecialchars_decode(stripslashes($ordInfo['seller_name'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['sellerBusinessName']			=	$ordInfo['seller_business_name']?htmlspecialchars_decode(stripslashes($ordInfo['seller_business_name'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['sellerPhone']					=	$ordInfo['seller_phone']?htmlspecialchars_decode(stripslashes($ordInfo['seller_phone'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['sellerEmail']					=	$ordInfo['seller_email']?htmlspecialchars_decode(stripslashes($ordInfo['seller_email'])):'';
				$k++; $l++;
			endforeach;  
			//echo '<pre>'; print_r($sessionarray); print_r($orderarray); print_r($rData); die;
			return $rData;
		else:
			return false;
		endif; 
	}

	/* * *********************************************************************
	 * * Function name : myorderdetails
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user my order details
	 * * Date : 13 OCTOBER 2018
	 * * **********************************************************************/
	public function myorderdetails()
	{	
		$this->frontauth_model->authCheck();
		$data['error'] 						= 	'';
		$data['leftMenu'] 					= 	'my-order-details';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');

		$this->layouts->set_title('My Order Details');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/myorderdetails',array(),$data);
	}

	/* * *********************************************************************
	 * * Function name : trackyourorders
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user track your orders
	 * * Date : 13 OCTOBER 2018
	 * * **********************************************************************/
	public function trackyourorders()
	{	
		$this->frontauth_model->authCheck();
		$data['error'] 						= 	'';
		$data['leftMenu'] 					= 	'track-your-orders';
		$data['userId'] 					= 	sessionData('SHELLIOS_USER_ID');

		$this->layouts->set_title('Track Your Orders');
		$this->layouts->set_description('');
		$this->layouts->set_keyword('');
		$this->layouts->front_view('front/user/trackyourorders',array(),$data);
	}

	/***********************************************************************
	** Function name : addtowishlist
	** Developed By : Manoj Kumar
	** Purpose  : This function used for addtowishlist page
	** Date : 22 OCTOBER 2018
	************************************************************************/
	public function addtowishlist()
	{
		$data 								=	array('success'=>0,'message'=>lang('INVALID_ACCESS'),'result'=>'');
		if($this->input->post('userId') && $this->input->post('productId')):
			$userId							=	base64_decode($this->input->post('userId'));
			$productId						=	base64_decode($this->input->post('productId'));
			
			$addParam['user_id']			=	$userId;
			$addParam['prod_id']			=	$productId;
			
			$checkWishlist					=	$this->front_model->checkInWishlist($addParam['user_id'],$addParam['prod_id']);
			if($checkWishlist == 'Y'):
				$dparams['user_id']			=	$userId;
				$dparams['prod_id']			=	$productId;
				$this->common_model->deleteByMultipleCondition('wishlist',$dparams);
				$data 						=	array('success'=>2,'message'=>lang('REMOVEFROMWISHLIST_SUCCESS'),'result'=>'');
			else:
				$addParam['creation_date']	=	currentDateTime();
				$addParam['status']			=	'Y';
				$walastInsertId				=	$this->common_model->addData('wishlist',$addParam);

				$wUparam['encrypt_id']		=	manojEncript($walastInsertId);
				$wUparam['wishlist_id']		=	generateUniqueId($walastInsertId);
				$wUwhere['id']				=	$walastInsertId;
				$this->common_model->editDataByMultipleCondition('wishlist',$wUparam,$wUwhere);

				$data 						=	array('success'=>1,'message'=>lang('ADDTOWISHLIST_SUCCESS'),'result'=>'');
			endif;			
		endif;
		header('Content-type: application/json');
		echo json_encode($data);
	}

	/***********************************************************************
	** Function name : deletefromwishlist
	** Developed By : Manoj Kumar
	** Purpose  : This function used for delete from wishlist
	** Date : 22 OCTOBER 2018
	************************************************************************/
	public function deletefromwishlist()
	{
		$data 								=	array('success'=>0,'message'=>lang('INVALID_ACCESS'),'result'=>'');
		if($this->input->post('userId') && $this->input->post('productId')):
			$userId							=	base64_decode($this->input->post('userId'));
			$productId						=	base64_decode($this->input->post('productId'));
			
			$dparams['user_id']				=	$userId;
			$dparams['prod_id']				=	$productId;
			$this->common_model->deleteByMultipleCondition('wishlist',$dparams);

			$productCount					=	$this->front_model->getProductCountInWishlist($userId);
			if($productCount > 0):
				$data 						=	array('success'=>1,'message'=>lang('REMOVEFROMWISHLIST_SUCCESS'),'result'=>'');
			else:
				$this->session->set_flashdata('alert_success',lang('REMOVEFROMWISHLIST_SUCCESS'));
				$data 						=	array('success'=>2,'message'=>'','result'=>'');
			endif;
		endif;
		header('Content-type: application/json');
		echo json_encode($data);
	}

	/***********************************************************************
	** Function name : onlydeletefromwishlist
	** Developed By : Manoj Kumar
	** Purpose  : This function used for only delete from wishlist
	** Date : 23 OCTOBER 2018
	************************************************************************/
	public function onlydeletefromwishlist()
	{
		$data 								=	array('success'=>0,'message'=>lang('INVALID_ACCESS'),'result'=>'');
		if($this->input->post('userId') && $this->input->post('productId')):
			$userId							=	base64_decode($this->input->post('userId'));
			$productId						=	base64_decode($this->input->post('productId'));
			
			$dparams['user_id']				=	$userId;
			$dparams['prod_id']				=	$productId;
			$this->common_model->deleteByMultipleCondition('wishlist',$dparams);

			$productCount					=	$this->front_model->getProductCountInWishlist($userId);
			if($productCount > 0):
				$data 						=	array('success'=>1,'message'=>lang('REMOVEFROMWISHLIST_SUCCESS'),'result'=>'');
			else:
				$data 						=	array('success'=>2,'message'=>lang('REMOVEFROMWISHLIST_SUCCESS'),'result'=>'');
			endif;
		endif;
		header('Content-type: application/json');
		echo json_encode($data);
	}

	/***********************************************************************
	** Function name : changeCartDataCookieToUser
	** Developed By : Manoj Kumar
	** Purpose  : This function used for change Cart Data Cookie To User
	** Date : 22 OCTOBER 2018
	************************************************************************/
	public function changeCartDataCookieToUser($userId='',$cookieId='')
	{  
		$uparams['cookie_id']	=	'';
		$uparams['update_date']	=	currentDateTime();

		$Wparams['user_id']		=	$userId;
		$this->common_model->editDataByMultipleCondition('cart',$uparams,$Wparams);

		$userQuery			=	"SELECT prod_id FROM ".getTablePrefix()."cart WHERE user_id = '".$userId."'"; 
		$userCartData		=	$this->common_model->getFieldInArray('prod_id',$userQuery);

		$cookieQuery		=	"SELECT cart_id,prod_id FROM ".getTablePrefix()."cart WHERE cookie_id = '".$cookieId."'"; 
		$cookieCartData		=	$this->common_model->getTwoFieldsInArray('cart_id','prod_id',$cookieQuery); 
		if($cookieCartData):
			foreach($cookieCartData as $cookieCartInfo):
				if(in_array($cookieCartInfo['prod_id'],$userCartData)):
					$dparams['cart_id']			=	$cookieCartInfo['cart_id'];
					$dparams['prod_id']			=	$cookieCartInfo['prod_id'];
					$this->common_model->deleteByMultipleCondition('cart',$dparams);
				else:
					$cuparams['user_id']		=	$userId;
					$cuparams['cookie_id']		=	'';
					$cuparams['update_date']	=	currentDateTime();

					$cWparams['cart_id']		=	$cookieCartInfo['cart_id'];
					$cWparams['prod_id']		=	$cookieCartInfo['prod_id'];
					$this->common_model->editDataByMultipleCondition('cart',$cuparams,$cWparams);
				endif;
			endforeach;
		endif;
		return true;
	}
}