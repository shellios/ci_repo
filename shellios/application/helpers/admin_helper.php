<?php
	
	/*
	 * show status
	 */
	if (!function_exists('showStatus')) {
		function showStatus($text='') {
			$statusArray	=	array('A'=>'<span class="status-success">Active</span>',
									  'I'=>'<span class="status-danger">Inactive</span>',
									  'B'=>'<span class="status-danger">Block</span>',
									  'D'=>'<span class="status-danger">Deleted</span>',
									  'Y'=>'<span class="status-success">Active</span>',
									  'N'=>'<span class="status-danger">Inactive</span>');
			return $statusArray[$text];
		}
	}
	
	/*
	 * show status
	 */
	if (!function_exists('showKYCStatus')) {
		function showKYCStatus($text='') {
			$statusArray	=	array('Y'=>'<span class="status-success">Verified</span>',
									  'N'=>'<span class="status-danger">Not Verify</span>');
			return $statusArray[$text];
		}
	}
	
	/*
	* Get url segment for pagination
	*/
	if (!function_exists('getUrlSegment')) {
		function getUrlSegment(){
			$urlSegment	=	array('Super admin'=>5,'Sub admin'=>5);
			return $urlSegment[sessionData('SHELLIOS_ADMIN_TYPE')];
		}
	}
	
	/*
	* Generate admin Pagination
	*/
	function adminPagination($url='',$suffix='',$rowsCount='',$perPage='',$uriSegment='') {
		$ci = & get_instance();
		$ci->load->library('pagination');
		
		$config = array();
		$config["base_url"] 		= 	$url;
		$config['suffix'] 			= 	$suffix;
		$config["total_rows"] 		= 	$rowsCount;
		$config["per_page"] 		= 	$perPage;
		$config["uri_segment"] 		= 	$uriSegment;
		$config['full_tag_open'] 	= 	'<ul class="pagination">';
		$config['full_tag_close'] 	= 	'</ul>';
		$config['num_tag_open'] 	= 	'<li class="current">';
		$config['num_tag_close'] 	= 	'</li>';
		$config['cur_tag_open'] 	= 	'<li class="current active"><a>';
		$config['cur_tag_close'] 	= 	'</a></li>';
		
		$config['next_link'] 		= 	'Next';
		$config['next_tag_open'] 	= 	'<li class="current">';
		$config['next_tag_close'] 	= 	'</li>';
		$config['prev_tag_open'] 	= 	'<li class="current">';
		$config['prev_link'] 		= 	'Previous';
		$config['prev_tag_close'] 	= 	'</li>';
		
		$config['first_link'] 		= 	'First';
		$config['first_tag_open'] 	= 	'<li class="current">';
		$config['first_tag_close'] 	= 	'</li>';
		
		$config['last_link'] 		= 	'Last';
		$config['last_tag_open'] 	= 	'<li class="current">';
		$config['last_tag_close'] 	= 	'</li>';
		
		$ci->pagination->initialize($config);
		return $ci->pagination->create_links();
	}

	/*
	* Get week day list
	*/
	if (!function_exists('getWeekDayList')) {
		function getWeekDayList($currentYear='',$currentWeek=''){
		
			if($currentWeek<=9): $currentWeek= "0".$currentWeek;  endif;
			
			$firstDay 		= 	date('Y-m-d',strtotime($currentYear."-W".$currentWeek));
			$monday 		= 	strtotime($firstDay); 
			$tuesday		= 	strtotime($firstDay." +1 days");
			$wednesday		= 	strtotime($firstDay." +2 days");
			$thursday		= 	strtotime($firstDay." +3 days");
			$friday			= 	strtotime($firstDay." +4 days");
			$saturday		= 	strtotime($firstDay." +5 days");
			$sunday			= 	strtotime($firstDay." +6 days");
			
			$dayArray[0]	=	'';
			$dayArray[1]	=	array('dayDate'=>date('Y-m-d',$monday),		'dayTime'=>$monday,		'dayName'=>strtoupper(date('D. M. d',$monday)));
			$dayArray[2]	=	array('dayDate'=>date('Y-m-d',$tuesday),	'dayTime'=>$tuesday,	'dayName'=>strtoupper(date('D. M. d',$tuesday)));
			$dayArray[3]	=	array('dayDate'=>date('Y-m-d',$wednesday),	'dayTime'=>$wednesday,	'dayName'=>strtoupper(date('D. M. d',$wednesday)));
			$dayArray[4]	=	array('dayDate'=>date('Y-m-d',$thursday),	'dayTime'=>$thursday,	'dayName'=>strtoupper(date('D. M. d',$thursday)));
			$dayArray[5]	=	array('dayDate'=>date('Y-m-d',$friday),		'dayTime'=>$friday,		'dayName'=>strtoupper(date('D. M. d',$friday)));
			$dayArray[6]	=	array('dayDate'=>date('Y-m-d',$saturday),	'dayTime'=>$saturday,	'dayName'=>strtoupper(date('D. M. d',$saturday)));
			$dayArray[7]	=	array('dayDate'=>date('Y-m-d',$sunday),		'dayTime'=>$sunday,		'dayName'=>strtoupper(date('D. M. d',$sunday)));
			
			return $dayArray;
		}
	}
	
	/*
	* Get url segment for pagination
	*/
	if (!function_exists('getYearLastWeekNumber')) {
		function getYearLastWeekNumber($year=''){
			$weekCount = date('W', strtotime($year.'-12-31'));
			if ($weekCount == '01'):
				$weekCount = date('W', strtotime($year.'-12-24'));
			endif;
			return $weekCount;
		}
	}
	
	/*
	* Get download date list
	*/
	if (!function_exists('getDownloadDateRange')) {
		function getDownloadDateRange($year='',$week=''){
		
			if($week<=9): $week= "0".$week;  endif;
			
			$curTime 				= 	strtotime($year."-W".$week); 
			
			$return[0]['fDate']  	= 	date('Y-m-d',strtotime('Monday this week',$curTime)); 
			$return[0]['lDate'] 	= 	date('Y-m-d',strtotime($return[0]['fDate']." +6 days")); 
			
			$return[1]['fDate'] 	= 	date('Y-m-d',strtotime('Monday this week +1 week',$curTime)); 
			$return[1]['lDate'] 	= 	date('Y-m-d',strtotime($return[1]['fDate']." +6 days"));  
			
			$week4				 	= 	date('Y-m-d',strtotime('Monday this week +4 week',$curTime)); 
			$return[2]['fDate'] 	= 	$return[1]['fDate']; 
			$return[2]['lDate'] 	= 	date('Y-m-d',strtotime($week4." +6 days")); 
			
			$week8 					= 	date('Y-m-d',strtotime('Monday this week +8 week',$curTime)); 
			$return[3]['fDate'] 	= 	$return[1]['fDate'] ; 
			$return[3]['lDate'] 	= 	date('Y-m-d',strtotime($week8." +6 days")); 
			
			$week12				 	= 	date('Y-m-d',strtotime('Monday this week +12 week',$curTime)); 
			$return[4]['fDate'] 	= 	$return[1]['fDate'];
			$return[4]['lDate'] 	= 	date('Y-m-d',strtotime($week12." +6 days"));  
			
			$week16				 	= 	date('Y-m-d',strtotime('Monday this week +16 week',$curTime)); 
			$return[5]['fDate'] 	= 	$return[1]['fDate'];
			$return[5]['lDate'] 	= 	date('Y-m-d',strtotime($week16." +6 days")); 
			
			$week20				 	= 	date('Y-m-d',strtotime('Monday this week +20 week',$curTime)); 
			$return[6]['fDate'] 	= 	$return[1]['fDate'];
			$return[6]['lDate'] 	= 	date('Y-m-d',strtotime($week20." +6 days")); 
			
			$return[7]['fDate'] 	= 	date('Y-m-d',strtotime('Monday this week -1 week',$curTime)); 
			$return[7]['lDate'] 	= 	date('Y-m-d',strtotime($return[7]['fDate']." +6 days")); 
			
			$return[8]['fDate'] 	= 	date('Y-m-d',strtotime('Monday this week -4 week',$curTime));
			$return[8]['lDate'] 	= 	$return[7]['lDate']; 
			
			$return[9]['fDate'] 	= 	date('Y-m-d',strtotime('Monday this week -8 week',$curTime)); 
			$return[9]['lDate'] 	= 	$return[7]['lDate'];
			
			$return[10]['fDate'] 	= 	date('Y-m-d',strtotime('Monday this week -12 week',$curTime));
			$return[10]['lDate'] 	= 	$return[7]['lDate']; 
			
			$return[11]['fDate'] 	= 	date('Y-m-d',strtotime('Monday this week -16 week',$curTime));
			$return[11]['lDate'] 	= 	$return[7]['lDate']; 
			
			$return[12]['fDate'] 	= 	date('Y-m-d',strtotime('Monday this week -20 week',$curTime));
			$return[12]['lDate'] 	= 	$return[7]['lDate']; 

			return $return;
		}
	}
	
	/*
	* Get day range list between two date
	*/
	if (!function_exists('dateRangeBetweenTwoDate')) {
		function dateRangeBetweenTwoDate($first='',$last='',$step='+1 day',$outputFormat='Y-m-d') {
		
			$dates		= 	array();
			$current 	= 	strtotime($first);
			$last 		= 	strtotime($last);
			while($current <= $last):
				$dates[] 	= 	date($outputFormat,$current);
				$current 	= 	strtotime($step,$current);
			endwhile;
			return $dates;
		}
	}
	
	/*
	 * Get charector acording to number
	 */
	if (!function_exists('getCharAccodNumber')){
		function getCharAccodNumber($num=''){
			$numeric 		= 	($num-1)%26;
			$letter 		= 	chr(65+$numeric);
			$num2 			= 	intval(($num-1)/26);
			if($num2 > 0):
				return getCharAccodNumber($num2).$letter;
			else:
				return $letter;
			endif;
		}
	}
	
	/*
	 * set cell alignment
	 */
	if (!function_exists('cellAlignment')){
		function cellAlignment($rotate=''){
			$rotate		=	$rotate==''?0:$rotate;
			return array(
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							'rotation'   => $rotate,
							'wrap'       => true
						)
					);
		}
	}
	
	/*
	 * set font bold
	 */
	if (!function_exists('fontSizeBold')){
		function fontSizeBold(){
			return array(
						'font' => array(
							'bold'  => true,
							'size'  => 16
						)
					);
		}
	}
	
	/*
	 * show venue blocked
	 */
	if (!function_exists('showVenueBlockStatus')) {
		function showVenueBlockStatus($text='') {
			$statusArray	=	array('No'=>'<span class="btn btn-success">Active</span>',
									  'Yes'=>'<span class="btn btn-danger">Blocked</span>');
			return $statusArray[$text];
		}
	}