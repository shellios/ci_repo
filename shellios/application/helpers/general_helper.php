<?php

	// make friendly url using any string
	if (!function_exists('friendlyURL')) {
		function friendlyURL($inputString){
			$url = strtolower($inputString);
			$patterns = $replacements = array();
			$patterns[0] = '/(&amp;|&)/i';
			$replacements[0] = '-and-';
			$patterns[1] = '/[^a-zA-Z01-9]/i';
			$replacements[1] = '-';
			$patterns[2] = '/(-+)/i';
			$replacements[2] = '-';
			$patterns[3] = '/(-$|^-)/i';
			$replacements[3] = '';
			$url = preg_replace($patterns, $replacements, $url);
		return $url;
		}
	}
	
	// sanitized number :  function auto remove unwanted character form given value 
	if (!function_exists('sanitizedNumber')) {
		function sanitizedNumber($_input) 
		{ 
			return (float) preg_replace('/[^0-9.]*/','',$_input); 
		}
	}
	
	// sanitized filename :  function auto remove unwanted character form given file name
	if (!function_exists('sanitizedFilename')) {
		function sanitizedFilename($filename){
			$sanitized = preg_replace('/[^a-zA-Z0-9-_\.]/','', $filename);
			return $sanitized;
		}
	}
	
	// check, is file exist in folder or not
	if (!function_exists('fileExist')) {
		function fileExist($source='', $file='', $defalut=''){
			if(!$file) return base_url().$source.$defalut;
				
			if(file_exists(FCPATH.$source.$file)):
				return base_url().$source.$file;
			else:
				return base_url().$source.$defalut;
			endif;
		}
	}
	
	if (!function_exists('myExplode')) {
		function myExplode($string){
			if($string):
			$array = explode(",",$string);
			// print_r($array);die;
				return $array;
				
			else:
				return '';
			endif;
		}
	}
	
	/*
	 * Show correct image
	 */
	if (!function_exists('correctImage')) {
		function correctImage($imageurl, $type = '') {
			if($type=='original'):
				$imageurl = str_replace('/thumb','',$imageurl);
			elseif($type):
				$imageurl = str_replace('thumb',$type,$imageurl);
			endif;
			return trim($imageurl);
		}
	}

	/*
	 * Encription
	 */
	if (!function_exists('manojEncript')) {
		function manojEncript($text) {
			$text	=	('MANOJ').$text.('KUMAR');
			return	base64_encode($text);
		}
	}
	
	/*
	 * Decription
	 */
	if (!function_exists('manojDecript')) {
		function manojDecript($text) {
			$text	=	base64_decode($text);
			$text	=	str_replace(('MANOJ'),'',$text);
			$text	=	str_replace(('KUMAR'),'',$text);
			return $text;
		}
	}
	
	/*
	 * Word Limiter
	 */
	define("STRING_DELIMITER", " ");
	if (!function_exists('wordLimiter')){
		function wordLimiter($str, $limit = 10){
			$str = strip_tags($str); 
			if (stripos($str, STRING_DELIMITER)){
				$ex_str = explode(STRING_DELIMITER, $str);
				if (count($ex_str) > $limit){
					for ($i = 0; $i < $limit; $i++){
						$str_s.=$ex_str[$i].'&nbsp;';
					}
					return $str_s.'...';
				}else{
					return $str;
				}
			}else{
				return $str;
			}
		}
	}

	/*
	 * Character Limiter
	 */
	if (!function_exists('characterLimit')){
		function characterLimit($value, $limit = 100, $end = '...'){
			$value		=	htmlspecialchars_decode(stripslashes($value));
		    if (mb_strwidth($value, 'UTF-8') <= $limit) {
		        return $value;
		    }
		    return rtrim(mb_strimwidth($value, 0, $limit, '', 'UTF-8')).' '.$end;
		}
	}
	
	if (!function_exists('currentDateTime')) {
		function currentDateTime() {
			date_default_timezone_set('Asia/Calcutta');
			return date("Y-m-d H:i:s");
		}
	}
	
	if (!function_exists('currentIp')) {
		function currentIp() {
			return $_SERVER['REMOTE_ADDR']=='::1'?'192.168.1.100':$_SERVER['REMOTE_ADDR'];
		}
	}
	
	if (!function_exists('generateRandomString')) {
		function generateRandomString($length = 10, $mode="sln") {
			$characters = "";
			if($mode=="sln"){$characters.="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";}
			elseif(strpos($mode,"s")!==false){$characters.="abcdefghijklmnopqrstuvwxyz";}
			elseif(strpos($mode,"l")!==false){$characters.="ABCDEFGHIJKLMNOPQRSTUVWXYZ";}
			elseif(strpos($mode,"n")!==false){$characters.="0123456789";}
		
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}
	}
	
	if (!function_exists('displayPrice')) {
		function displayPrice($price) {
			//if(checkFloat($price)):
				return number_format($price, 2);
			//else:
			//	return number_format($price);
			//endif;
		}
	}

	if (!function_exists('displayPercent')) {
		function displayPercent($price) {
			if(checkFloat($price)):
				return number_format($price, 2, '.', '').'%';
			else:
				return number_format($price).'%';
			endif;
		}
	}

	if (!function_exists('calculatePercent')) {
		function calculatePercent($prevpr,$curpr) {
			$val = $prevpr-$curpr;
			if($val<=0):
				$amountper = 0;
			else:
				$amountper = (($prevpr-$curpr)/($prevpr))*100;
			endif;
		
			if($amountper==100):
				$amountper = 0;
			endif;
		
			if(checkFloat($amountper)):
				return number_format($amountper,2, '.', '');
			else:
				return number_format($amountper);
			endif;
		}
	}
	
	if (!function_exists('checkFloat')) {
		function checkFloat($s_value) {
			$regex = '/^\s*[+\-]?(?:\d+(?:\.\d*)?|\.\d+)\s*$/';
			return preg_match($regex, $s_value);
		}
	}
	
	/*
	 * Get session data
	 */
	if (!function_exists('sessionData')) {
		function sessionData($text) {
			$CIOBJ = & get_instance();
			return	$CIOBJ->session->userdata($text);
		}
	}
	
	/*
	 * Get correct link
	 */
	if (!function_exists('correctLink')) {
		function correctLink($text='',$link='') {
			return	sessionData($text)?sessionData($text):$link;
		}
	}
	
	/*
	 * Get full url
	 */
	 if (!function_exists('currentFullUrl')) {
		function currentFullUrl()
		{
			$CI =& get_instance();
			$url = $CI->config->site_url($CI->uri->uri_string());
			return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
		}
	}
	
	if (!function_exists('generateUniqueId')) {
		function generateUniqueId($currentId = 1) {
			$newId		=	1000000000+$currentId;
			return $newId;
		}
	}

	if (!function_exists('getSessionForOrder')) {
		function getSessionForOrder() {
			return "SESSION".rand(10000000000,999999999999);
		}
	}

	if (!function_exists('getRandomOrderId')) {
		function getRandomOrderId() {
			return "ORDS".rand(10000000000,999999999999);
		}
	}

	/*
	 * Change ddmmyy to yymmdd
	 */
	if (!function_exists('DDMMYYtoYYMMDD')) {
		function DDMMYYtoYYMMDD($date) {
			if($date):
				$datedata			=	explode('-',$date);
				$datedata			=	$datedata[2].'-'.$datedata[1].'-'.$datedata[0];
			else:
				$datedata			=	'';
			endif;
			return $datedata;
		}
	}
	
	/*
	 * Change yymmdd to ddmmyy
	 */
	if (!function_exists('YYMMDDtoDDMMYY')) {
		function YYMMDDtoDDMMYY($date) {
			if($date && $date != '1970-01-01 00:00:00' && $date != '0000-00-00 00:00:00'):
				$datedata			=	explode(' ',$date);
				$datedata			=	explode('-',$datedata[0]);
				$datedata			=	$datedata[2].'-'.$datedata[1].'-'.$datedata[0];
			else:
				$datedata			=	'';
			endif;
			return $datedata;
		}
	}
	
	/*
	 * add time in date
	 */
	if (!function_exists('addTimeInDate')) {
		function addTimeInDate($date) {
			return $date.' 00:00:00';
		}
	}
	
	/*
	 * remove time from date
	 */
	if (!function_exists('removeTimeFromDate')) {
		function removeTimeFromDate($date) {
			$datedata				=	explode(' ',$date);
			return $datedata[0];
		}
	}
	
	/*
	 * Get charector acording to number
	 */
	if (!function_exists('getCharAccodNumber')){
		function getCharAccodNumber($num=''){
			$numeric 		= 	($num - 1) % 26;
			$letter 		= 	chr(65 + $numeric);
			$num2 			= 	intval(($num - 1) / 26);
			if($num2 > 0){
				return getCharAccodNumber($num2).$letter;
			}else{
				return $letter;
			}
		}
	}
	
	if (!function_exists('cellAlignment')){
		function cellAlignment($rotate=''){
			$rotate		=	$rotate==''?0:$rotate;
			return array(
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' 	 => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							'rotation'   => $rotate,
							'wrap'       => true
						)
					);
		}
	}
	
	if (!function_exists('cellColor')){
		function cellColor($type=''){
			$color	=	($type=='green'?'66FF00':($type=='yellow'?'FFFF33':($type=='orange'?'FF6600':'FFFFFF')));
			return array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' =>$color)
						),
						'borders' => array(
							'outline' => array(
							  'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						)
					);
		}
	}
	
	if (!function_exists('cellColorOnly')){
		function cellColorOnly($type=''){
			$color	=	($type=='green'?'66FF00':($type=='yellow'?'FFFF33':($type=='orange'?'FF6600':'FFFFFF')));
			return array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' =>$color)
						)
					);
		}
	}
	
	if (!function_exists('cellBorder')){
		function cellBorder(){
			return array(
						'borders' => array(
							'outline' => array(
							  'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						)
					);
		}
	}
	
	if (!function_exists('fontSizeBold')){
		function fontSizeBold($size=16){
			return array(
						'font' => array(
							'bold'  => true,
							'size'  => $size
						)
					);
		}
	}
	
	function getRomanNumerals($decimalInteger) 
	{
		$n = intval($decimalInteger);
		$res = '';
		
		$roman_numerals = array(
								'M'  => 1000,
								'CM' => 900,
								'D'  => 500,
								'CD' => 400,
								'C'  => 100,
								'XC' => 90,
								'L'  => 50,
								'XL' => 40,
								'X'  => 10,
								'IX' => 9,
								'V'  => 5,
								'IV' => 4,
								'I'  => 1);
		
		foreach ($roman_numerals as $roman => $numeral) 
		{
			$matches = intval($n / $numeral);
			$res .= str_repeat($roman, $matches);
			$n = $n % $numeral;
		}
		return $res;
	}


	// sanitized number :  function auto remove unwanted character form given value 
	if (!function_exists('getTablePrefix')) {
		function getTablePrefix() 
		{ 
			return 'shellios_'; 
		}
	}	

	/*
	 * Show product image
	 */
	if (!function_exists('showProductImage')) {
		function showProductImage($imageurl='', $type = '') {
			if($imageurl): 
				$repArray			=	array(base_url(),'https://cartamaam.com/','http://www.cartamaam.com/','http://cartamaam.com/');
				$imageFile 			=	str_replace($repArray,FCPATH,$imageurl);
				if(file_exists($imageFile)):
					if($type=='original'):
						$imageurl 	= 	str_replace('/thumb','',$imageurl);
					elseif($type):
						$imageurl 	= 	str_replace('thumb',$type,$imageurl);
					endif;
				else:
					if($type=='original'):
						$imageurl 	= 	base_url().'assets/front/image/detail.png';
					elseif($type=='medium'):
						$imageurl 	= 	base_url().'assets/front/image/list.png';
					elseif($type=='thumb'):
						$imageurl 	= 	base_url().'assets/front/image/thumb.png';
					else:
						$imageurl 	= 	base_url().'assets/front/image/list.png';
					endif;
				endif;
			else:
				if($type=='original'):
					$imageurl 		= 	base_url().'assets/front/image/detail.png';
				elseif($type=='medium'):
					$imageurl 		= 	base_url().'assets/front/image/list.png';
				elseif($type=='thumb'):
					$imageurl 		= 	base_url().'assets/front/image/thumb.png';
				else:
					$imageurl 		= 	base_url().'assets/front/image/list.png';
				endif;
			endif;
			return trim($imageurl);
		}
	}

	/*
	 * Show product rating
	 */
	if (!function_exists('showProductRating')) {
		function showProductRating($rating='0') {
			$ratingHtml  	=	'';
			$count 			=	5;
			$ratingData 	=	explode('.',$rating);
			if($ratingData[0]):
				for($i=0; $i < $ratingData[0]; $i++):
					$ratingHtml  	.=	'<i class="fa fa-star" aria-hidden="true"></i> ';
					$count--;
				endfor;
			endif;
			if($ratingData[1]):
				$ratingHtml  		.=	'<i class="fa fa-star-half-o" aria-hidden="true"></i> ';
				$count--;
			endif; 
			for($i=0; $i < $count; $i++):
				$ratingHtml  		.=	'<i class="fa fa-star-o" aria-hidden="true"></i> ';
			endfor;
			return $ratingHtml;
		}
	}

	/*
	 * Show product rating
	 */
	if (!function_exists('productDetailPageLink')) {
		function productDetailPageLink($frontUrl='',$cateSlug='',$subcateSlug='',$prodSlug='') {
			if($frontUrl && $cateSlug && $subcateSlug && $prodSlug):
				$pageLink 		=	$frontUrl.'product-details/'.$cateSlug.'/'.$subcateSlug.'/'.$prodSlug;
			else:
				$pageLink 		=	'javascript:void(0)';
			endif;
			return $pageLink;
		}
	}

	/*
	 * Show numeric string
	 */
	if (!function_exists('getNumericString')){
		function getNumericString($num=1){
			$numArray	=	array('1'=>'First',
								  '2'=>'Second',
								  '3'=>'Third',
								  '4'=>'Fourth',
								  '5'=>'Fifth',
								  '6'=>'Sixth',
								  '7'=>'Seventh',
								  '8'=>'Eighth',
								  '9'=>'Nineth',
								  '10'=>'Tenth',
								  '11'=>'Eleventh',
								  '12'=>'Twelve',
								  '13'=>'Thirteen',
								  '14'=>'Fourteen',
								  '15'=>'Fifteen',);
			return $numArray[$num];
		}
	}