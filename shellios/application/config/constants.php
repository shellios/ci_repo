<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR 	define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR 	define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR 	define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR 	define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR 	define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR 	define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR 	define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR 	define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR 	define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR 	define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR 	define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR 	define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR 	define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        	OR 	define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          	OR 	define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         	OR 	define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   	OR 	define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  	OR 	define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') 	OR 	define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     	OR 	define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       	OR 	define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      	OR 	define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      	OR 	define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('APIKEY')     			OR 	define('APIKEY',md5("SHELLIOS".date('Y-m-d'))); //2019-08-04
defined('TOKEN_NO')     		OR 	define('TOKEN_NO',md5("SHELLIOSAPPAPI".date('Y-m-d'))); 

defined('SHOW_NO_OF_DATA')      OR 	define('SHOW_NO_OF_DATA', 10); // show no of data in table

defined('MAIL_FROM_MAIL')      	OR 	define('MAIL_FROM_MAIL', 'vivek@algosoftech.in'); 
defined('SMS_ADMIN_NUMBER') 	OR 	define('SMS_ADMIN_NUMBER', '9711882641'); 
defined('MAIL_SITE_FULL_NAME') 	OR 	define('MAIL_SITE_FULL_NAME', 'SHELLIOUS'); 

defined('SELLIOUS_USER_ANDRIOD_KEY')		OR 	define('SELLIOUS_USER_ANDRIOD_KEY','AIzaSyCcTT7OteCs1LlIfb3Fpj4pt1D2rJEiEAE');
defined('SELLIOUS_USER_IOS_KEY')			OR 	define('SELLIOUS_USER_IOS_KEY','AIzaSyCE1A0tskpeAuAsKG23FHdB1pNhvjnk3iQ');

defined('SELLIOUS_SELLER_ANDRIOD_KEY')		OR 	define('SELLIOUS_SELLER_ANDRIOD_KEY','');
defined('SELLIOUS_SELLER_IOS_KEY')			OR 	define('SELLIOUS_SELLER_IOS_KEY','');

defined('ROZ_PAY_URL') 			OR 	define('ROZ_PAY_URL', 'https://checkout.razorpay.com/v1/checkout.js'); 
defined('ROZ_PAY_KEY_ID') 		OR 	define('ROZ_PAY_KEY_ID', 'rzp_test_6TslbiNKZ5yr03'); //'rzp_live_ALji1OlxGdu1jJ');  //https://docs.razorpay.com/docs/checkout-form
defined('ROZ_PAY_BUTTON_TEXT') 	OR 	define('ROZ_PAY_BUTTON_TEXT', 'Pay Now'); 
defined('ROZ_PAY_SITE_NAME') 	OR 	define('ROZ_PAY_SITE_NAME', 'CarTamaam'); 
defined('ROZ_PAY_SITE_DESC') 	OR 	define('ROZ_PAY_SITE_DESC', 'Online Shopping'); 
defined('ROZ_PAY_SITE_LOGO') 	OR 	define('ROZ_PAY_SITE_LOGO', 'assets/front/image/logo.png'); 
defined('ROZ_PAY_THEME_COLOR') 	OR 	define('ROZ_PAY_THEME_COLOR', '#C0B283'); 