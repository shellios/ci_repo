<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 			= 	'front/welcome/index';
$route['404_override'] 					= 	'';
$route['translate_uri_dashes'] 			= 	FALSE;

$route['admin'] 						= 	'admin/login/index';
$route['password-recover'] 				= 	'admin/login/password_recover';
$route['admin/dashboard'] 				= 	'admin/login/dashboard';
$route['admin/logout'] 					= 	'admin/login/logout';

$curUrl						=	explode('/',$_SERVER['REQUEST_URI']); // print_r($curUrl); die;
if($_SERVER['SERVER_NAME']=='localhost'):
	$firstSlug				=	isset($curUrl[2])?$curUrl[2]:'';
	$secondSlug				=	isset($curUrl[3])?$curUrl[3]:'';
	$thirdSlug				=	isset($curUrl[4])?$curUrl[4]:'';
	//echo $secondSlug; die;
else: 
	$firstSlug				=	isset($curUrl[3])?$curUrl[3]:'';
	$secondSlug				=	isset($curUrl[4])?$curUrl[4]:'';
	$thirdSlug				=	isset($curUrl[5])?$curUrl[5]:'';
endif;

if(isset($firstSlug) && $firstSlug == 'admin'):
	require_once(BASEPATH.'database/DB.php');
	$db 					=	& DB();
	$sSlugQuery 			= 	$db->select('encrypt_id')->from('shellios_admin')->where("admin_slug = '".$secondSlug."' AND status = 'A'")->get();
	$sSlugData				= 	$sSlugQuery->result();   
	
	if($sSlugData):
		if($thirdSlug == 'dashboard'):  
			$route[$firstSlug.'/'.$secondSlug.'/dashboard'] = 	'admin/login/dashboard';
		elseif($thirdSlug == 'profile'):  
			$route[$firstSlug.'/'.$secondSlug.'/profile'] 	= 	'admin/login/profile';
		elseif($thirdSlug == 'editprofile'):  
			$route[$firstSlug.'/'.$secondSlug.'/editprofile']= 	'admin/login/editprofile';
			$route[$firstSlug.'/'.$secondSlug.'/editprofile/(:any)']= 	'admin/login/editprofile/$1';
		else:
			$newCurUrl				=	explode('/'.$firstSlug.'/'.$secondSlug.'/',$_SERVER['REQUEST_URI']);
			
			if($_SERVER['SERVER_NAME']=='localhost'):
				$classFunction		=	isset($newCurUrl[1])?$newCurUrl[1]:'';
			else: 
				$classFunction		=	isset($newCurUrl[1])?$newCurUrl[1]:'';
			endif;  
			$classFunction			=	strpos($classFunction,'/?')?explode('/?',$classFunction):explode('?',$classFunction);
			
			if($classFunction[0]):
				$defaultClassArray	=	array('login','departmentlist','subadminlist');
				$classArray			=	explode('/',$classFunction[0]);
				if(isset($classArray[0])):  
					$classQuery 	= 	$db->select('encrypt_id')->from('shellios_admin_module')->where("module_name = '".$classArray[0]."'")->get();
					$classData		= 	$classQuery->result();
					$subClassQuery 	= 	$db->select('encrypt_id')->from('shellios_admin_module_child')->where("module_name = '".$classArray[0]."'")->get();
					$subClassData	= 	$subClassQuery->result();  
					if($classData):		
						$route[$firstSlug.'/'.$secondSlug.'/'.$classFunction[0]]= 	'admin/'.$classFunction[0]; 
					elseif($subClassData):	
						$route[$firstSlug.'/'.$secondSlug.'/'.$classFunction[0]]= 	'admin/'.$classFunction[0]; 
					elseif(in_array($classArray[0],$defaultClassArray)):  
						$route[$firstSlug.'/'.$secondSlug.'/'.$classFunction[0]]= 	'admin/'.$classFunction[0]; 
					endif;
				endif;
			endif;
		endif;
	endif;
elseif(isset($firstSlug) && $firstSlug == 'api'): 
	
	#################################################################################
	##########################		API FOR APP		  ###############################
	#################################################################################

	///////////////////////////		USER	//////////////////////////
	$route['api/user/signup'] 				= 	'api/user/signup';
	$route['api/user/otpVerification'] 		= 	'api/user/otpverification';	
	$route['api/user/reTryOTP'] 			= 	'api/user/retryotp';
	$route['api/user/socialLogin'] 			= 	'api/user/sociallogin';	
	$route['api/user/socialActivation']		= 	'api/user/socialsignupactivation';	
	$route['api/user/updateAccount'] 		= 	'api/user/updateaccount';
	$route['api/user/updateProfileImage'] 	= 	'api/user/updateprofileimage';
	$route['api/user/feedBack'] 			= 	'api/user/feedback';
	$route['api/user/faq'] 					= 	'api/user/faq';
	//$route['api/user/productList'] 			= 	'api/user/productlist';
	$route['api/user/productLink'] 			= 	'api/user/productlinking';
	$route['api/user/linkedProductList'] 	= 	'api/user/linkedproductlist';
	$route['api/user/serviceRequest'] 		= 	'api/user/servicerequest';
	$route['api/user/getListSerReq'] 		= 	'api/user/getlistserreq';
	
	

	//////////////////////				FOR WEB VIEW 			/////////////////////
	$route['api/webview/CancellationsAndRefunds'] = 	'api/webview/cancellationsandrefunds';
	$route['api/webview/Disclaimer'] 		= 	'api/webview/disclaimer';
	$route['api/webview/TermOfUse'] 		= 	'api/webview/termsofuse';
	$route['api/webview/AboutUs'] 			= 	'api/webview/aboutus';


else:
	$route['cancellations-and-refunds'] 	= 	'front/welcome/cancellationsandrefunds';
	$route['disclaimer'] 					= 	'front/welcome/disclaimer';
	$route['terms-of-use'] 					= 	'front/welcome/termsofuse';
	$route['about-us'] 						= 	'front/welcome/aboutus';
	$route['contact-us'] 					= 	'front/welcome/contactus';
	$route['become-a-seller'] 				= 	'front/welcome/becomeaseller';
	$route['welcome/addtonewsletter'] 		= 	'front/welcome/addtonewsletter';


	///////////////////////				PRODUCT SECTION			//////////////////////
	$route['product-listing/(:any)'] 		= 	'front/product/listing/$1';
	$route['product/getcerbrandmodel'] 		= 	'front/product/getcerbrandmodel';
	$route['product/getproductlist'] 		= 	'front/product/getproductlist';
	$route['product-details/(:any)/(:any)/(:any)']= 	'front/product/details/$1/$2/$3';
	$route['product/search'] 				= 	'front/product/search';

	///////////////////////				CART SECTION			///////////////////////
	$route['user/addtomycart'] 				= 	'front/cart/addtomycart';
	$route['user/getfrommycart'] 			= 	'front/cart/getfrommycart';
	$route['user/cart'] 					= 	'front/cart/cartview';
	$route['user/cart/getcartdata'] 		= 	'front/cart/getcartdata';
	$route['user/cart/getcheckoutcartdata'] = 	'front/cart/getcheckoutcartdata';
	$route['user/cart/plusminusquantity'] 	= 	'front/cart/plusminusquantity';
	$route['user/cart/deleteprodfromcart'] 	= 	'front/cart/deleteprodfromcart';
	$route['user/checkout'] 				= 	'front/cart/checkout';
	$route['user/checkout/installation/(:any)']= 	'front/cart/checkout/installation/$1';
	$route['user/checkout/delivery-here/(:any)']= 	'front/cart/checkout/deliveryhere/$1';
	
	$route['user/cart/getbuynowcheckoutdata']= 	'front/cart/getbuynowcheckoutdata';
	$route['user/cart/plusminusbuynowquantity']= 	'front/cart/plusminusbuynowquantity';
	$route['user/buy-now-checkout'] 		= 	'front/cart/buynowcheckout';
	$route['user/buy-now-checkout/productId/(:any)']= 	'front/cart/buynowcheckout/productId/$1';
	$route['user/buy-now-checkout/delivery-here/(:any)']= 	'front/cart/buynowcheckout/deliveryhere/$1';
	
	$route['user/payment'] 					= 	'front/cart/payment';
	$route['user/payment-success'] 			= 	'front/cart/paymentsuccess';
	$route['user/payment/thanks'] 			= 	'front/cart/thanks';
	$route['user/payment/failure'] 			= 	'front/cart/failure';


	///////////////////////				USER SECTION			///////////////////////
	$route['user/signup'] 					= 	'front/user/signup';
	$route['user/signup/(:any)'] 			= 	'front/user/signup/$1';
	$route['user/otp-verification'] 		= 	'front/user/otpverification';
	$route['user/login'] 					= 	'front/user/login';
	$route['user/login/(:any)'] 			= 	'front/user/login/$1';
	$route['user/mobile-verify'] 			= 	'front/user/mobileverify';
	$route['user/fbresponsedata'] 			= 	'front/user/fbresponsedata';
	$route['user/gplusresponsedata'] 		= 	'front/user/gplusresponsedata';
	$route['user/enter-mobile'] 			= 	'front/user/entermobile';
	$route['user/verify-mobile'] 			= 	'front/user/verifymobile';
	$route['user/forgot-password'] 			= 	'front/user/forgotpassword';
	$route['user/reset-password'] 			= 	'front/user/resetpassword';
	$route['user/logout'] 					= 	'front/user/logout';

	$route['user/my-account'] 				= 	'front/user/myaccount';
	$route['user/account-information'] 		= 	'front/user/accountinformation';
	$route['user/change-password'] 			= 	'front/user/changepassword';
	$route['user/address-book'] 			= 	'front/user/addressbook';
	$route['user/add-edit-address'] 		= 	'front/user/addeditaddress';
	$route['user/add-edit-address/(:any)'] 	= 	'front/user/addeditaddress/$1';
	$route['user/my-wishlist'] 				= 	'front/user/mywishlist';
	$route['user/my-order'] 				= 	'front/user/myorder';
	$route['user/my-order/cancel-order/(:any)']= 	'front/user/myorder/cancelorder/$1';
	$route['user/my-order-details/(:any)'] 	= 	'front/user/myorderdetails/$1';
	$route['user/track-your-orders'] 		= 	'front/user/trackyourorders';

	$route['user/addtowishlist'] 			= 	'front/user/addtowishlist';
	$route['user/deletefromwishlist'] 		= 	'front/user/deletefromwishlist';
	$route['user/onlydeletefromwishlist'] 	= 	'front/user/onlydeletefromwishlist';
endif;
//echo '<pre>'; print_r($route);
//die;