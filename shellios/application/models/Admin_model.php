<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
		$this->load->database(); 
	}
	
	/***********************************************************************
	** Function name : selectAdminData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for select admin data
	** Date : 09 AUGUST 2018
	************************************************************************/
	function selectAdminData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('adm.*');
		$this->db->from($tblName);
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : selectDepartmentData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for select Department Data
	** Date : 09 AUGUST 2018
	************************************************************************/
	function selectDepartmentData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('admdep.*');
		$this->db->from($tblName);
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : selectSubadminData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for select Subadmin Data
	** Date : 09 AUGUST 2018
	************************************************************************/
	function selectSubadminData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('adm.*, admt.admin_metadata_value as admin_type');
		$this->db->from($tblName);
		$this->db->join("admin_metadata as admt","adm.encrypt_id=admt.admin_id","LEFT");
		$this->db->join("admin_metadata as admta","adm.encrypt_id=admta.admin_id","LEFT");
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getAdminDepartment
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Admin Department
	** Date : 09 AUGUST 2018
	************************************************************************/
	function getAdminDepartment($departmentId='')
	{
		$html			=	'<option value="">Select Department Name</option>';
		$this->db->select('encrypt_id,department_name');
		$this->db->from('admin_department');
		$this->db->order_by("department_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() >0):	
			$data	=	$query->result_array();
			foreach($data as $info): 
				if($info['encrypt_id'] == $departmentId):  $select ='selected="selected"'; else: $select =''; endif;
				$html		.=	'<option value="'.$info['encrypt_id'].'" '.$select.'>'.$info['department_name'].'</option>';
			endforeach;
		endif;
			
		return $html;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : selectCustomerfeedbackData
	** Developed By : Ashish UMrao
	** Purpose  : This function used for select Customer feedback Data
	** Date : 14 AUGUST 2018
	************************************************************************/
	function selectCustomerfeedbackData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('feed.*,u.user_name,u.user_email');
		$this->db->from($tblName);
		$this->db->join('users as u','feed.userPhone = u.user_Phone','LEFT');
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : selectproductData
	** Developed By : Ashish UMrao
	** Purpose  : This function used for select product Data
	** Date : 29 APRIL 2019
	************************************************************************/
	function selectproductData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('p.*');
		$this->db->from($tblName);
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION


	
	/***********************************************************************
	** Function name : selectProdBrandData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for select Prod Brand Data
	** Date : 14 AUGUST 2018
	************************************************************************/
	function selectProdBrandData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('pbran.*');
		$this->db->from($tblName);
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION

	
	/***********************************************************************
	** Function name : selectfaqData
	** Developed By : Ashish Umrao
	** Purpose  : This function used for select faq Data
	** Date : 09 April 2019
	************************************************************************/
	function selectfaqData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('f.*');
		$this->db->from($tblName);
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : selectMailTemplateData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for select Mail Template Data
	** Date : 03 NOVEMBER 2018
	************************************************************************/
	function selectMailTemplateData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('emlt.*');
		$this->db->from($tblName);
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : selectCustomerData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for select Customer Data
	** Date : 22 NOVEMVER 2018
	************************************************************************/
	function selectCustomerData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('usr.*');
		$this->db->from($tblName);
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION
/***********************************************************************
	** Function name : selectservicerequestData
	** Developed By : Ashish UMrao
	** Purpose  : This function used for select Service Request Data
	** Date : 14 May 2019
	************************************************************************/
	function selectservicerequestData($action='',$tblName='',$whereCon='',$shortField='',$numPage='',$cnt='')
	{ 
		$this->db->select('ur.*,u.user_name,u.user_phone,p.prod_brand_name,p.prod_sr_no');
		$this->db->from($tblName);
		$this->db->join('users as u','ur.user_ID=u.id','LEFT');
		$this->db->join("product as p","ur.product_id=p.id","LEFT");
		if($whereCon['where']):	$this->db->where($whereCon['where']);	 	endif;
		if($whereCon['like']):  $this->db->where($whereCon['like']); 		endif;
		if($shortField):		$this->db->order_by($shortField);			endif;
		if($numPage):			$this->db->limit($numPage,$cnt);			endif;
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		if($action == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION
	
	
}