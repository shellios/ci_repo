<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Frontauth_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
		$this->load->database(); 
	}

	/* * *********************************************************************
	 * * Function name : Authenticate
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for user Login Page
	 * * Date : 10 OCTOBER 2018
	 * * **********************************************************************/
	public function Authenticate($userPhone='')
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_phone',$userPhone);
		$query	=	$this->db->get();
		if($query->num_rows() >0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/* * *********************************************************************
	 * * Function name : getUserDataByMobile
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get User Data By Mobile
	 * * Date : 10 OCTOBER 2018
	 * * **********************************************************************/
	public function getUserDataByMobile($userPhone='')
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_phone',$userPhone);
		$query	=	$this->db->get();
		if($query->num_rows() >0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/* * *********************************************************************
	 * * Function name : getUserDataByEmail
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get User Data By Email
	 * * Date : 12 OCTOBER 2018
	 * * **********************************************************************/
	public function getUserDataByEmail($userEmail='')
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_email',$userEmail);
		$query	=	$this->db->get();
		if($query->num_rows() >0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/* * *********************************************************************
	 * * Function name : getUserDataByUserId
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get User Data By User Id
	 * * Date : 12 OCTOBER 2018
	 * * **********************************************************************/
	public function getUserDataByUserId($userId='')
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_id',$userId);
		$query	=	$this->db->get();
		if($query->num_rows() >0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : encriptPassword
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for encript_password
	 * * Date : 16 AUGUST 2018
	 * * **********************************************************************/
	public function encriptPassword($password)
	{
		return $this->encrypt->encode($password, $this->config->item('encryption_key'));
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : decryptsPassword
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for encript_password
	 * * Date : 16 AUGUST 2018
	 * * **********************************************************************/
	public function decryptsPassword($password)
	{
		return $this->encrypt->decode($password, $this->config->item('encryption_key'));
	}	// END OF FUNCTION

	/* * *********************************************************************
	 * * Function name : checkOnlyUserLoginCookie
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for check Only User Login Cookie
	 * * Date : 12 OCTOBER 2018
	 * * **********************************************************************/
	public function checkOnlyUserLoginCookie()
	{  
		if($_COOKIE['SHELLIOS_USER_PHONE']):
			
			$result		=	$this->Authenticate($_COOKIE['SHELLIOS_USER_PHONE']);
			if($result): 
				if($result['status'] == 'A'):	

					$this->session->set_userdata(array(
											'SHELLIOS_USER_LOGGED_IN'=>true,
											'SHELLIOS_USER_ID'=>$result['user_id'],
											'SHELLIOS_USER_TYPE'=>$result['user_type'],
											'SHELLIOS_USER_BROWSE_TYPE'=>$result['browse_type'],
											'SHELLIOS_USER_NAME'=>$result['user_name'],
											'SHELLIOS_USER_EMAIL'=>$result['user_email'],
											'SHELLIOS_USER_PHONE'=>$result['user_phone'],
											'SHELLIOS_USER_IMAGE'=>$result['user_image'],
											'SHELLIOS_USER_LAST_LOGIN'=>$result['user_last_login'].' ('.$result['user_last_login_ip'].')'));
					
					setcookie('SHELLIOS_USER_PHONE',$result['user_phone'],time()+60*60*24*100,'/');
				endif;
			endif;
		endif;
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : checkUserLoginCookie
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for check User Login Cookie
	 * * Date : 10 OCTOBER 2018
	 * * **********************************************************************/
	public function checkUserLoginCookie()
	{  
		if($_COOKIE['SHELLIOS_USER_PHONE']):
			
			$result		=	$this->Authenticate($_COOKIE['SHELLIOS_USER_PHONE']);
			if($result): 
				if($result['status'] == 'A'):	

					$this->session->set_userdata(array(
											'SHELLIOS_USER_LOGGED_IN'=>true,
											'SHELLIOS_USER_ID'=>$result['user_id'],
											'SHELLIOS_USER_TYPE'=>$result['user_type'],
											'SHELLIOS_USER_BROWSE_TYPE'=>$result['browse_type'],
											'SHELLIOS_USER_NAME'=>$result['user_name'],
											'SHELLIOS_USER_EMAIL'=>$result['user_email'],
											'SHELLIOS_USER_PHONE'=>$result['user_phone'],
											'SHELLIOS_USER_IMAGE'=>$result['user_image'],
											'SHELLIOS_USER_LAST_LOGIN'=>$result['user_last_login'].' ('.$result['user_last_login_ip'].')'));
					
					setcookie('SHELLIOS_USER_PHONE',$result['user_phone'],time()+60*60*24*100,'/');
					
					if($_COOKIE['SHELLIOS_USER_REFERENCE_PAGES']):
						redirect(base_url().$_COOKIE['SHELLIOS_USER_REFERENCE_PAGES']);
					else:
						redirect($this->session->userdata('SHELLIOS_FRONT_CURRENT_PATH').'user/my-account');
					endif;
				endif;
			endif;
		endif;
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : authCheck
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for auth Check
	 * * Date : 10 OCTOBER 2018
	 * * **********************************************************************/
	public function authCheck()
	{
		if($this->session->userdata('SHELLIOS_USER_ID') == ""):
			setcookie('SHELLIOS_USER_REFERENCE_PAGES',uri_string(),time()+60*60*24*5,'/');
			redirect(base_url().'user/login');
		else:
			return true;
		endif;
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : checkOTP
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for Admin otp
	 * * Date : 10 OCTOBER 2018
	 * * **********************************************************************/
	public function checkOTP($userMobile='',$userOtp='')
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_phone',$userMobile);
		$this->db->where('user_otp',$userOtp);
		$query	=	$this->db->get();
		if($query->num_rows() >0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
}	