<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
		$this->load->database(); 
	}

	/* * *********************************************************************
	 * * Function name : encriptPassword
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for encript_password
	 * * Date : 27 AUGUST 2018
	 * * **********************************************************************/
	public function encriptPassword($password)
	{
		return $this->encrypt->encode($password, $this->config->item('encryption_key'));
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : decryptsPassword
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for encript_password
	 * * Date : 27 AUGUST 2018
	 * * **********************************************************************/
	public function decryptsPassword($password)
	{
		return $this->encrypt->decode($password, $this->config->item('encryption_key'));
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : checkInWishlist
	** Developed By : Ashish UMrao
	** Purpose  : This function used for check In Wishlist
	** Date : 24 SEPTEMBER 2018
	************************************************************************/
	function checkInWishlist($userId='',$productId='')
	{
		$this->db->select('wishlist_id');
		$this->db->from('wishlist');
		$this->db->where("user_id",$userId);
		$this->db->where("prod_id",$productId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return 'Y';
		else:
			return 'N';
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductImage
	** Developed By : Ashish UMrao
	** Purpose  : This function used for get Product Image
	** Date : 24 SEPTEMBER 2018
	************************************************************************/
	function getProductImage($productId='',$count='')
	{
		$this->db->select('prod_image');
		$this->db->from('prod_image');
		$this->db->where("prod_id",$productId);
		$this->db->limit($count);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getAllOrderId
	** Developed By : Ashish UMrao
	** Purpose  : This function used for get All Order Id
	** Date : 01 OCTOBER 2018
	************************************************************************/
	function getAllOrderIdBySeller($userId='')
	{
		$allsellerIds   = 	array();
		$AllOrderId   	= 	array();
		$this->db->select('prod.seller_id');
		$this->db->from('cart as cart');
		$this->db->join("product as prod","cart.prod_id=prod.prod_id","LEFT");
		$this->db->join("seller as sell","prod.seller_id=sell.seller_id","LEFT");
		$this->db->where("cart.user_id",$userId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			$data = $query->result_array();
			foreach($data as $info):
				if(!in_array($info['seller_id'],$allsellerIds)):
					array_push($allsellerIds,$info['seller_id']);
					$AllOrderId[$info['seller_id']]	=	getRandomOrderId();
				endif;
			endforeach;
		endif;
		return $AllOrderId;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getOrderData
	** Developed By : Ashish UMrao
	** Purpose  : This function used for get Order Data
	** Date : 03 OCTOBER 2018
	************************************************************************/
	function getOrderData($userId='',$orderId='')
	{
		$this->db->select('id,order_id,seller_id,total_amount,installation_charges');
		$this->db->from('orders');
		$this->db->where("user_id",$userId);
		$this->db->where("session_id",$orderId);
		$this->db->where("browse_type = 'app'");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getRatingbyProductId
	** Developed By : Ashish UMrao
	** Purpose  : This function used for get Rating by Product Id
	** Date : 16 OCTOBER 2018
	************************************************************************/
	function getRatingbyProductId($productId='')
	{
		$ratingData['rating']		=	floatval(0);
		$ratingData['count']		=	floatval(0);
		$this->db->select('sum(rating) as total_rating, count(rating) as total_count');
		$this->db->from('prod_user_review');
		$this->db->where("prod_id",$productId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):  
			$data 	=	$query->row_array(); 
			if($data['total_rating'] > 0 && $data['total_count'] > 0):
				$ratingData['rating']		=	floatval($data['total_rating']/$data['total_count']);
				$ratingData['count']		=	floatval($data['total_count']);
			endif;
		endif;
		return $ratingData;
	}	// END OF FUNCTION
}	