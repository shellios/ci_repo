<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
		$this->load->database(); 
	}
	
	/***********************************************************************
	** Function name : addData
	** Developed By : Ashish UMrao
	** Purpose  : This function used for add data
	** Date : 09 AUGUSt 2018
	************************************************************************/
	public function addData($tableName='',$param=array())
	{
		$this->db->insert($tableName,$param);
		return $this->db->insert_id();
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : editData
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for edit data
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	function editData($tableName='',$param='',$fieldName='',$fieldVallue='')
	{ 
		$this->db->where($fieldName,$fieldVallue);
		$this->db->update($tableName,$param);
		return true;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : editDataByMultipleCondition
	** Developed By : Ashish UMrao
	** Purpose  : This function used for edit data by multiple condition
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function editDataByMultipleCondition($tableName='',$param=array(),$whereCondition=array())
	{
		$this->db->where($whereCondition);
		$this->db->update($tableName,$param);
		return true;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : deleteData
	** Developed By : Ashish UMrao
	** Purpose  : This function used for delete data
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function deleteData($tableName='',$fieldName='',$fieldVallue='')
	{
		$this->db->delete($tableName,array($fieldName=>$fieldVallue));
		return true;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : deleteParticularData
	** Developed By : Ashish UMrao
	** Purpose  : This function used for delete particular data
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function deleteParticularData($tableName='',$fieldName='',$fieldValue='')
	{
		$this->db->delete($tableName,array($fieldName=>$fieldValue));
		return true;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : deleteByMultipleCondition
	** Developed By : Ashish UMrao
	** Purpose  : This function used for delete by multiple condition
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function deleteByMultipleCondition($tableName='',$whereCondition=array())
	{
		$this->db->delete($tableName,$whereCondition);
		return true;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name: getDataByParticularField
	** Developed By: Ashish UMrao
	** Purpose: This function used for get data by encryptId
	** Date : 09 AUGUSt 2018
	************************************************************************/
	public function getDataByParticularField($tableName='',$fieldName='',$fieldValue='')
	{  
		$this->db->select('*');
		$this->db->from($tableName);	
		$this->db->where($fieldName,$fieldValue);
		$query = $this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name: getDataByQuery
	** Developed By: Ashish UMrao
	** Purpose: This function used for get data by query
	** Date : 09 AUGUSt 2018
	************************************************************************/
	public function getDataByQuery($action='',$query='',$from='')
	{  
		$query = $this->db->query($query);
		if($from == 'procedure'):
			mysqli_next_result( $this->db->conn_id);
		endif;
		if($action == 'count'):	
			return $query->num_rows();
		elseif($action == 'single'):	
			if($query->num_rows() > 0):
				return $query->row_array();
			else:
				return false;
			endif;
		elseif($action == 'multiple'):	
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name: getFieldInArray
	** Developed By: Ashish UMrao
	** Purpose: This function used for get data by condition
	** Date : 09 AUGUSt 2018
	************************************************************************/
	public function getFieldInArray($field='',$query='')
	{  
		$returnarray			=	array();
		$query = $this->db->query($query);
		if($query->num_rows() > 0):
			$data	=	$query->result_array();
			foreach($data as $info):
				array_push($returnarray,trim($info[$field]));
			endforeach;
		endif;
		return $returnarray;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name: getTwoFieldsInArray
	** Developed By: Ashish UMrao
	** Purpose: This function used for get Two Fields In Array
	** Date : 22 OCTOBER 2018
	************************************************************************/
	public function getTwoFieldsInArray($firstField='',$secondField='',$query='')
	{  
		$returnarray			=	array();
		$query = $this->db->query($query);
		if($query->num_rows() > 0):
			$data	=	$query->result_array();
			foreach($data as $info):
				array_push($returnarray,array($firstField=>trim($info[$firstField]),$secondField=>trim($info[$secondField])));
			endforeach;
		endif;
		return $returnarray;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name: getParticularDataByFields
	** Developed By: Ashish UMrao
	** Purpose: This function used for get particular data by fields
	** Date : 09 AUGUSt 2018
	************************************************************************/
	public function getParticularDataByFields($selectField='',$tableName='',$fieldName='',$fieldValue='')
	{  
		$this->db->select($selectField);
		$this->db->from($tableName);	
		$this->db->where($fieldName,ucfirst(strtolower($fieldValue)));
		$this->db->or_where($fieldName,strtolower($fieldValue));
		$query = $this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name: getLastOrderByFields
	** Developed By: Ashish UMrao
	** Purpose: This function used for get Last Order By Fields
	** Date : 14 AUGUSt 2018
	************************************************************************/
	public function getLastOrderByFields($selectField='',$tableName='',$fieldName='',$fieldValue='')
	{  
		$this->db->select($selectField);
		$this->db->from($tableName);	
		if($fieldName && $fieldValue):
			$this->db->where($fieldName,$fieldValue);
		endif;
		$this->db->order_by($selectField.' DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0):
			$data 	=	$query->row_array();
			return $data[$selectField];
		else:
			return 0;
		endif;
	}	// END OF FUNCTION

	/* * *********************************************************************
	 * * Function name : setAttributeInUse
	 * * Developed By : Ashish UMrao
	 * * Purpose  : This function used for set Attribute In Use
	 * * Date : 20 AUGUSt 2018
	 * * **********************************************************************/
	function setAttributeInUse($tableName='',$param='',$fieldName='',$fieldValue='')
	{ 
		$paramarray[$param]	=	'Y';
		$this->db->where($fieldName,$fieldValue);
		$this->db->update($tableName,$paramarray);
		return true;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : increaseDecreaseCountByQuery
	** Developed By : Ashish UMrao
	** Purpose  : This function used for increase Decrease Count By Query
	** Date : 06 SEPTEMBER 2018
	************************************************************************/
	function increaseDecreaseCountByQuery($query='')
	{
		$this->db->query($query);
		return true;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name: getcustomerfeedbackdata
	** Developed By: Ashish UMrao
	** Purpose: This function used for get data by encryptId
	** Date : 30 APRIL 2019
	************************************************************************/
	public function getcustomerfeedbackdata($fieldValue='')
	{  //echo $fieldValue; die;
		$this->db->select('feed.*,u.user_name,u.user_email');
		$this->db->from('feedback as feed');	
		$this->db->join('users as u','feed.userPhone=u.user_phone','LEFT');
		$this->db->where('feed.id',$fieldValue);
		$query = $this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : package_status
	** Developed By : Ravi Kumar
	** Purpose  : This function used for package status
	** Date : 28 MAY 2019
	************************************************************************/
	function package_status(){
	$allrating = array('1'=>'1 Year','2'=>'2 Years','3'=>'3 Years','4'=>'4 Years','5'=>'5 Years');
	return $allrating;
	}
}	