<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Emailtemplate_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
		$this->load->database(); 
	}

	/***********************************************************************
	** Function name : getUserDataById
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get User Data By Id
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function getUserDataById($userId='')
	{
		$this->db->select('id,user_id,device_id,device_type,user_name,user_email,user_phone');
		$this->db->from('users');
		$this->db->where("user_id",$userId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getUserDataByPhone
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get User Data By Phone
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function getUserDataByPhone($userPhone='')
	{
		$this->db->select('id,user_id,device_id,device_type,user_name,user_email,user_phone');
		$this->db->from('users');
		$this->db->where("user_phone",$userPhone);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getSellerDataById
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Seller Data By Id
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function getSellerDataById($sellerId='')
	{
		$this->db->select('id,seller_id,device_id,device_type,seller_title,seller_name,seller_business_name,seller_phone,seller_email');
		$this->db->from('seller');
		$this->db->where("seller_id",$sellerId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getSellerDataByPhone
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Seller Data By Phone
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function getSellerDataByPhone($sellerPhone='')
	{
		$this->db->select('id,seller_id,device_id,device_type,seller_title,seller_name,seller_business_name,seller_phone,seller_email');
		$this->db->from('seller');
		$this->db->where("seller_phone",$sellerPhone);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getSellerIdByOrderData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Seller Id By Order Data
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function getSellerIdByOrderData($userId='',$sessionId='',$orderId='')
	{
		$this->db->select('seller_id');
		$this->db->from('orders');
		$this->db->where("user_id",$userId);
		if($sessionId):
			$this->db->where("session_id",$sessionId);
		endif;
		if($orderId):
			$this->db->where("order_id",$orderId);
		endif;
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			$data 	= $query->row_array();
			return $data['seller_id'];
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getUserIdByOrderData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get User Id By Order Data
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function getUserIdByOrderData($orderId='')
	{
		$this->db->select('user_id');
		$this->db->from('orders');
		$this->db->where("order_id",$orderId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			$data 	= $query->row_array();
			return $data['user_id'];
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getAdminData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Admin Data
	** Date : 24 NOVEMBER 2018
	************************************************************************/
	function getAdminData()
	{
		$this->db->select('admin_name,admin_display_name,admin_email_id,admin_mobile_number');
		$this->db->from('admin');
		$this->db->where("admin_id = '1'");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getEmailTemplateByMailType
	** Developed By : Manoj Kumar
	** Purpose  : This is for get Email Template By Mail Type
	** Date : 24 NOVEMBER 2018
	************************************************************************/
	function getEmailTemplateByMailType($type='') {  
		$this->db->select('from_email,to_email,bcc_email,subject,mail_header,mail_body,mail_footer,html');
		$this->db->from('email_template');
		$this->db->where('mailTypeData',$type);
		$query = $this->db->get();
		if($query->num_rows()>0):
			return $query->row_array();
		else:
			return false;
		endif;
	}

	/* * *********************************************************************
	 * * Function name : getOrderListData
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for get Order List Data
	 * * Date : 26 NOVEMBER 2018
	 * * **********************************************************************/
	public function getOrderListData($userId='',$sessionId='',$orderId='',$prodId='')
	{
		$this->db->select('ords.id, ords.session_id, ords.order_id, ords.quantity, ords.amount, ords.total_amount, ords.gst_amount, ords.installation_charges,
						   ords.shipping_name, ords.shipping_phone, ords.shipping_address1, ords.shipping_address2, ords.shipping_city, ords.shipping_state, ords.shipping_pincode, ords.shipping_add_type, 
						   ords.seller_accepted, ords.seller_remark, ords.seller_action_date, ords.order_status, ords.order_date, ords.order_return_request,
						   ords.est_ins_deliv_date, ords.packed_date, ords.shipped_date, ords.delivered_date, ords.canceled_date, ords.order_return_date,
						   prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   sell.seller_name, sell.seller_business_name, sell.seller_phone, sell.seller_email,
						   cate.prod_cate_slug, subcate.prod_sub_cate_slug');
		$this->db->from('orders as ords');
		$this->db->join("product as prod","ords.prod_id=prod.prod_id","LEFT");
		$this->db->join("product_details as prodet","prod.prod_id=prodet.prod_id","LEFT");
		$this->db->join("seller as sell","prod.seller_id=sell.seller_id","LEFT");
		$this->db->join("prod_cate as cate","prod.prod_cate_id=cate.prod_cate_id","LEFT");
		$this->db->join("prod_sub_cate as subcate","prod.prod_sub_cate_id=subcate.prod_sub_cate_id","LEFT");
		$this->db->where('ords.user_id',$userId);
		if($sessionId): $this->db->where('ords.session_id',$sessionId); endif;
		if($orderId): 	$this->db->where('ords.order_id',$orderId);		endif;
		if($prodId): 	$this->db->where('ords.prod_id',$prodId);		endif;
		$this->db->where("ords.payment_status = '1'");
		$this->db->where("prod.status = 'A'");
		$this->db->order_by("ords.id DESC, ords.order_id ASC, ords.session_id ASC");
		$query = $this->db->get();
		if($query->num_rows()>0):
			$ordData = $query->result_array();
			$i=0; $j=0; $k=0;$l=0;
			$sessionarray		=	array();
			$orderarray			=	array();
			foreach($ordData as $ordInfo):
				$changeJ		=	'N';
				if($l==0):
					array_push($sessionarray,$ordInfo['session_id']);
					array_push($orderarray,$ordInfo['order_id']);
				endif;
				if(!in_array($ordInfo['session_id'],$sessionarray)):	
					array_push($sessionarray,$ordInfo['session_id']);
					$i++; $j=0;  $k=0;
					$changeJ		=	'Y';
				endif;
				if(!in_array(trim($ordInfo['order_id']),$orderarray)):	
					array_push($orderarray,trim($ordInfo['order_id']));
					if($changeJ=='N'): $j++;  endif;  $k=0;   
				endif;	

				$prodImage 		=	'';
				$rData[$i]['sessionId']												=	$ordInfo['session_id']?$ordInfo['session_id']:'';
				$rData[$i]['sessionData'][$j]['orderId']							=	$ordInfo['order_id']?$ordInfo['order_id']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['id']				=	$ordInfo['id']?$ordInfo['id']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productId']			=	$ordInfo['prod_id']?$ordInfo['prod_id']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productImage']		=	$prodImage;
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productName']		=	$ordInfo['prod_name']?htmlspecialchars_decode(stripslashes($ordInfo['prod_name'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productSKU']		=	$ordInfo['prod_sku']?htmlspecialchars_decode(stripslashes($ordInfo['prod_sku'])):'';
				//$rData[$i]['sessionData'][$j]['orderData'][$k]['productDesc']		=	$ordInfo['prod_desc']?htmlspecialchars_decode(stripslashes($ordInfo['prod_desc'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productBrandName']	=	$ordInfo['prod_brand_name']?htmlspecialchars_decode(stripslashes($ordInfo['prod_brand_name'])):'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['productSlug']		=	$ordInfo['prod_slug']?htmlspecialchars_decode(stripslashes($ordInfo['prod_slug'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productCateSlug']	=	$ordInfo['prod_cate_slug']?htmlspecialchars_decode(stripslashes($ordInfo['prod_cate_slug'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productSubCateSlug']=	$ordInfo['prod_sub_cate_slug']?htmlspecialchars_decode(stripslashes($ordInfo['prod_sub_cate_slug'])):'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['productDate']					=	$ordInfo['order_date']?date('M, d Y',strtotime($ordInfo['order_date'])):'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['productOrderDate']				=	$ordInfo['order_date']!='1970-01-01 00:00:00'?$ordInfo['order_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productEstDeliveryDate']		=	$ordInfo['est_ins_deliv_date']!='1970-01-01 00:00:00'?$ordInfo['est_ins_deliv_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productPackedDate']				=	$ordInfo['packed_date']!='1970-01-01 00:00:00'?$ordInfo['packed_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productShippedDate']			=	$ordInfo['shipped_date']!='1970-01-01 00:00:00'?$ordInfo['shipped_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productDeliveredDate']			=	$ordInfo['delivered_date']!='1970-01-01 00:00:00'?$ordInfo['delivered_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productcancelDate']				=	$ordInfo['canceled_date']!='1970-01-01 00:00:00'?$ordInfo['canceled_date']:'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productRequestDate']			=	$ordInfo['order_return_date']!='1970-01-01 00:00:00'?$ordInfo['order_return_date']:'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['productStatus']					=	$ordInfo['order_status']?stripslashes($ordInfo['order_status']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productReturnRequest']			=	$ordInfo['order_return_request']?stripslashes($ordInfo['order_return_request']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productQuantity']				=	$ordInfo['quantity']?stripslashes($ordInfo['quantity']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productSingleAmount']			=	$ordInfo['amount']?stripslashes($ordInfo['amount']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productTotalAmount']			=	$ordInfo['total_amount']?stripslashes($ordInfo['total_amount']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productGSTAmount']				=	'18%';//$ordInfo['gst_amount']?stripslashes($ordInfo['gst_amount']):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['productInstallationCharges']	=	$ordInfo['installation_charges']?stripslashes($ordInfo['installation_charges']):'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingName']					=	$ordInfo['shipping_name']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_name'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingPhone']					=	$ordInfo['shipping_phone']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_phone'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingAddress1']				=	$ordInfo['shipping_address1']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_address1'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingAddress2']				=	$ordInfo['shipping_address2']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_address2'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingCity']					=	$ordInfo['shipping_city']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_city'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingState']					=	$ordInfo['shipping_state']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_state'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingPincode']				=	$ordInfo['shipping_pincode']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_pincode'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['shippingAddType']				=	$ordInfo['shipping_add_type']?htmlspecialchars_decode(stripslashes($ordInfo['shipping_add_type'])):'';

				$rData[$i]['sessionData'][$j]['orderData'][$k]['selerName']						=	$ordInfo['seller_name']?htmlspecialchars_decode(stripslashes($ordInfo['seller_name'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['sellerBusinessName']			=	$ordInfo['seller_business_name']?htmlspecialchars_decode(stripslashes($ordInfo['seller_business_name'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['sellerPhone']					=	$ordInfo['seller_phone']?htmlspecialchars_decode(stripslashes($ordInfo['seller_phone'])):'';
				$rData[$i]['sessionData'][$j]['orderData'][$k]['sellerEmail']					=	$ordInfo['seller_email']?htmlspecialchars_decode(stripslashes($ordInfo['seller_email'])):'';
				$k++; $l++;
			endforeach;  
			return $rData;
		else:
			return false;
		endif;
	}

	/***********************************************************************
	** Function name : getSuccessOrderData
	** Developed By : Manoj Kumar
	** Purpose  : This is for get get Success Order Data
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function getSuccessOrderData($userId='',$sessionId='') { 
		$returnData 	=	''; 
		$orderList 		=	$this->getOrderListData($userId,$sessionId,'',''); 	
		if($orderList <> ""):
			$returnData =   '<table width="100%" style="background:#d9d9d9;color: #000;border-collapse: collapse;margin-top:50px;">
                <tr>
                  <td style="border:2px solid #000;">
                    <table width="100%"  align="center" style="background:#d9d9d9;color: #000;border-collapse: collapse;text:center;">
                      <tr>
                        <td style="padding: 1%; text-align:center;">
                          <b style="font-weight: bold;">
                            <label style="display: inline-block;max-width: 100%;margin-bottom: 5px;">
                              RETAIL INVOICE
                            </label>
                          </b>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>';

			$OrderDate = $orderList[0]['sessionData'][0]['orderData'][0]['productOrderDate'];

			$returnData.=   '<table style="background: #fff;border-bottom:  2px solid #000;color: #000;border-collapse: collapse;" width="100%">
			                <tr>
			                  <td style="padding: 1%;border-left:2px solid #000;" width="50%">
			                    <b style="font-weight: bold;">ORDER # :</b> '.stripslashes($orderId).'
			                  </td>
			                  <td style="padding: 1%;border-left:2px solid #000;border-right:2px solid #000;" width="50%">
			                    <b style="font-weight: bold;">DATE :</b> '.date('D M, d Y',strtotime($OrderDate)).'
			                  </td>
			                </tr>
			              </table>';

			$orderShipData   = $orderList[0]['sessionData'][0]['orderData'][0];

			$returnData.=   '<table style="background-color: transparent;border-collapse: collapse;border-bottom: 2px solid #000;" width="100%">
			                <tr style="background: #fff;color: #000;border-bottom:1px solid transparent;">
			                  <td style="padding: 1%;border-left:2px solid #000;vertical-align: text-top;" width="50%">
			                    <b style="font-weight: bold;">Customer Details:</b><br>'.ucfirst(stripslashes($orderShipData['shippingName'])).'
			                    <br>'.ucfirst(stripslashes($orderShipData['shippingPhone'])).'
			                  </td>
			                  <td style="padding: 1%;border-left:2px solid #000;border-right:2px solid #000;vertical-align: text-top;" width="50%">
			                    <b style="font-weight: bold;">Customer Address:</b><br>'.stripslashes($orderShipData['shippingAddress1']).'
			                    <br>'.stripslashes($orderShipData['shippingAddress2']).'
			                    <br>'.stripslashes($orderShipData['shippingCity']).', 
			                    '.stripslashes($orderShipData['shippingState']).', 
			                    '.stripslashes($orderShipData['shippingPincode']).'
			                    <br>Address Type: '.stripslashes($orderShipData['shippingAddType']).'
			                  </td>
			                </tr>
			              </table>';
			       
			$returnData.=   '<table style="width:100%;background-color: transparent;border-collapse: collapse;">
			                <thead>
			                  <tr style="background: #fff;">
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:left;" width="50%">Items</th>
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;" width="18%">Price <img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"></th>
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;" width="14%">Qty</th>
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-right:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:right;" width="18%">Total <img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"></th>
			                  </tr>
			                </thead>
			                <tbody>';  


			if($orderList <> ""):  $stAmount = 0; $otAmount = 0;
			        foreach($orderList as $orderListData): 
			          foreach($orderListData['sessionData'] as $sessionData):
			            foreach($sessionData['orderData'] as $orderData):
			              $sAmount    =   $orderData['productSingleAmount'];
			              $sQuantity  =   $orderData['productQuantity'];
			              $tAmount    =   ($sAmount*$sQuantity);
			              $stAmount   =   ($stAmount+$tAmount);
			              $otAmount   =   ($otAmount+$orderData['productTotalAmount']);
			     
			    $returnData.=   '<tr style="background: #fff;">
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:left;">
			                      <span>'.stripslashes($orderData['productName']).'<br>
			                      '.stripslashes($orderData['productBrandName']).'</span>
			                    </td>
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;">
			                      '.displayPrice($sAmount).'
			                    </td>
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;">
			                      '.$sQuantity.'
			                    </td>
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;border-right:2px solid #000;color: #000;text-align:right;">
			                      '.displayPrice($tAmount).'
			                    </td>
			                  </tr>';
			     endforeach; endforeach; endforeach; endif; 
			$returnData.=     '</tbody>
			              </table>';

			$installationCharges = $orderList[0]['sessionData'][0]['orderData'][0]['productInstallationCharges'];
			$orderTotal          =  ($otAmount+$installationCharges);

			$returnData.=     '<table style="background-color:  #fff;border-collapse: collapse;" width="100%">
			                  <tbody>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Sub Total </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;"><img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"> '.displayPrice($stAmount).'</b>
			                      </td>
			                    </tr>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Installation </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;"><img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"> '.displayPrice($installationCharges).'</b>
			                      </td>
			                    </tr>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Tax (GST) </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;">18%</b>
			                      </td>
			                    </tr>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Order Total </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;"><img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"> '.displayPrice($orderTotal).'</b>
			                      </td>
			                    </tr>
			                  </tbody>
			                </table>'; 
		endif;
		return $returnData;
	}

	/***********************************************************************
	** Function name : getCancelOrderData
	** Developed By : Manoj Kumar
	** Purpose  : This is for get get Cancel Order Data
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function getCancelOrderData($userId='',$orderId='') {  
		$returnData 	=	''; 
		$orderList 		=	$this->getOrderListData($userId,'',$orderId,'');
		if($orderList <> ""):
			$returnData =   '<table width="100%" style="background:#d9d9d9;color: #000;border-collapse: collapse;margin-top:50px;">
                <tr>
                  <td style="border:2px solid #000;">
                    <table width="100%"  align="center" style="background:#d9d9d9;color: #000;border-collapse: collapse;text:center;">
                      <tr>
                        <td style="padding: 1%; text-align:center;">
                          <b style="font-weight: bold;">
                            <label style="display: inline-block;max-width: 100%;margin-bottom: 5px;">
                              RETAIL INVOICE
                            </label>
                          </b>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>';

			$OrderDate = $orderList[0]['sessionData'][0]['orderData'][0]['productOrderDate'];

			$returnData.=   '<table style="background: #fff;border-bottom:  2px solid #000;color: #000;border-collapse: collapse;" width="100%">
			                <tr>
			                  <td style="padding: 1%;border-left:2px solid #000;" width="50%">
			                    <b style="font-weight: bold;">ORDER # :</b> '.stripslashes($orderId).'
			                  </td>
			                  <td style="padding: 1%;border-left:2px solid #000;border-right:2px solid #000;" width="50%">
			                    <b style="font-weight: bold;">DATE :</b> '.date('D M, d Y',strtotime($OrderDate)).'
			                  </td>
			                </tr>
			              </table>';

			$orderShipData   = $orderList[0]['sessionData'][0]['orderData'][0];

			$returnData.=   '<table style="background-color: transparent;border-collapse: collapse;border-bottom: 2px solid #000;" width="100%">
			                <tr style="background: #fff;color: #000;border-bottom:1px solid transparent;">
			                  <td style="padding: 1%;border-left:2px solid #000;vertical-align: text-top;" width="50%">
			                    <b style="font-weight: bold;">Customer Details:</b><br>'.ucfirst(stripslashes($orderShipData['shippingName'])).'
			                    <br>'.ucfirst(stripslashes($orderShipData['shippingPhone'])).'
			                  </td>
			                  <td style="padding: 1%;border-left:2px solid #000;border-right:2px solid #000;vertical-align: text-top;" width="50%">
			                    <b style="font-weight: bold;">Customer Address:</b><br>'.stripslashes($orderShipData['shippingAddress1']).'
			                    <br>'.stripslashes($orderShipData['shippingAddress2']).'
			                    <br>'.stripslashes($orderShipData['shippingCity']).', 
			                    '.stripslashes($orderShipData['shippingState']).', 
			                    '.stripslashes($orderShipData['shippingPincode']).'
			                    <br>Address Type: '.stripslashes($orderShipData['shippingAddType']).'
			                  </td>
			                </tr>
			              </table>';
			       
			$returnData.=   '<table style="width:100%;background-color: transparent;border-collapse: collapse;">
			                <thead>
			                  <tr style="background: #fff;">
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:left;" width="50%">Items</th>
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;" width="18%">Price <img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"></th>
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;" width="14%">Qty</th>
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-right:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:right;" width="18%">Total <img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"></th>
			                  </tr>
			                </thead>
			                <tbody>';  


			if($orderList <> ""):  $stAmount = 0; $otAmount = 0;
			        foreach($orderList as $orderListData): 
			          foreach($orderListData['sessionData'] as $sessionData):
			            foreach($sessionData['orderData'] as $orderData):
			              $sAmount    =   $orderData['productSingleAmount'];
			              $sQuantity  =   $orderData['productQuantity'];
			              $tAmount    =   ($sAmount*$sQuantity);
			              $stAmount   =   ($stAmount+$tAmount);
			              $otAmount   =   ($otAmount+$orderData['productTotalAmount']);
			     
			    $returnData.=   '<tr style="background: #fff;">
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:left;">
			                      <span>'.stripslashes($orderData['productName']).'<br>
			                      '.stripslashes($orderData['productBrandName']).'</span>
			                    </td>
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;">
			                      '.displayPrice($sAmount).'
			                    </td>
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;">
			                      '.$sQuantity.'
			                    </td>
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;border-right:2px solid #000;color: #000;text-align:right;">
			                      '.displayPrice($tAmount).'
			                    </td>
			                  </tr>';
			     endforeach; endforeach; endforeach; endif; 
			$returnData.=     '</tbody>
			              </table>';

			$installationCharges = $orderList[0]['sessionData'][0]['orderData'][0]['productInstallationCharges'];
			$orderTotal          =  ($otAmount+$installationCharges);

			$returnData.=     '<table style="background-color:  #fff;border-collapse: collapse;" width="100%">
			                  <tbody>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Sub Total </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;"><img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"> '.displayPrice($stAmount).'</b>
			                      </td>
			                    </tr>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Installation </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;"><img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"> '.displayPrice($installationCharges).'</b>
			                      </td>
			                    </tr>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Tax (GST) </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;">18%</b>
			                      </td>
			                    </tr>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Order Total </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;"><img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"> '.displayPrice($orderTotal).'</b>
			                      </td>
			                    </tr>
			                  </tbody>
			                </table>'; 
		endif;
		return $returnData;
	}

	/***********************************************************************
	** Function name : getReturnRequestData
	** Developed By : Manoj Kumar
	** Purpose  : This is for get get Return Request Data
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function getReturnRequestData($userId='',$orderId='',$prodId='') {  
		$returnData 	=	''; 
		$orderList 		=	$this->getOrderListData($userId,'',$orderId,$prodId);
		if($orderList <> ""):
			$returnData =   '<table width="100%" style="background:#d9d9d9;color: #000;border-collapse: collapse;margin-top:50px;">
                <tr>
                  <td style="border:2px solid #000;">
                    <table width="100%"  align="center" style="background:#d9d9d9;color: #000;border-collapse: collapse;text:center;">
                      <tr>
                        <td style="padding: 1%; text-align:center;">
                          <b style="font-weight: bold;">
                            <label style="display: inline-block;max-width: 100%;margin-bottom: 5px;">
                              RETAIL INVOICE
                            </label>
                          </b>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>';

			$OrderDate = $orderList[0]['sessionData'][0]['orderData'][0]['productOrderDate'];

			$returnData.=   '<table style="background: #fff;border-bottom:  2px solid #000;color: #000;border-collapse: collapse;" width="100%">
			                <tr>
			                  <td style="padding: 1%;border-left:2px solid #000;" width="50%">
			                    <b style="font-weight: bold;">ORDER # :</b> '.stripslashes($orderId).'
			                  </td>
			                  <td style="padding: 1%;border-left:2px solid #000;border-right:2px solid #000;" width="50%">
			                    <b style="font-weight: bold;">DATE :</b> '.date('D M, d Y',strtotime($OrderDate)).'
			                  </td>
			                </tr>
			              </table>';

			$orderShipData   = $orderList[0]['sessionData'][0]['orderData'][0];

			$returnData.=   '<table style="background-color: transparent;border-collapse: collapse;border-bottom: 2px solid #000;" width="100%">
			                <tr style="background: #fff;color: #000;border-bottom:1px solid transparent;">
			                  <td style="padding: 1%;border-left:2px solid #000;vertical-align: text-top;" width="50%">
			                    <b style="font-weight: bold;">Customer Details:</b><br>'.ucfirst(stripslashes($orderShipData['shippingName'])).'
			                    <br>'.ucfirst(stripslashes($orderShipData['shippingPhone'])).'
			                  </td>
			                  <td style="padding: 1%;border-left:2px solid #000;border-right:2px solid #000;vertical-align: text-top;" width="50%">
			                    <b style="font-weight: bold;">Customer Address:</b><br>'.stripslashes($orderShipData['shippingAddress1']).'
			                    <br>'.stripslashes($orderShipData['shippingAddress2']).'
			                    <br>'.stripslashes($orderShipData['shippingCity']).', 
			                    '.stripslashes($orderShipData['shippingState']).', 
			                    '.stripslashes($orderShipData['shippingPincode']).'
			                    <br>Address Type: '.stripslashes($orderShipData['shippingAddType']).'
			                  </td>
			                </tr>
			              </table>';
			       
			$returnData.=   '<table style="width:100%;background-color: transparent;border-collapse: collapse;">
			                <thead>
			                  <tr style="background: #fff;">
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:left;" width="50%">Items</th>
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;" width="18%">Price <img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"></th>
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;" width="14%">Qty</th>
			                    <th style="padding: 1%;background: #d9d9d9;border-left:2px solid #000;border-right:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:right;" width="18%">Total <img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"></th>
			                  </tr>
			                </thead>
			                <tbody>';  


			if($orderList <> ""):  $stAmount = 0; $otAmount = 0;
			        foreach($orderList as $orderListData): 
			          foreach($orderListData['sessionData'] as $sessionData):
			            foreach($sessionData['orderData'] as $orderData):
			              $sAmount    =   $orderData['productSingleAmount'];
			              $sQuantity  =   $orderData['productQuantity'];
			              $tAmount    =   ($sAmount*$sQuantity);
			              $stAmount   =   ($stAmount+$tAmount);
			              $otAmount   =   ($otAmount+$orderData['productTotalAmount']);
			     
			    $returnData.=   '<tr style="background: #fff;">
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:left;">
			                      <span>'.stripslashes($orderData['productName']).'<br>
			                      '.stripslashes($orderData['productBrandName']).'</span>
			                    </td>
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;">
			                      '.displayPrice($sAmount).'
			                    </td>
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;color: #000;text-align:center;">
			                      '.$sQuantity.'
			                    </td>
			                    <td style="padding: 1%;border-left:2px solid #000;border-bottom:  2px solid #000;border-right:2px solid #000;color: #000;text-align:right;">
			                      '.displayPrice($tAmount).'
			                    </td>
			                  </tr>';
			     endforeach; endforeach; endforeach; endif; 
			$returnData.=     '</tbody>
			              </table>';

			$installationCharges = $orderList[0]['sessionData'][0]['orderData'][0]['productInstallationCharges'];
			$orderTotal          =  ($otAmount+$installationCharges);

			$returnData.=     '<table style="background-color:  #fff;border-collapse: collapse;" width="100%">
			                  <tbody>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Sub Total </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;"><img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"> '.displayPrice($stAmount).'</b>
			                      </td>
			                    </tr>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Installation </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;"><img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"> '.displayPrice($installationCharges).'</b>
			                      </td>
			                    </tr>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Tax (GST) </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;">18%</b>
			                      </td>
			                    </tr>
			                    <tr style="background: #fff;">
			                      <td style="padding: 1%;border:1px solid #fff;" width="50%">&nbsp;</td>
			                      <td style="padding: 1%;text-align:center;border-left:2px solid #000;border-bottom:2px solid #000;" width="32%">
			                        <b style="font-weight: bold;font-size: 13px;">Order Total </b>
			                      </td>
			                      <td style="padding:1%;text-align:right;border-left:2px solid #000;border-bottom:2px solid #000;border-right:2px solid  #000;" width="18%">
			                        <b style="font-weight: bold;font-size: 13px;"><img src="'.base_url().'/assets/seller/images/nGbfO.png" width="8" height="10"> '.displayPrice($orderTotal).'</b>
			                      </td>
			                    </tr>
			                  </tbody>
			                </table>'; 
		endif;
		return $returnData;
	}

	/***********************************************************************
	** Function name : getChangeOrderStatusData
	** Developed By : Manoj Kumar
	** Purpose  : This is for get get Change Order Status Data
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function getChangeOrderStatusData($orderId='') {  
		$returnData 		=	'';
		$this->db->select('order_id,order_date,order_status,est_ins_deliv_date,
			               shipping_name,shipping_phone,shipping_address1,shipping_address2,
			               shipping_city,shipping_state,shipping_pincode');
		$this->db->from('orders');
		$this->db->where('order_id',$orderId);
		$query = $this->db->get();
		if($query->num_rows()>0):
			$data 		=	$query->row_array();
			$address2   =	$data['shipping_address2']?stripslashes($data['shipping_address2']).', ':'';
			if($data['order_status'] == 'Packed'):
				$imgUrl =	base_url().'assets/emailers/packed.png';
			elseif($data['order_status'] == 'Shipped'):
				$imgUrl =	base_url().'assets/emailers/shipped.png';
			elseif($data['order_status'] == 'Delivered'):
				$imgUrl =	base_url().'assets/emailers/deleverd.png';
			else:
				$imgUrl =	'';
			endif;
			$returnData =	'<table border="0" cellpadding="0" cellspacing="0" style="width:640px;max-width:640px;padding-right:20px;padding-left:20px;background-color:#fff;padding-top:20px"> 
					           <tbody>
					            <tr> 
					             <td  align="left"> 
					              <table width="350" border="0" cellpadding="0" cellspacing="0" align="left"> 
					               <tbody>
					                <tr> 
					                 <td valign="top"><p style="font-family:Arial;color:#747474;font-size:11px;font-weight:normal;text-align: left;font-style:normal;line-height:12px;font-stretch:normal;margin-bottom:7px;padding-top:0px;color:#878787;margin-top:0;">Order ID <span style="font-weight:bold;color:#000">'.stripslashes($data['order_id']).'</span></p></td> 
					                </tr> 
					               </tbody>
					              </table> 
					              <table width="250" border="0" cellpadding="0" cellspacing="0" align="right"> 
					               <tbody>
					                <tr> 
					                 <td valign="top">  <p style="font-family:Arial;font-size:11px;color:#878787;line-height:1.22;text-align:right;padding-top:0px;margin-top:0;margin-bottom:7px">Order placed on <span style="font-weight:bold;color:#000">'.date('D M, d Y',strtotime($data['order_date'])).'</span></p> </td> 
					                </tr> 
					               </tbody>
					              </table> </td> 
					            </tr> 
					            <tr> 
					             <td border="1" align="left" style="background-color:rgba(245,245,245,0.5);background:rgba(245,245,245,0.5);border:.5px solid #6ed49e;border-radius:2px;padding-top:20px;border-color:#6ed49e;border-width:.08em;border-style:solid;border:.08em solid #6ed49e;padding-right:15px;padding-left:15px;padding-bottom:20px"> 
					              <table width="330" border="0" cellpadding="0" cellspacing="0" align="left"> 
					               <tbody>
					                <tr> 
					                 <td valign="top"> 
					                  <img src="'.$imgUrl.'" alt="journey" style="padding-top:3px">  
					                  <p style="font-family:Arial;font-size:12px;line-height:1.42;color:#212121;margin-top:20px;margin-bottom:20px">Estimated installation/delivery Date on <br>
					                   <span style="font-family:Arial;font-size:12px;font-weight:bold;line-height:1.42;color:#139b3b;padding-left:30px">'.date('D M, d Y',strtotime($data['est_ins_deliv_date'])).'</span>
					                   </p>   
					                  </td> 
					                </tr> 
					               </tbody>
					              </table> 
					              <table width="235" border="0" cellpadding="0" cellspacing="0" align="right"> 
					               <tbody>
					                <tr> 
					                 <td valign="top" align="left"> 
					                  <div style="max-width:220px;margin-top:0px;padding-top:0px;margin-bottom:20px"> 
					                   <p style="font-family:Arial;font-size:12px;line-height:1.52;color:#212121;margin-top:0px;padding-top:0px;margin-bottom:0px">
					                    <span style="font-family:Arial;font-size:14px;font-weight:bold;line-height:20px;color:#212121;display:block;margin-bottom:2px">Delivery Address</span> 
					                    '.stripslashes($data['shipping_name']).' ('.stripslashes($data['shipping_phone']).') <br>
					                    <span style="font-family:Arial;font-size:12px;line-height:1.42;color:#212121">
					                    '.stripslashes($data['shipping_address1']).', '.$address2.''.stripslashes($data['shipping_city']).',
					                    '.stripslashes($data['shipping_state']).', '.stripslashes($data['shipping_pincode']).'
					                    </span> 
					                    </p> 
					                  </div>  
					                 </td> 
					                </tr> 
					               </tbody>
					              </table> </td> 
					            </tr> 
					           </tbody>
					          </table>';
		endif;
		return $returnData;
	}
	
///////////////////////////////////////////////////////////////////////////////
/////////////////////////////		ADMIN SECTION 		///////////////////////
///////////////////////////////////////////////////////////////////////////////	
	/***********************************************************************
	** Function name : sendPasswordRecoverMail
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Password Recover Mail
	** Date : 24 NOVEMBER 2018
	************************************************************************/
	function sendPasswordRecoverMail($result=array(),$param=array())
	{ 
		if(!empty($result['admin_email_id']) && !empty($param['admin_password_otp'])):
			$MTData			=	$this->getEmailTemplateByMailType('AdminForgotPasswordMailToAdmin'); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$result['admin_email_id'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{USERNAME}', $result['admin_name'], $MBData);
			$MBData 		= 	str_replace('{USEROTP}', $param['admin_password_otp'], $MBData);
			$MBData 		= 	str_replace('{OTPPAGELINK}', $sitefullurl.'password-recover', $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : UserRegistrationMailToAdmin
	** Developed By : Manoj Kumar
	** Purpose  : This function used for User Registration Mail To Admin
	** Date : 24 NOVEMBER 2018
	************************************************************************/
	function UserRegistrationMailToAdmin($userId='')
	{ 
		if(!empty($userId)):
			$MTData			=	$this->getEmailTemplateByMailType('UserResignationMailToAdmin');
			$AdminData		=	$this->getAdminData();  
			$UserData		=	$this->getUserDataById($userId);  

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$AdminData['admin_email_id'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{ADMINNAME}', $AdminData['admin_name'], $MBData);
			$MBData 		= 	str_replace('{USERNAME}', $UserData['user_name'], $MBData);
			$MBData 		= 	str_replace('{USERPHONE}', $UserData['user_phone'], $MBData);
			$MBData 		= 	str_replace('{USEREMAIL}', $UserData['user_email'], $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : BecomeASellerMailToAdmin
	** Developed By : Manoj Kumar
	** Purpose  : This function used for Become A Seller Mail To Admin
	** Date : 24 NOVEMBER 2018
	************************************************************************/
	function BecomeASellerMailToAdmin($sellerData=array())
	{ 
		if(!empty($sellerData['sellerName']) && !empty($sellerData['sellerEmail'])):
			$MTData			=	$this->getEmailTemplateByMailType('BecomeaSellerMailToAdmin');
			$AdminData		=	$this->getAdminData();   

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$AdminData['admin_email_id'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{ADMINNAME}', $AdminData['admin_name'], $MBData);
			$MBData 		= 	str_replace('{USERNAME}', $sellerData['sellerName'], $MBData);
			$MBData 		= 	str_replace('{USERPHONE}', $sellerData['sellerPhone'], $MBData);
			$MBData 		= 	str_replace('{USEREMAIL}', $sellerData['sellerEmail'], $MBData);
			$MBData 		= 	str_replace('{USERCITY}', $sellerData['sellerCity'], $MBData);
			$MBData 		= 	str_replace('{USERPINCODE}', $sellerData['sellerPincode'], $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : ContactUsMailToAdmin
	** Developed By : Manoj Kumar
	** Purpose  : This function used for Contact Us Mail To Admin
	** Date : 24 NOVEMBER 2018
	************************************************************************/
	function ContactUsMailToAdmin($userData=array())
	{ 
		if(!empty($userData['contact_name']) && !empty($userData['contact_email'])):
			$MTData			=	$this->getEmailTemplateByMailType('ContactusMailToAdmin');
			$AdminData		=	$this->getAdminData();   

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$AdminData['admin_email_id'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{ADMINNAME}', $AdminData['admin_name'], $MBData);
			$MBData 		= 	str_replace('{USERNAME}', $sellerdata['contact_name'], $MBData);
			$MBData 		= 	str_replace('{USERPHONE}', $sellerdata['contact_phone'], $MBData);
			$MBData 		= 	str_replace('{USEREMAIL}', $sellerdata['contact_email'], $MBData);
			$MBData 		= 	str_replace('{USERSUBJECT}', $sellerdata['contact_reason'], $MBData);
			$MBData 		= 	str_replace('{USERMESSAGE}', $sellerdata['contact_message'], $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : sendPaymentSuccessMailToAdmin
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Payment Success Mail To Admin
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendPaymentSuccessMailToAdmin($userId='',$sessionId='')
	{ 
		if(!empty($userId) && !empty($sessionId)):
			$MTData			=	$this->getEmailTemplateByMailType('OrderMailToAdmin');
			$AdminData		=	$this->getAdminData(); 

			$orderData		=	$this->getSuccessOrderData($userId,$sessionId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$AdminData['admin_email_id'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{ADMINNAME}', $AdminData['admin_name'], $MBData);
			$MBData 		= 	str_replace('{NEWORDERDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : sendCancelOrderMailToAdmin
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Cancel Order Mail To Admin
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendCancelOrderMailToAdmin($userId='',$orderId='')
	{ 
		if(!empty($userId) && !empty($orderId)):
			$MTData			=	$this->getEmailTemplateByMailType('OrderCancelMailToAdmin');
			$AdminData		=	$this->getAdminData(); 

			$orderData		=	$this->getCancelOrderData($userId,$orderId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$AdminData['admin_email_id'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{ADMINNAME}', $AdminData['admin_name'], $MBData);
			$MBData 		= 	str_replace('{CANCELEDORDERDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : sendReturnRequestMailToAdmin
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Return Request Mail To Admin
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendReturnRequestMailToAdmin($userId='',$orderId='',$prodId='')
	{ 
		if(!empty($userId) && !empty($orderId) && !empty($prodId)):
			$MTData			=	$this->getEmailTemplateByMailType('ProductReturnRequestMailToAdmin');
			$AdminData		=	$this->getAdminData(); 

			$orderData		=	$this->getReturnRequestData($userId,$orderId,$prodId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$AdminData['admin_email_id'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{ADMINNAME}', $AdminData['admin_name'], $MBData);
			$MBData 		= 	str_replace('{REFUNDPRODUCTDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION


///////////////////////////////////////////////////////////////////////////////
/////////////////////////////		SELLER SECTION 		///////////////////////
///////////////////////////////////////////////////////////////////////////////	

	/***********************************************************************
	** Function name : sendPaymentSuccessMailToSeller
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Payment Success Mail To Seller
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendPaymentSuccessMailToSeller($userId='',$sessionId='')
	{ 
		if(!empty($userId) && !empty($sessionId)):
			$MTData			=	$this->getEmailTemplateByMailType('OrderMailToSeller');
			$sellerId 		=	$this->getSellerIdByOrderData($userId,$sessionId,'');
			$sellerData		=	$this->getSellerDataById($sellerId);  

			$orderData		=	$this->getSuccessOrderData($userId,$sessionId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$sellerData['seller_email'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{SELLERNAME}', $sellerData['seller_name'], $MBData);
			$MBData 		= 	str_replace('{NEWORDERDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : sendCancelOrderMailToSeller
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Cancel Order Mail To Seller
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendCancelOrderMailToSeller($userId='',$orderId='')
	{ 
		if(!empty($userId) && !empty($orderId)):
			$MTData			=	$this->getEmailTemplateByMailType('OrderCancelMailToSeller');
			$sellerId 		=	$this->getSellerIdByOrderData($userId,'',$orderId);
			$sellerData		=	$this->getSellerDataById($sellerId);   

			$orderData		=	$this->getCancelOrderData($userId,$orderId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$sellerData['seller_email'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{SELLERNAME}', $sellerData['seller_name'], $MBData);
			$MBData 		= 	str_replace('{CANCELEDORDERDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : sendReturnRequestMailToSeller
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Return Request Mail To Seller
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendReturnRequestMailToSeller($userId='',$orderId='',$prodId='')
	{ 
		if(!empty($userId) && !empty($orderId) && !empty($prodId)):
			$MTData			=	$this->getEmailTemplateByMailType('ProductReturnRequestMailToSeller');
			$sellerId 		=	$this->getSellerIdByOrderData($userId,'',$orderId);
			$sellerData		=	$this->getSellerDataById($sellerId);

			$orderData		=	$this->getReturnRequestData($userId,$orderId,$prodId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$sellerData['seller_email'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{SELLERNAME}', $sellerData['seller_name'], $MBData);
			$MBData 		= 	str_replace('{REFUNDPRODUCTDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION


///////////////////////////////////////////////////////////////////////////////
/////////////////////////////		CUSTOMER SECTION 	///////////////////////
///////////////////////////////////////////////////////////////////////////////		
	/***********************************************************************
	** Function name : UserRegistrationMailToUser
	** Developed By : Manoj Kumar
	** Purpose  : This function used for User Registration Mail To User
	** Date : 24 NOVEMBER 2018
	************************************************************************/
	function UserRegistrationMailToUser($userId='',$userPassword='')
	{ 
		if(!empty($userId) && !empty($userPassword)):
			$MTData			=	$this->getEmailTemplateByMailType('UserResignationMailToUser');
			$UserData		=	$this->getUserDataById($userId);  

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$UserData['user_email'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{USERNAME}', $UserData['user_name'], $MBData);
			$MBData 		= 	str_replace('{USERPHONE}', $UserData['user_phone'], $MBData);
			$MBData 		= 	str_replace('{USERPASSWORD}', $userPassword, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : sendPaymentSuccessMailToUser
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Payment Success Mail To User
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendPaymentSuccessMailToUser($userId='',$sessionId='')
	{ 
		if(!empty($userId) && !empty($sessionId)):
			$MTData			=	$this->getEmailTemplateByMailType('OrderMailToUser');
			$userData		=	$this->getUserDataById($userId);  

			$orderData		=	$this->getSuccessOrderData($userId,$sessionId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$userData['user_email'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{USERNAME}', $userData['user_name'], $MBData);
			$MBData 		= 	str_replace('{NEWORDERDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : sendCancelOrderMailToUser
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Cancel Order Mail To User
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendCancelOrderMailToUser($userId='',$orderId='')
	{ 
		if(!empty($userId) && !empty($orderId)):
			$MTData			=	$this->getEmailTemplateByMailType('OrderCancelMailToUser');
			$userData		=	$this->getUserDataById($userId);  

			$orderData		=	$this->getCancelOrderData($userId,$orderId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$userData['user_email'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{USERNAME}', $userData['user_name'], $MBData);
			$MBData 		= 	str_replace('{CANCELEDORDERDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : sendReturnRequestMailToUser
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Return Request Mail To User
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendReturnRequestMailToUser($userId='',$orderId='',$prodId='')
	{ 
		if(!empty($userId) && !empty($orderId) && !empty($prodId)):
			$MTData			=	$this->getEmailTemplateByMailType('ProductReturnRequestMailToUser');
			$userData		=	$this->getUserDataById($userId);  

			$orderData		=	$this->getReturnRequestData($userId,$orderId,$prodId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$userData['user_email'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{USERNAME}', $userData['user_name'], $MBData);
			$MBData 		= 	str_replace('{REFUNDPRODUCTDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : sendOrderStatusChangeMailToUser
	** Developed By : Manoj Kumar
	** Purpose  : This function used for send Order Status Change Mail To User
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function sendOrderStatusChangeMailToUser($orderId='')
	{ 
		if(!empty($orderId)):
			$MTData			=	$this->getEmailTemplateByMailType('SellerChangeOrderStatusMailToUser');
			$userId 		=	$this->getUserIdByOrderData($orderId);
			$userData		=	$this->getUserDataById($userId);  

			$orderData		=	$this->getChangeOrderStatusData($orderId); 

			$fromMail  		= 	MAIL_FROM_MAIL;
			$siteFullName	=	MAIL_SITE_FULL_NAME;
			$toMail  		= 	$userData['user_email'];
			$subject 		= 	$MTData['subject'];
			$sitefullurl	= 	base_url();
			
			#.............................. message section ............................#
			$MHData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_header']));
			
			$MBData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_body']));
			$MBData 		= 	str_replace('{USERNAME}', $userData['user_name'], $MBData);
			$MBData 		= 	str_replace('{ORDERSTATUSDATA}', $orderData, $MBData);

			$MFData 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['mail_footer']));

			$MHtml	 		= 	str_replace('../../../../', $sitefullurl, stripslashes($MTData['html']));
			$MHtml	 		= 	str_replace('{MAILERHEAD}', $MHData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERBODY}', $MBData, $MHtml);
			$MHtml	 		= 	str_replace('{MAILERFOOTER}', $MFData, $MHtml);

			//echo $MHtml; die;
			$this->email->from($fromMail,$siteFullName);
			$this->email->to($toMail); 
			if($MTData['bcc_email']):
				$this->email->bcc($MTData['bcc_email']);
			endif;
			$this->email->subject($subject);
			$this->email->set_mailtype('html');
			$this->email->message($MHtml);	
			$this->email->send();
			//echo $this->email->print_debugger(); die;
		endif;
	}	// END OF FUNCTION
}	