<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
		$this->load->database(); 
	}

	/***********************************************************************
	** Function name : getUserDataById
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get User Data By Id
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function getUserDataById($userId='')
	{
		$this->db->select('id,user_id,device_id,device_type,user_name,user_email,user_phone');
		$this->db->from('users');
		$this->db->where("user_id",$userId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getUserDataByPhone
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get User Data By Phone
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function getUserDataByPhone($userPhone='')
	{
		$this->db->select('id,user_id,device_id,device_type,user_name,user_email,user_phone');
		$this->db->from('users');
		$this->db->where("user_phone",$userPhone);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getSellerDataById
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Seller Data By Id
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function getSellerDataById($sellerId='')
	{
		$this->db->select('id,seller_id,device_id,device_type,seller_phone,seller_business_name');
		$this->db->from('seller');
		$this->db->where("seller_id",$sellerId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getSellerDataByPhone
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Seller Data By Phone
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function getSellerDataByPhone($sellerPhone='')
	{
		$this->db->select('id,seller_id,device_id,device_type,seller_phone,seller_business_name');
		$this->db->from('seller');
		$this->db->where("seller_phone",$sellerPhone);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	#########################################################################################################################
	################################################		USER SECTION 		#############################################
	#########################################################################################################################

	/* * *********************************************************************
	 * * Function name : sendNotificationToUserFunction 
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function use for send Notification To User Function
	 * * Date : 01 NOVEMBER 2018
	 * * **********************************************************************/
	public function sendNotificationToUserFunction($registrationIds='',$message='',$data=array(),$deviceType='') {
		if(!empty($registrationIds) && !empty($message) && !empty($data) && !empty($deviceType)):
			
			$fields 		= 	array('to'=>$registrationIds,'notification'=>$message,'data'=>$data);
			if($deviceType == 'Andriod'):
				$headers 	= 	array('Authorization: key='.SHELLIOS_USER_ANDRIOD_KEY,'Content-Type:application/json');
			elseif($deviceType == 'IOS'):
				$headers 	= 	array('Authorization: key='.SHELLIOS_USER_IOS_KEY,'Content-Type:application/json');
			endif;
			#Send Reponse To FireBase Server	
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL,'https://fcm.googleapis.com/fcm/send');
			curl_setopt( $ch,CURLOPT_POST,true);
			curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);
			#Echo Result Of FireBase Server
			return $result;
		endif;
	}	// END OF FUNCTION	

	/***********************************************************************
	** Function name : sendPaymentSuccessNotificationToUser
	** Developed By : Manoj Kumar
	** Purpose  : This is use for send Payment Success Notification To User
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function sendPaymentSuccessNotificationToUser($mobileNumber='',$orderId='') {  
		if($mobileNumber && $orderId):
			$userData 		=	$this->getUserDataByPhone($mobileNumber);
			$message 		= 	array('body'=>'Your purchase is successful. Thanks Cartamaam Team',
						 	 		  'title'=>'Product purchage',
             	         	 		  'icon'=>'myicon',
              	         	 		  'sound'=>'mySound'
          				 	 		  );
			$data			=	array('notCatId'=>'1','orderId'=>$orderId);
			$returnMessage	=	$this->sendNotificationToUserFunction($userData['device_id'],$message,$data,$userData['device_type']);
			return $returnMessage;
		endif;
	}

	/***********************************************************************
	** Function name : sendPaymentFalureNotificationToUser
	** Developed By : Manoj Kumar
	** Purpose  : This is use for send Payment Success Notification To User
	** Date : 05 NOVEMBER 2018
	************************************************************************/
	function sendPaymentFalureNotificationToUser($mobileNumber='',$orderId='') {  
		if($mobileNumber && $orderId):
			$userData 		=	$this->getUserDataByPhone($mobileNumber);
			$message 		= 	array('body'=>'Your Transaction failed on Cartamaam',
						 	 		  'title'=>'Product purchage',
             	         	 		  'icon'=>'myicon',
              	         	 		  'sound'=>'mySound'
          				 	 		  );
			$data			=	array('notCatId'=>'1','orderId'=>$orderId);
			$returnMessage	=	$this->sendNotificationToUserFunction($userData['device_id'],$message,$data,$userData['device_type']);
			return $returnMessage;
		endif;
	}




	#########################################################################################################################
	################################################		SELLERT SECTION 		#############################################
	#########################################################################################################################

	/* * *********************************************************************
	 * * Function name : sendNotificationToSellerFunction 
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function use for send Notification To Seller Function
	 * * Date : 05 NOVEMBER 2018
	 * * **********************************************************************/
	public function sendNotificationToSellerFunction($registrationIds='',$message='',$data=array(),$deviceType='') {
		if(!empty($registrationIds) && !empty($message) && !empty($data) && !empty($deviceType)):
			
			$fields 		= 	array('to'=>$registrationIds,'notification'=>$message,'data'=>$data);
			if($deviceType == 'Andriod'):
				$headers 	= 	array('Authorization: key='.SHELLIOS_SELLER_ANDRIOD_KEY,'Content-Type:application/json');
			elseif($deviceType == 'IOS'):
				$headers 	= 	array('Authorization: key='.SHELLIOS_SELLER_IOS_KEY,'Content-Type:application/json');
			endif;
			#Send Reponse To FireBase Server	
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL,'https://fcm.googleapis.com/fcm/send');
			curl_setopt( $ch,CURLOPT_POST,true);
			curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);
			#Echo Result Of FireBase Server
			return $result;
		endif;
	}	// END OF FUNCTION	

	
}	