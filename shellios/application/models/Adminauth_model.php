<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Adminauth_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
		$this->load->database(); 
	}
	
	/* * *********************************************************************
	 * * Function name : Authenticate
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for Admin Login Page
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function Authenticate($userEmail='')
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('admin_email_id',$userEmail);
		$query	=	$this->db->get();
		if($query->num_rows() >0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : encriptPassword
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for encript_password
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function encriptPassword($password)
	{
		return $this->encrypt->encode($password, $this->config->item('encryption_key'));
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : decryptsPassword
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for encript_password
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function decryptsPassword($password)
	{
		return $this->encrypt->decode($password, $this->config->item('encryption_key'));
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : checkAdminLoginCookie
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for check Admin Login Cookie
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function checkAdminLoginCookie()
	{  
		if($_COOKIE['SHELLIOS_ADMIN_LOGIN_EMAIL']):
			
			$result		=	$this->Authenticate($_COOKIE['SHELLIOS_ADMIN_LOGIN_EMAIL']);
			if($result): 
				if($result['status'] == 'A'):	
					$ststusError	=	0;
					$departmentName	=	'';
					
					$ATQuery		=	"SELECT admin_metadata_value FROM ".getTablePrefix()."admin_metadata 
										 WHERE admin_id = '".$result['encrypt_id']."' AND admin_metadata_key = '_admin_type'";  
					$ATData			=	$this->common_model->getDataByQuery('single',$ATQuery); 
					
					$ALQuery		=	"SELECT admin_metadata_value FROM ".getTablePrefix()."admin_metadata 
										 WHERE admin_id = '".$result['encrypt_id']."' AND admin_metadata_key = '_admin_label'";  
					$ALData			=	$this->common_model->getDataByQuery('single',$ALQuery); 
					
					if($ATData['admin_metadata_value'] == 'Super admin' && $ALData['admin_metadata_value'] == '0'):
						
						$adminCurrentId				=	$result['encrypt_id'];
						$adminCurrentName			=	$result['admin_name'];
						$adminCurrentDisplayName	=	$result['admin_display_name'];
						$adminCurrentPath			=	base_url().'admin/'.$result['admin_slug'].'/';
						$adminCurrentLogo			=	$result['admin_image'];
						
					elseif($ATData['admin_metadata_value'] == 'Sub admin' && $ALData['admin_metadata_value'] == '1'):
						
						$AdmQuery					=	"SELECT ad.encrypt_id, ad.admin_name, ad.admin_display_name, ad.admin_slug, ad.admin_image
											 	 	 	 FROM ".getTablePrefix()."admin_metadata as am LEFT JOIN ".getTablePrefix()."admin as ad ON am.admin_metadata_value= ad.encrypt_id
											 	 	 	 WHERE am.admin_id = '".$result['encrypt_id']."' AND am.admin_metadata_key = '_admin_parent'";
						$AdmData					=	$this->common_model->getDataByQuery('single',$AdmQuery); 
						
						$adminCurrentId				=	$AdmData['encrypt_id'];
						$adminCurrentName			=	$AdmData['admin_name'];
						$adminCurrentDisplayName	=	$AdmData['admin_display_name'];
						$adminCurrentPath			=	base_url().'admin/'.$AdmData['admin_slug'].'/';
						$adminCurrentLogo			=	$AdmData['admin_image'];
													
						$ADQuery					=	"SELECT admin_metadata_value FROM ".getTablePrefix()."admin_metadata 
												 	 	 WHERE admin_id = '".$result['encrypt_id']."' AND admin_metadata_key = '_admin_department'";  
						$ADData						=	$this->common_model->getDataByQuery('single',$ADQuery);
						$departmentName				=	$ADData['admin_metadata_value'];
					endif;
					
					if($ststusError == 0):	
						$this->session->set_userdata(array(
												'SHELLIOS_ADMIN_LOGGED_IN'=>true,
												'SHELLIOS_ADMIN_ID'=>$result['encrypt_id'],
												'SHELLIOS_ADMIN_NAME'=>$result['admin_name'],
												'SHELLIOS_ADMIN_DISLPLAY_NAME'=>$result['admin_display_name'],
												'SHELLIOS_ADMIN_SLUG'=>$result['admin_slug'],
												'SHELLIOS_ADMIN_EMAIL'=>$result['admin_email_id'],
												'SHELLIOS_ADMIN_MOBILE'=>$result['admin_mobile_number'],
												'SHELLIOS_ADMIN_EMPLOYEE_ID'=>$result['admin_employee_id'],
												'SHELLIOS_ADMIN_LOGO'=>$result['admin_image'],
												'SHELLIOS_ADMIN_ADDRESS'=>$result['admin_address'],
												'SHELLIOS_ADMIN_LOCALITY'=>$result['admin_locality'],
												'SHELLIOS_ADMIN_CITY'=>$result['admin_city'],
												'SHELLIOS_ADMIN_STATE'=>$result['admin_state'],
												'SHELLIOS_ADMIN_ZIPCODE'=>$result['admin_zipcode'],
												'SHELLIOS_ADMIN_TYPE'=>$ATData['admin_metadata_value'],
												'SHELLIOS_ADMIN_LABEL'=>$ALData['admin_metadata_value'],
												'SHELLIOS_ADMIN_DEPARTMENT'=>$departmentName,
												'SHELLIOS_ADMIN_CURRENT_ID'=>$adminCurrentId,
												'SHELLIOS_ADMIN_CURRENT_NAME'=>$adminCurrentName,
												'SHELLIOS_ADMIN_CURRENT_DISPLAY_NAME'=>$adminCurrentDisplayName,
												'SHELLIOS_ADMIN_CURRENT_PATH'=>$adminCurrentPath,
												'SHELLIOS_ADMIN_CURRENT_LOGO'=>$adminCurrentLogo,
												'SHELLIOS_ADMIN_LAST_LOGIN'=>$result['admin_last_login'].' ('.$result['admin_last_login_ip'].')'));
						
						setcookie('SHELLIOS_ADMIN_LOGIN_EMAIL',$result['admin_email_id'],time()+60*60*24*100,'/');
						
						if($_COOKIE['SHELLIOS_ADMIN_REFERENCE_PAGES']):
							redirect(base_url().$_COOKIE['SHELLIOS_ADMIN_REFERENCE_PAGES']);
						else:
							redirect($adminCurrentPath.'dashboard');
						endif;
					endif;
				endif;
			endif;
		endif;
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : authCheck
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for auth Check
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function authCheck($userType='',$showType='')
	{
		if($this->session->userdata('SHELLIOS_ADMIN_ID') == ""):
			setcookie('SHELLIOS_ADMIN_REFERENCE_PAGES',uri_string(),time()+60*60*24*5,'/');
			redirect(base_url().'admin');
		else:
			if($userType == "admin"):	
				if($showType == ''):
					return true;
				elseif($this->checkPermission($showType)):  
					return true;
				else:		
					$this->session->set_flashdata('alert_warning',lang('accessdenied'));
					redirect($this->session->userdata('SHELLIOS_ADMIN_CURRENT_PATH').'dashboard');
				endif;
			endif;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name: check_permission
	** Developed By: Manoj Kumar
	** Purpose: This function used for check permission
	** Date: 09 AUGUSt 2018
	************************************************************************/
	public function checkPermission($showType='')
	{  
		$SAPermission	=	array('departmentlist','subadminlist');
		
		if($this->session->userdata('SHELLIOS_ADMIN_TYPE') == 'Super admin'):
			if(in_array($this->router->fetch_class(),$SAPermission)):
				return true;
			elseif($showType == 'view_data' || $showType == 'add_data' || $showType == 'edit_data' || $showType == 'delete_data'):
				$this->db->select('encrypt_id');
				$this->db->from('admin_module');
				$this->db->where('module_name',$this->router->fetch_class());
				$this->db->where("display_super_admin = 'Y'");
				$this->db->order_by("module_orders ASC");
				$query = $this->db->get();
				if($query->num_rows() > 0):
					return true;
				else:
					$this->db->select('encrypt_id,module_name,module_display_name');
					$this->db->from('admin_module_child');
					$this->db->where('module_name',$this->router->fetch_class());
					$this->db->where("display_super_admin = 'Y'");
					$this->db->order_by("module_orders ASC");
					$query = $this->db->get();
					if($query->num_rows() > 0):
						return true;
					else:
						return false;
					endif;
				endif;
			else:  
				return false;
			endif;
		elseif($this->session->userdata('SHELLIOS_ADMIN_TYPE') == 'Sub admin'):  
			$this->db->select($showType);
			$this->db->from('admin_module_permission');
			$this->db->where('module_name',$this->router->fetch_class());
			$this->db->where('admin_id',$this->session->userdata('SHELLIOS_ADMIN_ID'));
			$this->db->where("child_data = 'N'");
			$query	=	$this->db->get();
			if($query->num_rows() > 0):		
				$mdata				=	$query->row_array();
				if($mdata[$showType] == 'Y'):
					return true;
				else:
					return false;
				endif;
			else:		
				$this->db->select($showType);
				$this->db->from('admin_module_child_permission');
				$this->db->where('module_name',$this->router->fetch_class());
				$this->db->where('admin_id',$this->session->userdata('SHELLIOS_ADMIN_ID'));
				$cquery	=	$this->db->get();
				if($cquery->num_rows() > 0):	
					$cmdata			=	$cquery->row_array();
					if($cmdata[$showType] == 'Y'):
						return true;
					else:
						return false;
					endif;
				endif;	
			endif;
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name: getPermissionType
	** Developed By: Manoj Kumar
	** Purpose: This function used for get permission type
	** Date: 09 AUGUSt 2018
	************************************************************************/
	public function getPermissionType(&$data)	
	{  
		$SAPermission	=	array('departmentlist','subadminlist');	 
		if($this->session->userdata('SHELLIOS_ADMIN_TYPE')=='Super admin'):
			if(in_array($this->router->fetch_class(),$SAPermission)):
				$data['view_data']		=	'Y';
				$data['add_data']		=	'Y';
				$data['edit_data']		=	'Y';
				$data['delete_data']	=	'Y';
			else:
				$data['view_data']		=	'Y';
				$data['add_data']		=	'Y';
				$data['edit_data']		=	'Y';
				$data['delete_data']	=	'Y';
			endif;
		elseif($this->session->userdata('SHELLIOS_ADMIN_TYPE')=='Sub admin'): 	
			$data['view_data']			=	'N';
			$data['add_data']			=	'N';
			$data['edit_data']			=	'N';
			$data['delete_data']		=	'N';
			
			$this->db->select('view_data,add_data,edit_data,delete_data');
			$this->db->from('admin_module_permission');
			$this->db->where('module_name',$this->router->fetch_class());
			$this->db->where('admin_id',$this->session->userdata('SHELLIOS_ADMIN_ID'));
			$this->db->where("child_data = 'N'");
			$query	=	$this->db->get();
			if($query->num_rows() > 0):	
				$mdata						=	$query->row_array();
				$data['view_data']			=	$mdata['view_data'];
				$data['add_data']			=	$mdata['add_data'];
				$data['edit_data']			=	$mdata['edit_data'];
				$data['delete_data']		=	$mdata['delete_data'];
			else:	
				$this->db->select('view_data,add_data,edit_data,delete_data');
				$this->db->from('admin_module_child_permission');
				$this->db->where('module_name',$this->router->fetch_class());
				$this->db->where('admin_id',$this->session->userdata('SHELLIOS_ADMIN_ID'));
				$cquery	=	$this->db->get();
				if($cquery->num_rows() > 0):	
					$cmdata					=	$cquery->row_array();
					$data['view_data']		=	$cmdata['view_data'];
					$data['add_data']		=	$cmdata['add_data'];
					$data['edit_data']		=	$cmdata['edit_data'];
					$data['delete_data']	=	$cmdata['delete_data'];
				endif;	
			endif;
		endif;
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : getMenuModule  
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This is get menu module
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	function getMenuModule()
	{ //echo "<pre>";	print_r($this->session->userdata()); die;
		if($this->session->userdata('SHELLIOS_ADMIN_TYPE') == 'Super admin'):
			$this->db->select('encrypt_id,module_name,module_display_name,module_icone,child_data');
			$this->db->from('admin_module');
			$this->db->where("display_super_admin = 'Y'");
			$this->db->order_by("module_orders ASC");
			$query = $this->db->get();
			//echo $this->db->last_query(); die;
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($this->session->userdata('SHELLIOS_ADMIN_TYPE') == 'Sub admin'): 
			$this->db->select('encrypt_id,module_id,module_name,module_display_name,module_icone,child_data');
			$this->db->from('admin_module_permission');
			$this->db->where('admin_id',$this->session->userdata('SHELLIOS_ADMIN_ID'));
			$this->db->group_start();
			$this->db->where("view_data = 'Y'");
			$this->db->where("child_data = 'N'");
			$this->db->or_where("view_data = 'N'");
			$this->db->or_where("child_data = 'Y'");
			$this->db->group_end();
			$this->db->order_by("module_orders ASC");
			$query = $this->db->get();
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		endif;
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : getMenuChildModule  
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This is get menu child module
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	function getMenuChildModule($miduleId='')
	{ 	
		if($this->session->userdata('SHELLIOS_ADMIN_TYPE') == 'Super admin'):
			$this->db->select('encrypt_id,module_name,module_display_name');
			$this->db->from('admin_module_child');
			$this->db->where("module_id",$miduleId);
			$this->db->where("display_super_admin = 'Y'");
			$this->db->order_by("module_orders ASC");
			$query = $this->db->get();
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($this->session->userdata('SHELLIOS_ADMIN_TYPE') == 'Sub admin'): 
			$this->db->select('encrypt_id,module_name,module_display_name');
			$this->db->from('admin_module_child_permission');
			$this->db->where("permission_id",$miduleId);
			$this->db->where('admin_id',$this->session->userdata('SHELLIOS_ADMIN_ID'));
			$this->db->where("view_data = 'Y'");
			$this->db->order_by("module_orders ASC");
			$query = $this->db->get();
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : admin_module
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get module
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function getModule()
	{
		$this->db->select('*');
		$this->db->from('admin_module');
		$this->db->order_by("module_orders ASC");
		$query	=	$this->db->get();
		if($query->num_rows() >0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : getModuleChild
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get module child
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function getModuleChild($miduleId='')
	{
		$this->db->select('*');
		$this->db->from('admin_module_child');
		$this->db->where('module_id',$miduleId);
		$this->db->order_by("module_orders ASC");
		$query	=	$this->db->get();
		if($query->num_rows() >0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : getPermission
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get permission
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function getPermission($adminId='')
	{
		$selecarray				=	array();
		$this->db->select('*');
		$this->db->from('admin_module_permission');
		$this->db->where('admin_id',$adminId);
		$this->db->order_by("module_orders ASC");
		$query	=	$this->db->get();
		if($query->num_rows() >0):	
			$data	=	$query->result_array();
			foreach($data as $info):	
				$selecarray['mainmodule'.$info['module_id']]							=	'Y';
				if($info['child_data'] == 'N'):
					if($info['view_data'] == 'Y'):
						$selecarray['mainmodule_view_data'.$info['module_id']]			=	'Y';
					endif;
					if($info['add_data'] == 'Y'):
						$selecarray['mainmodule_add_data'.$info['module_id']]			=	'Y';
					endif;
					if($info['edit_data'] == 'Y'):
						$selecarray['mainmodule_edit_data'.$info['module_id']]			=	'Y';
					endif;
					if($info['delete_data'] == 'Y'):
						$selecarray['mainmodule_delete_data'.$info['module_id']]		=	'Y';
					endif; 
				else:
					$this->db->select('*');
					$this->db->from('admin_module_child_permission');
					$this->db->where('permission_id',$info['encrypt_id']);
					$this->db->where('admin_id',$adminId);
					$this->db->order_by("module_orders ASC");
					$query1	=	$this->db->get();
					if($query1->num_rows() >0):
						$data1	=	$query1->result_array();
						foreach($data1 as $info1):
							$selecarray['childmodule'.$info['module_id'].'_'.$info1['module_id']]						=	'Y';
							if($info1['view_data'] == 'Y'):
								$selecarray['childmodule_view_data'.$info['module_id'].'_'.$info1['module_id']]			=	'Y';
							endif;
							if($info1['add_data'] == 'Y'):
								$selecarray['childmodule_add_data'.$info['module_id'].'_'.$info1['module_id']]			=	'Y';
							endif;
							if($info1['edit_data'] == 'Y'):
								$selecarray['childmodule_edit_data'.$info['module_id'].'_'.$info1['module_id']]			=	'Y';
							endif;
							if($info1['delete_data'] == 'Y'):
								$selecarray['childmodule_delete_data'.$info['module_id'].'_'.$info1['module_id']]		=	'Y';
							endif;
						endforeach;
					endif;
				endif;
			endforeach;
		endif;
		return $selecarray;
	}	// END OF FUNCTION
	
	/***********************************************************************
	** Function name : deletePermissionData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for delete permission data
	** Date : 09 AUGUSt 2018
	************************************************************************/
	function deletePermissionData($adminId='')
	{
		$this->db->delete('admin_module_permission', array('admin_id' => $adminId));
		$this->db->delete('admin_module_child_permission', array('admin_id' => $adminId));
		return true;
	}	// END OF FUNCTION
	
	/* * *********************************************************************
	 * * Function name : checkOTP
	 * * Developed By : Manoj Kumar
	 * * Purpose  : This function used for Admin otp
	 * * Date : 09 AUGUSt 2018
	 * * **********************************************************************/
	public function checkOTP($userOtp='')
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('admin_password_otp',$userOtp);
		$query	=	$this->db->get();
		if($query->num_rows() >0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
}	