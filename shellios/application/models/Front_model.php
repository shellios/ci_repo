<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Front_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
		$this->load->database(); 
	}

	/***********************************************************************
	** Function name : getCMSData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get CMS Data
	** Date : 09 OCTOBER 2018
	************************************************************************/
	function getCMSData($pageUrl='')
	{
		$this->db->select('cms_id,title,content,seo_title,seo_keyword,seo_description');
		$this->db->from('cms');
		$this->db->where("page_url",$pageUrl);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductCategory
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Product Category
	** Date : 10 OCTOBER 2018
	************************************************************************/
	function getProductCategory()
	{
		$this->db->select('prod_cate_id,prod_cate_name,prod_cate_slug,prod_cate_short_name,prod_cate_image');
		$this->db->from('prod_cate');
		$this->db->where("status = 'Y'");
		$this->db->order_by("prod_cate_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductCategoryBySlug
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Product Category By Slug
	** Date : 18 OCTOBER 2018
	************************************************************************/
	function getProductCategoryBySlug($cateSlug='')
	{
		$this->db->select('prod_cate_id,prod_cate_name,prod_cate_slug,prod_cate_short_name,prod_cate_image');
		$this->db->from('prod_cate');
		$this->db->where("prod_cate_slug",$cateSlug);
		$this->db->where("status = 'Y'");
		$this->db->order_by("prod_cate_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductSubCategory
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Product Sub Category
	** Date : 10 OCTOBER 2018
	************************************************************************/
	function getProductSubCategory($categoryId='')
	{
		$this->db->select('prod_sub_cate_id,prod_sub_cate_name,prod_sub_cate_slug,prod_sub_cate_image');
		$this->db->from('prod_sub_cate');
		$this->db->where('prod_cate_id',$categoryId);
		$this->db->where("status = 'Y'");
		$this->db->order_by("prod_sub_cate_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductSubCategoryBySlug
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Product Sub Category By Slug
	** Date : 22 OCTOBER 2018
	************************************************************************/
	function getProductSubCategoryBySlug($subCateSlug='')
	{
		$this->db->select('prod_sub_cate_id,prod_sub_cate_name,prod_sub_cate_slug,prod_sub_cate_image');
		$this->db->from('prod_sub_cate');
		$this->db->where("prod_sub_cate_slug",$subCateSlug);
		$this->db->where("status = 'Y'");
		$this->db->order_by("prod_sub_cate_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getCarBrand
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Car Brand
	** Date : 18 OCTOBER 2018
	************************************************************************/
	function getCarBrand()
	{
		$this->db->select('car_brand_id,car_brand_name,car_brand_image');
		$this->db->from('car_brand');
		$this->db->where("status = 'Y'");
		$this->db->order_by("car_brand_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	/***********************************************************************
	** Function name : getCarBrand
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Car Brand
	** Date : 18 OCTOBER 2018
	************************************************************************/
	function getCarBrandModel($brandId='')
	{
		$this->db->select('car_brand_model_id,car_brand_model_name,car_brand_model_image');
		$this->db->from('car_brand_model');
		$this->db->where('car_brand_id',$brandId);
		$this->db->where("status = 'Y'");
		$this->db->order_by("car_brand_model_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getHomePageBanner
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Home Page Banner
	** Date : 10 OCTOBER 2018
	************************************************************************/
	function getHomePageBanner()
	{
		$this->db->select('slider_id,slider_image');
		$this->db->from('home_slider');
		$this->db->where("status = 'Y'");
		$this->db->order_by("slider_id ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getTestimonial
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Testimonial
	** Date : 10 OCTOBER 2018
	************************************************************************/
	function getTestimonial()
	{
		$this->db->select('testimonial_id,name,designation,content,image');
		$this->db->from('testimonial');
		$this->db->where("status = 'Y'");
		$this->db->order_by("name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getSmallCMS
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Small CMS
	** Date : 10 OCTOBER 2018
	************************************************************************/
	function getSmallCMS($smallCmsId='')
	{
		$this->db->select('small_cms_id,title,content');
		$this->db->from('small_cms');
		$this->db->where("small_cms_id",$smallCmsId);
		$this->db->where("status = 'Y'");
		$this->db->order_by("small_cms_id ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getAddressData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Address Data
	** Date : 13 OCTOBER 2018
	************************************************************************/
	function getAddressData($userId='',$count='')
	{
		$this->db->select('address_id,add_name,add_phone,add_address1,add_address2,add_city,add_state,add_pincode,add_type');
		$this->db->from('users_address');
		$this->db->where("user_id",$userId);
		$this->db->where("status = 'Y'");
		$this->db->order_by("address_id ASC");
		if($count):
			$this->db->limit($count);
		endif;
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getMostPopular
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Most Popular
	** Date : 17 OCTOBER 2018
	************************************************************************/
	function getMostPopular($mostPopularProdIds=array())	
	{
		//$prodids 		=	array('1000000001');
		$this->db->select('prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   cate.prod_cate_slug, subcate.prod_sub_cate_slug');
		$this->db->from('product as prod');
		$this->db->join('product_details as prodet','prod.prod_id=prodet.prod_id','LEFT');
		$this->db->join('prod_cate as cate','prod.prod_cate_id=cate.prod_cate_id','LEFT');
		$this->db->join('prod_sub_cate as subcate','prod.prod_sub_cate_id=subcate.prod_sub_cate_id','LEFT');
		$this->db->where_in("prod.prod_id",$mostPopularProdIds);
		$this->db->where("prod.status = 'A'");
		$this->db->order_by("prod.prod_name ASC");
		$this->db->limit(20);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getRecentViewed
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Recent Viewed
	** Date : 17 OCTOBER 2018
	************************************************************************/
	function getRecentViewed($cookieId='')
	{
		$this->db->select('prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   cate.prod_cate_slug, subcate.prod_sub_cate_slug');
		$this->db->from('recently_viewed as rview');
		$this->db->join('product as prod','rview.prod_id=prod.prod_id','LEFT');
		$this->db->join('product_details as prodet','prod.prod_id=prodet.prod_id','LEFT');
		$this->db->join('prod_cate as cate','prod.prod_cate_id=cate.prod_cate_id','LEFT');
		$this->db->join('prod_sub_cate as subcate','prod.prod_sub_cate_id=subcate.prod_sub_cate_id','LEFT');
		$this->db->where("rview.cookie_id",$cookieId);
		$this->db->where("prod.status = 'A'");
		$this->db->order_by("prod.prod_name ASC");
		$this->db->limit(20);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductImage
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Product Image
	** Date : 17 OCTOBER 2018
	************************************************************************/
	function getProductImage($productId='',$count='')
	{
		$this->db->select('prod_image');
		$this->db->from('prod_image');
		$this->db->where("prod_id",$productId);
		$this->db->limit($count);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : checkInWishlist
	** Developed By : Manoj Kumar
	** Purpose  : This function used for check In Wishlist
	** Date : 17 OCTOBER 2018
	************************************************************************/
	function checkInWishlist($userId='',$productId='')
	{
		$this->db->select('wishlist_id');
		$this->db->from('wishlist');
		$this->db->where("user_id",$userId);
		$this->db->where("prod_id",$productId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return 'Y';
		else:
			return 'N';
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getRatingbyProductId
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Rating by Product Id
	** Date : 17 OCTOBER 2018
	************************************************************************/
	function getRatingbyProductId($productId='')
	{
		$ratingData['rating']		=	floatval(0);
		$ratingData['count']		=	floatval(0);
		$this->db->select('sum(rating) as total_rating, count(rating) as total_count');
		$this->db->from('prod_user_review');
		$this->db->where("prod_id",$productId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):  
			$data 	=	$query->row_array(); 
			if($data['total_rating'] > 0 && $data['total_count'] > 0):
				$ratingData['rating']		=	floatval($data['total_rating']/$data['total_count']);
				$ratingData['count']		=	floatval($data['total_count']);
			endif;
		endif; 
		return $ratingData;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getRatingReviewbyProductId
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Rating Review by Product Id
	** Date : 23 OCTOBER 2018
	************************************************************************/
	function getRatingReviewbyProductId($productId='')
	{
		$this->db->select('urev.review_id, urev.rating, urev.heading, urev.content, urev.creation_date,
						   users.user_id, users.user_name');
		$this->db->from('prod_user_review as urev');
		$this->db->join('users as users','urev.user_id=users.user_id','LEFT');
		$this->db->where("urev.prod_id",$productId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):  
			return $query->result_array(); 
		else:
			return false;
		endif; 
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : checkInCart
	** Developed By : Manoj Kumar
	** Purpose  : This function used for check In Cart
	** Date : 22 OCTOBER 2018
	************************************************************************/
	function checkInCart($userId='',$cookieId='',$productId='')
	{
		$this->db->select('cart_id');
		$this->db->from('cart');
		if($userId):
			$this->db->where("user_id",$userId);
		else:
			$this->db->where("cookie_id",$cookieId);
		endif;
		$this->db->where("prod_id",$productId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return 'Y';
		else:
			return 'N';
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductCountInCart
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Product Count In Cart
	** Date : 22 OCTOBER 2018
	************************************************************************/
	function getProductCountInCart($userId='',$cookieId='')
	{
		$this->db->select('sum(quantity) as totalProduct');
		$this->db->from('cart');
		if($userId):
			$this->db->where("user_id",$userId);
		else:
			$this->db->where("cookie_id",$cookieId);
		endif;
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			$data 	=	$query->row_array();
			return $data['totalProduct'];
		else:
			return 0;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getWishlistData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Wishlist Data
	** Date : 22 OCTOBER 2018
	************************************************************************/
	function getWishlistData($userId='')
	{
		$this->db->select('wlist.wishlist_id, prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   cate.prod_cate_slug, subcate.prod_sub_cate_slug');
		$this->db->from('wishlist as wlist');
		$this->db->join('product as prod','wlist.prod_id=prod.prod_id','LEFT');
		$this->db->join('product_details as prodet','prod.prod_id=prodet.prod_id','LEFT');
		$this->db->join('prod_cate as cate','prod.prod_cate_id=cate.prod_cate_id','LEFT');
		$this->db->join('prod_sub_cate as subcate','prod.prod_sub_cate_id=subcate.prod_sub_cate_id','LEFT');
		$this->db->where("wlist.user_id",$userId);
		$this->db->where("prod.status = 'A'");
		$this->db->order_by("prod.prod_name ASC");
		$this->db->limit(20);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductCountInWishlist
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Product ount In Wishlist
	** Date : 22 OCTOBER 2018
	************************************************************************/
	function getProductCountInWishlist($userId='')
	{
		$this->db->select('wishlist_id');
		$this->db->from('wishlist');
		$this->db->where("user_id",$userId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->num_rows();
		else:
			return 0;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductBySlug
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Product By Slug
	** Date : 22 OCTOBER 2018
	************************************************************************/
	function getProductBySlug($productSlug='')
	{
		$this->db->select('prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   cate.prod_cate_slug, subcate.prod_sub_cate_slug, seller.seller_name, seller.seller_business_name');
		$this->db->from('product as prod');
		$this->db->join('product_details as prodet','prod.prod_id=prodet.prod_id','LEFT');
		$this->db->join('prod_cate as cate','prod.prod_cate_id=cate.prod_cate_id','LEFT');
		$this->db->join('prod_sub_cate as subcate','prod.prod_sub_cate_id=subcate.prod_sub_cate_id','LEFT');
		$this->db->join('seller as seller','prod.seller_id=seller.seller_id','LEFT');
		$this->db->where("prod.prod_slug",$productSlug);
		$this->db->where("prod.status = 'A'");
		$this->db->order_by("prod.prod_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getSimilarProducts
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Similar Products
	** Date : 23 OCTOBER 2018
	************************************************************************/
	function getSimilarProducts($prodCateId='',$prodSubCateId='',$productId='')
	{
		$this->db->select('prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   cate.prod_cate_slug, subcate.prod_sub_cate_slug, seller.seller_name, seller.seller_business_name');
		$this->db->from('product as prod');
		$this->db->join('product_details as prodet','prod.prod_id=prodet.prod_id','LEFT');
		$this->db->join('prod_cate as cate','prod.prod_cate_id=cate.prod_cate_id','LEFT');
		$this->db->join('prod_sub_cate as subcate','prod.prod_sub_cate_id=subcate.prod_sub_cate_id','LEFT');
		$this->db->join('seller as seller','prod.seller_id=seller.seller_id','LEFT');
		$this->db->where("prod.prod_cate_id",$prodCateId);
		$this->db->where("prod.prod_sub_cate_id",$prodSubCateId);
		$this->db->where("prod.prod_id !=",$productId);
		$this->db->where("prod.status = 'A'");
		$this->db->order_by("prod.prod_name ASC");
		$this->db->limit(20);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : checkProductInOrder
	** Developed By : Manoj Kumar
	** Purpose  : This function used for check Product In Order
	** Date : 24 OCTOBER 2018
	************************************************************************/
	function checkProductInOrder($userId='',$productId='')
	{
		$this->db->select('order_id');
		$this->db->from('orders');
		$this->db->where("user_id",$userId);
		$this->db->where("prod_id",$productId);
		$this->db->where("payment_status = '1'");
		$this->db->where("order_status = 'Delivered'");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return 'Y';
		else:
			return 'N';
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getProductListData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Product List Data
	** Date : 24 OCTOBER 2018
	************************************************************************/
	function getProductListData($data=array())
	{	
		$this->db->select('prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   cate.prod_cate_slug, subcate.prod_sub_cate_slug');
		$this->db->from('product as prod');
		$this->db->join('product_details as prodet','prod.prod_id=prodet.prod_id','LEFT');
		$this->db->join('prod_cate as cate','prod.prod_cate_id=cate.prod_cate_id','LEFT');
		$this->db->join('prod_sub_cate as subcate','prod.prod_sub_cate_id=subcate.prod_sub_cate_id','LEFT');
		$this->db->join('prod_brand_model as brmod','prod.prod_id=brmod.prod_id','LEFT');
		if($data['categoryId']):
			$this->db->where('prod.prod_cate_id',$data['categoryId']);
		endif;
		if($data['carBrand']):
			$this->db->where_in('brmod.car_brand_id',$data['carBrand']);
		endif;
		if($data['carBrandModel']):
			$this->db->where_in('brmod.car_brand_model_id',$data['carBrandModel']);
		endif;
		if($data['subCategory']):
			$this->db->where_in('prod.prod_sub_cate_id',$data['subCategory']);
		endif;
		if($data['priceData']):
			$priceArray    = explode('_____',$data['priceData']);
			if($priceArray[0]):
				$this->db->where('prod.prod_rmp >',$priceArray[0]);
			endif;
			if($priceArray[1]):
				$this->db->where('prod.prod_rmp <=',$priceArray[1]);
			endif;
		endif;	
		$this->db->where("prod.status = 'A'");
		$this->db->group_by("prod.prod_id");
		if($data['shortBy']):
			if($data['shortBy'] == 'Popular'):
				$this->db->order_by("prod.prod_name ASC");
			else:
				$this->db->order_by(str_replace('_____',' ',$data['shortBy']));
			endif;
		endif;
		if($data['showTo']):	
			$this->db->limit($data['showTo'],$data['showFrom']);
		endif;
		$query = $this->db->get();
		if($data['action'] == 'data'):
			if($query->num_rows() > 0):
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($data['action'] == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : checkProductInRecentView
	** Developed By : Manoj Kumar
	** Purpose  : This function used for check Product In Recent View
	** Date : 26 OCTOBER 2018
	************************************************************************/
	function checkProductInRecentView($cookieId='',$productId='')
	{
		$this->db->select('viewed_id');
		$this->db->from('recently_viewed');
		$this->db->where("cookie_id",$cookieId);
		$this->db->where("prod_id",$productId);
		$query	=	$this->db->get();
		if($query->num_rows()):
			return 'Y';
		else:
			return 'N';
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : checkEmailInNewsletter
	** Developed By : Manoj Kumar
	** Purpose  : This function used for check Email In Newsletter
	** Date : 26 OCTOBER 2018
	************************************************************************/
	function checkEmailInNewsletter($emailId='')
	{
		$this->db->select('newsletter_id');
		$this->db->from('newsletter');
		$this->db->where("newsletter_email",$emailId);
		$query	=	$this->db->get();
		if($query->num_rows()):
			return 'Y';
		else:
			return 'N';
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getCartData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Cart Data
	** Date : 26 OCTOBER 2018
	************************************************************************/
	function getCartData($userId='',$cookieId='')
	{
		$this->db->select('cart.cart_id, cart.quantity, prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   prodbm.car_brand_id, prodbm.car_brand_other, prodbm.car_brand_model_id,
						   seller.seller_installation_charges, cate.prod_cate_slug, subcate.prod_sub_cate_slug');
		$this->db->from('cart as cart');
		$this->db->join('product as prod','cart.prod_id=prod.prod_id','LEFT');
		$this->db->join('product_details as prodet','prod.prod_id=prodet.prod_id','LEFT');
		$this->db->join('prod_brand_model as prodbm','prod.prod_id=prodbm.prod_id','LEFT');
		$this->db->join('seller as seller','prod.seller_id=seller.seller_id','LEFT');
		$this->db->join('prod_cate as cate','prod.prod_cate_id=cate.prod_cate_id','LEFT');
		$this->db->join('prod_sub_cate as subcate','prod.prod_sub_cate_id=subcate.prod_sub_cate_id','LEFT');
		$this->db->join('prod_brand_model as brmod','prod.prod_id=brmod.prod_id','LEFT');
		if($userId):
			$this->db->where("cart.user_id",$userId);
		else:
			$this->db->where("cart.cookie_id",$cookieId);
		endif;
		$this->db->where("prod.status = 'A'");
		$this->db->group_by("cart.prod_id");
		$this->db->order_by("prod.prod_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getAllOrderId
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get All Order Id
	** Date : 29 OCTOBER 2018
	************************************************************************/
	function getAllOrderIdBySeller($userId='')
	{
		$allsellerIds   = 	array();
		$AllOrderId   	= 	array();
		$this->db->select('prod.seller_id');
		$this->db->from('cart as cart');
		$this->db->join("product as prod","cart.prod_id=prod.prod_id","LEFT");
		$this->db->join("seller as sell","prod.seller_id=sell.seller_id","LEFT");
		$this->db->where("cart.user_id",$userId);
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			$data = $query->result_array();
			foreach($data as $info):
				if(!in_array($info['seller_id'],$allsellerIds)):
					array_push($allsellerIds,$info['seller_id']);
					$AllOrderId[$info['seller_id']]	=	getRandomOrderId();
				endif;
			endforeach;
		endif;
		return $AllOrderId;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getBuyNowCheckoutData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Buy Now Checkout Data
	** Date : 30 OCTOBER 2018
	************************************************************************/
	function getBuyNowCheckoutData($prodId='')
	{
		$this->db->select('prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   prodbm.car_brand_id, prodbm.car_brand_other, prodbm.car_brand_model_id,
						   seller.seller_installation_charges, cate.prod_cate_slug, subcate.prod_sub_cate_slug');
		$this->db->from('product as prod');
		$this->db->join('product_details as prodet','prod.prod_id=prodet.prod_id','LEFT');
		$this->db->join('prod_brand_model as prodbm','prod.prod_id=prodbm.prod_id','LEFT');
		$this->db->join('seller as seller','prod.seller_id=seller.seller_id','LEFT');
		$this->db->join('prod_cate as cate','prod.prod_cate_id=cate.prod_cate_id','LEFT');
		$this->db->join('prod_sub_cate as subcate','prod.prod_sub_cate_id=subcate.prod_sub_cate_id','LEFT');
		$this->db->join('prod_brand_model as brmod','prod.prod_id=brmod.prod_id','LEFT');
		$this->db->where("prod.prod_id",$prodId);
		$this->db->where("prod.status = 'A'");
		$this->db->group_by("prod.prod_id");
		$this->db->order_by("prod.prod_name ASC");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->row_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getSearchProductListData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Search Product List Data
	** Date : 01 NOVEMBER 2018
	************************************************************************/
	function getSearchProductListData($searchText='',$action='',$limit=20)
	{	
		           
		$this->db->select('prod.prod_id, prod.seller_id, prod.prod_cate_id, prod.prod_sub_cate_id, prod.prod_sub_cate_other,
						   prod.prod_brand_name, prod.prod_name, prod.prod_slug, prod.prod_sku, prod.prod_rmp, prod.prod_gst,
						   prodet.prod_color, prodet.prod_material, prodet.prod_dimension, prodet.prod_warranty, prodet.prod_desc,
						   cate.prod_cate_slug, subcate.prod_sub_cate_slug,  CONCAT(prod.prod_brand_name, " ",prod.prod_name) as probrand');
		$this->db->from('product as prod');
		$this->db->join('product_details as prodet','prod.prod_id=prodet.prod_id','LEFT');
		$this->db->join('prod_cate as cate','prod.prod_cate_id=cate.prod_cate_id','LEFT');
		$this->db->join('prod_sub_cate as subcate','prod.prod_sub_cate_id=subcate.prod_sub_cate_id','LEFT');
		$this->db->join('prod_brand_model as brmod','prod.prod_id=brmod.prod_id','LEFT');
		$this->db->group_start();
			$this->db->like("prod.prod_brand_name",$searchText,'both');
			$this->db->or_like("prod.prod_name",$searchText,'both');
			$this->db->or_like("CONCAT(prod.prod_brand_name, ' ',prod.prod_name)",$searchText,'both'); //jitendra
		$this->db->group_end();
		$this->db->where("prod.status = 'A'");
		
		$this->db->group_by("prod.prod_id");
		$this->db->order_by("prod.prod_name ASC");
		$this->db->limit($limit);
		$query = $this->db->get();
		if($action == 'data'):
			if($query->num_rows() > 0):
				//echo "<pre>"; print_r($query->result_array()); die;
				return $query->result_array();
			else:
				return false;
			endif;
		elseif($action == 'count'):
			return $query->num_rows();
		endif;
	}	// END OF FUNCTION

	/***********************************************************************
	** Function name : getOrderData
	** Developed By : Manoj Kumar
	** Purpose  : This function used for get Order Data
	** Date : 26 NOVEMBER 2018
	************************************************************************/
	function getOrderData($userId='',$orderId='')
	{
		$this->db->select('id,order_id,seller_id');
		$this->db->from('orders');
		$this->db->where("user_id",$userId);
		$this->db->where("session_id",$orderId);
		$this->db->where("browse_type = 'web'");
		$query	=	$this->db->get();
		if($query->num_rows() > 0):
			return $query->result_array();
		else:
			return false;
		endif;
	}	// END OF FUNCTION
}