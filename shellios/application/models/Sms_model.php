<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sms_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->database(); 
	}
	
	####################################################################################
	##############################		USER SECTION 		############################
	####################################################################################
	
	/** *********************************************************************
	* * Function name : sendOtpSmsToUser
	* * Developed By : Ravi Kumar
	* * Purpose  : This is use for send Otp Sms To User
	* * Date : 30 MAY 2019
	* * **********************************************************************/
	function sendOtpSmsToUser($mobileNumber='',$otp='') {  
		if($mobileNumber && $otp):
			$message		=	"Your One Time Password For Verification is ".$otp.". Thanks SHELLIOS Team";
			$returnMessage	=	$this->sendSms($mobileNumber,$message,$otp);
			return $returnMessage;
		endif;
	}
	
	/** *********************************************************************
	* * Function name : sendOtpRetrySms
	* * Developed By : Ravi Kumar
	* * Purpose  : This is use for send Otp Retry Sms To User
	* * Date : 30 MAY 2019
	* * **********************************************************************/
	function sendOtpRetrySms($mobileNumber='') {  
		if($mobileNumber):
			//$message		=	"Your One Time Password For Verification is ".$otp.". Thanks SHELLIOS Team";
			$returnMessage	=	$this->reTrySendSms($mobileNumber);
			return $returnMessage;
		endif;
	}
	
	####################################################################################
	###############################		CALL API's 		################################
	####################################################################################
		
	/** *********************************************************************
	* * Function name : sendSms
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for send Sms
	* * Date : 30 MAY 2019
	* * **********************************************************************/
	public function sendSms($mobileNumber='',$message='',$otp='')
	{
		if($mobileNumber !='' && $message !=''):
			$authKey 			= 	"251890AfwSSUhx03W5c139629";
	        $countryCode 		= 	"91" ;
	        $sender 			= 	"SHELOS"; //maximum 6 capital caractor
	        $mobileNumber		=	trim($mobileNumber);
	        $message 			= 	urlencode($message);
			$otp				=	trim($otp);
	        //$url  				=  	'http://api.msg91.com/api/sendhttp.php?sender='.$sender.'&route=4&mobiles='.$mobileNumber.'&authkey='.$authKey.'&country='.$countryCode.'&message='.$message ;
			$url  				=  	'https://control.msg91.com/api/sendotp.php?otp='.$otp.'&sender='.$sender.'&message='.$message.'&mobile='.$mobileNumber.'&authkey='.$authKey ;
			
			//print_r($url); die;
	        $curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_SSL_VERIFYHOST => 0,
			  CURLOPT_SSL_VERIFYPEER => 0,
			));

			$response 			= 	curl_exec($curl);
			$err 				= 	curl_error($curl);
			curl_close($curl);
			//print_r($err); print_r($response); die;
			if($err):
			 	return false;
			else:
			   return true;
			endif;
		else:
			return false;
		endif;
	}	// END OF FUNCTION
	
	/** *********************************************************************
	* * Function name : reTrySendSms
	* * Developed By : Ravi Kumar
	* * Purpose  : This function used for retry send Sms
	* * Date : 30 MAY 2019
	* * **********************************************************************/
	public function reTrySendSms($mobileNumber='')
	{
		if($mobileNumber !=''):
			$authKey 			= 	"251890AfwSSUhx03W5c139629";
	        $mobileNumber		=	trim($mobileNumber);
			$url  				=  	'https://control.msg91.com/api/retryotp.php?mobile='.$mobileNumber.'&authkey='.$authKey.'&retrytype=text' ;
			//print_r($url); die;
	        $curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_SSL_VERIFYHOST => 0,
			  CURLOPT_SSL_VERIFYPEER => 0,
			));

			$response 			= 	curl_exec($curl);
			$err 				= 	curl_error($curl);
			curl_close($curl);
			//print_r($err); print_r($response); die;
			if($err):
			 	return false;
			else:
			   return true;
			endif;
		else:
			return false;
		endif;
	}	// END OF FUNCTION



	

	
}	